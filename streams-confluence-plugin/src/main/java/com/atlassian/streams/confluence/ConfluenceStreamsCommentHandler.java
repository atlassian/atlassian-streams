package com.atlassian.streams.confluence;

import java.net.URI;

import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.spi.StreamsCommentHandler;
import com.atlassian.user.User;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.size;

import static com.atlassian.streams.api.common.Preconditions.checkNotBlank;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.DELETED_OR_PERMISSION_DENIED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNKNOWN_ERROR;

/**
 * Handles incoming comments.
 *
 * @since 2.0
 */
public class ConfluenceStreamsCommentHandler implements StreamsCommentHandler {
    private ApplicationProperties applicationProperties;
    private PageManager pageManager;
    private CommentManager commentManager;
    private final UserManager userManager;
    private final PermissionManager permissionManager;
    private final UserAccessor userAccessor;

    public ConfluenceStreamsCommentHandler(
            ApplicationProperties applicationProperties,
            @Qualifier("pageManager") PageManager pageManager,
            @Qualifier("commentManager") CommentManager commentManager,
            UserManager salUserManager,
            PermissionManager permissionManager,
            UserAccessor userAccessor) {
        this.applicationProperties = checkNotNull(applicationProperties, "Application Properties");
        this.pageManager = checkNotNull(pageManager, "Page Manager");
        this.commentManager = checkNotNull(commentManager, "Comment Manager");
        this.userManager = checkNotNull(salUserManager, "salUserManager");
        this.permissionManager = checkNotNull(permissionManager, "permissionManager");
        this.userAccessor = checkNotNull(userAccessor, "userAccessor");
    }

    public Either<PostReplyError, URI> postReply(URI baseUri, Iterable<String> itemPath, String comment)
            throws StreamsException {
        try {
            checkArgument(size(itemPath) == 2, "Item path must contain exactly 2 parts.");
            String type = checkNotBlank(get(itemPath, 0), "Type");
            long id = Long.parseLong(get(itemPath, 1));
            ContentEntityObject page;
            Comment parentComment = null;
            if (type.equals(Comment.CONTENT_TYPE)) {
                parentComment = commentManager.getComment(id);
                if (parentComment == null) {
                    return Either.left(new PostReplyError(DELETED_OR_PERMISSION_DENIED));
                }
                page = parentComment.getContainer();
            } else {
                page = pageManager.getAbstractPage(id);
            }
            final UserProfile remoteUser = userManager.getRemoteUser();
            User user = userAccessor.getUserByName(remoteUser != null ? remoteUser.getUsername() : null);
            if (!permissionManager.hasCreatePermission(user, page, Comment.class)) {
                return Either.left(new PostReplyError(UNAUTHORIZED));
            }
            Comment newComment = commentManager.addCommentToObject(page, parentComment, comment);
            return Either.right(
                    URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + newComment.getUrlPath()));
        } catch (Exception ex) {
            return Either.left(new PostReplyError(UNKNOWN_ERROR, ex));
        }
    }

    @Override
    public Either<PostReplyError, URI> postReply(final Iterable<String> itemPath, final String comment) {
        return postReply(URI.create(applicationProperties.getBaseUrl()), itemPath, comment);
    }
}
