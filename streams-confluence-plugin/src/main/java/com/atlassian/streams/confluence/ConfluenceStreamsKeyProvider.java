package com.atlassian.streams.confluence;

import com.atlassian.streams.spi.StreamsKeyProvider;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Returns a set of space keys from Confluence that the user has permission to see.
 *
 * @since v3.1
 */
public class ConfluenceStreamsKeyProvider implements StreamsKeyProvider {
    private final SpaceKeys spaceKeys;

    public ConfluenceStreamsKeyProvider(SpaceKeys spaceKeys) {
        this.spaceKeys = checkNotNull(spaceKeys, "spaceKeys");
    }

    public Iterable<StreamsKey> getKeys() {
        return spaceKeys.get();
    }
}
