package com.atlassian.streams.confluence;

import java.util.Comparator;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;

import static com.google.common.collect.Iterables.transform;
import static java.util.Objects.requireNonNull;

import static com.atlassian.confluence.spaces.SpaceType.GLOBAL;
import static com.atlassian.confluence.spaces.SpaceType.PERSONAL;
import static com.atlassian.confluence.spaces.SpacesQuery.newQuery;

public class SpaceKeys {
    private final SpaceManager spaceManager;
    private final TransactionTemplate transactionTemplate;

    public SpaceKeys(SpaceManager spaceManager, TransactionTemplate transactionTemplate) {
        this.spaceManager = requireNonNull(spaceManager, "spaceManager");
        this.transactionTemplate = requireNonNull(transactionTemplate, "transactionTemplate");
    }

    public Iterable<StreamsKey> get() {
        return ImmutableList.<StreamsKey>builder().addAll(spaceKeys()).build();
    }

    private Iterable<StreamsKey> spaceKeys() {
        return transform(fetchSpaces(), this::toStreamsKey);
    }

    private StreamsKey toStreamsKey(Space space) {
        return new StreamsKey(space.getKey(), space.getName());
    }

    @SuppressWarnings("unchecked")
    private Iterable<Space> fetchSpaces() {
        return (Iterable<Space>) transactionTemplate.execute((TransactionCallback) () -> {
            SpacesQuery spacesQuery =
                    newQuery().forUser(AuthenticatedUserThreadLocal.get()).build();
            ListBuilder<Space> spaces = spaceManager.getSpaces(spacesQuery);
            return Ordering.from(spaceSorter).sortedCopy(spaces.getRange(0, spaces.getAvailableSize()));
        });
    }

    private Comparator<Space> spaceSorter = (space1, space2) -> {
        // sort all global spaces above personal spaces, then sort alphabetically
        if (space1.getSpaceType().equals(PERSONAL) && space2.getSpaceType().equals(GLOBAL)) {
            return 1;
        } else if (space1.getSpaceType().equals(GLOBAL) && space2.getSpaceType().equals(PERSONAL)) {
            return -1;
        } else {
            return space1.getName().compareToIgnoreCase(space2.getName());
        }
    };
}
