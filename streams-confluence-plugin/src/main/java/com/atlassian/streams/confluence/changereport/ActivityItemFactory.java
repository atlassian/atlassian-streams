package com.atlassian.streams.confluence.changereport;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.model.people.User;
import com.atlassian.confluence.api.service.network.NetworkService;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.confluence.ConfluenceFilterOptionProvider;
import com.atlassian.streams.spi.Filters;

import static com.google.common.base.Functions.forPredicate;
import static com.google.common.base.Predicates.in;
import static com.google.common.collect.Iterables.addAll;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Iterables.unmodifiableIterable;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityObjectTypes.getActivityObjectTypes;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.spi.Filters.anyInUsers;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getNotValues;
import static com.atlassian.streams.spi.Filters.inActivities;
import static com.atlassian.streams.spi.Filters.inDateRange;
import static com.atlassian.streams.spi.Filters.inProjectKeys;
import static com.atlassian.streams.spi.Filters.notInUsers;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;

public class ActivityItemFactory {
    private static final Logger log = LoggerFactory.getLogger(ActivityItemFactory.class);
    private static final SimplePageRequest PAGE_REQUEST = new SimplePageRequest(1, 1000);

    private final AttachmentActivityItemFactory attachmentActivityItemFactory;
    private final ContentEntityActivityItemFactory contentEntityActivityItemFactory;
    private final NetworkService networkService;
    private final PageManager pageManager;
    private final UserManager userManager;

    public ActivityItemFactory(
            ContentEntityActivityItemFactory contentEntityActivityItemFactory,
            AttachmentActivityItemFactory attachmentActivityItemFactory,
            PageManager pageManager,
            UserManager userManager,
            NetworkService networkService) {
        this.contentEntityActivityItemFactory =
                requireNonNull(contentEntityActivityItemFactory, "contentEntityActivityItemFactory can't be null");
        this.attachmentActivityItemFactory =
                requireNonNull(attachmentActivityItemFactory, "attachmentActivityItemFactory can't be null");
        this.pageManager = requireNonNull(pageManager, "thumbnailManager can't be null");
        this.userManager = requireNonNull(userManager, "userManager can't be null");
        this.networkService = requireNonNull(networkService, "networkService can't be null");
    }

    public Iterable<ActivityItem> getActivityItems(
            final Iterable<ConfluenceEntityObject> searchables, ActivityRequest request) {
        return getActivityItems(ImmutableList.of(), searchables, request);
    }

    public Iterable<ActivityItem> getActivityItems(
            final Iterable<ActivityItem> baseItems,
            final Iterable<ConfluenceEntityObject> searchables,
            ActivityRequest request) {
        return new GetActivityItems(request, baseItems).add(searchables).getResults();
    }

    /**
     * This class encapsulates the scope of a single call to getActivityItems, to provide shared
     * state for the various activity item factory methods.
     */
    private final class GetActivityItems {
        private final BoundedActivityItemTreeSet activityItems;
        private final boolean shouldIncludeAttachments;
        private final boolean hasSpaceFilter;
        private final Option<Date> requestMinDate;
        private Predicate<Pair<ActivityObjectType, ActivityVerb>> inActivities;
        private Predicate<String> inProjectKeys;
        private Predicate<Iterable<String>> anyUsers;
        private Predicate<Iterable<String>> notInUsers;
        private java.util.function.Predicate<String> followedUsers;
        private Predicate<Date> inDateRange;
        private URI baseUri;

        GetActivityItems(ActivityRequest request, Iterable<ActivityItem> baseItems) {
            this.activityItems = new BoundedActivityItemTreeSet(request.getMaxResults(), activityItemSorter);
            addAll(activityItems, baseItems);

            // calculate a few request filter properties that we'll refer to frequently
            shouldIncludeAttachments =
                    any(ImmutableList.of(pair(file(), post()), pair(file(), update())), inActivities(request)::test);
            hasSpaceFilter =
                    !isEmpty(getIsValues(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList())));
            requestMinDate = Filters.getMinDate(request);

            // Calculate the predicates beforehand
            inActivities = inActivities(request)::test;
            inProjectKeys = inProjectKeys(request)::test;
            anyUsers = anyInUsers(request)::test;
            notInUsers = notInUsers(request)::test;
            followedUsers = getFollowedUsersPredicate(request, userManager.getRemoteUserKey());
            inDateRange = inDateRange(request)::test;

            baseUri = request.getContextUri();
        }

        public GetActivityItems add(Iterable<ConfluenceEntityObject> searchables) {
            for (final ConfluenceEntityObject searchable : searchables) {
                try {
                    for (ActivityItem item : toActivityItems(baseUri, searchable)) {
                        if (shouldIncludeItem(item)) {
                            if (!activityItems.add(item)) {
                                // No more room in the result set-- and this item was greater (older) than
                                // the last entry in the set, so there's no point in iterating more items.
                                break;
                            }
                        } else {
                            // Items are always produced in reverse chronological order, so if we've passed
                            // the start of a date range, we should stop iterating.
                            if (requestMinDate.isDefined()) {
                                if (item.getModified().before(requestMinDate.get())) {
                                    break;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    // for whatever reason we could not build an ActivityItem from the ConfluenceEntityObject. move on
                    // and produce a streams feed without it.
                    log.warn("Error building ActivityItem from ConfluenceEntityObject", e);
                }
            }
            return this;
        }

        public Iterable<ActivityItem> getResults() {
            return unmodifiableIterable(activityItems);
        }

        private boolean shouldIncludeItem(ActivityItem item) {
            Option<String> author = option(item.getChangedBy());
            return any(getActivities(item), inActivities)
                    && item.getSpaceKey().map(forPredicate(inProjectKeys)).getOrElse(!hasSpaceFilter)
                    && anyUsers.apply(author)
                    && notInUsers.apply(author)
                    && followedUsers.test(item.getChangedBy())
                    && inDateRange.apply(item.getModified());
        }

        /**
         * Return activity item(s) generated from a single Confluence entity.  The items
         * must be in reverse chronological order, newest to oldest.
         */
        private Iterable<ActivityItem> toActivityItems(URI baseUri, ConfluenceEntityObject entity) {
            if (entity instanceof Attachment) {
                if (shouldIncludeAttachments) {
                    return getAttachmentActivityItems((Attachment) entity);
                } else {
                    return ImmutableList.of();
                }
            } else if (entity instanceof AbstractPage) {
                return getPageHistoryActivityItems((AbstractPage) entity);
            } else if (entity instanceof SpaceDescription) {
                return getSpaceActivityItems((SpaceDescription) entity);
            } else if (entity instanceof Comment) {
                return some(contentEntityActivityItemFactory.newActivityItem(baseUri, (Comment) entity));
            } else {
                throw new IllegalArgumentException("Unsupported entity type: " + entity);
            }
        }

        private Iterable<ActivityItem> getAttachmentActivityItems(Attachment attachment) {
            // see if there's already an attachment change report that we can attach this change to
            // (i.e. different attachment, same person, time, etc)
            for (AttachmentActivityItem attachmentItem : getMatchingAttachmentItem(activityItems, attachment)) {
                ActivityItem updatedItem =
                        attachmentActivityItemFactory.newActivityItem(baseUri, attachment, attachmentItem);
                // replace the existing attachment report with the updated version
                activityItems.remove(attachmentItem);
                activityItems.add(updatedItem);
                return ImmutableList.of();
            }

            return some(attachmentActivityItemFactory.newActivityItem(baseUri, attachment));
        }

        private Iterable<ActivityItem> getPageHistoryActivityItems(final AbstractPage page) {
            return () -> new PageHistoryIterator(baseUri, page);
        }

        private Iterable<ActivityItem> getSpaceActivityItems(SpaceDescription space) {
            // A space doesn't have a full history like a page, but we can at least tell whether
            // it's ever been edited.  If it hasn't, we produce just one item for its creation.
            // If it has, we produce one for the creation and one for the last edit.
            ActivityItem createdItem = contentEntityActivityItemFactory.newActivityItem(space, true);
            if (space.isNew()) {
                return some(createdItem);
            } else {
                ActivityItem editedItem = contentEntityActivityItemFactory.newActivityItem(space, false);
                return ImmutableList.of(editedItem, createdItem);
            }
        }
    }

    /**
     * Creates a {@code Predicate} to apply the filter by network of "followed users".
     *
     * @param request request to generate the predicate from
     * @param user the user requesting the Activity Stream
     * @return {@code Predicate} to determine if a user matches the filter of the request
     */
    private java.util.function.Predicate<String> getFollowedUsersPredicate(
            final ActivityRequest request, final UserKey user) {
        final Collection<Pair<StreamsFilterType.Operator, Iterable<String>>> networkFilters =
                request.getProviderFiltersMap()
                        .getOrDefault(ConfluenceFilterOptionProvider.NETWORK_FILTER, emptyList());

        if (networkFilters.isEmpty()) {
            return s -> true;
        }

        final List<String> followedUsers = getFollowers(user);

        if (!getIsValues(networkFilters).isEmpty()) {
            // Return a predicate on followed users
            return in(followedUsers)::apply;
        }
        if (!getNotValues(networkFilters).isEmpty()) {
            // Return a predicate on followed users
            return ((java.util.function.Predicate<String>) in(followedUsers)::apply).negate();
        }

        return s -> true;
    }

    private List<String> getFollowers(final UserKey user) {
        PageResponse<User> pageResponse;
        final List<String> result = new ArrayList<>();
        do {
            pageResponse = networkService.getFollowers(user, PAGE_REQUEST);
            pageResponse.getResults().stream().map(User::getUsername).forEach(result::add);
        } while (pageResponse.hasMore());
        return result;
    }

    private Option<AttachmentActivityItem> getMatchingAttachmentItem(
            final Iterable<? extends ActivityItem> activityItems, final Attachment attachment) {
        for (final ActivityItem activityItem : activityItems) {
            if (activityItem instanceof AttachmentActivityItem) {
                final AttachmentActivityItem attachmentItem = (AttachmentActivityItem) activityItem;

                if (attachmentItem.matches(attachment)) {
                    return some(attachmentItem);
                }
            }
        }
        return none();
    }

    private Iterable<Pair<ActivityObjectType, ActivityVerb>> getActivities(ActivityItem activityItem) {
        final ActivityVerb verb = activityItem.getVerb();
        return transform(
                getActivityObjectTypes(activityItem.getActivityObjects()),
                activityObjectType -> Pair.pair(activityObjectType, verb));
    }

    private final class PageHistoryIterator implements Iterator<ActivityItem> {
        private AbstractPage page;
        private boolean haveNext;
        private final URI baseUri;

        PageHistoryIterator(final URI baseUri, AbstractPage page) {
            this.page = page;
            this.baseUri = baseUri;
            haveNext = true;
        }

        public boolean hasNext() {
            return prepareNext();
        }

        public ActivityItem next() {
            if (!prepareNext()) {
                return null;
            }
            ActivityItem ret = contentEntityActivityItemFactory.newActivityItem(baseUri, page);
            haveNext = false;
            return ret;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        private boolean prepareNext() {
            if (!haveNext) {
                if (page != null) {
                    page = (AbstractPage) pageManager.getPreviousVersion(page);
                }
                haveNext = (page != null);
            }
            return haveNext;
        }
    }

    /**
     * Used to sort {@code ActivityItem}s into the order in which they will appear in the stream.
     */
    static final Ordering<ActivityItem> activityItemSorter = (new Ordering<ActivityItem>() {
                public int compare(ActivityItem i1, ActivityItem i2) {
                    int cTime = Ordering.natural()
                            .compare(
                                    i1.getModified().getTime(), i2.getModified().getTime());
                    int cVersion = Ordering.natural().compare(i1.getVersion(), i2.getVersion());
                    if (cTime != 0) {
                        // Sort in reverse order
                        return cTime;
                    } else if (cVersion != 0) {
                        // CONFDEV-4493 We can't depend on timestamps as multiple versions of a single page
                        // may have the same timestamp (due to Confluence 4.0 migration)
                        return cVersion;
                    } else if (i1.getId() != null && i2.getId() != null) {
                        return Ordering.natural().compare(i1.getId(), i2.getId());
                    } else {
                        return Ordering.natural().compare(i1.hashCode(), i2.hashCode());
                    }
                }
            })
            .reverse();
}
