package com.atlassian.streams.confluence;

import java.util.List;

import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.user.User;

import static java.util.Objects.requireNonNull;

public class ConfluenceWatchPageInlineActionHandler implements ConfluenceWatchInlineActionHandler<Long> {
    private final NotificationManager notificationManager;
    private final PageManager pageManager;
    private final PermissionManager permissionManager;
    private final ConfluenceWatchHelper<AbstractPage, Long> watchHelper = new ConfluenceWatchHelper<>();

    public ConfluenceWatchPageInlineActionHandler(
            final NotificationManager notificationManager,
            final PageManager pageManager,
            final PermissionManager permissionManager) {
        this.notificationManager = requireNonNull(notificationManager, "notificationManager");
        this.pageManager = requireNonNull(pageManager, "pageManager");
        this.permissionManager = requireNonNull(permissionManager, "permissionManager");
    }

    public boolean startWatching(Long key) {
        AbstractPage page = pageManager.getAbstractPage(key);
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();

        // Note that if page is null, that is, key is invalid, hasPermission is documented to say no permission, so
        // we 412 this which is desirable - it means we don't leak valid page ids
        if (!permissionManager.hasPermission(user, Permission.VIEW, page)) {
            // no permission to see page, don't let them watch it
            return false;
        }

        if (user != null && notificationManager.isUserWatchingPageOrSpace(user, null, page)) {
            // already watching the page. return true
            return true;
        }

        return watchHelper.startWatching(page, this::addPageNotification, this::getPageNotifications);
    }

    private Void addPageNotification(Pair<User, AbstractPage> params) {
        notificationManager.addContentNotification(params.first(), params.second());
        return null;
    }

    private List<Notification> getPageNotifications(AbstractPage entity) {
        return notificationManager.getNotificationsByContent(entity);
    }
}
