package com.atlassian.streams.confluence.changereport;

import java.net.URI;

import com.atlassian.confluence.pages.Attachment;

interface AttachmentActivityItemFactory {

    ActivityItem newActivityItem(URI baseUri, Attachment attachment);

    ActivityItem newActivityItem(URI baseUri, Attachment attachment, AttachmentActivityItem attachmentItem);
}
