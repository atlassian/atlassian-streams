package com.atlassian.streams.confluence.changereport;

import java.util.Date;

import com.google.common.base.Predicate;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;

import static com.google.common.base.Predicates.alwaysFalse;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.confluence.changereport.ContentEntityObjects.isBlogPost;
import static com.atlassian.streams.confluence.changereport.ContentEntityObjects.isMail;

class ContentEntityActivityItem implements ActivityItem {
    private final ContentEntityObject entity;
    private final Iterable<ActivityObject> activityObjects;
    private final Option<ActivityObject> target;
    private final Renderer renderer;
    private final Predicate<String> canCommentPredicate;

    public ContentEntityActivityItem(
            ContentEntityObject entity,
            Iterable<ActivityObject> activityObjects,
            Option<ActivityObject> target,
            Renderer renderer,
            Predicate<String> canCommentPredicate) {
        this.entity = entity;
        this.activityObjects = activityObjects;
        this.target = target;
        this.renderer = renderer;
        this.canCommentPredicate = canCommentPredicate;
    }

    public ContentEntityActivityItem(
            ContentEntityObject entity,
            Iterable<ActivityObject> activityObjects,
            Option<ActivityObject> target,
            Renderer renderer) {
        this.entity = entity;
        this.activityObjects = activityObjects;
        this.target = target;
        this.renderer = renderer;
        this.canCommentPredicate = alwaysFalse();
    }

    public Iterable<ActivityObject> getActivityObjects() {
        return activityObjects;
    }

    public Option<ActivityObject> getTarget() {
        return target;
    }

    public String getChangedBy() {
        return entity.isNew() ? entity.getCreatorName() : entity.getLastModifierName();
    }

    public String getContentType() {
        return entity.getType();
    }

    public String getIconPath() {
        if (isBlogPost(entity)) {
            return "/images/icons/blogentry_16.gif";
        } else if (isMail(entity)) {
            return "/images/icons/mail_content_16.gif";
        } else {
            return "/images/icons/docs_16.gif";
        }
    }

    public Long getId() {
        return entity.getLatestVersionId();
    }

    public Date getModified() {
        // STRM-1486: Confluence screws up the creation date sometimes, so modification date is
        // always preferred if it's available
        return (entity.getLastModificationDate() != null) ? entity.getLastModificationDate() : entity.getCreationDate();
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public Option<String> getSpaceKey() {
        if (entity instanceof SpaceContentEntityObject) {
            return option(((SpaceContentEntityObject) entity.getLatestVersion()).getSpaceKey());
        } else {
            return none();
        }
    }

    public String getType() {
        return entity.getType() + "." + (isNew() ? "added" : "modified");
    }

    public String getUrlPath() {
        return entity.getUrlPath();
    }

    public ActivityVerb getVerb() {
        return isNew() ? post() : update();
    }

    public boolean isNew() {
        return entity.isNew();
    }

    public boolean isAcceptingCommentsFromUser(String username) {
        return canCommentPredicate.apply(username);
    }

    public int getVersion() {
        return entity.getVersion();
    }

    public ContentEntityObject getEntity() {
        return entity;
    }
}
