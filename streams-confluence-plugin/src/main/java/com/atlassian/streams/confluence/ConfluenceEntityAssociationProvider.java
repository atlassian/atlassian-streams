package com.atlassian.streams.confluence;

import java.net.URI;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableList;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;

public class ConfluenceEntityAssociationProvider implements StreamsEntityAssociationProvider {
    private static final String URL_PREFIX = "/display/";
    private static final Pattern SPACE_PATTERN = Pattern.compile("([A-Za-z0-9]+)([/#?].*)?");

    private final ApplicationProperties applicationProperties;
    private final SpaceManager spaceManager;
    private final SpacePermissionManager spacePermissionManager;
    private final PermissionManager permissionManager;

    public ConfluenceEntityAssociationProvider(
            ApplicationProperties applicationProperties,
            SpaceManager spaceManager,
            SpacePermissionManager spacePermissionManager,
            PermissionManager permissionManager) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.spaceManager = checkNotNull(spaceManager, "spaceManager");
        this.spacePermissionManager = checkNotNull(spacePermissionManager, "spacePermissionManager");
        this.permissionManager = checkNotNull(permissionManager, "permissionManager");
    }

    @Override
    public Iterable<EntityIdentifier> getEntityIdentifiers(URI target) {
        String targetStr = target.toString();
        if (target.isAbsolute()) {
            if (!targetStr.startsWith(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + URL_PREFIX)) {
                return ImmutableList.of();
            }
            return matchEntities(targetStr.substring(
                    applicationProperties.getBaseUrl(UrlMode.CANONICAL).length() + URL_PREFIX.length()));
        } else {
            return matchEntities(targetStr);
        }
    }

    @Override
    public Option<URI> getEntityURI(EntityIdentifier identifier) {
        if (identifier.getType().equals(space().iri())) {
            return some(URI.create(
                    applicationProperties.getBaseUrl(UrlMode.CANONICAL) + URL_PREFIX + encode(identifier.getValue())));
        }
        return none();
    }

    @Override
    public Option<String> getFilterKey(EntityIdentifier identifier) {
        if (identifier.getType().equals(space().iri())) {
            return some(PROJECT_KEY);
        }
        return none();
    }

    @Override
    public Option<Boolean> getCurrentUserViewPermission(EntityIdentifier identifier) {
        return getCurrentUserPermission(identifier, SpacePermission.VIEWSPACE_PERMISSION);
    }

    @Override
    public Option<Boolean> getCurrentUserEditPermission(EntityIdentifier identifier) {
        return getCurrentUserPermission(identifier, SpacePermission.COMMENT_PERMISSION);
    }

    @Override
    public Optional<Boolean> getCurrentUserViewPermissionForTargetlessEntity() {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        return Optional.of(
                permissionManager.hasPermission(user, Permission.VIEW, PermissionManager.TARGET_APPLICATION));
    }

    private Option<Boolean> getCurrentUserPermission(EntityIdentifier identifier, String permission) {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        if (identifier.getType().equals(space().iri())) {
            Space space = spaceManager.getSpace(identifier.getValue());
            if (space != null) {
                return some(spacePermissionManager.hasPermission(permission, space, user));
            }
        }
        return none();
    }

    private Iterable<EntityIdentifier> matchEntities(String input) {
        Matcher spaceMatcher = SPACE_PATTERN.matcher(input);
        if (spaceMatcher.matches()) {
            String spaceKey = spaceMatcher.group(1);
            Space space = spaceManager.getSpace(spaceKey);
            if (space != null) {
                URI canonicalUri =
                        URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + URL_PREFIX + encode(spaceKey));
                return ImmutableList.of(new EntityIdentifier(space().iri(), spaceKey, canonicalUri));
            }
        }

        return ImmutableList.of();
    }
}
