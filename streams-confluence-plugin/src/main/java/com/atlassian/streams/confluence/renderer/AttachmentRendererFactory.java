package com.atlassian.streams.confluence.renderer;

import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem.Entry;
import com.atlassian.templaterenderer.TemplateRenderer;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.size;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.renderer.Renderers.render;

public final class AttachmentRendererFactory {
    private final StreamsEntryRendererFactory rendererFactory;
    private final I18nResolver i18nResolver;
    private final TemplateRenderer templateRenderer;
    private final ApplicationProperties applicationProperties;

    public AttachmentRendererFactory(
            StreamsEntryRendererFactory rendererFactory,
            I18nResolver i18nResolver,
            TemplateRenderer templateRenderer,
            ApplicationProperties applicationProperties) {
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public Renderer newInstance(Iterable<Entry> entries) {
        return new AttachmentRenderer(entries);
    }

    public final class EntryWrapper {
        private AttachmentActivityItem.Entry entry;
        private String absoluteDownloadPath;

        public EntryWrapper(AttachmentActivityItem.Entry entry, String absoluteDownloadPath) {
            this.entry = entry;
            this.absoluteDownloadPath = absoluteDownloadPath;
        }

        public AttachmentActivityItem.Entry getEntry() {
            return entry;
        }

        public String getAbsoluteDownloadPath() {
            return absoluteDownloadPath;
        }
    }

    private final class AttachmentRenderer implements Renderer {
        private final java.util.function.Function<Iterable<UserProfile>, Html> authorsRenderer =
                rendererFactory.newAuthorsRendererFunc();
        private final java.util.function.Function<ActivityObject, Option<Html>> targetRenderer =
                rendererFactory.newActivityObjectRendererWithSummaryFunc();
        private final Iterable<Entry> entries;

        public AttachmentRenderer(Iterable<Entry> entries) {
            this.entries = entries;
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry) {
            return entry.getTarget()
                    .flatMap(targetRenderer)
                    .map(renderAttachedTo(entry))
                    .getOrElse(renderAttached(entry));
        }

        private Supplier<Html> renderAttached(final StreamsEntry entry) {
            return () -> new Html(i18nResolver.getText(
                    "streams.confluence.attached",
                    authorsRenderer.apply(entry.getAuthors()),
                    size(entry.getActivityObjects())));
        }

        private Function<Html, Html> renderAttachedTo(final StreamsEntry entry) {
            return target -> new Html(i18nResolver.getText(
                    "streams.confluence.attached.to",
                    authorsRenderer.apply(entry.getAuthors()),
                    size(entry.getActivityObjects()),
                    target));
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry) {
            return none();
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry) {
            final String baseUrl = applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);
            final Collection<EntryWrapper> previewableEntries = StreamSupport.stream(
                            filter(entries, previewable()).spliterator(), false)
                    .map(entryItem -> new EntryWrapper(
                            entryItem, getAttachmentDownloadPath(baseUrl, entryItem.getDownloadPath())))
                    .collect(Collectors.toList());
            final Collection<EntryWrapper> nonPreviewableEntries = StreamSupport.stream(
                            filter(entries, not(previewable())).spliterator(), false)
                    .map(entryItem -> new EntryWrapper(
                            entryItem, getAttachmentDownloadPath(baseUrl, entryItem.getDownloadPath())))
                    .collect(Collectors.toList());

            ImmutableMap<String, Object> context = ImmutableMap.of(
                    "previewable", ImmutableList.copyOf(previewableEntries),
                    "nonpreviewable", ImmutableList.copyOf(nonPreviewableEntries),
                    "applicationProperties", applicationProperties);
            return some(new Html(render(templateRenderer, "attachment-content.vm", context)));
        }

        /**
         * Using URL class to do concat url
         *
         * @param baseUrl
         * @param attachmentPath
         * @return try to return absolute path, if not then relative path
         */
        private String getAttachmentDownloadPath(String baseUrl, String attachmentPath) {
            URI uri = URI.create(baseUrl);
            String newPath = uri.getPath() + attachmentPath;
            return uri.resolve(newPath).toString();
        }
    }

    private final Predicate<AttachmentActivityItem.Entry> previewable() {
        return Previewable.INSTANCE;
    }

    private enum Previewable implements Predicate<AttachmentActivityItem.Entry> {
        INSTANCE;

        public boolean apply(AttachmentActivityItem.Entry attachment) {
            return attachment.getPreview().isDefined();
        }
    };
}
