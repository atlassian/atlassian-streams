package com.atlassian.streams.confluence;

import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.streams.spi.StreamsValidator;

/**
 * Confluence implementation of the validator
 *
 * @since v3.1
 */
public class ConfluenceStreamsValidator implements StreamsValidator {
    private final SpaceManager spaceManager;
    private final SpacePermissionManager spacePermissionManager;

    public ConfluenceStreamsValidator(SpaceManager spaceManager, SpacePermissionManager spacePermissionManager) {
        this.spaceManager = spaceManager;
        this.spacePermissionManager = spacePermissionManager;
    }

    public boolean isValidKey(final String key) {
        final Space space = spaceManager.getSpace(key);
        return space != null
                && spacePermissionManager.hasPermission(
                        SpacePermission.VIEWSPACE_PERMISSION, space, AuthenticatedUserThreadLocal.getUser());
    }
}
