package com.atlassian.streams.confluence;

import java.net.URI;

import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.user.User;

import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;

public class ConfluenceUserProfileAccessor implements UserProfileAccessor {
    private final UserManager userManager;
    private final ApplicationProperties applicationProperties;
    private final StreamsI18nResolver i18nResolver;
    private final SpacePermissionManager spacePermissionManager;

    public ConfluenceUserProfileAccessor(
            UserManager userManager,
            ApplicationProperties applicationProperties,
            StreamsI18nResolver i18nResolver,
            SpacePermissionManager spacePermissionManager) {
        this.userManager = userManager;
        this.applicationProperties = applicationProperties;
        this.i18nResolver = i18nResolver;
        this.spacePermissionManager = spacePermissionManager;
    }

    private URI getUserProfileUri(URI baseUri, String username) {
        return URI.create(baseUri.toASCIIString() + "/display/~" + encode(username));
    }

    private URI getProfilePictureUri(URI baseUri, com.atlassian.sal.api.user.UserProfile user) {
        // If the viewing user is anonymous (null) we want to display the default user photo instead of the profile
        // picture
        User currentUser = AuthenticatedUserThreadLocal.get();
        boolean viewUserProfiles =
                spacePermissionManager.hasPermission(SpacePermission.BROWSE_USERS_PERMISSION, null, currentUser);

        if ((currentUser == null) && !viewUserProfiles) {
            return getDefaultProfilePicture(baseUri);
        } else {
            URI profilePictureUri = user.getProfilePictureUri();
            if (profilePictureUri == null) {
                return getDefaultProfilePicture(baseUri);
            }
            return profilePictureUri.isAbsolute()
                    ? profilePictureUri
                    : URI.create(baseUri.toASCIIString() + profilePictureUri);
        }
    }

    private URI getDefaultProfilePicture(URI baseUri) {
        return URI.create(baseUri.toASCIIString() + ProfilePictureInfo.DEFAULT_PROFILE_PATH);
    }

    public UserProfile getAnonymousUserProfile(URI baseUri) {
        return new UserProfile.Builder(i18nResolver.getText("streams.confluence.authors.unknown.username"))
                .fullName(i18nResolver.getText("streams.confluence.authors.unknown.fullname"))
                .profilePictureUri(some(getAnonymousProfilePictureUri(baseUri)))
                .build();
    }

    private URI getAnonymousProfilePictureUri(URI baseUri) {
        return URI.create(baseUri + ProfilePictureInfo.ANONYMOUS_PROFILE_PATH);
    }

    @Override
    public UserProfile getUserProfile(final String username) {
        return getUserProfile(URI.create(applicationProperties.getBaseUrl()), username);
    }

    @Override
    public UserProfile getAnonymousUserProfile() {
        return getAnonymousUserProfile(URI.create(applicationProperties.getBaseUrl()));
    }

    public UserProfile getUserProfile(URI baseUri, String username) {
        if (username == null) {
            return getAnonymousUserProfile(baseUri);
        }

        com.atlassian.sal.api.user.UserProfile user = userManager.getUserProfile(username);

        if (user == null) {
            return new UserProfile.Builder(username)
                    .profilePictureUri(some(getAnonymousProfilePictureUri(baseUri)))
                    .build();
        } else {
            return new UserProfile.Builder(username)
                    .fullName(user.getFullName())
                    .email(option(user.getEmail()))
                    .profilePageUri(option(getUserProfileUri(baseUri, username)))
                    .profilePictureUri(option(getProfilePictureUri(baseUri, user)))
                    .build();
        }
    }
}
