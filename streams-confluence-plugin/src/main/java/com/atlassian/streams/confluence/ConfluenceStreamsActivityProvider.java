package com.atlassian.streams.confluence;

import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Qualifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.search.v2.ChangesSearch;
import com.atlassian.confluence.search.v2.ContentPermissionsQueryFactory;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.SiteSearchPermissionsQueryFactory;
import com.atlassian.confluence.search.v2.SpacePermissionQueryFactory;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.TermQuery;
import com.atlassian.confluence.search.v2.sort.ModifiedSort;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.persistence.dao.ConfluenceUserDao;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.confluence.changereport.ActivityItem;
import com.atlassian.streams.confluence.changereport.ActivityItemFactory;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StreamsActivityProvider;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.size;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import static com.atlassian.confluence.search.v2.SearchFieldMappings.LAST_MODIFIER;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.confluence.ConfluenceFilterOptionProvider.activities;
import static com.atlassian.streams.confluence.ConfluenceFilters.getExcludedSearchTerms;
import static com.atlassian.streams.confluence.ConfluenceFilters.getSearchTerms;
import static com.atlassian.streams.spi.Filters.getAuthors;
import static com.atlassian.streams.spi.Filters.getMaxDate;
import static com.atlassian.streams.spi.Filters.getMinDate;
import static com.atlassian.streams.spi.Filters.getProjectKeys;
import static com.atlassian.streams.spi.Filters.getRequestedActivityObjectTypes;

public class ConfluenceStreamsActivityProvider implements StreamsActivityProvider {
    private static final Logger log = LoggerFactory.getLogger(ConfluenceStreamsActivityProvider.class);

    public static final String PROVIDER_KEY = "wiki";

    private final SearchManager searchManager;
    private final ConfluenceEntryFactory entryFactory;
    private final I18nResolver i18nResolver;
    private final ActivityItemFactory activityItemFactory;
    private final ConfluenceUserDao confluenceUserDao;
    private final SiteSearchPermissionsQueryFactory siteSearchPermissionsQueryFactory;
    private final SpacePermissionQueryFactory spacePermissionQueryFactory;
    private final ContentPermissionsQueryFactory contentPermissionsQueryFactory;

    public ConfluenceStreamsActivityProvider(
            @Qualifier("searchManager") final SearchManager searchManager,
            final ConfluenceEntryFactory entryFactory,
            final I18nResolver i18nResolver,
            final ActivityItemFactory activityItemFactory,
            final ConfluenceUserDao confluenceUserDao,
            final SiteSearchPermissionsQueryFactory siteSearchPermissionsQueryFactory,
            final SpacePermissionQueryFactory spacePermissionQueryFactory,
            final ContentPermissionsQueryFactory contentPermissionsQueryFactory) {
        this.searchManager = checkNotNull(searchManager, "searchManager");
        this.entryFactory = checkNotNull(entryFactory, "entryFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.activityItemFactory = checkNotNull(activityItemFactory, "activityItemFactory");
        this.confluenceUserDao = checkNotNull(confluenceUserDao, "confluenceUserDao");
        this.siteSearchPermissionsQueryFactory =
                checkNotNull(siteSearchPermissionsQueryFactory, "siteSearchPermissionsQueryFactory");
        this.spacePermissionQueryFactory = checkNotNull(spacePermissionQueryFactory, "spacePermissionQueryFactory");
        this.contentPermissionsQueryFactory =
                checkNotNull(contentPermissionsQueryFactory, "contentPermissionsQueryFactory");
    }

    /**
     * Get the activity feed for the given request
     *
     * @param request The request
     * @return The ATOM feed
     */
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest request) {
        return new CancellableTask<StreamsFeed>() {
            final AtomicBoolean cancelled = new AtomicBoolean(false);

            @Override
            public StreamsFeed call() {
                final Iterable<StreamsEntry> entries = getStreamsEntries(request, cancelled::get);

                return new StreamsFeed(
                        i18nResolver.getText("portlet.activityfeed.name"),
                        take(request.getMaxResults(), entries),
                        some(i18nResolver.getText("portlet.activityfeed.description")));
            }

            @Override
            public Result cancel() {
                cancelled.set(true);
                return Result.CANCELLED;
            }
        };
    }

    /**
     * Fetches the appropriate amount of {@code StreamsEntry}s requested by the provided {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing streams filters and parameters
     * @return the {@code StreamsEntry}s
     */
    private Iterable<StreamsEntry> getStreamsEntries(final ActivityRequest request, Supplier<Boolean> cancelled) {
        int offset = 0;

        Iterable<ConfluenceEntityObject> searchables = search(request, offset, cancelled);
        if (isEmpty(searchables)) {
            return ImmutableList.of();
        }

        Iterable<ActivityItem> activityItems = activityItemFactory.getActivityItems(searchables, request);
        Iterable<StreamsEntry> entries = toStreamsEntries(request, activityItems, cancelled);
        while (size(entries) < request.getMaxResults()) {
            offset += request.getMaxResults();
            searchables = search(request, offset, cancelled);
            if (isEmpty(searchables)) {
                // no more content to be found. return the list we have built up until now.
                return entries;
            }
            // more content was found. let's add it to the entry list
            activityItems = activityItemFactory.getActivityItems(activityItems, searchables, request);
            entries = toStreamsEntries(request, activityItems, cancelled);
        }
        return entries;
    }

    /**
     * Converts an {@code Iterable} of {@code ActivityItem}s to an {@code Iterable} of {@code StreamsEntry}s,
     * while respecting a set of filters specified by a {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filters
     * @param activityItems {@code ActivityItem}s to convert
     * @return the filtered {@code Iterable} of {@code StreamsEntry}s
     */
    private Iterable<StreamsEntry> toStreamsEntries(
            final ActivityRequest request, Iterable<ActivityItem> activityItems, Supplier<Boolean> cancelled) {
        // we use Iterables.size immediately after we return from here, so we might as well create a list now
        return ImmutableList.copyOf(take(
                request.getMaxResults(),
                catOptions(StreamSupport.stream(activityItems.spliterator(), false)
                        .map(toStreamsEntry(request.getContextUri(), cancelled))
                        .collect(toList()))));
    }

    /**
     * Searches for the next {@code request.getMaxResults()} number of {@code ConfluenceEntityObject} matching
     * the {@code ActivityRequest}'s filters, starting at the specified offset.
     *
     * @param request the {@code ActivityRequest} containing streams filters and parameters
     * @param startOffset the offset to search at
     * @param cancelled
     * @return the next group of {@code ConfluenceEntityObject}s
     */
    private Iterable<ConfluenceEntityObject> search(
            final ActivityRequest request, int startOffset, Supplier<Boolean> cancelled) {
        final ISearch search = buildSearch(request, startOffset);

        try {
            final SearchResults results = searchManager.search(search);
            if (isEmpty(results)) {
                return ImmutableList.of();
            }
            if (cancelled.get()) {
                throw new CancelledException();
            }
            List<Searchable> entities =
                    searchManager.convertToEntities(results, SearchManager.EntityVersionPolicy.INDEXED_VERSION);
            return entities.stream()
                    .filter((ConfluenceEntityObject.class)::isInstance)
                    .map(e -> (ConfluenceEntityObject) e)
                    .collect(toList());
        } catch (final InvalidSearchException e) {
            log.warn("Invalid search occurred", e);
            return ImmutableList.of();
        }
    }

    /**
     * Returns the search query for the given {@code ActivityRequest} starting at the specified offset.
     *
     * @param request the {@code ActivityRequest} from which to build the search query
     * @param startOffset the offset at which the search should begin
     * @return the {@code ISearch} to query for confluence entity objects
     */
    private ISearch buildSearch(final ActivityRequest request, int startOffset) {
        Set<String> authors = ImmutableSet.copyOf(getAuthors(request));
        Iterable<String> searchTerms = getSearchTerms(request);
        Iterable<String> excludedSearchTerms = getExcludedSearchTerms(request);
        Iterable<ActivityObjectType> activityObjectTypes = getRequestedActivityObjectTypes(request, activities);

        final ConfluenceSearchQueryBuilder queryBuilder = new ConfluenceSearchQueryBuilder(confluenceUserDao)
                .inSpace(getProjectKeys(request))
                .searchFor(searchTerms)
                .excludeTerms(excludedSearchTerms)
                .activityObjects(activityObjectTypes)
                .minDate(getMinDate(request))
                .maxDate(getMaxDate(request));
        final SearchSort sort = ModifiedSort.DESCENDING; // latest modified content first

        final ConfluenceUser authenticatedUser = AuthenticatedUserThreadLocal.get();
        final BooleanQuery.Builder filterBuilder = BooleanQuery.builder();
        filterBuilder.addFilter(siteSearchPermissionsQueryFactory.create());
        filterBuilder.addFilter(spacePermissionQueryFactory.create(authenticatedUser));
        contentPermissionsQueryFactory.create(authenticatedUser).ifPresent(filterBuilder::addFilter);

        if (isEmpty(searchTerms) && isEmpty(excludedSearchTerms)) {
            if (!isEmpty(authors)) {
                filterBuilder.addFilters(
                        authors.stream().map(this::buildLastModifierFilter).collect(toSet()));
            }

            queryBuilder.addFilters(filterBuilder.build());

            // Search just for changes (page edits/adds) instead of actual content.
            return new ChangesSearch(queryBuilder.build(), sort, startOffset, request.getMaxResults());
        } else {
            queryBuilder.addFilters(filterBuilder.build());

            // Must search page content, as well. A content search will be slower than ChangesSearch,
            // but is a necessary evil to implement JIRA issue key filter.
            return new ContentSearch(
                    queryBuilder.createdOrLastModifiedBy(authors).build(), sort, startOffset, request.getMaxResults());
        }
    }

    private Function<ActivityItem, Option<StreamsEntry>> toStreamsEntry(
            final URI baseUri, final Supplier<Boolean> cancelled) {
        return activityItem -> {
            try {
                // getting item properties may be expensive, so check for cancellation here
                if (cancelled.get()) {
                    throw new CancelledException();
                }

                return some(entryFactory.buildStreamsEntry(baseUri, activityItem));
            } catch (Exception e) {
                log.warn("Error creating streams entry", e);
                return none();
            }
        };
    }

    private TermQuery buildLastModifierFilter(String username) {
        final ConfluenceUser user = confluenceUserDao.findByUsername(username);
        final String userKey = user != null ? user.getKey().getStringValue() : "";
        return new TermQuery(LAST_MODIFIER.getName(), userKey);
    }
}
