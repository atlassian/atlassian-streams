package com.atlassian.streams.confluence;

import java.util.List;
import java.util.Objects;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.user.User;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.AdditionalAnswers.delegatesTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceWatchPageInlineActionHandlerTest {
    private static final Long NO_SUCH_PAGE_ID = 6462L;
    private static final Long ALICES_SECRET_PAGE_ID = 28227L;
    private static final Long BOBS_PUBLIC_PAGE_ID = 14443L;

    private MockNotificationManager mockNotificationManager = new MockNotificationManager();

    private NotificationManager notificationManager =
            mock(NotificationManager.class, delegatesTo(mockNotificationManager));

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @InjectMocks
    private ConfluenceWatchPageInlineActionHandler actionHandler;

    @Mock
    private ConfluenceUser alice;

    @Mock
    private ConfluenceUser bob;

    @Mock
    private AbstractPage alicesSecretPage;

    @Mock
    private AbstractPage bobsPublicPage;

    // The ConfluenceWatchHelper uses statefulness to see if the watch worked, so we need to mock this
    // Methods are used reflectively from Mockito
    @SuppressWarnings("all")
    public static class MockNotificationManager {
        long nextId = 0;
        Multimap<ContentEntityObject, Notification> pageNotificationMap = HashMultimap.create();

        public boolean isUserWatchingPageOrSpace(final User user, final Space space, final AbstractPage page) {
            return pageNotificationMap.get(page).stream()
                    .anyMatch(notification -> Objects.equals(notification.getReceiver(), user));
        }

        public List<Notification> getNotificationsByContent(final ContentEntityObject page) {
            return ImmutableList.copyOf(pageNotificationMap.get(page));
        }

        public Notification addContentNotification(final User user, final ContentEntityObject page) {
            final Notification notification = new Notification();
            // Notification gets EntityObject#equals, which compares on id, so fake it
            notification.setId(nextId++);
            notification.setReceiver((ConfluenceUser) user);
            pageNotificationMap.put(page, notification);
            return notification;
        }
    }

    @Before
    public void setUp() {
        when(pageManager.getAbstractPage(ALICES_SECRET_PAGE_ID)).thenReturn(alicesSecretPage);
        when(pageManager.getAbstractPage(BOBS_PUBLIC_PAGE_ID)).thenReturn(bobsPublicPage);
        when(permissionManager.hasPermission(alice, Permission.VIEW, alicesSecretPage))
                .thenReturn(true);
        when(permissionManager.hasPermission(any(), eq(Permission.VIEW), eq(bobsPublicPage)))
                .thenReturn(true);
        // permissionManager mock will default to returning false, so bob and null (anonymous) don't have permission
    }

    private void setCurrentUser(final ConfluenceUser user) {
        // It's important to go via the ConfluenceUser variant here, that has non vertigo fallbacks that we need
        // Yes its dodgy to rely on this long term
        AuthenticatedUserThreadLocal.set(user);
    }

    @After
    public void tearDown() {
        AuthenticatedUserThreadLocal.reset();
    }

    @Test
    public void cannotWatchNonExistentPageAnonymously() {
        final boolean result = actionHandler.startWatching(NO_SUCH_PAGE_ID);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void cannotWatchNonExistentPageAsAlice() {
        setCurrentUser(alice);
        final boolean result = actionHandler.startWatching(NO_SUCH_PAGE_ID);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void cannotWatchNonPermittedPageAnonymously() {
        final boolean result = actionHandler.startWatching(ALICES_SECRET_PAGE_ID);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void cannotWatchNonPermittedPageAsBob() {
        setCurrentUser(bob);
        final boolean result = actionHandler.startWatching(ALICES_SECRET_PAGE_ID);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void watchingUnwatchedPageSetsUpNotification() {
        setCurrentUser(alice);
        final boolean result = actionHandler.startWatching(ALICES_SECRET_PAGE_ID);
        assertThat(result, is(true));
        verify(notificationManager).addContentNotification(alice, alicesSecretPage);
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void watchingPageWatchedByCurrentUserDoesntAddNewNotification() {
        setCurrentUser(alice);
        mockNotificationManager.addContentNotification(alice, alicesSecretPage);

        final boolean result = actionHandler.startWatching(ALICES_SECRET_PAGE_ID);
        assertThat(result, is(true));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void watchingPageWatchedByOtherUserSetsUpNotification() {
        setCurrentUser(alice);
        mockNotificationManager.addContentNotification(bob, bobsPublicPage);

        // Alice is going to watch bobs page that he is already watching
        final boolean result = actionHandler.startWatching(BOBS_PUBLIC_PAGE_ID);
        assertThat(result, is(true));
        verify(notificationManager).addContentNotification(alice, bobsPublicPage);
        verifyNoMoreNotificationChanges();
    }

    private void verifyNoMoreNotificationChanges() {
        // We use atLeast(0) to whitelist read queries made by the implementations ...
        verify(notificationManager, atLeast(0)).getNotificationsByContent(any());
        verify(notificationManager, atLeast(0)).isUserWatchingPageOrSpace(any(), any(), any());
        // Then verify no more so that things fail if we adapt the code to move away, rather than needing to explicitly
        // block the replacement methods.
        verifyNoMoreInteractions(notificationManager);
    }
}
