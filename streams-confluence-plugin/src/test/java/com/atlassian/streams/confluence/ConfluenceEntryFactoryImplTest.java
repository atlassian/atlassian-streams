package com.atlassian.streams.confluence;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.CaptchaManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.confluence.changereport.ActivityItem;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.api.StreamsEntry.ActivityObject.params;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceEntryFactoryImplTest {
    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private NotificationManager notificationManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private UserProfileAccessor userProfileAccessor;

    @Mock
    private CaptchaManager captchaManager;

    @Mock
    private ActivityItem activityItem;

    @Mock
    private UserManager userManager;

    @Mock
    private UriProvider uriProvider;

    @Mock
    private StreamsI18nResolver i18nResolver;

    @Mock
    private Space space;

    private ActivityObject activityObject;
    private ConfluenceEntryFactoryImpl confluenceEntryFactory;

    @Before
    public void createStreamsActivityProvider() {
        activityObject = new ActivityObject(params().id("ID").activityObjectType(space()));
        confluenceEntryFactory = new ConfluenceEntryFactoryImpl(
                applicationProperties,
                notificationManager,
                pageManager,
                spaceManager,
                userProfileAccessor,
                captchaManager,
                userManager,
                uriProvider,
                i18nResolver);

        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://example.com");

        when(activityItem.isAcceptingCommentsFromUser(any())).thenReturn(true);
        when(activityItem.getContentType()).thenReturn("text/html");
        when(activityItem.getId()).thenReturn(12345L);
        when(notificationManager.isUserWatchingPageOrSpace(any(), any(), any())).thenReturn(false);
    }

    @Test
    public void assertThatReplyToLinkDoesNotExistWhenCaptchaIsOn() {
        when(captchaManager.showCaptchaForCurrentUser()).thenReturn(true);
        assertFalse(confluenceEntryFactory.buildReplyTo(activityItem).isDefined());
    }

    @Test
    public void assertThatReplyToLinkExistsWhenCaptchaIsOff() {
        when(captchaManager.showCaptchaForCurrentUser()).thenReturn(false);
        assertTrue(confluenceEntryFactory.buildReplyTo(activityItem).isDefined());
    }

    @Test
    public void assertThatWatchUrlExistsWhenActivityTypeIsSpace() {
        ConfluenceUser user = mock(ConfluenceUser.class);
        AuthenticatedUserThreadLocal.set(user);
        when(spaceManager.getSpace("DS")).thenReturn(space);
        when(activityItem.getActivityObjects()).thenReturn(ImmutableList.of(activityObject));
        when(activityItem.getSpaceKey()).thenReturn(some("DS"));
        assertTrue(confluenceEntryFactory.getWatchLink(activityItem).isDefined());
    }

    @Test
    public void assertThatPersonalSpaceWithSpaceReturnsEncodedWatchUrl() {
        ConfluenceUser user = mock(ConfluenceUser.class);
        AuthenticatedUserThreadLocal.set(user);
        when(spaceManager.getSpace("MY SPACE")).thenReturn(space);
        when(activityItem.getActivityObjects()).thenReturn(ImmutableList.of(activityObject));
        when(activityItem.getSpaceKey()).thenReturn(some("MY SPACE"));
        assertThat(
                confluenceEntryFactory
                        .getWatchLink(activityItem)
                        .get()
                        .getHref()
                        .toASCIIString(),
                equalTo("http://example.com/rest/confluence-activity-stream/1.0/actions/space-watch/MY+SPACE"));
    }
}
