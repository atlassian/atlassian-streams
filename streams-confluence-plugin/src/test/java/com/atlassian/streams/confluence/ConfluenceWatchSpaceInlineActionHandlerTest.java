package com.atlassian.streams.confluence;

import java.util.List;
import java.util.Objects;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.user.User;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.AdditionalAnswers.delegatesTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceWatchSpaceInlineActionHandlerTest {

    private static final String NO_SUCH_SPACE_KEY = "NOSPACE";
    private static final String ALICES_SECRET_SPACE_KEY = "SECRET";
    private static final String BOBS_PUBLIC_SPACE_KEY = "PUBLIC";

    private final MockNotificationManager mockNotificationManager = new MockNotificationManager();
    private final NotificationManager notificationManager =
            mock(NotificationManager.class, delegatesTo(mockNotificationManager));

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private PermissionManager permissionManager;

    @InjectMocks
    private ConfluenceWatchSpaceInlineActionHandler actionHandler;

    @Mock
    private ConfluenceUser alice;

    @Mock
    private ConfluenceUser bob;

    @Mock
    private Space alicesSecretSpace;

    @Mock
    private Space bobsPublicSpace;

    // The ConfluenceWatchHelper uses statefulness to see if the watch worked, so we need to mock this
    // Methods are used reflectively from Mockito
    @SuppressWarnings("all")
    public static class MockNotificationManager {
        long nextId = 0;

        Multimap<Space, Notification> spaceNotificationMap = HashMultimap.create();

        public boolean isUserWatchingPageOrSpace(final User user, final Space space, final AbstractPage page) {
            return spaceNotificationMap.get(space).stream()
                    .anyMatch(notification -> Objects.equals(notification.getReceiver(), user));
        }

        public List<Notification> getNotificationsBySpaceAndType(final Space space, final ContentTypeEnum type) {
            return ImmutableList.copyOf(spaceNotificationMap.get(space));
        }

        public Notification addSpaceNotification(final User user, final Space space, final ContentTypeEnum type) {
            final Notification notification = new Notification();
            // Notification gets EntityObject#equals, which compares on id, so fake it
            notification.setId(nextId++);
            notification.setReceiver((ConfluenceUser) user);
            spaceNotificationMap.put(space, notification);
            return notification;
        }
    }

    @Before
    public void setUp() {
        when(spaceManager.getSpace(ALICES_SECRET_SPACE_KEY)).thenReturn(alicesSecretSpace);
        when(spaceManager.getSpace(BOBS_PUBLIC_SPACE_KEY)).thenReturn(bobsPublicSpace);
        when(permissionManager.hasPermission(alice, Permission.VIEW, alicesSecretSpace))
                .thenReturn(true);
        when(permissionManager.hasPermission(any(), eq(Permission.VIEW), eq(bobsPublicSpace)))
                .thenReturn(true);
        // permissionManager mock will default to returning false, so bob and null (anonymous) don't have permission
    }

    private void setCurrentUser(final ConfluenceUser user) {
        // It's important to go via the ConfluenceUser variant here, that has non vertigo fallbacks that we need
        // Yes its dodgy to rely on this long term
        AuthenticatedUserThreadLocal.set(user);
    }

    @After
    public void tearDown() {
        AuthenticatedUserThreadLocal.reset();
    }

    @Test
    public void cannotWatchNonExistentSpaceAnonymously() {
        final boolean result = actionHandler.startWatching(NO_SUCH_SPACE_KEY);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void cannotWatchNonExistentSpaceAsAlice() {
        setCurrentUser(alice);
        final boolean result = actionHandler.startWatching(NO_SUCH_SPACE_KEY);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void cannotWatchNonPermittedSpaceAnonymously() {
        final boolean result = actionHandler.startWatching(ALICES_SECRET_SPACE_KEY);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void cannotWatchNonPermittedSpaceAsBob() {
        setCurrentUser(bob);
        final boolean result = actionHandler.startWatching(ALICES_SECRET_SPACE_KEY);
        assertThat(result, is(false));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void watchingUnwatchedSpaceSetsUpNotification() {
        setCurrentUser(alice);
        final boolean result = actionHandler.startWatching(ALICES_SECRET_SPACE_KEY);
        assertThat(result, is(true));
        verify(notificationManager).addSpaceNotification(alice, alicesSecretSpace, null);
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void watchingSpaceWatchedByCurrentUserDoesntAddNewNotification() {
        setCurrentUser(alice);
        mockNotificationManager.addSpaceNotification(alice, alicesSecretSpace, null);

        final boolean result = actionHandler.startWatching(ALICES_SECRET_SPACE_KEY);
        assertThat(result, is(true));
        verifyNoMoreNotificationChanges();
    }

    @Test
    public void watchingSpaceWatchedByOtherUserSetsUpNotification() {
        setCurrentUser(alice);
        mockNotificationManager.addSpaceNotification(bob, bobsPublicSpace, null);

        // Alice is going to watch bobs page that he is already watching
        final boolean result = actionHandler.startWatching(BOBS_PUBLIC_SPACE_KEY);
        assertThat(result, is(true));
        verify(notificationManager).addSpaceNotification(alice, bobsPublicSpace, null);
        verifyNoMoreNotificationChanges();
    }

    private void verifyNoMoreNotificationChanges() {
        // We use atLeast(0) to whitelist read queries made by the implementations ...
        verify(notificationManager, atLeast(0)).getNotificationsBySpaceAndType(any(), any());
        verify(notificationManager, atLeast(0)).isUserWatchingPageOrSpace(any(), any(), any());
        // Then verify no more so that things fail if we adapt the code to move away, rather than needing to explicitly
        // block the replacement methods.
        verifyNoMoreInteractions(notificationManager);
    }
}
