package com.atlassian.streams.confluence;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.thumbnail.DefaultThumbnailManager;
import com.atlassian.core.util.thumbnail.Thumbnail;

@Ignore
public class TestThumbnailManager extends DefaultThumbnailManager {
    private final Map<Attachment, Thumbnail> thumbnailMap;

    public TestThumbnailManager() {
        this.thumbnailMap = new HashMap<Attachment, Thumbnail>();
    }

    @Override
    public boolean isThumbnailable(final Attachment attachment) {
        return thumbnailMap.get(attachment) != null;
    }

    @Override
    public Thumbnail getThumbnail(final Attachment attachment) {
        return thumbnailMap.get(attachment);
    }
}
