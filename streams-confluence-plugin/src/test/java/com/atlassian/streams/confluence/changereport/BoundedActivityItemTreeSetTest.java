package com.atlassian.streams.confluence.changereport;

import java.time.ZonedDateTime;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.streams.api.DateUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.confluence.changereport.ActivityItemFactory.activityItemSorter;

@RunWith(MockitoJUnitRunner.class)
public class BoundedActivityItemTreeSetTest {
    @Mock
    private ActivityItem item1;

    @Mock
    private ActivityItem item2;

    @Mock
    private ActivityItem item3;

    @Mock
    private ActivityItem item4;

    @Mock
    private ActivityItem item5;

    private BoundedActivityItemTreeSet set;

    @Before
    public void setup() {
        when(item1.getModified()).thenReturn(DateUtil.toDate(ZonedDateTime.now().plusMinutes(1)));
        when(item2.getModified()).thenReturn(DateUtil.toDate(ZonedDateTime.now().plusMinutes(2)));
        when(item3.getModified()).thenReturn(DateUtil.toDate(ZonedDateTime.now().plusMinutes(3)));
        when(item4.getModified()).thenReturn(DateUtil.toDate(ZonedDateTime.now().plusMinutes(4)));

        set = new BoundedActivityItemTreeSet(3, activityItemSorter);
    }

    @Test
    public void testAddDuplicatePageRevisions() {
        set.addAll(ImmutableList.of(item2, item1));
        assertFalse(set.add(item1));
        assertSetEquals(set, item2, item1);
    }

    @Test
    public void testRemove() {
        assertTrue(set.addAll(ImmutableList.of(item4, item3, item1)));
        assertTrue(set.remove(item3));
        assertSetEquals(set, item4, item1);
        assertFalse(set.remove(item5));
        assertSetEquals(set, item4, item1);
    }

    @Test
    public void testRemoveAll() {
        assertTrue(set.addAll(ImmutableList.of(item3, item2, item1)));
        assertTrue(set.removeAll(ImmutableList.of(item1, item2)));
        assertSetEquals(set, item3);
        assertFalse(set.removeAll(ImmutableList.of(item4, item5)));
        assertSetEquals(set, item3);
    }

    @Test
    public void testClear() {
        assertTrue(set.addAll(ImmutableList.of(item3, item2, item1)));
        set.clear();
        assertSetEquals(set); // empty set
    }

    @Test
    public void testRetainAll() {
        assertTrue(set.addAll(ImmutableList.of(item3, item2, item1)));
        assertTrue(set.retainAll(ImmutableList.of(item1, item2)));
        assertSetEquals(set, item2, item1);
        assertFalse(set.retainAll(ImmutableList.of(item1, item2)));
        assertSetEquals(set, item2, item1);
        assertTrue(set.retainAll(ImmutableList.of(item5)));
        assertSetEquals(set); // empty set
    }

    @SafeVarargs
    private static <T> void assertSetEquals(Collection<T> set, T... expected) {
        assertEquals("Sets are not equal length", expected.length, set.size());
        int i = 0;
        for (T item : set) {
            assertEquals("Item " + i + " not equals", expected[i], item);
            i++;
        }
    }
}
