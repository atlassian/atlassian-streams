package com.atlassian.streams.confluence;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.ContributorQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.MultiTextFieldQuery;
import com.atlassian.confluence.user.persistence.dao.ConfluenceUserDao;
import com.atlassian.streams.api.DateUtil;

import static org.junit.Assert.assertEquals;

import static com.atlassian.confluence.search.service.ContentTypeEnum.ATTACHMENT;
import static com.atlassian.confluence.search.service.ContentTypeEnum.BLOG;
import static com.atlassian.confluence.search.service.ContentTypeEnum.COMMENT;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PAGE;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PERSONAL_SPACE_DESCRIPTION;
import static com.atlassian.confluence.search.service.ContentTypeEnum.SPACE_DESCRIPTION;
import static com.atlassian.confluence.search.v2.BooleanOperator.OR;
import static com.atlassian.confluence.search.v2.query.BooleanQuery.composeOrQuery;
import static com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRangeQueryType.MODIFIED;
import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.atlassian.streams.confluence.ConfluenceSearchQueryBuilder.CONTENT_TYPES;
import static com.atlassian.streams.confluence.ConfluenceSearchQueryBuilder.TEXT_FIELDS;

public class ConfluenceSearchQueryBuilderTest {
    private static final boolean INCLUDE_TO = true;
    private static final boolean INCLUDE_FROM = true;

    private static final ZonedDateTime OCT_10_2010_10_10_10 =
            ZonedDateTime.of(2010, 10, 10, 10, 10, 10, 0, ZoneId.systemDefault());

    private static final ZonedDateTime OCT_12_2010_05_00_00 =
            ZonedDateTime.of(2010, 10, 12, 5, 0, 0, 0, ZoneId.systemDefault());

    @Rule
    public final MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private ConfluenceUserDao userDao;

    @Test
    public final void testSingleCreator() {
        assertEquals(
                newQuery(new CreatorQuery("bob")),
                new ConfluenceSearchQueryBuilder(userDao).createdBy("bob").build());
    }

    @Test
    public final void testMultipleCreators() {
        assertEquals(
                newQuery(composeOrQuery(ImmutableSet.of(new CreatorQuery("bob"), new CreatorQuery("mary")))),
                new ConfluenceSearchQueryBuilder(userDao)
                        .createdBy("bob", "mary")
                        .build());
    }

    @Test
    public final void testNullOrBlankCreators() {
        assertEquals(
                newQuery(new CreatorQuery("bob")),
                new ConfluenceSearchQueryBuilder(userDao)
                        .createdBy("bob", null, " ")
                        .build());
    }

    @Test
    public final void testDuplicateUntrimmedCreator() {
        assertEquals(
                newQuery(new CreatorQuery("bob")),
                new ConfluenceSearchQueryBuilder(userDao)
                        .createdBy(" bob ", "bob")
                        .build());
    }

    @Test
    public final void testSingleLastModifier() {
        assertEquals(
                newQuery(new ContributorQuery("bob", userDao)),
                new ConfluenceSearchQueryBuilder(userDao).lastModifiedBy("bob").build());
    }

    @Test
    public final void testMultipleLastModifiers() {
        assertEquals(
                newQuery(composeOrQuery(
                        ImmutableSet.of(new ContributorQuery("bob", userDao), new ContributorQuery("mary", userDao)))),
                new ConfluenceSearchQueryBuilder(userDao)
                        .lastModifiedBy("bob", "mary")
                        .build());
    }

    @Test
    public final void testNullOrBlankLastModifiers() {
        assertEquals(
                newQuery(new ContributorQuery("bob", userDao)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .lastModifiedBy(null, " ", "bob")
                        .build());
    }

    @Test
    public final void testDuplicateUntrimmedLastModifier() {
        assertEquals(
                newQuery(new ContributorQuery("bob", userDao)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .lastModifiedBy(" bob ", "bob")
                        .build());
    }

    @Test
    public final void testSingleSpaceKey() {
        assertEquals(
                newQuery(new InSpaceQuery("JST")),
                new ConfluenceSearchQueryBuilder(userDao).inSpace("JST").build());
    }

    @Test
    public final void testMultipleSpaceKeys() {
        assertEquals(
                newQuery(new InSpaceQuery(ImmutableSet.of("JST", "PLUG"))),
                new ConfluenceSearchQueryBuilder(userDao).inSpace("JST", "PLUG").build());
    }

    @Test
    public final void testNullOrBlankSpaceKeys() {
        assertEquals(
                newQuery(new InSpaceQuery("JST")),
                new ConfluenceSearchQueryBuilder(userDao)
                        .inSpace("JST", null, " ")
                        .build());
    }

    @Test
    public final void testDuplicateUntrimmedSpaceKeys() {
        assertEquals(
                newQuery(new InSpaceQuery("JST")),
                new ConfluenceSearchQueryBuilder(userDao)
                        .inSpace(" JST ", "JST")
                        .build());
    }

    @Test
    public final void testSingleSearchTerm() {
        assertEquals(
                newQuery(new MultiTextFieldQuery("confluence", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .searchFor("confluence")
                        .build());
    }

    @Test
    public final void testMultipleSearchTerms() {
        assertEquals(
                newQuery(new MultiTextFieldQuery("jira confluence", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .searchFor("jira", "confluence")
                        .build());
    }

    @Test
    public final void testNullOrBlankSearchTerms() {
        assertEquals(
                newQuery(new MultiTextFieldQuery("jira", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .searchFor("jira", null, " ")
                        .build());
    }

    @Test
    public final void testDuplicateUntrimmedSearchTerm() {
        assertEquals(
                newQuery(new MultiTextFieldQuery("jira", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .searchFor(" jira ", "jira")
                        .build());
    }

    @Test
    public final void testSingleCreatorOrLastModifier() {
        assertEquals(
                newQuery(
                        composeOrQuery(ImmutableSet.of(new CreatorQuery("bob"), new ContributorQuery("bob", userDao)))),
                new ConfluenceSearchQueryBuilder(userDao)
                        .createdOrLastModifiedBy("bob")
                        .build());
    }

    @Test
    public final void testMultipleCreatorOrLastModifiers() {
        assertEquals(
                newQuery(composeOrQuery(ImmutableSet.of(
                        new CreatorQuery("bob"), new ContributorQuery("bob", userDao),
                        new CreatorQuery("mary"), new ContributorQuery("mary", userDao)))),
                new ConfluenceSearchQueryBuilder(userDao)
                        .createdOrLastModifiedBy("bob", "mary")
                        .build());
    }

    @Test
    public final void testThatSpecifyingOnlyMinDateCreatesDateRangeWithMaxDateNull() {
        assertEquals(
                newQuery(new DateRangeQuery(
                        DateUtil.toDate(OCT_10_2010_10_10_10), null, INCLUDE_FROM, INCLUDE_TO, MODIFIED)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .minDate(some(DateUtil.toDate(OCT_10_2010_10_10_10)))
                        .build());
    }

    @Test
    public final void testThatSpecifyingOnlyMaxDateCreatesDateRangeWithMinDateNull() {
        assertEquals(
                newQuery(new DateRangeQuery(
                        null, DateUtil.toDate(OCT_12_2010_05_00_00), INCLUDE_FROM, INCLUDE_TO, MODIFIED)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .maxDate(some(DateUtil.toDate(OCT_12_2010_05_00_00)))
                        .build());
    }

    @Test
    public final void testThatSpecifyingBothMinAndMaxDateCreatesDateRangeWithBothDate() {
        assertEquals(
                newQuery(new DateRangeQuery(
                        DateUtil.toDate(OCT_10_2010_10_10_10),
                        DateUtil.toDate(OCT_12_2010_05_00_00),
                        INCLUDE_FROM,
                        INCLUDE_TO,
                        MODIFIED)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .minDate(some(DateUtil.toDate(OCT_10_2010_10_10_10)))
                        .maxDate(some(DateUtil.toDate(OCT_12_2010_05_00_00)))
                        .build());
    }

    @Test
    public final void testThatExcludedSearchTermsAreAddedAsMustNots() {
        assertEquals(
                newQueryWithNot(new MultiTextFieldQuery("confluence", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .excludeTerms(ImmutableList.of("confluence"))
                        .build());
    }

    @Test
    public final void testThatArticleRequestSearchesForOnlyBlogContent() {
        assertEquals(
                newQuery(Collections.emptyList(), Collections.emptyList(), ImmutableList.of(BLOG)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(article()))
                        .build());
    }

    @Test
    public final void testThatPageRequestSearchesForOnlyPageContent() {
        assertEquals(
                newQuery(ImmutableList.of(), ImmutableList.of(), ImmutableList.of(PAGE)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(page()))
                        .build());
    }

    @Test
    public final void testThatAttachmentRequestSearchesForOnlyAttachments() {
        assertEquals(
                newQuery(ImmutableList.of(), ImmutableList.of(), ImmutableList.of(ATTACHMENT)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(file()))
                        .build());
    }

    @Test
    public final void testThatCommentRequestSearchesForOnlyComments() {
        assertEquals(
                newQuery(ImmutableList.of(), ImmutableList.of(), ImmutableList.of(COMMENT)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(comment()))
                        .build());
    }

    @Test
    public final void testThatPersonalSpaceRequestSearchesForOnlyPersonalSpaceDescriptions() {
        assertEquals(
                newQuery(ImmutableList.of(), ImmutableList.of(), ImmutableList.of(PERSONAL_SPACE_DESCRIPTION)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(personalSpace()))
                        .build());
    }

    @Test
    public final void testThatSpaceRequestSearchesForOnlySpaceDescriptions() {
        assertEquals(
                newQuery(
                        ImmutableList.of(),
                        ImmutableList.of(),
                        ImmutableList.of(SPACE_DESCRIPTION, PERSONAL_SPACE_DESCRIPTION)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(space()))
                        .build());
    }

    @Test
    public final void testThatRequestForMultipleActivityObjectsSearchesForMultipleContentTypes() {
        assertEquals(
                newQuery(ImmutableList.of(), ImmutableList.of(), ImmutableList.of(PAGE, COMMENT)),
                new ConfluenceSearchQueryBuilder(userDao)
                        .activityObjects(ImmutableList.of(page(), comment()))
                        .build());
    }

    @Test
    public final void testCombinationOfAllQueriesInMultitudeWithNullBlankAndUntrimmedDuplicates() {
        Set<SearchQuery> userSet = ImmutableSet.of(
                new CreatorQuery("bob"),
                new ContributorQuery("bob", userDao),
                new CreatorQuery("mary"),
                new ContributorQuery("mary", userDao));
        List<SearchQuery> rootList = ImmutableList.of(
                new InSpaceQuery(ImmutableSet.of("JST", "PLUG")),
                composeOrQuery(userSet),
                new MultiTextFieldQuery("jira confluence", TEXT_FIELDS, OR));

        assertEquals(
                newQuery(rootList),
                new ConfluenceSearchQueryBuilder(userDao)
                        .createdOrLastModifiedBy("bob", null, " ", "mary", " bob ")
                        .inSpace("JST", null, " ", "PLUG", " JST ")
                        .searchFor("jira", null, " ", "confluence", " jira ")
                        .build());
    }

    private SearchQuery newQuery(SearchQuery query) {
        return newQuery(ImmutableList.of(query));
    }

    private SearchQuery newQuery(List<SearchQuery> subQueries) {
        return newQuery(subQueries, ImmutableList.of(), CONTENT_TYPES);
    }

    private SearchQuery newQueryWithNot(SearchQuery query) {
        return newQuery(ImmutableList.of(), ImmutableList.of(query), CONTENT_TYPES);
    }

    private SearchQuery newQuery(
            Collection<SearchQuery> andTerms,
            Collection<SearchQuery> notTerms,
            Collection<ContentTypeEnum> contentTypes) {
        List<SearchQuery> mustClause = new ArrayList<>(andTerms);
        mustClause.add(new ContentTypeQuery(contentTypes));
        return new BooleanQuery(mustClause, Collections.emptyList(), notTerms);
    }
}
