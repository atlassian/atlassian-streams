package com.atlassian.streams.thirdparty.api;

import java.net.URI;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;

/**
 * Describes an image that can be associated with an {@link Activity}.
 * <p>
 * To construct an instance of this class, use {@link #builder(URI)} or {@link #withUrl(URI)}.
 */
public class Image {
    private final Option<Integer> height;
    private final URI url;
    private final Option<Integer> width;

    private Image(Builder builder) {
        this.height = builder.height;
        this.url = builder.url;
        this.width = builder.width;
    }

    /**
     * Constructs an {@link Image} with only an image URI, and no dimensions specified.  Use this
     * constructor only if the URL is known to be valid; it will throw an {@link IllegalArgumentException}
     * otherwise.
     * @param url  a {@link URI}; cannot be null
     * @return  an {@link Image}
     */
    public static Image withUrl(URI url) {
        Either<ValidationErrors, Image> ret = builder(url).build();
        if (ret.isLeft()) {
            throw new IllegalArgumentException(ret.left().get().toString());
        }
        return ret.right().get();
    }

    /**
     * Returns a {@link Builder} instance that allows {@link Image} properties to be configured.
     * @param url  a {@link URI}; cannot be null
     * @return  a {@link Builder}
     */
    public static Builder builder(URI url) {
        return new Builder(url);
    }

    /**
     * The optional height of the image, in pixels.
     * @return  an {@link Option} containing an Integer, or {@link Option#none()} if not specified
     */
    public Option<Integer> getHeight() {
        return height;
    }

    /**
     * The image URI.
     * @return  a {@link URI}; cannot be null
     */
    public URI getUrl() {
        return url;
    }

    /**
     * The optional width of the image, in pixels.
     * @return  an {@link Option} containing an Integer, or {@link Option#none()} if not specified
     */
    public Option<Integer> getWidth() {
        return width;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Image) {
            Image i = (Image) other;
            return url.equals(i.url) && height.equals(i.height) && width.equals(i.width);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ((height.hashCode() * 37) + url.hashCode()) * 37 + width.hashCode();
    }

    /**
     * Stateful builder class for constructing {@link Image} instances.
     */
    public static final class Builder {
        private final ValidationErrors.Builder errors = new ValidationErrors.Builder();
        private URI url;
        private Option<Integer> height = none();
        private Option<Integer> width = none();

        public Builder(URI url) {
            errors.checkAbsoluteUri(some(url), "url");
            this.url = url;
        }

        public Builder(String urlString) {
            url = errors.checkAbsoluteUriString(some(urlString), "url").getOrElse((URI) null);
        }

        /**
         * Attempts to create an immutable {@link Image} instance using the current properties
         * of this {@link Builder}.
         * @return  an {@link Either} containing an {@link Image} ({@link Either#right()})
         *   if successful, or {@link ValidationErrors} ({@link Either#left()}) if any of the
         *   builder's properties were invalid
         */
        public Either<ValidationErrors, Image> build() {
            if (errors.isEmpty()) {
                return right(new Image(this));
            } else {
                return left(errors.build());
            }
        }

        /**
         * Sets the optional height of the image, in pixels.
         * @param height  an {@link Option} containing an Integer, or {@link Option#none()} if not specified
         * @return  the same Builder instance
         */
        public Builder height(Option<Integer> height) {
            this.height = checkNotNull(height, "height");
            return this;
        }

        /**
         * Sets the optional height of the image, in pixels.
         * @param width  an {@link Option} containing an Integer, or {@link Option#none()} if not specified
         * @return  the same Builder instance
         */
        public Builder width(Option<Integer> width) {
            this.width = checkNotNull(width, "width");
            return this;
        }
    }
}
