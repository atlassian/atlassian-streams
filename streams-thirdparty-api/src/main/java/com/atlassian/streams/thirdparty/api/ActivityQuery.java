package com.atlassian.streams.thirdparty.api;

import java.util.Date;

import com.google.common.collect.ImmutableList;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Pair.pair;

/**
 * Query criteria for use with {@link ActivityService#activities(ActivityQuery)}.
 * Use {@link #builder()} to obtain a builder for constructing instances of this class,
 * or {@link #all()} if no criteria need to be specified.
 */
public final class ActivityQuery {
    public static final int DEFAULT_START_INDEX = 0;
    public static final int DEFAULT_MAX_RESULTS = 10;

    private Option<Date> startDate;
    private Option<Date> endDate;
    private Iterable<String> userNames;
    private Iterable<String> excludeUserNames;
    private Iterable<Pair<String, String>> entityFilters;
    private Iterable<Pair<String, String>> excludeEntityFilters;
    private Iterable<String> providerKeys;
    private Iterable<String> excludeProviderKeys;
    private int startIndex;
    private int maxResults;

    /**
     * Returns a {@link ActivityQuery} that matches all activities (no criteria).
     */
    public static ActivityQuery all() {
        return new ActivityQuery();
    }

    /**
     * Returns a {@link Builder} for constructing an instance with query criteria.
     */
    public static Builder builder() {
        return new Builder();
    }

    private ActivityQuery() {
        startDate = none();
        endDate = none();
        userNames = excludeUserNames = ImmutableList.of();
        entityFilters = excludeEntityFilters = ImmutableList.of();
        providerKeys = excludeProviderKeys = ImmutableList.of();
        startIndex = DEFAULT_START_INDEX;
        maxResults = DEFAULT_MAX_RESULTS;
    }

    private ActivityQuery(Builder builder) {
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
        this.userNames = ImmutableList.copyOf(builder.userNames);
        this.excludeUserNames = ImmutableList.copyOf(builder.excludeUserNames);
        this.entityFilters = builder.entityFilters.build();
        this.excludeEntityFilters = builder.excludeEntityFilters.build();
        this.providerKeys = ImmutableList.copyOf(builder.providerKeys);
        this.excludeProviderKeys = ImmutableList.copyOf(builder.excludeProviderKeys);
        this.startIndex = builder.startIndex;
        this.maxResults = builder.maxResults;
    }

    /**
     * Returns the start date query parameter, or {@code none()} if none exists.
     * @return the start date query parameter, or {@code none()} if none exists.
     */
    public Option<Date> getStartDate() {
        return startDate;
    }

    /**
     * Returns the end date query parameter, or {@code none()} if none exists.
     * @return the end date query parameter, or {@code none()} if none exists.
     */
    public Option<Date> getEndDate() {
        return endDate;
    }

    /**
     * Returns a list of allowable usernames, or an empty list if none were specified.
     * @return an {@link Iterable} of usernames to match
     */
    public Iterable<String> getUserNames() {
        return userNames;
    }

    /**
     * Returns a list of disallowed usernames, or an empty list if none were specified.
     * @return an {@link Iterable} of usernames to exclude
     */
    public Iterable<String> getExcludeUserNames() {
        return excludeUserNames;
    }

    /**
     * Returns a list of key-value pairs to search for, corresponding to entities such as
     * project or issues, or an empty list if none were specified.  The {@link Pair#first()}
     * property of each pair is a filter key such as {@link com.atlassian.streams.spi.StandardStreamsFilterOption#PROJECT_KEY};
     * the {@link Pair#second()} property is the corresponding project/issue key.
     * @return an {@link Iterable} of key-value pairs to match
     */
    public Iterable<Pair<String, String>> getEntityFilters() {
        return entityFilters;
    }

    /**
     * Returns a list of key-value pairs to exclude, corresponding to entities such as
     * project or issues, or an empty list if none were specified.  The {@link Pair#first()}
     * property of each pair is a filter key such as {@link com.atlassian.streams.spi.StandardStreamsFilterOption#PROJECT_KEY};
     * the {@link Pair#second()} property is the corresponding project/issue key.
     * @return an {@link Iterable} of key-value pairs to exclude
     */
    public Iterable<Pair<String, String>> getExcludeEntityFilters() {
        return excludeEntityFilters;
    }

    public Iterable<String> getProviderKeys() {
        return providerKeys;
    }

    public Iterable<String> getExcludeProviderKeys() {
        return excludeProviderKeys;
    }

    /**
     * Returns the starting index for pagination.
     * @return the starting index for pagination.
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * Returns the maximum number of results that should be returned per query.
     * @return the maximum number of results that should be returned per query.
     */
    public int getMaxResults() {
        return maxResults;
    }

    public static final class Builder {
        private Option<Date> startDate = none();
        private Option<Date> endDate = none();
        private Iterable<String> userNames = ImmutableList.of();
        private Iterable<String> excludeUserNames = ImmutableList.of();
        private ImmutableList.Builder<Pair<String, String>> entityFilters = ImmutableList.builder();
        private ImmutableList.Builder<Pair<String, String>> excludeEntityFilters = ImmutableList.builder();
        private Iterable<String> providerKeys = ImmutableList.of();
        private Iterable<String> excludeProviderKeys = ImmutableList.of();
        private int startIndex = DEFAULT_START_INDEX;
        private int maxResults = DEFAULT_MAX_RESULTS;

        /**
         * Sets the optional starting date. If specified, the query will exclude any
         * activities whose published date is before this date.
         * @param startDate  an {@link Option} containing the starting date, or {@link Option#none()}
         * @return  the same Builder instance
         */
        public Builder startDate(Option<Date> startDate) {
            this.startDate = startDate;
            return this;
        }

        /**
         * Sets the optional ending date. If specified, the query will exclude any
         * activities whose published date is on or after this date.
         * @param endDate  an {@link Option} containing the ending date, or {@link Option#none()}
         * @return  the same Builder instance
         */
        public Builder endDate(Option<Date> endDate) {
            this.endDate = endDate;
            return this;
        }

        /**
         * Sets the allowable usernames. If this list is not empty, the query will exclude
         * any activities whose usernames are not matched.
         * @param userNames an {@link Iterable} of usernames to match
         * @return  the same Builder instance
         */
        public Builder userNames(Iterable<String> userNames) {
            this.userNames = userNames;
            return this;
        }

        /**
         * Sets the disallowed usernames. If this list is not empty, the query will exclude
         * any activities whose usernames are matched.
         * @param excludeUserNames an {@link Iterable} of usernames to exclude
         * @return  the same Builder instance
         */
        public Builder excludeUserNames(Iterable<String> excludeUserNames) {
            this.excludeUserNames = excludeUserNames;
            return this;
        }

        /**
         * Specifies an associated entity to search for, such as a project or issue. If at
         * least one such filter is specified, the query will exclude any activities that are
         * not associated with at least one of the specified entities.
         * @param filterKey  a filter key such as {@link com.atlassian.streams.spi.StandardStreamsFilterOption#PROJECT_KEY}
         * @param value  the corresponding project or issue key
         * @return  the same Builder instance
         */
        public Builder addEntityFilter(String filterKey, String value) {
            entityFilters.add(pair(filterKey, value));
            return this;
        }

        /**
         * Specifies an associated entity to exclude, such as a project or issue. The query
         * will exclude any activities that are associated with any of the specified entities.
         * @param filterKey  a filter key such as {@link com.atlassian.streams.spi.StandardStreamsFilterOption#PROJECT_KEY}
         * @param value  the corresponding project or issue key
         * @return  the same Builder instance
         */
        public Builder addExcludeEntityFilter(String filterKey, String value) {
            excludeEntityFilters.add(pair(filterKey, value));
            return this;
        }

        /**
         * Sets the allowable provider (generator) keys. The format of each key is
         * "generatorId@generatorDisplayName".
         * @param providerKeys an {@link Iterable} of generator keys to match
         * @return  the same Builder instance
         */
        public Builder providerKeys(Iterable<String> providerKeys) {
            this.providerKeys = providerKeys;
            return this;
        }

        /**
         * Sets the disallowed provider keys. The format of each key is
         * "generatorId@generatorDisplayName".
         * @param excludeProviderKeys an {@link Iterable} of generator keys to exclude
         * @return  the same Builder instance
         */
        public Builder excludeProviderKeys(Iterable<String> excludeProviderKeys) {
            this.excludeProviderKeys = excludeProviderKeys;
            return this;
        }

        /**
         * Sets the starting index. If specified, the query will skip {@code startIndex} number of results
         * before building the results set. If unspecified, defaults to {@code DEFAULT_START_INDEX}.
         * @param startIndex the starting index
         * @return  the same Builder instance
         */
        public Builder startIndex(int startIndex) {
            this.startIndex = startIndex;
            return this;
        }

        /**
         * Sets the max results value. If specified, the query limits the result set to {@code maxResults} values.
         * If unspecified, defaults to {@code DEFAULT_MAX_RESULTS}.
         * @param maxResults the maximum number of results
         * @return  the same Builder instance
         */
        public Builder maxResults(int maxResults) {
            this.maxResults = maxResults;
            return this;
        }

        /**
         * Returns a {@link ActivityQuery} instance using the current properties of this Builder.
         */
        public ActivityQuery build() {
            return new ActivityQuery(this);
        }
    }
}
