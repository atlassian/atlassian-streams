package com.atlassian.streams.thirdparty.api;

import java.net.URI;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.MAX_STRING_LENGTH;

/**
 * Represents additional machine-readable information about an {@link Activity}.  An Activity
 * can have up to two ActivityObjects: one in the {@link Activity#getObject() object} property,
 * which describes the activity itself, and one in {@link Activity#getTarget() target} property,
 * which describes the context of the activity or the entity that was acted on.  These correspond
 * to the {@code object} and {@code target} properties in the {@code activitystrea.ms} specification,
 * although only a subset of properties from the specification are supported.  All properties are
 * optional.
 * <p>
 * The output RSS feed includes all of these properties, but most are not displayed by the
 * Activity Streams front end.  However, the {@code target} object can be used to associate an
 * activity with a host application entity such as a JIRA issue; see {@link Activity.Builder2#target(ActivityObject)}.
 * <p>
 * To construct a new instance of this class, create a {@link Builder} {@link #builder},
 * configure the builder and call {@link Builder#build()}.
 */
public class ActivityObject {
    private final Option<String> displayName;
    private final Option<URI> id;
    private final Option<URI> type;
    private final Option<Html> summary;
    private final Option<URI> url;

    /**
     * Returns a {@link Builder} for constructing an {@link ActivityObject} instance.
     * @return  a {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    private ActivityObject(Builder builder) {
        this.displayName = builder.displayName;
        this.id = builder.id;
        this.type = builder.type;
        this.summary = builder.summary;
        this.url = builder.url;
    }

    /**
     * An optional human-readable name for the object.  This is plain text; any HTML tags will
     * be stripped.
     * @return  an {@link Option} containing a string, or {@link Option#none} if none was specified
     */
    public Option<String> getDisplayName() {
        return displayName;
    }

    /**
     * An optional URI that uniquely identifies the object.
     * @return  an {@link Option} containing a URI, or {@link Option#none} if none was specified
     */
    public Option<URI> getId() {
        return id;
    }

    /**
     * An optional summary description of the object, which may contain HTML.
     * @return  an {@link Option} containing an HTML string, or {@link Option#none} if none was specified
     */
    public Option<Html> getSummary() {
        return summary;
    }

    /**
     * An optional URI that describes the object type.  This should be either a simple name,
     * or an IRI as described in the {@code activitystrea.ms} specification.
     * @return  an {@link Option} containing a URI, or {@link Option#none} if none was specified
     */
    public Option<URI> getType() {
        return type;
    }

    /**
     * An optional URI for the object's human-readable content on the Web.
     * @return  an {@link Option} containing a URI, or {@link Option#none} if none was specified
     */
    public Option<URI> getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ActivityObject) {
            ActivityObject o = (ActivityObject) other;
            return displayName.equals(o.displayName)
                    && id.equals(o.id)
                    && summary.equals(o.summary)
                    && type.equals(o.type)
                    && url.equals(o.url);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return displayName.hashCode()
                + 37 * (id.hashCode() + 37 * (summary.hashCode() + 37 * (type.hashCode() + 37 * (url.hashCode()))));
    }

    /**
     * Stateful builder class for constructing {@link ActivityObject} instances.
     * <p>
     * The Builder will not allow you to construct an {@link ActivityObject} with invalid properties
     * that cannot be accepted by the {@link ActivityService}.  Value constraints are described
     * in {@link ValidationErrors}.  If you pass any invalid property values, the setter methods
     * will not throw exceptions, but the {@link #build()} method will return {@link ValidationErrors}.
     * However, passing {@code null} to any setter method will throw a {@link NullPointerException};
     * nulls are never allowed, all optional properties use {@link Option} instead).
     */
    public static final class Builder {
        private final ValidationErrors.Builder errors = new ValidationErrors.Builder();
        private Option<String> displayName = none();
        private Option<URI> id = none();
        private Option<URI> type = none();
        private Option<Html> summary = none();
        private Option<URI> url = none();

        /**
         * Attempts to create an immutable {@link ActivityObject} instance using the current properties
         * of this {@link Builder}.
         * @return  an {@link Either} containing an {@link ActivityObject} ({@link Either#right()})
         *   if successful, or {@link ValidationErrors} ({@link Either#left()}) if any of the
         *   builder's properties were invalid
         */
        public Either<ValidationErrors, ActivityObject> build() {
            if (errors.isEmpty()) {
                return right(new ActivityObject(this));
            } else {
                return left(errors.build());
            }
        }

        /**
         * Sets an optional human-readable name for the object.  This is plain text; any HTML tags will
         * be stripped.
         * @param displayName an {@link Option} containing a string, or {@link Option#none} if none
         *   is specified
         * @return  the same Builder instance
         */
        public Builder displayName(Option<String> displayName) {
            this.displayName = errors.checkString(displayName, "displayName");
            return this;
        }

        /**
         * Sets an optional URI that uniquely identifies the object.  If specified, this must be an
         * absolute URI.
         * @param id  an {@link Option} containing a URI, or {@link Option#none} if none is specified
         * @return  the same Builder instance
         */
        public Builder id(Option<URI> id) {
            this.id = errors.checkAbsoluteUri(id, "id");
            return this;
        }

        /**
         * Same as {@link #id}, but specifies the property as a String rather than a URI.  This
         * allows URI syntax errors to be reported by the {@link ValidationErrors} mechanism rather
         * than having to catch URISyntaxExceptions.
         * @param id  an {@link Option} containing the object ID as a string, or {@link Option#none()}
         *   if none is specified
         * @return  the same Builder instance
         */
        public Builder idString(Option<String> id) {
            this.id = errors.checkAbsoluteUriString(id, "id");
            return this;
        }

        /**
         * Sets an optional summary description of the object, which may contain HTML.
         * @param summary  an {@link Option} containing an HTML string, or {@link Option#none} if none
         *   is specified
         * @return  the same Builder instance
         */
        public Builder summary(Option<Html> summary) {
            this.summary = errors.checkHtml(summary, "summary", MAX_STRING_LENGTH);
            return this;
        }

        /**
         * Sets an optional URI that describes the object type.  This should be either a simple name,
         * or an IRI as described in the {@code activitystrea.ms} specification.
         * @param type  an {@link Option} containing a URI, or {@link Option#none} if none
         *   is specified
         * @return  the same Builder instance
         */
        public Builder type(Option<URI> type) {
            this.type = errors.checkSimpleNameOrAbsoluteUri(type, "type");
            return this;
        }

        /**
         * Same as {@link #type}, but specifies the property as a String rather than a URI.  This
         * allows URI syntax errors to be reported by the {@link ValidationErrors} mechanism rather
         * than having to catch URISyntaxExceptions.
         * @param type  an {@link Option} containing the object type as a string, or {@link Option#none()}
         *   if none is specified
         * @return  the same Builder instance
         */
        public Builder typeString(Option<String> type) {
            this.type = errors.checkSimpleNameOrAbsoluteUriString(type, "type");
            return this;
        }

        /**
         * Sets an optional URI for the object's human-readable content on the Web.  Rather than an
         * absolute URI, you may specify an identifier such as a JIRA issue key, which will be changed
         * into the corresponding URL; see {@link Activity.Builder2#target(ActivityObject)}.
         * @param url  an {@link Option} containing a URI, or {@link Option#none} if none is specified
         * @return  the same Builder instance
         */
        public Builder url(Option<URI> url) {
            this.url = errors.checkSimpleNameOrAbsoluteUri(url, "url");
            return this;
        }

        /**
         * Same as {@link #url}, but specifies the property as a String rather than a URI.  This
         * allows URI syntax errors to be reported by the {@link ValidationErrors} mechanism rather
         * than having to catch URISyntaxExceptions.
         * @param url  an {@link Option} containing the object's URI as a string, or {@link Option#none()}
         *   if none is specified
         * @return  the same Builder instance
         */
        public Builder urlString(Option<String> url) {
            this.url = errors.checkSimpleNameOrAbsoluteUriString(url, "url");
            return this;
        }
    }
}
