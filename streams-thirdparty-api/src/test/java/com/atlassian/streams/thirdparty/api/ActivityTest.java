package com.atlassian.streams.thirdparty.api;

import java.net.URI;
import java.time.ZonedDateTime;

import org.hamcrest.Matchers;
import org.junit.Test;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Either;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.api.Application.application;
import static com.atlassian.streams.thirdparty.api.TestData.RELATIVE_URI;
import static com.atlassian.streams.thirdparty.api.TestData.SIMPLE_NAME;
import static com.atlassian.streams.thirdparty.api.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.assertValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.makeAbsoluteUri;
import static com.atlassian.streams.thirdparty.api.TestData.makeString;
import static com.atlassian.streams.thirdparty.api.TestData.makeUri;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.MAX_CONTENT_LENGTH;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.MAX_STRING_LENGTH;

public class ActivityTest {
    private static final URI DEFAULT_ID = URI.create("http://id");
    private static final Application APPLICATION = application("appname", URI.create("http://appid"));
    private static final UserProfile USER = new UserProfile.Builder("user").build();

    public void idAndUrlCannotBothBeOmitted() {
        assertValidationError(
                Activity.builder2(APPLICATION, ZonedDateTime.now(), USER).build());
    }

    @Test(expected = NullPointerException.class)
    public void applicationCannotBeNull() {
        Activity.builder2(null, ZonedDateTime.now(), "user");
    }

    @Test
    public void contentDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getContentOrNull(), equalTo(null));
    }

    @Test(expected = NullPointerException.class)
    public void contentCannotBeNull() {
        builder().content(null);
    }

    @Test
    public void contentOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().content(html(makeString(MAX_CONTENT_LENGTH))).build());
    }

    @Test
    public void contentOverMaxLengthReturnsError() {
        assertValidationError(
                builder().content(html(makeString(MAX_CONTENT_LENGTH + 1))).build());
    }

    @Test
    public void iconDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getIcon(), equalTo(none(Image.class)));
    }

    @Test(expected = NullPointerException.class)
    public void iconCannotBeNull() {
        builder().icon((Image) null);
    }

    @Test
    public void idDefaultsToNone() {
        assertThat(
                assertNotValidationError(Activity.builder2(APPLICATION, ZonedDateTime.now(), USER)
                                .url(DEFAULT_ID)
                                .build())
                        .getIdOrNull(),
                equalTo(null));
    }

    @Test(expected = NullPointerException.class)
    public void idCannotBeNull() {
        builder().id(null);
    }

    @Test
    public void idWithMaliciousSchemeReturnsError() {
        final Either<ValidationErrors, Activity> result =
                builder().idString("javascript:alert('you_have_been_hacked')").build();
        assertValidationError(result);
        // Check the error message to ensure the test is not passing for spurious reasons
        assertThat(
                result.left().get().getMessages(),
                Matchers.hasItem(Matchers.containsString("must start with a valid scheme")));
    }

    @Test
    public void idOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().id(makeAbsoluteUri(MAX_STRING_LENGTH)).build());
    }

    @Test
    public void idOverMaxLengthReturnsError() {
        assertValidationError(
                builder().id(makeAbsoluteUri(MAX_STRING_LENGTH + 1)).build());
    }

    @Test
    public void idNotAbsoluteReturnsError() {
        assertValidationError(builder().id(RELATIVE_URI).build());
    }

    @Test
    public void objectDefaultsToNone() {
        assertThat(builder().build().right().get().getObject(), equalTo(none(ActivityObject.class)));
    }

    @Test(expected = NullPointerException.class)
    public void objectCannotBeNull() {
        builder().object((ActivityObject) null);
    }

    @Test(expected = NullPointerException.class)
    public void postedDateCannotBeNull() {
        Activity.builder2(APPLICATION, null, USER);
    }

    @Test
    public void targetDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getTarget(), equalTo(none(ActivityObject.class)));
    }

    @Test(expected = NullPointerException.class)
    public void targetCannotBeNull() {
        builder().target((ActivityObject) null);
    }

    @Test
    public void titleDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getTitle(), equalTo(none(Html.class)));
    }

    @Test(expected = NullPointerException.class)
    public void titleCannotBeNull() {
        builder().title(null);
    }

    @Test
    public void titleOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().title(html(makeString(MAX_STRING_LENGTH))).build());
    }

    @Test
    public void titleOverMaxLengthReturnsError() {
        assertValidationError(
                builder().title(html(makeString(MAX_STRING_LENGTH + 1))).build());
    }

    @Test
    public void urlDefaultsToNone() {
        assertThat(
                assertNotValidationError(Activity.builder2(APPLICATION, ZonedDateTime.now(), USER)
                                .id(DEFAULT_ID)
                                .build())
                        .getUrlOrNull(),
                equalTo(null));
    }

    @Test(expected = NullPointerException.class)
    public void urlCannotBeNull() {
        builder().url(null);
    }

    @Test
    public void urlOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().url(makeAbsoluteUri(MAX_STRING_LENGTH)).build());
    }

    @Test
    public void urlOverMaxLengthReturnsError() {
        assertValidationError(
                builder().url(makeAbsoluteUri(MAX_STRING_LENGTH + 1)).build());
    }

    @Test
    public void urlNotAbsoluteReturnsError() {
        assertValidationError(builder().url(RELATIVE_URI).build());
    }

    @Test(expected = NullPointerException.class)
    public void userCannotBeNull() {
        Activity.builder2(APPLICATION, ZonedDateTime.now(), (UserProfile) null);
    }

    @Test(expected = NullPointerException.class)
    public void usernameInConstructorCannotBeNull() {
        Activity.builder2(APPLICATION, ZonedDateTime.now(), (String) null);
    }

    @Test
    public void usernameInConstructorOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(Activity.builder2(APPLICATION, ZonedDateTime.now(), makeString(MAX_STRING_LENGTH))
                .id(DEFAULT_ID)
                .build());
    }

    @Test
    public void usernameInConstructorOverMaxLengthReturnsError() {
        assertValidationError(Activity.builder2(APPLICATION, ZonedDateTime.now(), makeString(MAX_STRING_LENGTH + 1))
                .build());
    }

    @Test
    public void userFullNameOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(Activity.builder2(
                        APPLICATION,
                        ZonedDateTime.now(),
                        new UserProfile.Builder("name")
                                .fullName(makeString(MAX_STRING_LENGTH))
                                .build())
                .id(DEFAULT_ID)
                .build());
    }

    @Test
    public void userFullNameOverMaxLengthReturnsError() {
        assertValidationError(Activity.builder2(
                        APPLICATION,
                        ZonedDateTime.now(),
                        new UserProfile.Builder("name")
                                .fullName(makeString(MAX_STRING_LENGTH + 1))
                                .build())
                .build());
    }

    @Test
    public void userPictureUrlOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(Activity.builder2(
                        APPLICATION,
                        ZonedDateTime.now(),
                        new UserProfile.Builder("name")
                                .profilePictureUri(some(makeAbsoluteUri(MAX_STRING_LENGTH)))
                                .build())
                .id(DEFAULT_ID)
                .build());
    }

    @Test
    public void userPictureUrlOverMaxLengthReturnsError() {
        assertValidationError(Activity.builder2(
                        APPLICATION,
                        ZonedDateTime.now(),
                        new UserProfile.Builder("name")
                                .profilePictureUri(some(makeAbsoluteUri(MAX_STRING_LENGTH + 1)))
                                .build())
                .build());
    }

    @Test
    public void verbDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getVerb(), equalTo(none(URI.class)));
    }

    @Test(expected = NullPointerException.class)
    public void verbCannotBeNull() {
        builder().verb(null);
    }

    @Test
    public void verbOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(builder().verb(makeUri(MAX_STRING_LENGTH)).build());
    }

    @Test
    public void verbOverMaxLengthReturnsError() {
        assertValidationError(builder().verb(makeUri(MAX_STRING_LENGTH + 1)).build());
    }

    @Test
    public void verbSimpleNameDoesNotReturnError() {
        assertNotValidationError(builder().verb(SIMPLE_NAME).build());
    }

    @Test
    public void verbNotAbsoluteButNotSimpleNameDoesNotReturnError() {
        assertValidationError(builder().verb(RELATIVE_URI).build());
    }

    @Test
    public void verbStringSimpleNameDoesNotReturnError() {
        assertNotValidationError(
                builder().verbString(SIMPLE_NAME.toASCIIString()).build());
    }

    private Activity.Builder2 builder() {
        return Activity.builder2(APPLICATION, ZonedDateTime.now(), USER).id(DEFAULT_ID);
    }
}
