package com.atlassian.streams.thirdparty.api;

import java.net.URI;

import org.junit.Test;

import com.atlassian.streams.api.common.Option;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.thirdparty.api.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.assertValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.makeAbsoluteUri;
import static com.atlassian.streams.thirdparty.api.TestData.makeUri;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.MAX_STRING_LENGTH;

public class ImageTest {
    private static final URI DEFAULT_URI = URI.create("http://image");

    @Test(expected = NullPointerException.class)
    public void urlNullInFactoryMethodThrowsException() {
        Image.withUrl(null);
    }

    @Test(expected = NullPointerException.class)
    public void urlNullThrowsException() {
        assertValidationError(Image.builder(null).build());
    }

    @Test
    public void urlOfMaxLengthDoesNotThrowException() {
        assertNotValidationError(
                Image.builder(makeAbsoluteUri(MAX_STRING_LENGTH)).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void urlOverMaxLengthInFactoryMethodThrowsException() {
        Image.withUrl(makeAbsoluteUri(MAX_STRING_LENGTH + 1));
    }

    @Test
    public void urlOverMaxLengthReturnsValidationError() {
        assertValidationError(
                Image.builder(makeAbsoluteUri(MAX_STRING_LENGTH + 1)).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void urlNotAbsoluteInFactoryMethodThrowsException() {
        Image.withUrl(makeUri(MAX_STRING_LENGTH));
    }

    @Test
    public void urlNotAbsoluteReturnsValidationError() {
        assertValidationError(Image.builder(makeUri(MAX_STRING_LENGTH)).build());
    }

    @Test
    public void heightDefaultsToNone() {
        assertThat(Image.builder(DEFAULT_URI).build().right().get().getHeight(), equalTo(none(Integer.class)));
    }

    @Test(expected = NullPointerException.class)
    public void heightCannotBeNull() {
        Image.builder(DEFAULT_URI).height((Option<Integer>) null);
    }

    @Test
    public void widthDefaultsToNone() {
        assertThat(Image.builder(DEFAULT_URI).build().right().get().getWidth(), equalTo(none(Integer.class)));
    }

    @Test(expected = NullPointerException.class)
    public void widthCannotBeNull() {
        Image.builder(DEFAULT_URI).width((Option<Integer>) null);
    }
}
