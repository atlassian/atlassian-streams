package com.atlassian.streams.thirdparty.api;

import java.net.URI;

import org.junit.Test;

import com.atlassian.streams.api.Html;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.api.ActivityObject.builder;
import static com.atlassian.streams.thirdparty.api.TestData.INVALID_URI_STRING;
import static com.atlassian.streams.thirdparty.api.TestData.RELATIVE_URI;
import static com.atlassian.streams.thirdparty.api.TestData.SIMPLE_NAME;
import static com.atlassian.streams.thirdparty.api.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.assertValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.makeAbsoluteUri;
import static com.atlassian.streams.thirdparty.api.TestData.makeString;
import static com.atlassian.streams.thirdparty.api.TestData.makeUri;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.MAX_STRING_LENGTH;

public class ActivityObjectTest {
    @Test
    public void displayNameDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getDisplayName(), equalTo(none(String.class)));
    }

    @Test(expected = NullPointerException.class)
    public void displayNameCannotBeNull() {
        builder().displayName(null);
    }

    @Test
    public void displayNameOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().displayName(some(makeString(MAX_STRING_LENGTH))).build());
    }

    @Test
    public void displayNameOverMaxLengthReturnsError() {
        assertValidationError(
                builder().displayName(some(makeString(MAX_STRING_LENGTH + 1))).build());
    }

    @Test
    public void idDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getId(), equalTo(none(URI.class)));
    }

    @Test(expected = NullPointerException.class)
    public void idCannotBeNull() {
        builder().id(null);
    }

    @Test
    public void idOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().id(some(makeAbsoluteUri(MAX_STRING_LENGTH))).build());
    }

    @Test
    public void idOverMaxLengthReturnsError() {
        assertValidationError(
                builder().id(some(makeAbsoluteUri(MAX_STRING_LENGTH + 1))).build());
    }

    @Test
    public void idNotAbsoluteReturnsError() {
        assertValidationError(builder().id(some(RELATIVE_URI)).build());
    }

    @Test
    public void idStringNotAbsoluteReturnsError() {
        assertValidationError(
                builder().idString(some(RELATIVE_URI.toASCIIString())).build());
    }

    @Test
    public void idStringNotValidUriReturnsError() {
        assertValidationError(builder().idString(some(INVALID_URI_STRING)).build());
    }

    @Test
    public void summaryDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getSummary(), equalTo(none(Html.class)));
    }

    @Test(expected = NullPointerException.class)
    public void summaryCannotBeNull() {
        builder().summary(null);
    }

    @Test
    public void summaryOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().summary(some(html(makeString(MAX_STRING_LENGTH)))).build());
    }

    @Test
    public void summaryOverMaxLengthReturnsError() {
        assertValidationError(
                builder().summary(some(html(makeString(MAX_STRING_LENGTH + 1)))).build());
    }

    @Test
    public void typeDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getType(), equalTo(none(URI.class)));
    }

    @Test(expected = NullPointerException.class)
    public void typeCannotBeNull() {
        builder().type(null);
    }

    @Test
    public void typeOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().type(some(makeUri(MAX_STRING_LENGTH))).build());
    }

    @Test
    public void typeOverMaxLengthReturnsError() {
        assertValidationError(
                builder().type(some(makeUri(MAX_STRING_LENGTH + 1))).build());
    }

    @Test
    public void typeSimpleNameDoesNotReturnError() {
        assertNotValidationError(builder().type(some(SIMPLE_NAME)).build());
    }

    @Test
    public void typeNotAbsoluteButNotSimpleNameReturnsError() {
        assertValidationError(builder().type(some(RELATIVE_URI)).build());
    }

    @Test
    public void typeStringSimpleNameDoesNotReturnError() {
        assertNotValidationError(
                builder().typeString(some(SIMPLE_NAME.toASCIIString())).build());
    }

    @Test
    public void urlDefaultsToNone() {
        assertThat(assertNotValidationError(builder().build()).getUrl(), equalTo(none(URI.class)));
    }

    @Test(expected = NullPointerException.class)
    public void urlCannotBeNull() {
        builder().url(null);
    }

    @Test
    public void urlOfMaxLengthDoesNotReturnError() {
        assertNotValidationError(
                builder().url(some(makeAbsoluteUri(MAX_STRING_LENGTH))).build());
    }

    @Test
    public void urlOverMaxLengthReturnsError() {
        assertValidationError(
                builder().url(some(makeAbsoluteUri(MAX_STRING_LENGTH + 1))).build());
    }

    @Test
    public void urlSimpleNameDoesNotReturnError() {
        assertNotValidationError(builder().url(some(SIMPLE_NAME)).build());
    }

    @Test
    public void urlNotAbsoluteButNotSimpleNameReturnsError() {
        assertValidationError(builder().url(some(RELATIVE_URI)).build());
    }

    @Test
    public void urlStringSimpleNameDoesNotReturnError() {
        assertNotValidationError(
                builder().urlString(some(SIMPLE_NAME.toASCIIString())).build());
    }
}
