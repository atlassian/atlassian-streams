package com.atlassian.streams.common.renderer;

import java.io.Serializable;
import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.toArray;

final class TitleRenderer implements Function<StreamsEntry, Html> {
    private final I18nResolver i18nResolver;
    private final String key;
    private final java.util.function.Function<Iterable<UserProfile>, Html> authorsRenderer;
    private final java.util.function.Function<Iterable<ActivityObject>, Option<Html>> activityObjectRenderer;
    private final java.util.function.Function<ActivityObject, Option<Html>> targetRenderer;

    /**
     *
     * @deprecated use {@link #TitleRenderer(I18nResolver, String,
     *                                       java.util.function.Function,
     *                                       java.util.function.Function,
     *                                       java.util.function.Function)}
     *  instead
     */
    @Deprecated
    TitleRenderer(
            I18nResolver i18nResolver,
            String key,
            Function<Iterable<UserProfile>, Html> authorsRenderer,
            Option<Function<Iterable<ActivityObject>, Option<Html>>> activityObjectRenderer,
            Option<Function<ActivityObject, Option<Html>>> targetRenderer) {
        this.i18nResolver = i18nResolver;
        this.key = key;
        this.authorsRenderer = authorsRenderer;
        this.activityObjectRenderer = activityObjectRenderer.isDefined() ? activityObjectRenderer.get() : null;
        this.targetRenderer = targetRenderer.isDefined() ? targetRenderer.get() : null;
    }

    TitleRenderer(
            I18nResolver i18nResolver,
            String key,
            java.util.function.Function<Iterable<UserProfile>, Html> authorsRenderer,
            @Nullable java.util.function.Function<Iterable<ActivityObject>, Option<Html>> activityObjectRenderer,
            @Nullable java.util.function.Function<ActivityObject, Option<Html>> targetRenderer) {
        this.i18nResolver = i18nResolver;
        this.key = key;
        this.authorsRenderer = authorsRenderer;
        this.activityObjectRenderer = activityObjectRenderer;
        this.targetRenderer = targetRenderer;
    }

    public Html apply(StreamsEntry entry) {
        Option<Html> objectHtml = activityObjectRenderer != null
                ? activityObjectRenderer.apply(entry.getActivityObjects())
                : Option.none();
        Option<Html> targetHtml = targetRenderer != null ? entry.getTarget().flatMap(targetRenderer) : Option.none();

        return new Html(getText(
                key,
                concat(
                        ImmutableList.of(authorsRenderer.apply(entry.getAuthors())),
                        objectHtml.map(ImmutableList::of).getOrElse(ImmutableList.<Html>of()),
                        targetHtml.map(ImmutableList::of).getOrElse(ImmutableList.<Html>of()))));
    }

    private String getText(String key, Iterable<Html> args) {
        return i18nResolver.getText(key, toArray(args, Serializable.class));
    }
}
