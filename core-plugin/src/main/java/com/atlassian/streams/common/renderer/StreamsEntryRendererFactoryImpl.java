package com.atlassian.streams.common.renderer;

import java.net.URI;
import javax.annotation.Nullable;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;

import static com.google.common.base.Preconditions.checkNotNull;

public class StreamsEntryRendererFactoryImpl implements StreamsEntryRendererFactory {
    private final I18nResolver i18nResolver;
    private final TemplateRenderer templateRenderer;

    public StreamsEntryRendererFactoryImpl(StreamsI18nResolver i18nResolver, TemplateRenderer templateRenderer) {
        this.i18nResolver = i18nResolver;
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
    }

    public Renderer newCommentRenderer(String comment) {
        return newCommentRenderer(newCommentTitleRendererFunc(), comment);
    }

    public Renderer newCommentRenderer(Html comment) {
        return newCommentRenderer(newCommentTitleRendererFunc(), comment);
    }

    @Override
    public Renderer newCommentRenderer(java.util.function.Function<StreamsEntry, Html> titleRenderer, String message) {
        return new CommentRenderer(templateRenderer, titleRenderer, message);
    }

    @Override
    public Renderer newCommentRenderer(java.util.function.Function<StreamsEntry, Html> titleRenderer, Html message) {
        return newCommentRenderer(titleRenderer, message, null);
    }

    @Override
    public Renderer newCommentRenderer(
            java.util.function.Function<StreamsEntry, Html> titleRenderer, Html apply, @Nullable URI styleLink) {
        return new CommentRenderer(templateRenderer, titleRenderer, apply, Option.option(styleLink));
    }

    @Override
    public java.util.function.Function<StreamsEntry, Html> newTitleRendererFunc(String key) {
        return newTitleRendererFunc(
                key,
                newAuthorsRendererFunc(),
                newActivityObjectsRendererFunc(),
                newActivityObjectRendererWithSummaryFunc());
    }

    @Override
    public java.util.function.Function<StreamsEntry, Html> newTitleRendererFunc(
            String key,
            java.util.function.Function<Iterable<UserProfile>, Html> authorsRenderer,
            @Nullable java.util.function.Function<Iterable<ActivityObject>, Option<Html>> activityObjectRenderer,
            @Nullable java.util.function.Function<ActivityObject, Option<Html>> targetRenderer) {
        return new TitleRenderer(i18nResolver, key, authorsRenderer, activityObjectRenderer, targetRenderer);
    }

    @Override
    public java.util.function.Function<Iterable<ActivityObject>, Option<Html>> newActivityObjectsRendererFunc() {
        return newActivityObjectsRendererFunc(newActivityObjectRendererWithSummaryFunc());
    }

    @Override
    public java.util.function.Function<Iterable<ActivityObject>, Option<Html>> newActivityObjectsRendererFunc(
            java.util.function.Function<ActivityObject, Option<Html>> objectRenderer) {
        return new CompoundStatementRenderer<ActivityObject>(i18nResolver, objectRenderer);
    }

    @Override
    public java.util.function.Function<Iterable<UserProfile>, Html> newAuthorsRendererFunc() {
        return new AuthorsRenderer(i18nResolver, templateRenderer, true);
    }

    @Override
    public java.util.function.Function<Iterable<UserProfile>, Html> newUserProfileRendererFunc() {
        return new AuthorsRenderer(i18nResolver, templateRenderer, false);
    }

    @Override
    public java.util.function.Function<ActivityObject, Option<Html>> newActivityObjectRendererWithSummaryFunc() {
        return new ActivityObjectRenderer(templateRenderer, true);
    }

    @Override
    public java.util.function.Function<ActivityObject, Option<Html>> newActivityObjectRendererWithoutSummaryFunc() {
        return new ActivityObjectRenderer(templateRenderer, false);
    }

    @Override
    public <T> java.util.function.Function<Iterable<T>, Option<Html>> newCompoundStatementRendererFunc(
            java.util.function.Function<T, Option<Html>> render) {
        return new CompoundStatementRenderer<T>(i18nResolver, render);
    }

    @Override
    public java.util.function.Function<StreamsEntry, Html> newCommentTitleRendererFunc() {
        return CommentRenderer.standardTitleRenderer(this);
    }
}
