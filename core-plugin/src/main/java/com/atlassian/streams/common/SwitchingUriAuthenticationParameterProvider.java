package com.atlassian.streams.common;

import org.osgi.framework.BundleContext;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.OptionalService;
import com.atlassian.streams.spi.UriAuthenticationParameterProvider;

import static com.google.common.base.Preconditions.checkNotNull;

public class SwitchingUriAuthenticationParameterProvider extends OptionalService<UriAuthenticationParameterProvider>
        implements UriAuthenticationParameterProvider {
    private final UriAuthenticationParameterProvider defaultProvider;

    public SwitchingUriAuthenticationParameterProvider(
            final UriAuthenticationParameterProvider defaultProvider, final BundleContext bundleContext) {
        super(UriAuthenticationParameterProvider.class, bundleContext);
        this.defaultProvider = checkNotNull(defaultProvider, "defaultProvider");
    }

    @Override
    public Option<Pair<String, String>> get() {
        return getService().getOrElse(defaultProvider).get();
    }
}
