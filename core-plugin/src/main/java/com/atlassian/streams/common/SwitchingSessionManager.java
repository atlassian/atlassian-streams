package com.atlassian.streams.common;

import java.util.function.Supplier;

import org.osgi.framework.BundleContext;

import com.atlassian.streams.spi.DelegatingSessionManager;
import com.atlassian.streams.spi.OptionalService;
import com.atlassian.streams.spi.SessionManager;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link SessionManager} that delegates to whatever {@link SessionManager} has been provided
 * by another module, or, if there isn't any, to a default implementation.
 * <p>
 * This module exports a single instance of this class, which can be imported by referring to
 * the {@link DelegatingSessionManager} interface.
 */
public class SwitchingSessionManager extends OptionalService<SessionManager> implements DelegatingSessionManager {
    private final SessionManager defaultSessionManager;

    public SwitchingSessionManager(final SessionManager defaultSessionManager, final BundleContext bundleContext) {
        super(SessionManager.class, bundleContext);
        this.defaultSessionManager = checkNotNull(defaultSessionManager, "defaultSessionManager");
    }

    @Override
    public <T> T withSession(Supplier<T> s) {
        return getService().getOrElse(defaultSessionManager).withSession(s);
    }
}
