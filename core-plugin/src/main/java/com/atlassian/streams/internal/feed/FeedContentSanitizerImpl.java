package com.atlassian.streams.internal.feed;

import java.io.IOException;
import java.io.InputStream;

import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.owasp.validator.html.ScanException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.streams.api.FeedContentSanitizer;

/**
 * Provides a way to sanitize input strings to ensure that it is safe to output that content in a Feed, without risk of
 * XSS attacks.
 *
 * @author pkaeding
 * @see "antisamy-policy.xml"
 */
public class FeedContentSanitizerImpl implements FeedContentSanitizer {

    private static final Logger log = LoggerFactory.getLogger(FeedContentSanitizerImpl.class);
    public static final String ANTISAMY_POLICY_FILE = "antisamy-policy.xml";

    private AntiSamy as;
    private Policy policy;

    public FeedContentSanitizerImpl() {
        as = new AntiSamy();
        // Call to getClass().getClassLoader().getResource(...) can return URL like "bundle://url".
        // From antisamy 1.6.3 policy file can't be read from "bundle://url".
        // That's why we're reading the resource as stream.
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream(ANTISAMY_POLICY_FILE)) {
            policy = Policy.getInstance(stream);
        } catch (PolicyException | IOException e) {
            log.error("Error loading AntiSamy policy file", e);
        }
    }

    /* (non-Javadoc)
     * @see com.atlassian.streams.internal.feed.FeedContentSanitizer#sanitize(java.lang.String)
     */
    @Override
    public String sanitize(String taintedInput) {
        CleanResults cr;
        try {
            cr = as.scan(taintedInput, policy);
        } catch (PolicyException e) {
            log.error("Error loading AntiSamy policy file", e);
            // If we can't sanitize the input, we will just swallow the whole thing, and
            // return an empty string, rather than display tainted content
            return "";
        } catch (ScanException e) {
            log.debug("Error scanning input with AntiSamy", e);
            return "";
        }
        return cr.getCleanHTML();
    }
}
