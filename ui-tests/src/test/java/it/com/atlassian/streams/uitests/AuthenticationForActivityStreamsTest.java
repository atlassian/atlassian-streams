package it.com.atlassian.streams.uitests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.jira.JiraProfileActivityStream;
import com.atlassian.streams.testing.pageobjects.jira.JiraViewProfilePageWithActivityView;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

import static com.atlassian.streams.testing.UiTestGroups.MULTI;

@TestGroups(MULTI)
@RunWith(UiTestsTesterRunner.class)
public class AuthenticationForActivityStreamsTest {

    private JiraViewProfilePageWithActivityView pageUnderTest;

    @Before
    public void goToViewProfilePage() {
        pageUnderTest = TestedProductFactory.create(JiraTestedProduct.class, "jira-applinks-oauth", null)
                .quickLogin("user", "user", JiraViewProfilePageWithActivityView.class);
    }

    @Test
    public void assertThatAuthenticationMessagesAreShown() {
        String authenticationMessage =
                "Additional information may be available, please authenticate for more information";

        JiraProfileActivityStream activityStream = pageUnderTest.activityStreamView();

        assertThat(activityStream.getAuthenticationMessage(), containsString(authenticationMessage));
        assertThat(activityStream.getAllAuthenticationMessages().size(), is(1));
        assertThat(activityStream.getAuthenticationMessageAtIndex(0), containsString("Authenticate with Confluence."));
    }
}
