package it.com.atlassian.streams.uitests;

import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.AddDashboardPage;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.pageobjects.ActivityItem;
import com.atlassian.streams.pageobjects.ActivityStreamConfiguration;
import com.atlassian.streams.pageobjects.ActivityStreamGadget;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.jira.ExtendedDashboardPage;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.pageobjects.Matchers.hasSummary;
import static com.atlassian.streams.pageobjects.Matchers.hasVisibleInlineActions;
import static com.atlassian.streams.pageobjects.Matchers.withSummary;
import static com.atlassian.streams.testing.UiTestGroups.JIRA;

@TestGroups(JIRA)
@RunWith(UiTestsTesterRunner.class)
public class GadgetTest {
    private static JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);
    private static Backdoor backdoor = new Backdoor(new TestKitLocalEnvironmentData());

    @Rule
    public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule();

    private ActivityStreamGadget gadget;

    @BeforeClass
    public static void restoreDataAndLogIn() {
        final String jiraBaseURLProp = "jira.baseurl";
        final String baseURL = backdoor.applicationProperties().getString(jiraBaseURLProp);
        backdoor.restoreDataFromResource("jira/backups/all-entry-types.zip");
        backdoor.applicationProperties().setString(jiraBaseURLProp, baseURL);
        product.gotoLoginPage().loginAsSysAdmin(DashboardPage.class);
    }

    @Before
    public void createNewDashboard() {
        AddDashboardPage addDashboard = product.visit(AddDashboardPage.class);
        addDashboard.setName("Test Dashboard - " + RandomUtils.nextInt());
        addDashboard.submit();
        gadget = product.visit(ExtendedDashboardPage.class)
                .openAddGadgetDialog()
                .loadAllGadgets()
                .addGadget("Activity Stream")
                .finished()
                .getGadgetWithTitle("Activity Stream")
                .viewAs(ActivityStreamGadget.class);
    }

    @Test
    public void assertThatFeedLinkUriContainsOsAuthBasic() {
        assertThat(gadget.getFeedLink().getHref(), containsString("os_authType=basic"));
    }

    @Test
    public void assertThatSwitchViewOptionsContainsTabIndex() {
        assertThat(gadget.getFullViewOption().getTabIndex(), equalTo("0"));
        assertThat(gadget.getListViewOption().getTabIndex(), equalTo("0"));
        assertThat(gadget.getFilterViewOption().getTabIndex(), equalTo("0"));
    }

    @Test
    public void assertThatViewOptionSupportTabNavigation() {
        ActivityStreamGadget.SwitchViewOption fullViewOption = gadget.getFullViewOption();
        ActivityStreamGadget.SwitchViewOption listViewOption = gadget.getListViewOption();
        ActivityStreamGadget.SwitchViewOption filterViewOption = gadget.getFilterViewOption();

        fullViewOption.pressTab();

        assertThat(fullViewOption.hasFocus(), is(false));
        assertThat(listViewOption.hasFocus(), is(true));
        assertThat(filterViewOption.hasFocus(), is(false));

        listViewOption.pressTab();

        assertThat(fullViewOption.hasFocus(), is(false));
        assertThat(listViewOption.hasFocus(), is(false));
        assertThat(filterViewOption.hasFocus(), is(true));

        filterViewOption.pressShiftTab();

        assertThat(fullViewOption.hasFocus(), is(false));
        assertThat(listViewOption.hasFocus(), is(true));
        assertThat(filterViewOption.hasFocus(), is(false));

        listViewOption.pressShiftTab();

        assertThat(fullViewOption.hasFocus(), is(true));
        assertThat(listViewOption.hasFocus(), is(false));
        assertThat(filterViewOption.hasFocus(), is(false));
    }

    @Test
    public void assertThatViewOptionsSupportsEnterKeyPress() {
        // force close the configuration that is open when the gadget is first opened
        gadget.openConfiguration().save();

        ActivityStreamGadget.SwitchViewOption fullViewOption = gadget.getFullViewOption();
        ActivityStreamGadget.SwitchViewOption listViewOption = gadget.getListViewOption();
        ActivityStreamGadget.SwitchViewOption filterViewOption = gadget.getFilterViewOption();

        listViewOption.pressEnter();
        assertListViewIsDisplayed();

        filterViewOption.pressEnter();
        // configuration form is visible
        assertThat(gadget.getConfigurationSubmitButton().isDisplayed(), is(true));

        // assumes Full View is the default and visible when the test starts
        // so check it after we navigated away and back
        fullViewOption.pressEnter();
        assertFullViewIsDisplayed();
    }

    @Test
    public void assertThatViewOptionsSupportsClick() {
        // force close the configuration that is open when the gadget is first opened
        gadget.openConfiguration().save();

        ActivityStreamGadget.SwitchViewOption fullViewOption = gadget.getFullViewOption();
        ActivityStreamGadget.SwitchViewOption listViewOption = gadget.getListViewOption();
        ActivityStreamGadget.SwitchViewOption filterViewOption = gadget.getFilterViewOption();

        listViewOption.click();
        assertListViewIsDisplayed();

        filterViewOption.click();
        // configuration form is visible
        assertThat(gadget.getConfigurationSubmitButton().isDisplayed(), is(true));

        // assumes Full View is the default and visible when the test starts
        // so check it after we navigated away and back
        fullViewOption.click();
        assertFullViewIsDisplayed();
    }

    @Test
    public void assertThatTitleIsProperlySet() {
        String title = "New Title";
        gadget.openConfiguration().setTitle(title).save();
        assertThat(gadget.getTitle(), is(equalTo(title)));
    }

    /**
     * Note: this will definitely fail if assertThatTitleIsProperlySet() is broken.
     */
    @Test
    public void assertThatTitleIsProperlyEscaped() {
        String title = "<script>alert('bwahahaha!')</script>";
        gadget.openConfiguration().setTitle(title).save();
        assertThat(gadget.getTitle(), is(equalTo(title)));
    }

    @Test
    public void assertThatOnlyEntriesRelatingToUserArePresentWhenUsernameFilterIsSpecified() {
        addUsernameFilter("admin");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("admin"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatOnlyIssueAddedEntriesArePresentWhenOnlyIssueAddedActivityFilterIsSelected() {
        selectActivity("Issue created");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("created"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatOnlyIssueUpdatedEntriesArePresentWhenOnlyIssueUpdatedActivityFilterIsSelected() {
        selectActivity("Issue edited");

        assertThat(
                gadget.getActivityItems(),
                allOf(
                        everyItem(anyOf(hasSummary(containsString("updated")), hasSummary(containsString("changed")))),
                        not(is(emptyIterable()))));
    }

    @Test
    public void
            assertThatIssueTransitionEntriesWithNonSystemTransitionsArePresentWhenOnlyCustomIssueTransitionsActivityFilterIsSelected() {
        selectActivity("Issue transitioned");

        Matcher<? super Iterable<ActivityItem>> hasItemWithChangedStatusInSummary =
                hasItem(withSummary(containsString("changed the status")));
        assertThat(gadget.getActivityItems(), hasItemWithChangedStatusInSummary);
    }

    @Test
    public void assertThatOnlyIssueReopenedEntriesArePresentWhenOnlyIssueReopenedActivityFilterIsSelected() {
        selectActivity("Issue reopened");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("reopened"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatOnlyIssueClosedEntriesArePresentWhenOnlyIssueClosedActivityFilterIsSelected() {
        selectActivity("Issue closed");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("closed"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatOnlyIssueResolvedEntriesArePresentWhenOnlyIssueResolvedActivityFilterIsSelected() {
        selectActivity("Issue resolved");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("resolved"))), not(is(emptyIterable()))));
    }

    @Test
    public void
            assertThatOnlyIssueProgressStartedEntriesArePresentWhenOnlyIssueProgressStartedActivityFilterIsSelected() {
        selectActivity("Issue progress started");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("started progress"))), not(is(emptyIterable()))));
    }

    @Test
    public void
            assertThatOnlyIssueProgressStoppedEntriesArePresentWhenOnlyIssueProgressStoppedActivityFilterIsSelected() {
        selectActivity("Issue progress stopped");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("stopped progress"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatOnlyCommentedEntriesArePresentWhenOnlyCommentAddedActivityFilterIsSelected() {
        selectActivity("Comment added");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("commented"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatOnlyAttachmentAddedEntriesArePresentWhenOnlyAttachmentAddedActivityFilterIsSelected() {
        selectActivity("Attachment added");

        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasSummary(containsString("attached"))), not(is(emptyIterable()))));
    }

    @Test
    public void assertThatTimeOutIsDisplayedWhenProvidedInTheFeed() {
        // We need to close the configuration panel first
        gadget.openConfiguration().save();

        ActivityStreamGadget gadgetWithCustomFeed = gadget.setServletPath("/plugins/servlet/jstest-resources/feed.xml");
        assertThat(gadgetWithCustomFeed.getTimeoutSources(), contains("Applink A", "Applink B"));
    }

    private void addUsernameFilter(String username) {
        addGlobalFilter("Username", username);
    }

    private void selectActivity(String activity) {
        addGlobalFilter("Activity", activity);
    }

    private void addGlobalFilter(String rule, String value) {
        addGlobalFilter(rule, "is", value);
    }

    private void addGlobalFilter(String rule, String operator, String value) {
        ActivityStreamConfiguration configuration = gadget.openConfiguration();
        configuration
                .addGlobalFilter()
                .getFirstFilter()
                .selectRule(rule)
                .selectOperator(operator)
                .setValue(value);
        configuration.save();
    }

    private void assertListViewIsDisplayed() {
        // configuration form is not visible
        assertThat(gadget.getConfigurationSubmitButton().isDisplayed(), is(false));

        // check we are in list view, no items have actions
        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasVisibleInlineActions(is(false))), not(is(emptyIterable()))));
    }

    private void assertFullViewIsDisplayed() {
        // configuration form is not visible
        assertThat(gadget.getConfigurationSubmitButton().isDisplayed(), is(false));

        // check we are in full view, items have actions
        assertThat(
                gadget.getActivityItems(),
                allOf(everyItem(hasVisibleInlineActions(is(true))), not(is(emptyIterable()))));
    }
}
