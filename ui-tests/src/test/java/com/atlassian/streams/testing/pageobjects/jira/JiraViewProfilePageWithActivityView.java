package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.jira.pageobjects.pages.ViewProfilePage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class JiraViewProfilePageWithActivityView extends ViewProfilePage {

    @ElementBy(xpath = "//div[@id='activity-profile-fragment']//iframe")
    private PageElement activityStreamContainer;

    public JiraProfileActivityStream activityStreamView() {
        Poller.waitUntilTrue(activityStreamContainer.timed().isVisible());
        String frameId = activityStreamContainer.getAttribute("id");
        driver.switchTo().frame(frameId);
        return pageBinder.bind(JiraProfileActivityStream.class, "1");
    }
}
