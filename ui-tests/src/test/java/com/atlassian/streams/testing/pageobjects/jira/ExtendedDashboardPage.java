package com.atlassian.streams.testing.pageobjects.jira;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.streams.pageobjects.Gadget;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

import static com.atlassian.streams.pageobjects.Predicates.text;
import static com.atlassian.webdriver.utils.element.ElementConditions.isVisible;

public class ExtendedDashboardPage extends DashboardPage {
    @FindBy(id = "dashboard")
    WebElement dashboard;

    @ElementBy(id = "add-gadget")
    PageElement addGadget;

    @Inject
    Backdoor backdoor;

    public AddGadgetDialog openAddGadgetDialog() {
        backdoor.flags().clearFlags();
        Poller.waitUntilTrue(addGadget.timed().isEnabled());
        addGadget.click();
        return pageBinder.bind(AddGadgetDialog.class);
    }

    public Gadget getGadgetWithTitle(String gadgetTitle) {
        backdoor.flags().clearFlags();
        By dashboardItemTitleSelector = By.className("dashboard-item-title");
        webDriverPoller.waitUntil(isVisible(dashboardItemTitleSelector));
        String gadgetId = find(dashboard.findElements(dashboardItemTitleSelector), text(equalTo(gadgetTitle)))
                .getAttribute("id")
                .replace("-title", "");
        return pageBinder.bind(Gadget.class, gadgetId);
    }
}
