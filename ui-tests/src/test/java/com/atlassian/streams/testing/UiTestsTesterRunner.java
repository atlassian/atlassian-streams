package com.atlassian.streams.testing;

import org.junit.runners.model.InitializationError;

import com.atlassian.integrationtesting.runner.CompositeTestRunner;
import com.atlassian.integrationtesting.runner.TestGroupRunner;

public class UiTestsTesterRunner extends CompositeTestRunner {
    public UiTestsTesterRunner(Class<?> klass) throws InitializationError {
        super(klass, compose());
    }

    public static Composer compose() {
        return CompositeTestRunner.compose().from(TestGroupRunner.compose());
    }
}
