package com.atlassian.streams.testing.pageobjects.jira;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;

public class AddGadgetDialog {
    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "gadget-dialog")
    private PageElement dialog;

    @FindBy(id = "category-all")
    private final String currentCategory = "all";

    @FindBy(css = ".aui-dialog2-header .aui-close-button")
    private WebElement finishButton;

    @Inject
    private PageElementFinder elementFinder;

    @WaitUntil
    public void waitForDialogToBeVisible() {
        Poller.waitUntilTrue(elementFinder.find(By.id("gadget-dialog")).timed().isVisible());
    }

    public AddGadgetDialog addGadget(String gadgetTitle) {
        final PageElement addButton = dialog.find(By.xpath("//*/div[@data-item-title='" + gadgetTitle + "']"))
                .find(By.className("add-dashboard-item-selector"));
        addButton.click();
        Poller.waitUntilTrue(addButton.timed().isEnabled());
        return this;
    }

    public AddGadgetDialog loadAllGadgets() {
        waitForLoadAllGadgetsButtonToAppear();
        elementFinder.find(By.id("load-more-directory-items")).click();
        return this;
    }

    private void waitForLoadAllGadgetsButtonToAppear() {
        Poller.waitUntilTrue(
                elementFinder.find(By.id("load-more-directory-items")).timed().isPresent());
    }

    public ExtendedDashboardPage finished() {
        finishButton.click();

        Poller.waitUntilFalse(dialog.find(By.className("dialog-title")).timed().isVisible());
        return pageBinder.bind(ExtendedDashboardPage.class);
    }
}
