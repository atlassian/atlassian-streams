package com.atlassian.streams.crucible;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.ActivityOptions;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;

import static com.google.common.collect.Iterables.transform;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.review;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.abandon;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.close;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.complete;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.reopen;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.start;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarize;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.uncomplete;

public class CrucibleFilterOptionProvider implements StreamsFilterOptionProvider {
    static final Pair<ActivityObjectType, ActivityVerb> REVIEW_ADDED = pair(review(), post());
    static final Pair<ActivityObjectType, ActivityVerb> REVIEW_STARTED = pair(review(), start());
    static final Pair<ActivityObjectType, ActivityVerb> REVIEW_COMPLETED = pair(review(), complete());
    static final Pair<ActivityObjectType, ActivityVerb> REVIEW_UNCOMPLETED = pair(review(), uncomplete());
    static final Pair<ActivityObjectType, ActivityVerb> COMMENT_ADDED = pair(comment(), post());

    public static final Iterable<Pair<ActivityObjectType, ActivityVerb>> activities = ImmutableList.of(
            REVIEW_ADDED,
            REVIEW_STARTED,
            REVIEW_COMPLETED,
            REVIEW_UNCOMPLETED,
            COMMENT_ADDED,
            pair(review(), abandon()),
            pair(review(), close()),
            pair(review(), reopen()),
            pair(review(), summarize()));

    private final Function<Pair<ActivityObjectType, ActivityVerb>, ActivityOption> toActivityOptions;

    public CrucibleFilterOptionProvider(I18nResolver i18nResolver) {
        this.toActivityOptions = ActivityOptions.toActivityOptionFunc(i18nResolver, "streams.filter.crucible")::apply;
    }

    public Iterable<StreamsFilterOption> getFilterOptions() {
        return ImmutableList.of();
    }

    public Iterable<ActivityOption> getActivities() {
        return transform(activities, toActivityOptions);
    }
}
