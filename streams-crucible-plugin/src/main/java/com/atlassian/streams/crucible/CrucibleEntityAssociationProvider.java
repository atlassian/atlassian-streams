package com.atlassian.streams.crucible;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cenqua.crucible.model.managers.UserActionManager;
import com.google.common.collect.ImmutableList;

import com.atlassian.crucible.spi.data.ProjectData;
import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.project;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;

public class CrucibleEntityAssociationProvider implements StreamsEntityAssociationProvider {
    private static final String PROJECT_URL_PREFIX = "/project/";

    /**
     * Regex to parse project URIs.
     *
     * Group 1: project key, e.g. CR-STRM
     * Group 2: request parameters and hash
     */
    private static final Pattern PROJECT_PATTERN = Pattern.compile("([^\\?#]+)(.*)");

    private final ApplicationProperties applicationProperties;
    private final ProjectService projectService;

    public CrucibleEntityAssociationProvider(
            ApplicationProperties applicationProperties, ProjectService projectService) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.projectService = checkNotNull(projectService, "projectService");
    }

    @Override
    public Iterable<EntityIdentifier> getEntityIdentifiers(URI target) {
        String targetStr = target.toString();
        if (target.isAbsolute()) {
            // the target URI must reference an entity on this server
            if (!targetStr.startsWith(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX)) {
                return ImmutableList.of();
            }
            return matchEntities(
                    targetStr.substring(applicationProperties.getBaseUrl().length() + PROJECT_URL_PREFIX.length()));
        } else {
            return matchEntities(targetStr);
        }
    }

    @Override
    public Option<URI> getEntityURI(EntityIdentifier identifier) {
        if (identifier.getType().equals(project().iri())) {
            return some(URI.create(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX + identifier.getValue()));
        }
        return none();
    }

    @Override
    public Option<Boolean> getCurrentUserViewPermission(EntityIdentifier identifier) {
        if (identifier.getType().equals(project().iri())) {
            ProjectData projectData = projectService.getProject(identifier.getValue());

            if (projectData != null) {
                return some(Boolean.valueOf(
                        projectService.hasPermission(projectData.getKey(), UserActionManager.ACTION_VIEW)));
            }
        }
        return none();
    }

    @Override
    public Option<String> getFilterKey(EntityIdentifier identifier) {
        if (identifier.getType().equals(project().iri())) {
            return some(PROJECT_KEY);
        }
        return none();
    }

    @Override
    public Option<Boolean> getCurrentUserEditPermission(EntityIdentifier identifier) {
        // There doesn't seem to be an appropriate project permission for this in Crucible
        return getCurrentUserViewPermission(identifier);
    }

    private Iterable<EntityIdentifier> matchEntities(String input) {
        Matcher matcher = PROJECT_PATTERN.matcher(input);
        // the target URI is of an unknown format
        if (!matcher.matches()) {
            return ImmutableList.of();
        }

        String projectKey = matcher.group(1);
        ProjectData project = projectService.getProject(projectKey);
        if (project != null) {
            URI canonicalUri = URI.create(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX + projectKey);
            return ImmutableList.of(new EntityIdentifier(project().iri(), projectKey, canonicalUri));
        } else {
            return ImmutableList.of();
        }
    }
}
