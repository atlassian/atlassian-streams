package com.atlassian.streams.crucible;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Principal;
import com.cenqua.crucible.model.Project;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.SecureProjectManager;
import com.cenqua.crucible.model.managers.UserActionManager;

import com.atlassian.fecru.user.EffectiveUserProvider;

import static com.cenqua.crucible.tags.ReviewUtil.principalCanDoProjectAction;
import static com.cenqua.crucible.tags.ReviewUtil.principalCanDoReviewAction;
import static com.google.common.base.Preconditions.checkNotNull;

public class CruciblePermissionAccessorImpl implements CruciblePermissionAccessor {
    private final CrucibleCommentManager commentManager;
    private final EffectiveUserProvider effectiveUserProvider;
    private final SecureProjectManager secureProjectManager;

    public CruciblePermissionAccessorImpl(
            CrucibleCommentManager commentManager,
            EffectiveUserProvider effectiveUserProvider,
            SecureProjectManager secureProjectManager) {
        this.commentManager = checkNotNull(commentManager, "commentManager");
        this.effectiveUserProvider = checkNotNull(effectiveUserProvider, "effectiveUserProvider");
        this.secureProjectManager = checkNotNull(secureProjectManager, "secureProjectManager");
    }

    public boolean currentUserCanView(Review review) {
        try {
            return principalCanDoReviewAction(getCurrentUser(), UserActionManager.ACTION_VIEW, review, null);
        } catch (NullPointerException e) {
            // for some reason, IDOG is throwing a NPE when getting the PermissionScheme id in
            // com.cenqua.crucible.tags.ReviewUtil$PrincipalReviewKey.create (STRM-828)
            return false;
        }
    }

    public boolean currentUserCanView(Project project) {
        return principalCanDoProjectAction(getCurrentUser(), UserActionManager.ACTION_VIEW, project);
    }

    public boolean currentUserCanReplyToComment(Comment comment) {
        return currentUserCanComment(comment.getReview());
    }

    public boolean currentUserCanComment(Review review) {
        return review.checkWriteAccess(effectiveUserProvider.getEffectiveUser())
                && commentManager.canAddComment(review);
    }

    public Iterable<Project> getCurrentlyVisibleProjects() {
        return secureProjectManager.getVisibleProjects(getCurrentUser());
    }

    /**
     * Returns the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     * @return the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     */
    private Principal getCurrentUser() {
        return effectiveUserProvider.getEffectivePrincipal();
    }
}
