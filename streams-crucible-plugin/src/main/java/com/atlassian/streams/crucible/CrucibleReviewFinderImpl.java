package com.atlassian.streams.crucible;

import java.util.Date;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cenqua.crucible.model.CrucibleActivityItem;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.CrucibleActivityItemManager;
import com.cenqua.crucible.model.managers.CrucibleActivityItemParameter;
import com.cenqua.crucible.model.managers.Order;
import com.cenqua.crucible.model.managers.ReviewManager;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.Pairs;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StandardStreamsFilterOption;

import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.ActivityItemKey.CREATE_DATE;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.ActivityItemKey.LOG_ACTION;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.ActivityItemKey.PROJECT_KEY;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.ActivityItemKey.REVIEW_ID;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.ActivityItemKey.USERNAME;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.Operator.GT;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.Operator.IN;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.Operator.LT;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.Operator.NOT_IN;
import static com.cenqua.crucible.model.managers.CrucibleActivityItemParameter.builder;
import static com.cenqua.crucible.model.managers.LogAction.COMMENT_ADDED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.api.common.Predicates.containsAnyIssueKey;
import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.review;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.abandon;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.close;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.complete;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.reopen;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.start;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarize;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.uncomplete;
import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.SUPPORTED_ACTIONS;
import static com.atlassian.streams.spi.Filters.getAllValues;
import static com.atlassian.streams.spi.Filters.getIsAndNotValues;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getIssueKeys;
import static com.atlassian.streams.spi.Filters.getMaxDate;
import static com.atlassian.streams.spi.Filters.getMinDate;
import static com.atlassian.streams.spi.Filters.getNotIssueKeys;
import static com.atlassian.streams.spi.Filters.inOptionActivities;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;

public class CrucibleReviewFinderImpl implements CrucibleReviewFinder {
    // Limit the number of ActivityItems we request from CrucibleActivityItemManager.getActivityForReviewsAndUsers()
    // so we do not retrieve the entire set of ActivityItems before post-processing here, which would hammer Crucible
    // and cause the provider to time out.
    private static final int MAX_ACTIVITY_ITEMS = 10000;
    private static final Logger log = LoggerFactory.getLogger(CrucibleReviewFinderImpl.class);

    private final ReviewManager reviewManager;
    private final CrucibleActivityItemManager crucibleActivityItemManager;
    private final CruciblePermissionAccessor permissionAccessor;

    public CrucibleReviewFinderImpl(
            final ReviewManager reviewManager,
            final CrucibleActivityItemManager crucibleActivityItemManager,
            final CruciblePermissionAccessor permissionAccessor) {
        this.reviewManager = checkNotNull(reviewManager, "reviewManager");
        this.crucibleActivityItemManager = checkNotNull(crucibleActivityItemManager, "crucibleActivityItemManager");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
    }

    private Option<Set<Review>> getReviews(ActivityRequest request) {
        Iterable<String> issueKeys = getIssueKeys(request);

        final Option<Set<Review>> reviews;
        if (!isEmpty(issueKeys)) {
            reviews = some(searchForReviewsContaining(issueKeys));
        } else {
            reviews = none();
        }

        // The IS operators were filtered above, but we still have to take care of any NOT operators.
        // Here we filter out any reviews that match the NOT issue-key filter options.
        return reviews.map(filterReviews(request));
    }

    private Function<Set<Review>, Set<Review>> filterReviews(final ActivityRequest request) {
        return new Function<Set<Review>, Set<Review>>() {
            public Set<Review> apply(Set<Review> reviews) {
                Pair<Set<String>, Set<String>> isAndNotIssueKeys = getIsAndNotValues(
                        request.getStandardFiltersMap().getOrDefault(ISSUE_KEY.getKey(), emptyList()));
                Iterable<String> issueKeys = isAndNotIssueKeys.first();
                Iterable<String> notIssueKeys = isAndNotIssueKeys.second();

                return Sets.filter(
                        reviews,
                        and(
                                nameOrDescription(
                                        containsIssueKeys(issueKeys).getOrElse(Predicates.<String>alwaysTrue())),
                                not(nameOrDescription(
                                        containsIssueKeys(notIssueKeys).getOrElse(Predicates.<String>alwaysFalse())))));
            }
        };
    }

    private Predicate<Review> nameOrDescription(final Predicate<String> p) {
        return new Predicate<Review>() {
            public boolean apply(Review r) {
                return p.apply(r.getName()) || p.apply(r.getDescription());
            }

            @Override
            public String toString() {
                return String.format("nameOrDescription(%s)", p);
            }
        };
    }

    private Option<Predicate<String>> containsIssueKeys(Iterable<String> issueKeys) {
        if (isEmpty(issueKeys)) {
            return none();
        }
        return some(containsAnyIssueKey(issueKeys)::test);
    }

    public Iterable<Pair<StreamsCrucibleActivityItem, ActivityVerb>> getNotificationsFor(ActivityRequest request) {
        if (isEmpty(permissionAccessor.getCurrentlyVisibleProjects())) {
            return ImmutableList.of();
        }

        Option<Set<Review>> reviews = getReviews(request);
        if (reviews.isDefined() && reviews.get().isEmpty()) {
            return ImmutableList.of();
        }

        CancelledException.throwIfInterrupted();

        Iterable<StreamsCrucibleActivityItem> activityItems = Iterables.transform(
                crucibleActivityItemManager.getActivityForReviewsAndUsers(
                        activityItemParameters(request, reviews), MAX_ACTIVITY_ITEMS, Order.DESC),
                new Function<CrucibleActivityItem, StreamsCrucibleActivityItem>() {
                    @Override
                    public StreamsCrucibleActivityItem apply(CrucibleActivityItem from) {
                        return new StreamsCrucibleActivityItem(from);
                    }
                });

        CancelledException.throwIfInterrupted();

        // Filter the log data based on the activity filter and whether or not the user can see the review
        // This should also filter out all entries without a valid activity object type and activity verb.
        // We double the maxResults because we collapse created and started entries and summarized and closed entries
        // that are close together into a single stream entry, so if we just get maxResults we'll probably get
        // too few entries in the final stream.
        return catOptions(transform(
                take(
                        request.getMaxResults() * 2,
                        filter(
                                transform(activityItems, toReviewActivityPair()),
                                and(ImmutableList.of(
                                        notPhantomReview(),
                                        inCrucibleActivities(request),
                                        activityItemReview(nameOrDescription(
                                                containsIssueKeys(getIssueKeys(request))
                                                        .getOrElse(Predicates.<String>alwaysTrue()))),
                                        activityItemReview(not(
                                                nameOrDescription(
                                                        containsIssueKeys(getNotIssueKeys(request))
                                                                .getOrElse(Predicates.<String>alwaysFalse())))),
                                        userVisible)))),
                toActivityItemVerbPair()));
    }

    private final Predicate<StreamsCrucibleActivityItem> notPhantomReview =
            new Predicate<StreamsCrucibleActivityItem>() {
                public boolean apply(StreamsCrucibleActivityItem data) {
                    try {
                        data.getReview().getAuthor();
                        return true;
                    } catch (final Exception e) {
                        if (log.isDebugEnabled()) {
                            log.debug("Could not load review", e);
                        }
                        return false;
                    }
                }

                @Override
                public String toString() {
                    return "notPhantomReview()";
                }
            };

    private Predicate<Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>>
            activityItemReview(final Predicate<Review> p) {
        return Pairs.<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>withFirst(
                new java.util.function.Predicate<StreamsCrucibleActivityItem>() {
                    @Override
                    public boolean test(StreamsCrucibleActivityItem item) {
                        return p.apply(item.getReview());
                    }

                    @Override
                    public String toString() {
                        return String.format("activityItemReview(%s)", p);
                    }
                })::test;
    }

    private Iterable<CrucibleActivityItemParameter> activityItemParameters(
            ActivityRequest request, Option<Set<Review>> reviews) {
        ImmutableList.Builder<CrucibleActivityItemParameter> builder =
                new ImmutableList.Builder<CrucibleActivityItemParameter>();

        builder.add(builder(PROJECT_KEY, IN)
                .values(getIsValues(request.getStandardFiltersMap()
                        .getOrDefault(StandardStreamsFilterOption.PROJECT_KEY, emptyList())))
                .build());
        builder.add(builder(USERNAME, IN)
                .values(getIsValues(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList())))
                .build());
        builder.add(builder(REVIEW_ID, IN)
                .values(transform(reviews.getOrElse(ImmutableSet.<Review>of()), toReviewId()))
                .build());
        builder.add(builder(LOG_ACTION, IN).values(SUPPORTED_ACTIONS).build());

        for (Date date : getMinDate(request)) {
            builder.add(builder(CREATE_DATE, GT).value(date).build());
        }
        for (Date date : getMaxDate(request)) {
            builder.add(builder(CREATE_DATE, LT).value(date).build());
        }

        builder.add(builder(PROJECT_KEY, NOT_IN)
                .values(getAllValues(
                        NOT,
                        request.getStandardFiltersMap()
                                .getOrDefault(StandardStreamsFilterOption.PROJECT_KEY, emptyList())))
                .build());
        builder.add(builder(USERNAME, NOT_IN)
                .values(getAllValues(NOT, request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList())))
                .build());

        return builder.build();
    }

    private Function<
                    Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>,
                    Option<Pair<StreamsCrucibleActivityItem, ActivityVerb>>>
            toActivityItemVerbPair() {
        return new ToActivityItemVerbPairFunction();
    }

    private final class ToActivityItemVerbPairFunction
            implements Function<
                    Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>,
                    Option<Pair<StreamsCrucibleActivityItem, ActivityVerb>>> {
        public Option<Pair<StreamsCrucibleActivityItem, ActivityVerb>> apply(
                Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>> activity) {
            for (Pair<ActivityObjectType, ActivityVerb> activityPair : activity.second()) {
                return some(pair(activity.first(), activityPair.second()));
            }
            return none();
        }
    }

    private Function<
                    StreamsCrucibleActivityItem,
                    Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>>
            toReviewActivityPair() {
        return new ToReviewActivityPairFunction();
    }

    private final class ToReviewActivityPairFunction
            implements Function<
                    StreamsCrucibleActivityItem,
                    Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>> {
        public Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>> apply(
                StreamsCrucibleActivityItem activityItem) {
            return pair(activityItem, typeAndVerb(activityItem));
        }
    }

    private Predicate<Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>>
            notPhantomReview() {
        return Pairs.<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>withFirst(
                notPhantomReview::apply)::test;
    }

    private Predicate<Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>>
            inCrucibleActivities(ActivityRequest request) {
        return Pairs.<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>withSecond(
                inOptionActivities(request))::test;
    }

    private Option<Pair<ActivityObjectType, ActivityVerb>> typeAndVerb(StreamsCrucibleActivityItem activityItem) {
        Option<ActivityObjectType> type = type(activityItem);
        Option<ActivityVerb> verb = verb(activityItem);

        if (!type.isDefined() || !verb.isDefined()) {
            return none();
        }

        return some(pair(type.get(), verb.get()));
    }

    private Option<ActivityObjectType> type(StreamsCrucibleActivityItem activityItem) {
        if (!SUPPORTED_ACTIONS.contains(activityItem.getLogAction())) {
            return none();
        }

        if (COMMENT_ADDED.equals(activityItem.getLogAction())) {
            return some(comment());
        }
        return some(review());
    }

    private Option<ActivityVerb> verb(StreamsCrucibleActivityItem activityItem) {
        switch (activityItem.getLogAction()) {
            case REVIEW_COMPLETED:
                return some(complete());
            case REVIEW_UNCOMPLETED:
                return some(uncomplete());
            case REVIEW_CREATED:
                return some(post());
            case REVIEW_STATE_CHANGE:
                return getStateChangeVerb(activityItem);
            case COMMENT_ADDED:
                return some(post());
        }

        return none();
    }

    private Option<ActivityVerb> getStateChangeVerb(StreamsCrucibleActivityItem activityItem) {
        if (activityItem.getEntityString() == null) {
            return none();
        } else if (activityItem.getEntityString().endsWith("reopenReview")) {
            return some(reopen());
        } else if (activityItem.getEntityString().endsWith("abandonReview")) {
            return some(abandon());
        } else if (activityItem.getEntityString().endsWith("approveReview")) {
            return some(start());
        } else if (activityItem.getEntityString().endsWith("closeReview")) {
            return some(close());
        } else if (activityItem.getEntityString().endsWith("summarizeReview")) {
            return some(summarize());
        }
        return none();
    }

    private final Predicate<Pair<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>>
            userVisible = Pairs.<StreamsCrucibleActivityItem, Option<Pair<ActivityObjectType, ActivityVerb>>>withFirst(
                    new java.util.function.Predicate<StreamsCrucibleActivityItem>() {
                        @Override
                        public boolean test(StreamsCrucibleActivityItem data) {
                            try {
                                return permissionAccessor.currentUserCanView(data.getReview());
                            } catch (final Exception e) {
                                if (log.isInfoEnabled()) {
                                    log.info("Could not load review", e);
                                }
                                return false;
                            }
                        }

                        @Override
                        public String toString() {
                            return "userVisible()";
                        }
                    })::test;

    private static Function<Review, Integer> toReviewId() {
        return ToReviewId.INSTANCE;
    }

    private enum ToReviewId implements Function<Review, Integer> {
        INSTANCE;

        public Integer apply(Review review) {
            return review.getId();
        }
    }

    /**
     * Find all reviews containing the given filter keys.
     *
     * @param filterKeys the filter keys to search for
     * @return the resulting set of review keys
     */
    private Set<Review> searchForReviewsContaining(final Iterable<String> filterKeys) {
        return ImmutableSet.copyOf(concat(filter(transform(filterKeys, filterKeyToReviews), notNull())));
    }

    private final Function<String, Set<Review>> filterKeyToReviews = new Function<String, Set<Review>>() {
        @SuppressWarnings("unchecked")
        public Set<Review> apply(String filterKey) {
            return reviewManager.searchReviewForTerm(filterKey.toLowerCase(), "review", null, null);
        }
    };
}
