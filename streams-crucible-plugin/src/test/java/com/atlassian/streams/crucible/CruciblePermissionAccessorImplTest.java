package com.atlassian.streams.crucible;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.SecureProjectManager;

import com.atlassian.fecru.user.EffectiveUserProvider;
import com.atlassian.fecru.user.FecruUser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CruciblePermissionAccessorImplTest {
    @Mock
    private CrucibleCommentManager commentManager;

    @Mock
    private EffectiveUserProvider effectiveUserProvider;

    @Mock
    private SecureProjectManager secureProjectManager;

    @Mock
    private Comment comment;

    @Mock
    private Review review;

    private CruciblePermissionAccessorImpl test;

    @Before
    public void setUp() {
        when(comment.getReview()).thenReturn(review);
        test = new CruciblePermissionAccessorImpl(commentManager, effectiveUserProvider, secureProjectManager);
    }

    @Test
    public void testCanReplyToCommentAffirmative() {
        when(review.checkWriteAccess(any())).thenReturn(true);
        when(commentManager.canAddComment(any())).thenReturn(true);
        assertTrue("canReplyToComment should return true", test.currentUserCanReplyToComment(comment));
    }

    @Test
    public void testCanReplyToCommentNegativeReview() {
        when(review.checkWriteAccess(nullable(FecruUser.class))).thenReturn(true);
        when(commentManager.canAddComment(any(Review.class))).thenReturn(false);
        assertFalse(
                "canReplyToComment should return false b/c the review isn't accepting comments",
                test.currentUserCanReplyToComment(comment));
    }

    @Test
    public void testCanReplyToCommentNegativeUser() {
        when(review.checkWriteAccess(nullable(FecruUser.class))).thenReturn(false);
        lenient().when(commentManager.canAddComment(any(Review.class))).thenReturn(true);
        assertFalse(
                "canReplyToComment should return false b/c the user can't comment",
                test.currentUserCanReplyToComment(comment));
    }
}
