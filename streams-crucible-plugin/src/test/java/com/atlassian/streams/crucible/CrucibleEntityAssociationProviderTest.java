package com.atlassian.streams.crucible;

import java.net.URI;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.cenqua.crucible.model.managers.UserActionManager;

import com.atlassian.crucible.spi.data.ProjectData;
import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;
import com.atlassian.streams.testing.AbstractEntityAssociationProviderTest;

import static org.mockito.Mockito.when;

import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.project;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CrucibleEntityAssociationProviderTest extends AbstractEntityAssociationProviderTest {
    @Mock
    private ProjectService projectService;

    @Mock
    private ProjectData projectData;

    @Override
    public StreamsEntityAssociationProvider createProvider() {
        return new CrucibleEntityAssociationProvider(applicationProperties, projectService);
    }

    @Override
    protected String getProjectUriPath(String key) {
        return "/project/" + key;
    }

    @Override
    protected URI getProjectEntityType() {
        return project().iri();
    }

    @Override
    protected void setProjectExists(String key, boolean exists) {
        when(projectService.getProject(key)).thenReturn(exists ? projectData : null);
    }

    @Override
    protected void setProjectViewPermission(String key, boolean permitted) {
        when(projectService.getProject(key)).thenReturn(projectData);
        when(projectService.hasPermission(projectData.getKey(), UserActionManager.ACTION_VIEW))
                .thenReturn(permitted);
    }

    @Override
    protected void setProjectEditPermission(String key, boolean permitted) {
        setProjectViewPermission(key, permitted);
    }
}
