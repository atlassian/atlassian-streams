package com.atlassian.streams.crucible;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.cenqua.crucible.model.Comment;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;

@RunWith(MockitoJUnitRunner.class)
public class CrucibleEntryFactoryImplTest {
    @Mock
    private CrucibleCommentManager commentManager;

    @Mock
    private UserProfileAccessor userProfileAccessor;

    @Mock
    private UriProvider uriProvider;

    @Mock
    private ReviewRendererFactory reviewRendererFactory;

    @Mock
    private UserManager userManager;

    @Mock
    private StreamsI18nResolver i18nResolver;

    @Mock
    private CruciblePermissionAccessor permissionAccessor;

    @Mock
    private Comment comment;

    @Mock
    private ApplicationProperties applicationProperties;

    private CrucibleEntryFactoryImpl test;

    @Before
    public void setUp() {
        when(comment.getReviewId()).thenReturn(123);
        when(comment.getId()).thenReturn(456);
        when(applicationProperties.getBaseUrl()).thenReturn("http://foo/bar");
        test = new CrucibleEntryFactoryImpl(
                commentManager,
                userProfileAccessor,
                uriProvider,
                reviewRendererFactory,
                userManager,
                i18nResolver,
                permissionAccessor,
                applicationProperties);
    }

    @Test
    public void testBuildReplyToWhenUserCantReply() {
        when(permissionAccessor.currentUserCanReplyToComment(comment)).thenReturn(false);
        assertThat(test.buildReplyTo(comment), is(none(URI.class)));
    }

    @Test
    public void testBuildReplyToWhenUserCanReply() {
        when(permissionAccessor.currentUserCanReplyToComment(comment)).thenReturn(true);
        assertThat(
                test.buildReplyTo(comment),
                is(some(URI.create("http://foo/bar/plugins/servlet/streamscomments/reviews/123/456"))));
    }
}
