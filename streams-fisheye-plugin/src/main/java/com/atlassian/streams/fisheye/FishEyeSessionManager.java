package com.atlassian.streams.fisheye;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cenqua.crucible.hibernate.HibernateUtil;

import com.atlassian.streams.spi.SessionManager;

public final class FishEyeSessionManager implements SessionManager {
    private static final Logger log = LoggerFactory.getLogger(FishEyeSessionManager.class);

    @Override
    public <T> T withSession(Supplier<T> s) {
        // if there isn't a current session, one will be created for us when we do our querying.
        boolean sessionCreated = !HibernateUtil.isCurrentSession();

        try {
            return s.get();
        } finally {
            // If a new session was created for us, we need to explicitly close the Hibernate session indirectly
            // created since activity providers execute in a different thread than the request thread and hence are
            // not automatically cleaned up.
            if (sessionCreated && HibernateUtil.isCurrentSession()) {
                try {
                    HibernateUtil.closeSession();
                } catch (Exception e) {
                    log.warn("Error closing Hibernate session", e);
                }
            }
        }
    }
}
