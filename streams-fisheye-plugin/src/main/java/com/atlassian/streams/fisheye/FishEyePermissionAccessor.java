package com.atlassian.streams.fisheye;

import com.cenqua.fisheye.rep.RepositoryHandle;

/**
 * Exists as a wrapper around Crucible's classes which are heavily coupled with Hibernate in order to mock them out for our unit tests.
 */
public interface FishEyePermissionAccessor {
    /**
     * Returns true if the current user (logged in or anonymous) can create reviews, false if not.
     *
     * @return true if the current user (logged in or anonymous) can create reviews, false if not.
     */
    boolean isCreateReviewAllowed();

    /**
     * Returns true if the current user (logged in or anonymous) can view the specified {@link RepositoryHandle}, false if not.
     *
     * @param repository the repository
     * @return true if the current user (logged in or anonymous) can view the specified {@link RepositoryHandle}, false if not.
     */
    boolean currentUserCanView(RepositoryHandle repository);

    /**
     * Returns true if the current user (logged in or anonymous) is allowed to access the instance.
     *
     * @return true if the current user (logged in or anonymous) is allowed to access the instance, false if not.
     */
    boolean currentUserCanAccessInstance();
}
