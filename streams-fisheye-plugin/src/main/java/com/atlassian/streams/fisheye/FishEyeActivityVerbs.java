package com.atlassian.streams.fisheye;

import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.ActivityVerbs.VerbFactory;

import static com.atlassian.streams.api.ActivityVerbs.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityVerbs.newVerbFactory;
import static com.atlassian.streams.api.ActivityVerbs.post;

public final class FishEyeActivityVerbs {
    private static final String FISHEYE_IRI_BASE = ATLASSIAN_IRI_BASE + "fisheye/";
    private static final VerbFactory fisheyeVerbs = newVerbFactory(FISHEYE_IRI_BASE);

    public static ActivityVerb push() {
        return fisheyeVerbs.newVerb("push", post());
    }
}
