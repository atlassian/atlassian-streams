package com.atlassian.streams.fisheye.external.activity;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;

import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.plugin.util.ContextClassLoaderSwitchingUtil;

import static java.lang.Math.max;

/**
 * A ROME-based implementation of {@code ExternalActivityItemFactory}.
 */
public class RomeExternalActivityItemFactory implements ExternalActivityItemFactory {
    private static final String HYPERLINK_END_TAG = "</a>";

    private static final Logger log = LoggerFactory.getLogger(RomeExternalActivityItemFactory.class);

    public Iterable<ExternalActivityItem> getItems(final String feedContents) {
        List<ExternalActivityItem> itemList = new ArrayList<ExternalActivityItem>();

        // parse the feed (complete hack due to rome osgi classloading issues)
        RomeRunnable rome = new RomeRunnable(feedContents);
        ContextClassLoaderSwitchingUtil.runInContext(SyndFeed.class.getClassLoader(), rome);
        SyndFeed feed = rome.feed;

        // generate entries off of the feed
        for (Object entryObject : feed.getEntries()) {
            SyndEntry entry = (SyndEntry) entryObject;
            @SuppressWarnings("unchecked")
            List<SyndContent> contents = entry.getContents();
            SyndPerson person = (SyndPerson) entry.getAuthors().get(0);
            String author = person.getName();
            String username = getUsername(person);
            String entryText = contents.isEmpty()
                    ? entry.getDescription() != null ? entry.getDescription().getValue() : null
                    : contents.get(0).getValue();

            // must parse the username out from the front of the title and summary to get it in the format fecru
            // desires.
            int titleStartingIndex;
            if (entry.getTitle().contains(author + HYPERLINK_END_TAG)) {
                // in streams 4.0 usernames are linked
                titleStartingIndex = entry.getTitle().indexOf(author + HYPERLINK_END_TAG)
                        + (author + HYPERLINK_END_TAG).length()
                        + 1; // include trailing space
            } else if (entry.getTitle().contains(author)) {
                // in earlier streams usernames are not linked
                titleStartingIndex = entry.getTitle().indexOf(author) + author.length() + 1; // include trailing space
            } else {
                // backup situation in case an entry doesn't have a username
                titleStartingIndex = 0;
            }

            ExternalActivityItem activityItem = new ExternalActivityItemImpl.Builder()
                    .title(entry.getTitle().substring(titleStartingIndex))
                    .date(entry.getPublishedDate())
                    .author(author)
                    .username(username)
                    .url(entry.getLink())
                    .summary(entryText)
                    .build();
            itemList.add(activityItem);
        }

        return itemList;
    }

    private static class RomeRunnable implements Runnable {
        private final String feedContents;
        private SyndFeed feed;

        public RomeRunnable(String feedContents) {
            this.feedContents = feedContents;
        }

        public void run() {
            try {
                feed = new SyndFeedInput()
                        .build(new InputStreamReader(new ByteArrayInputStream(feedContents.getBytes())));
            } catch (FeedException e) {
                log.warn("Cannot parse remote feed contents from:\n" + feedContents, e);
            }
        }
    }

    /**
     * Another ROME-instigated hack due to the fact that we cannot parse/extract custom elements from {@code SyndPerson} instances
     *
     * @param author the entry author
     * @return the entry author's username
     */
    private String getUsername(SyndPerson author) {
        // unfortunately the author uri is not always provided.
        String authorUri = author.getUri();
        if (authorUri != null) {
            int usernameIndex =
                    max(authorUri.lastIndexOf("/"), max(authorUri.lastIndexOf("~"), authorUri.lastIndexOf("=")));
            return authorUri.substring(usernameIndex + 1);
        } else {
            // return full name as a last resort
            return author.getName();
        }
    }
}
