package com.atlassian.streams.fisheye;

import com.cenqua.crucible.model.Principal;
import com.cenqua.crucible.model.managers.SecureProjectManager;
import com.cenqua.crucible.model.managers.UserActionManager;
import com.cenqua.fisheye.AppConfig;
import com.cenqua.fisheye.config1.SecurityType;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.cenqua.fisheye.user.UserManager;

import com.atlassian.fecru.user.EffectiveUserProvider;

import static com.cenqua.crucible.model.Principal.Anonymous.isAnon;
import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyePermissionAccessorImpl implements FishEyePermissionAccessor {
    private final EffectiveUserProvider effectiveUserProvider;
    private final SecureProjectManager secureProjectManager;
    private final UserManager userManager;

    public FishEyePermissionAccessorImpl(
            EffectiveUserProvider effectiveUserProvider,
            SecureProjectManager secureProjectManager,
            UserManager userManager) {
        this.effectiveUserProvider = checkNotNull(effectiveUserProvider, "effectiveUserProvider");
        this.secureProjectManager = checkNotNull(secureProjectManager, "secureProjectManager");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public boolean isCreateReviewAllowed() {
        return secureProjectManager.isAtLeastOneProjectCanDoActionIn(
                getCurrentUser(), UserActionManager.Action.getByName(UserActionManager.ACTION_CREATE));
    }

    public boolean currentUserCanView(RepositoryHandle repository) {
        Principal user = getCurrentUser();
        return userManager.hasPermissionToAccess(user, repository)
                || (isAnon(user)
                        && AppConfig.getsConfig().isCruAnonAccessAllowed()
                        && repository.getCfg().isAnonAccessAllowed());
    }

    public boolean currentUserCanAccessInstance() {
        SecurityType security = AppConfig.getsConfig().getConfig().getSecurity();
        return security.getAllowAnon()
                || !Principal.Anonymous.ANON.equals(effectiveUserProvider.getEffectivePrincipal());
    }

    /**
     * Returns the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     * @return the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     */
    private Principal getCurrentUser() {
        return effectiveUserProvider.getEffectivePrincipal();
    }
}
