package com.atlassian.streams.fisheye;

import java.net.URI;

import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.base.Supplier;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;

/**
 * Finds FishEye objects to be used in streams production.
 */
public interface FishEyeFinder {
    /**
     * Get all {@code RepositoryHandle}s specified by the {@code ActivityRequest}'s filters.
     *
     * @param request the {@code ActivityRequest} containing filters
     * @return the {@code RepositoryHandle}s
     */
    Iterable<RepositoryHandle> getRepositories(ActivityRequest request);

    /**
     * Get all {@code StreamsEntry}s from the specified {@code RepositoryHandle} which match
     * the specified {@code ActivityRequest}'s filters.
     *
     * @param baseUri the base URI (context URI) of the application.
     * @param repository the {@code RepositoryHandle} from which changesets should be found
     * @param request the {@code ActivityRequest} containing filters
     * @return the {@code StreamsEntry}s
     *
     * @deprecated the {@link com.google.common.base.Supplier} parameter will be replaced with a
     *      {@link java.util.function.Supplier} in the next major version.
     */
    @Deprecated
    Iterable<StreamsEntry> getChangesets(
            URI baseUri, RepositoryHandle repository, ActivityRequest request, Supplier<Boolean> cancelled);
}
