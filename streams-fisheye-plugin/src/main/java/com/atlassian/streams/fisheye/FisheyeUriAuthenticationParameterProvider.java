package com.atlassian.streams.fisheye;

import javax.servlet.http.HttpServletRequest;

import com.cenqua.fisheye.rep.DbException;
import com.cenqua.fisheye.user.LoginCookie;
import com.cenqua.fisheye.user.UserLogin;
import com.cenqua.fisheye.user.UserManager;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.UriAuthenticationParameterProvider;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;

public class FisheyeUriAuthenticationParameterProvider implements UriAuthenticationParameterProvider {
    static final ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();

    private final UserManager userManager;

    private FisheyeUriAuthenticationParameterProvider(UserManager userManager) {
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public Option<Pair<String, String>> get() {
        UserLogin user = userManager.getCurrentUser(request.get());
        if (user == null) {
            return none();
        }
        try {
            LoginCookie lc = userManager.preCookUrl(request.get(), "/activity", true);
            return some(pair("FEAUTH", lc.encode()));
        } catch (DbException e) {
            throw new RuntimeException(e);
        }
    }
}
