package com.atlassian.streams.fisheye;

import com.cenqua.fisheye.config.RepositoryManager;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityResolver;

import static com.google.common.base.Preconditions.checkNotNull;

public class RepositoryEntityResolver implements EntityResolver {
    private final RepositoryManager repositoryManager;

    private RepositoryEntityResolver(RepositoryManager repositoryManager) {
        this.repositoryManager = checkNotNull(repositoryManager, "repositoryManager");
    }

    public Option<Object> apply(String key) {
        return Option.<Object>option(repositoryManager.getRepository(key));
    }
}
