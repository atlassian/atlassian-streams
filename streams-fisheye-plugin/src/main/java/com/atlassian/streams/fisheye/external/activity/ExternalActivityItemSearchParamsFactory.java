package com.atlassian.streams.fisheye.external.activity;

import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;

/**
 * Generates {@code ExternalActivityItemSearchParams} to look for remote {@code ExternalActivityItem}s.
 */
public interface ExternalActivityItemSearchParamsFactory {
    ExternalActivityItemSearchParams laterActivityParams(ExternalActivityItem item);

    ExternalActivityItemSearchParams earlierActivityParams(ExternalActivityItem item);
}
