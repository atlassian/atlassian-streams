package com.atlassian.streams.fisheye;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityObjectTypes.TypeFactory;

import static com.atlassian.streams.api.ActivityObjectTypes.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityObjectTypes.newTypeFactory;

public final class FishEyeActivityObjectTypes {
    private static final TypeFactory fisheyeTypes = newTypeFactory(ATLASSIAN_IRI_BASE);

    public static ActivityObjectType changeset() {
        return fisheyeTypes.newType("changeset");
    }

    public static ActivityObjectType repository() {
        return fisheyeTypes.newType("repository");
    }
}
