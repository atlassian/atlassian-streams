package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cenqua.fisheye.config.RepositoryManager;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.base.Joiner;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets.SetView;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.services.RecentChangesetsService;
import com.atlassian.fisheye.spi.services.RecentChangesetsService.QueryParameters;
import com.atlassian.fisheye.spi.services.RecentChangesetsService.QueryParameters.Builder;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.spi.CancelledException;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Sets.intersection;
import static java.util.Collections.emptyList;

import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Iterables.memoize;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Predicates.containsAnyIssueKey;
import static com.atlassian.streams.spi.Filters.getAllValues;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getIssueKeys;
import static com.atlassian.streams.spi.Filters.getNotIssueKeys;
import static com.atlassian.streams.spi.Filters.inDateRange;
import static com.atlassian.streams.spi.Filters.inProjectKeys;
import static com.atlassian.streams.spi.Filters.inUsers;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;

public class FishEyeFinderImpl implements FishEyeFinder {
    private static final Logger log = LoggerFactory.getLogger(FishEyeFinderImpl.class);

    private final RecentChangesetsService recentChangesetsService;
    private final RepositoryManager repositoryManager;
    private final FishEyeEntryFactory fishEyeEntryFactory;
    private final FishEyePermissionAccessor permissionAccessor;

    public FishEyeFinderImpl(
            RecentChangesetsService recentChangesetsService,
            FishEyeEntryFactory fishEyeEntryFactory,
            RepositoryManager repositoryManager,
            FishEyePermissionAccessor permissionAccessor) {
        this.recentChangesetsService = checkNotNull(recentChangesetsService, "recentChangesetsService");
        this.repositoryManager = checkNotNull(repositoryManager, "repositoryManager");
        this.fishEyeEntryFactory = checkNotNull(fishEyeEntryFactory, "fishEyeEntryFactory");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
    }

    public Iterable<RepositoryHandle> getRepositories(ActivityRequest request) {
        Collection<String> projectKeys =
                getIsValues(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList()));

        Stream<RepositoryHandle> repoHandles;
        if (!isEmpty(projectKeys)) {
            repoHandles = projectKeys.stream().map(toRepositoryHandles()).filter(Objects::nonNull);
        } else {
            repoHandles = repositoryManager.getHandles().stream();
        }

        // Filter out any repository handles that match the NOT project_key filter option
        return repoHandles.filter(repoName(inProjectKeys(request))).collect(Collectors.toList());
    }

    public Iterable<StreamsEntry> getChangesets(
            final URI baseUri,
            final RepositoryHandle repository,
            final ActivityRequest request,
            Supplier<Boolean> cancelled) {
        if (!permissionAccessor.currentUserCanView(repository) || !repository.isRunning()) {
            return ImmutableList.of();
        }

        Stream<ChangesetDataFE> changesets;
        Set<String> users = getIsValues(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList()));
        if (!isEmpty(users)) {
            changesets = users.stream()
                    .flatMap(toChangesetsPerUser(request, repository.getName(), cancelled::get))
                    .sorted(byCommitDate);
        } else {
            changesets = listChangesets(
                    repository.getName(), getQueryParametersBuilder(request).build())
                    .stream();
        }

        // Filter out any changesets that match the NOT filter_user filter option
        if (!isEmpty(getAllValues(NOT, request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList())))) {
            changesets = changesets.filter(committedBy(inUsers(request)));
        }

        // Filter out any changesets that don't match date range, issue key, or branch filters
        Predicate<ChangesetDataFE> filters = commitDate(inDateRange(request))
                .and(
                        isEmpty(getIssueKeys(request))
                                ? cs -> true
                                : commitComment(containsAnyIssueKey(getIssueKeys(request))))
                .and(
                        isEmpty(getNotIssueKeys(request))
                                ? cs -> true
                                : commitComment(containsAnyIssueKey(getNotIssueKeys(request))
                                        .negate()))
                .and(hasCorrectBranch(request));

        return memoize(take(
                request.getMaxResults(),
                changesets
                        .filter(filters)
                        .map(toStreamsEntry(baseUri, repository))
                        .flatMap(opt -> opt.map(Stream::of).orElseGet(Stream::of))
                        .collect(Collectors.toList())));
    }

    private static final Ordering<ChangesetDataFE> byCommitDate = (new Ordering<ChangesetDataFE>() {
                public int compare(ChangesetDataFE lhs, ChangesetDataFE rhs) {
                    int d = lhs.getDate().compareTo(rhs.getDate());
                    if (d != 0) {
                        return d;
                    }
                    return lhs.getCsid().compareTo(rhs.getCsid());
                }
            })
            .reverse();

    private Function<ChangesetDataFE, Optional<StreamsEntry>> toStreamsEntry(
            final URI baseUri, final RepositoryHandle repository) {
        return changeset -> {
            try {
                return Optional.of(fishEyeEntryFactory.getEntry(baseUri, changeset, repository));
            } catch (Exception e) {
                log.warn("Error creating streams entry", e);
                return Optional.empty();
            }
        };
    }

    private Function<String, RepositoryHandle> toRepositoryHandles() {
        return new ToRepositoryHandles();
    }

    private final class ToRepositoryHandles implements Function<String, RepositoryHandle> {
        public RepositoryHandle apply(String key) {
            return repositoryManager.getRepository(key);
        }
    }

    private Function<String, Stream<ChangesetDataFE>> toChangesetsPerUser(
            final ActivityRequest request, final String repositoryName, final BooleanSupplier cancelled) {
        return username -> {
            if (StringUtils.isEmpty(username)) {
                return Stream.of();
            }
            if (cancelled.getAsBoolean()) {
                throw new CancelledException();
            }
            final Builder queryBuilder = getQueryParametersBuilder(request).committer(username);
            return listChangesets(repositoryName, queryBuilder.build()).stream();
        };
    }

    private Collection<ChangesetDataFE> listChangesets(String repositoryName, QueryParameters parameters) {
        // This will handle STRM-1593, so that the fisheye feed will not fail when trying to anonymously retrieve
        // a changeset from a particular repository.
        try {
            return recentChangesetsService.listChangesets(repositoryName, parameters);
        } catch (Exception e) {
            log.error("Error retrieving changesets", e);
            return ImmutableList.of();
        }
    }

    private Builder getQueryParametersBuilder(ActivityRequest request) {
        Iterable<String> issueKeys = getIssueKeys(request);
        final Builder queryBuilder = recentChangesetsService.getQueryBuilder();
        if (!isEmpty(issueKeys)) {
            queryBuilder.comment(Joiner.on(" ").join(issueKeys));
        }
        return queryBuilder;
    }

    private Predicate<RepositoryHandle> repoName(Predicate<String> p) {
        return new RepoName(p);
    }

    private static class RepoName implements Predicate<RepositoryHandle> {
        private final Predicate<String> p;

        public RepoName(Predicate<String> p) {
            this.p = p;
        }

        public boolean test(RepositoryHandle repoHandle) {
            return p.test(repoHandle.getName());
        }

        @Override
        public String toString() {
            return String.format("repoName(%s)", p);
        }
    }

    private Predicate<ChangesetDataFE> committedBy(Predicate<String> p) {
        return new CommittedBy(p);
    }

    private static class CommittedBy implements Predicate<ChangesetDataFE> {
        private final Predicate<String> p;

        public CommittedBy(Predicate<String> p) {
            this.p = p;
        }

        public boolean test(ChangesetDataFE changeset) {
            return p.test(changeset.getAuthor());
        }

        @Override
        public String toString() {
            return String.format("committedBy(%s)", p);
        }
    }

    private Predicate<ChangesetDataFE> commitDate(final Predicate<Date> p) {
        return new CommitDate(p);
    }

    private static class CommitDate implements Predicate<ChangesetDataFE> {
        private final Predicate<Date> p;

        public CommitDate(Predicate<Date> p) {
            this.p = p;
        }

        public boolean test(ChangesetDataFE changeset) {
            return p.test(changeset.getDate());
        }

        @Override
        public String toString() {
            return String.format("commitDate(%s)", p);
        }
    }

    private static Predicate<ChangesetDataFE> commitComment(final Predicate<String> p) {
        return new CommitComment(p);
    }

    private static final class CommitComment implements Predicate<ChangesetDataFE> {
        private final Predicate<String> p;

        private CommitComment(Predicate<String> p) {
            this.p = p;
        }

        public boolean test(ChangesetDataFE changeset) {
            return p.test(changeset.getComment());
        }

        @Override
        public String toString() {
            return String.format("commitComment(%s)", p);
        }
    }

    /** @deprecated function will be made package-private in the next major version. */
    @Deprecated
    protected com.google.common.base.Predicate<ChangesetDataFE> hasCorrectBranch(ActivityRequest request) {
        return new HasCorrectBranch(request);
    }

    private static class HasCorrectBranch implements com.google.common.base.Predicate<ChangesetDataFE> {
        private final ActivityRequest request;

        public HasCorrectBranch(ActivityRequest request) {
            this.request = request;
        }

        public boolean apply(final ChangesetDataFE changeset) {
            if (changeset == null) {
                return false;
            }

            return request
                    .getProviderFiltersMap()
                    .getOrDefault(FishEyeFilterOptionProvider.BRANCH, emptyList())
                    .stream()
                    .allMatch(filter -> {
                        SetView<String> intersection =
                                intersection(changeset.getBranches(), ImmutableSet.copyOf(filter.second()));
                        if (Operator.IS.equals(filter.first())) { // we are looking for an intersection
                            return !intersection.isEmpty();
                        }
                        if (NOT.equals(filter.first())) { // we are looking for NO intersection
                            return intersection.isEmpty();
                        }
                        // the only operators we should really see are IS and NOT, but this is a
                        // fail-safe
                        return false;
                    });
        }
    }
    ;
}
