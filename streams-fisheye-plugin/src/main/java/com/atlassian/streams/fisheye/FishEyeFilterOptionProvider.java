package com.atlassian.streams.fisheye;

import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOption.Builder;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.StreamsFilterType.STRING_EXACT;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.changeset;
import static com.atlassian.streams.fisheye.FishEyeActivityVerbs.push;

public class FishEyeFilterOptionProvider implements StreamsFilterOptionProvider {
    private final I18nResolver i18nResolver;
    public static final String BRANCH = "branch";

    public FishEyeFilterOptionProvider(I18nResolver i18nResolver) {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public Iterable<StreamsFilterOption> getFilterOptions() {
        return ImmutableList.of(getBranchFilter());
    }

    public Iterable<ActivityOption> getActivities() {
        return ImmutableList.of(
                new ActivityOption(i18nResolver.getText("streams.filter.fisheye.changeset.push"), changeset(), push()));
    }

    private StreamsFilterOption getBranchFilter() {
        return new Builder(BRANCH, STRING_EXACT)
                .displayName(i18nResolver.getText("streams.filter.fisheye.branch"))
                .helpTextI18nKey("streams.filter.help.fisheye.branch")
                .i18nKey("streams.filter.fisheye.branch")
                .unique(true)
                .build();
    }
}
