package com.atlassian.streams.fisheye.external.activity;

import java.util.Date;

import com.atlassian.fisheye.activity.ExternalActivityItem;

public class ExternalActivityItemImpl implements ExternalActivityItem {
    private final String author;
    private final Date date;
    private final String summary;
    private final String title;
    private final String url;
    private final String username;

    public ExternalActivityItemImpl(Builder builder) {
        this.author = builder.author;
        this.date = builder.date;
        this.summary = builder.summary;
        this.title = builder.title;
        this.url = builder.url;
        this.username = builder.username;
    }

    public String getAuthor() {
        return author;
    }

    public Date getDate() {
        return date;
    }

    public String getSummary() {
        return summary;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public static class Builder {
        private String author;
        private Date date;
        private String summary;
        private String title;
        private String url;
        private String username;

        public ExternalActivityItem build() {
            return new ExternalActivityItemImpl(this);
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder date(Date date) {
            this.date = date;
            return this;
        }

        public Builder summary(String summary) {
            this.summary = summary;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }
    }
}
