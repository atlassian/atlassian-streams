package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cenqua.fisheye.config.RepositoryManager;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.repository;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;

public class FishEyeEntityAssociationProvider implements StreamsEntityAssociationProvider {
    private static final String PROJECT_URL_PREFIX = "/browse/";

    /**
     * Regex to parse project URIs.
     *
     * Group 1: project key, e.g. STRM
     * Group 2: request parameters and hash
     */
    private static final Pattern PROJECT_PATTERN = Pattern.compile("([^\\?#]+)(.*)");

    private final ApplicationProperties applicationProperties;
    private final RepositoryManager repositoryManager;
    private final FishEyePermissionAccessor permissionAccessor;

    public FishEyeEntityAssociationProvider(
            ApplicationProperties applicationProperties,
            RepositoryManager repositoryManager,
            FishEyePermissionAccessor permissionAccessor) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.repositoryManager = checkNotNull(repositoryManager, "repositoryManager");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
    }

    @Override
    public Iterable<EntityIdentifier> getEntityIdentifiers(URI target) {
        String targetStr = target.toString();
        if (target.isAbsolute()) {
            // the target URI must reference an entity on this server
            if (!targetStr.startsWith(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX)) {
                return ImmutableList.of();
            }
            return matchEntities(
                    targetStr.substring(applicationProperties.getBaseUrl().length() + PROJECT_URL_PREFIX.length()));
        } else {
            return matchEntities(targetStr);
        }
    }

    @Override
    public Option<URI> getEntityURI(EntityIdentifier identifier) {
        if (identifier.getType().equals(repository().iri())) {
            return some(URI.create(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX + identifier.getValue()));
        }
        return none();
    }

    @Override
    public Option<String> getFilterKey(EntityIdentifier identifier) {
        if (identifier.getType().equals(repository().iri())) {
            return some(PROJECT_KEY);
        }
        return none();
    }

    @Override
    public Option<Boolean> getCurrentUserViewPermission(EntityIdentifier identifier) {
        if (identifier.getType().equals(repository().iri())) {
            RepositoryHandle repo = repositoryManager.getRepository(identifier.getValue());
            if (repo != null) {
                return some(Boolean.valueOf(permissionAccessor.currentUserCanView(repo)));
            }
        }
        return none();
    }

    @Override
    public Option<Boolean> getCurrentUserEditPermission(EntityIdentifier identifier) {
        // There doesn't seem to be an appropriate permission for this in Fisheye
        return getCurrentUserViewPermission(identifier);
    }

    private Iterable<EntityIdentifier> matchEntities(String input) {
        Matcher matcher = PROJECT_PATTERN.matcher(input);
        // the target URI is of an unknown format
        if (!matcher.matches()) {
            return ImmutableList.of();
        }

        String repoKey = matcher.group(1);
        RepositoryHandle repo = repositoryManager.getRepository(repoKey);
        if (repo != null) {
            URI canonicalUri = URI.create(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX + repoKey);
            return ImmutableList.of(new EntityIdentifier(repository().iri(), repoKey, canonicalUri));
        } else {
            return ImmutableList.of();
        }
    }

    @Override
    public Optional<Boolean> getCurrentUserViewPermissionForTargetlessEntity() {
        return Optional.of(permissionAccessor.currentUserCanAccessInstance());
    }
}
