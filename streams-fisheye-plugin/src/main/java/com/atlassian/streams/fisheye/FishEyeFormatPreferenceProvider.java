package com.atlassian.streams.fisheye;

import java.time.ZoneId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cenqua.crucible.model.Principal;
import com.cenqua.fisheye.AppConfig;
import com.cenqua.fisheye.rep.DbException;

import com.atlassian.fecru.user.EffectiveUserProvider;
import com.atlassian.streams.spi.DefaultFormatPreferenceProvider;

import static com.cenqua.crucible.model.Principal.Anonymous.isAnon;
import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyeFormatPreferenceProvider extends DefaultFormatPreferenceProvider {
    private static final Logger log = LoggerFactory.getLogger(FishEyeFormatPreferenceProvider.class);
    private final EffectiveUserProvider effectiveUserProvider;

    public FishEyeFormatPreferenceProvider(EffectiveUserProvider effectiveUserProvider) {
        this.effectiveUserProvider = checkNotNull(effectiveUserProvider, "effectiveUserProvider");
    }

    @Override
    public ZoneId getUserTimeZoneId() {
        Principal user = effectiveUserProvider.getEffectivePrincipal();

        if (!isAnon(user)) {
            try {
                return AppConfig.getUserTimeZone(user.getUserName()).toZoneId();
            } catch (DbException e) {
                log.debug("Unable to get user timezone. Using system timezone.", e);
            }
        }

        return AppConfig.getsConfig().getTimezone().toZoneId();
    }
}
