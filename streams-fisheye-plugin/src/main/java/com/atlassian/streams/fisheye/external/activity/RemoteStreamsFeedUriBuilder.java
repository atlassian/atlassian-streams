package com.atlassian.streams.fisheye.external.activity;

import java.net.URI;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;

public interface RemoteStreamsFeedUriBuilder {
    URI buildUri(ApplicationLink appLink, ExternalActivityItemSearchParams params);
}
