package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.cenqua.fisheye.RepositoryConfig;
import com.cenqua.fisheye.config1.RepositoryType;
import com.cenqua.fisheye.rep.RepositoryHandle;

import com.atlassian.crucible.spi.data.UserData;
import com.atlassian.crucible.spi.services.ServerException;
import com.atlassian.crucible.spi.services.UserService;
import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.fisheye.FishEyeStreamsActivityProvider.CHANGESET_REVIEW_REL;
import static com.atlassian.streams.testing.matchers.Matchers.hasStreamsLink;
import static com.atlassian.streams.testing.matchers.Matchers.whereStreamsRel;

@RunWith(MockitoJUnitRunner.class)
public class FishEyeEntryFactoryTest {
    private static final URI BASE_URI = URI.create("http://localhost:3990/streams");
    private static final String REPO = "some_repo";
    private static final String COMMITTER = "someone";

    @Mock
    private ChangesetDataFE changeSet;

    @Mock
    private RepositoryHandle repositoryHandle;

    @Mock
    private RepositoryConfig cfg;

    @Mock
    private RepositoryType type;

    @Mock
    private UserService userService;

    @Mock
    private UserProfileAccessor userProfileAccessor;

    @Mock
    private ChangeSetRendererFactory rendererFactory;

    @Mock
    private StreamsEntry.Renderer renderer;

    @Mock
    private UriProvider uriProvider;

    @Mock
    private FishEyePermissionAccessor permissionAccessor;

    @Mock
    private StreamsI18nResolver i18nResolver;

    private FishEyeEntryFactory fishEyeEntryFactory;

    @Before
    public void createFishEyeEntryFactory() throws ServerException {
        setUpRepositoryHandle();
        setUpChangesetData();

        when(i18nResolver.getText("streams.item.fisheye.tooltip.changeset")).thenReturn("tooltip");

        when(userService.getMappedUser(REPO, COMMITTER))
                .thenReturn(new UserData(
                        "someone_else", "someone_else_name", "http://www.gravatar.com/avatar/someone_else"));

        when(userProfileAccessor.getUserProfile(any(), anyString()))
                .thenReturn(new UserProfile.Builder("fred").build());

        when(uriProvider.getChangeSetUri(BASE_URI, changeSet, repositoryHandle))
                .thenReturn(URI.create("http://example.com/TST?cs=1234"));
        when(uriProvider.getRepositoryUri(BASE_URI, repositoryHandle)).thenReturn(URI.create("http://example.com/TST"));

        when(rendererFactory.newRenderer(any(), any(), any())).thenReturn(renderer);

        when(permissionAccessor.isCreateReviewAllowed()).thenReturn(true);

        fishEyeEntryFactory = new FishEyeEntryFactoryImpl(
                userService, userProfileAccessor, uriProvider, rendererFactory, permissionAccessor, i18nResolver);
    }

    public void setUpRepositoryHandle() {
        when(type.getDescription()).thenReturn("Test");
        when(cfg.getRepositoryTypeConfig()).thenReturn(type);
        when(repositoryHandle.getCfg()).thenReturn(cfg);
        when(repositoryHandle.getName()).thenReturn("TST");
    }

    public void setUpChangesetData() {
        when(changeSet.getDate()).thenReturn(new Date(10L));
        when(changeSet.getRepositoryName()).thenReturn(REPO);
        when(changeSet.getAuthor()).thenReturn(COMMITTER);
        when(changeSet.getCsid()).thenReturn("1234");
    }

    @Test
    public void testGetEntryFromReviewReturnsEntry() {
        assertNotNull(fishEyeEntryFactory.getEntry(BASE_URI, changeSet, repositoryHandle));
    }

    @Test
    public void testChangesetReviewInlineActionLinkIsAddedWhenAllowedToCreateReviews() {
        Collection<StreamsEntry.Link> linkValues =
                fishEyeEntryFactory.getEntry(BASE_URI, changeSet, repositoryHandle).getLinksMap().values().stream()
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());

        assertThat(linkValues, hasStreamsLink(whereStreamsRel(is(equalTo(CHANGESET_REVIEW_REL)))));
    }

    @Test
    public void testChangesetReviewInlineActionLinkIsNotAddedWhenNotAllowedToCreateReviews() {
        when(permissionAccessor.isCreateReviewAllowed()).thenReturn(false);

        Collection<StreamsEntry.Link> linkValues =
                fishEyeEntryFactory.getEntry(BASE_URI, changeSet, repositoryHandle).getLinksMap().values().stream()
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());

        assertThat(linkValues, not(hasStreamsLink(whereStreamsRel(is(equalTo(CHANGESET_REVIEW_REL))))));
    }
}
