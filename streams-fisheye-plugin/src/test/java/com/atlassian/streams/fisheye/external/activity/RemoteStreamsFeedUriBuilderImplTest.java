package com.atlassian.streams.fisheye.external.activity;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.crucible.spi.data.ProjectData;
import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;
import com.atlassian.fisheye.spi.data.RepositoryDataFE;
import com.atlassian.fisheye.spi.services.RepositoryService;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory;
import com.atlassian.streams.testing.builder.ActivityStreamsTestUriBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemoteStreamsFeedUriBuilderImplTest {
    private static final String BASE_URL = "http://localhost:2990/streams";
    private static final String SERVLET_URL = "http://localhost:2990/streams/plugins/servlet/streams";

    @Mock
    private StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory;

    @Mock
    private ProjectService projectService;

    @Mock
    private RepositoryService repositoryService;

    @Mock
    private EntityLinkService entityLinkService;

    @Mock
    private ApplicationLink appLink;

    @Mock
    private ApplicationLink someOtherAppLink;

    @Mock
    private ExternalActivityItemSearchParams searchParams;

    @Mock
    private ProjectData projectData;

    @Mock
    private RepositoryDataFE repository;

    private RemoteStreamsFeedUriBuilderImpl uriBuilder;

    @Before
    public void setUp() {
        uriBuilder = new RemoteStreamsFeedUriBuilderImpl(
                streamsFeedUriBuilderFactory, projectService, repositoryService, entityLinkService);

        when(appLink.getRpcUrl()).thenReturn(URI.create(BASE_URL));
        ActivityStreamsTestUriBuilder testUriBuilder = new ActivityStreamsTestUriBuilder(BASE_URL);
        when(streamsFeedUriBuilderFactory.getStreamsFeedUriBuilder(anyString())).thenReturn(testUriBuilder);
        when(searchParams.getMaxItems()).thenReturn(20);
        when(projectService.getAllProjects()).thenReturn(ImmutableList.of(projectData));
        when(repositoryService.listRepositories()).thenReturn(ImmutableList.of(repository));
        when(projectService.getProject(anyInt())).thenReturn(projectData);
        when(repositoryService.getRepositoryInfo(anyString())).thenReturn(repository);
        when(projectData.getKey()).thenReturn("CR-ONE");
        when(entityLinkService.getEntityLinks(any(), any())).thenReturn(ImmutableList.of());
    }

    @Test
    public void assertThatBuildUriForFisheyeDashboardWithNoEntityLinksGeneratesUnfilteredUri() {
        setSearchParams(null, null);
        setEntityLinks(any());

        assertThat(
                uriBuilder.buildUri(appLink, searchParams).toASCIIString(),
                is(equalTo(SERVLET_URL + "?local=true&maxResults=20")));
    }

    @Test
    public void assertThatBuildUriForFisheyeDashboardWithEntityLinksGeneratesFilteredUri() {
        setSearchParams(null, null);
        setupEntityLinks();

        assertThat(
                uriBuilder.buildUri(appLink, searchParams).toASCIIString(),
                is(equalTo(SERVLET_URL + "?local=true&key=RED&key=JONE&maxResults=20")));
    }

    @Test
    public void assertThatBuildUriForRepositoryViewGeneratesUriFilteredByRepositoryEntityLink() {
        setSearchParams(null, "ONE");
        setupEntityLinks();

        assertThat(
                uriBuilder.buildUri(appLink, searchParams).toASCIIString(),
                is(equalTo(SERVLET_URL + "?local=true&key=JONE&maxResults=20")));
    }

    @Test
    public void testBuildUriForCrucibleProjectViewGeneratesUriFilteredByProjectEntityLink() {
        setSearchParams(1, null);
        setupEntityLinks();

        assertThat(
                uriBuilder.buildUri(appLink, searchParams).toASCIIString(),
                is(equalTo(SERVLET_URL + "?local=true&key=RED&maxResults=20")));
    }

    @Test
    public void assertThatBuildUriForRepositoryViewWithNoEntityLinkGeneratesUriFilteredByRepositoryName() {
        setSearchParams(null, "REPO");

        assertThat(
                uriBuilder.buildUri(appLink, searchParams).toASCIIString(),
                is(equalTo(SERVLET_URL + "?local=true&key=REPO&maxResults=20")));
    }

    @Test
    public void testBuildUriForCrucibleProjectViewWithNoEntityLinkGeneratesUriFilteredByProjectName() {
        setSearchParams(1, null);
        when(projectData.getKey()).thenReturn("CR-UCPROJ");

        assertThat(
                uriBuilder.buildUri(appLink, searchParams).toASCIIString(),
                is(equalTo(SERVLET_URL + "?local=true&key=UCPROJ&maxResults=20")));
    }

    private void setupEntityLinks() {
        EntityLink link1 = newEntityLink("JONE", appLink);
        EntityLink link2 = newEntityLink("JTWO", someOtherAppLink);
        EntityLink link3 = newEntityLink("RED", appLink);
        EntityLink link4 = newEntityLink("BLUE", someOtherAppLink);
        setEntityLinks(eq(projectData), link3, link4);
        setEntityLinks(eq(repository), link1, link2);
    }

    private void setSearchParams(Integer projectId, String repo) {
        when(searchParams.getProjectId()).thenReturn(projectId);
        when(searchParams.getRepFilter()).thenReturn(repo);
    }

    private EntityLink newEntityLink(String key, ApplicationLink appLink) {
        EntityLink link = mock(EntityLink.class);
        when(link.getKey()).thenReturn(key);
        when(link.getApplicationLink()).thenReturn(appLink);

        return link;
    }

    private void setEntityLinks(Object entity, EntityLink... entityLinks) {
        when(entityLinkService.getEntityLinks(entity, eq(JiraProjectEntityType.class)))
                .thenReturn(ImmutableList.copyOf(entityLinks));
    }
}
