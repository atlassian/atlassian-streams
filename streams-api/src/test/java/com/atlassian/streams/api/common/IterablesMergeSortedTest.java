package com.atlassian.streams.api.common;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import com.google.common.collect.ImmutableList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

import static com.atlassian.streams.api.common.Iterables.mergeSorted;

public class IterablesMergeSortedTest {
    @Test
    public void assertThatMergingEmptyIterablesGivesAnEmptyIterable() {
        assertThat(
                mergeSorted(
                        ImmutableList.of(new ArrayList<String>(), new LinkedList<String>()), Comparator.naturalOrder()),
                is(emptyIterable(String.class)));
    }

    @Test
    public void assertThatMergingNonEmptyAndEmptyIterablesGivesTheMergedIterable() {
        assertThat(
                mergeSorted(
                        ImmutableList.of(ImmutableList.of("a"), ImmutableList.<String>of()), Comparator.naturalOrder()),
                contains("a"));
    }

    @Test
    public void assertThatMergingEmptyAndNonEmptyIterablesGivesTheMergedIterable() {
        assertThat(
                mergeSorted(
                        ImmutableList.of(ImmutableList.<String>of(), ImmutableList.of("a")), Comparator.naturalOrder()),
                contains("a"));
    }

    @Test
    public void assertThatMergingNonEmptyIterablesInOrderGivesMergedIterable() {
        assertThat(
                mergeSorted(ImmutableList.of(ImmutableList.of("a"), ImmutableList.of("b")), Comparator.naturalOrder()),
                contains("a", "b"));
    }

    @Test
    public void assertThatMergingNonEmptyIterablesOutOfOrderGivesMergedIterable() {
        assertThat(
                mergeSorted(ImmutableList.of(ImmutableList.of("b"), ImmutableList.of("a")), Comparator.naturalOrder()),
                contains("a", "b"));
    }

    @Test
    public void assertThatMergingNonEmptyIterablesOutOfOrderGivesMergedIterableInOrder() {
        assertThat(
                mergeSorted(
                        ImmutableList.of(ImmutableList.of("b", "d"), ImmutableList.of("a", "c", "e")),
                        Comparator.naturalOrder()),
                contains("a", "b", "c", "d", "e"));
    }

    private static <A> Matcher<java.lang.Iterable<? extends A>> emptyIterable(Class<A> a) {
        return Matchers.<A>emptyIterable();
    }
}
