package com.atlassian.streams.api.common;

import org.junit.Test;
import com.google.common.collect.ImmutableList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.streams.api.common.Predicates.containsAnyIssueKey;
import static com.atlassian.streams.api.common.Predicates.containsIssueKeyPredicate;

public class PredicatesTest {
    @Test
    public void assertThatContainsIssueKeyMatchesIssueKey() {
        assertTrue(containsIssueKeyPredicate("JRA-9").test("Let me tell you the story of JRA-9"));
    }

    @Test
    public void assertThatContainsIssueKeyDoesNotMatchCrucibleReviewKeys() {
        assertFalse(containsIssueKeyPredicate("JRA-9").test("JRA-12 / CR-JRA-9: Fixed indentation"));
    }

    @Test
    public void assertThatContainsIssueKeyDoesNotFindTrivialSubstringMatches() {
        assertFalse(containsIssueKeyPredicate("ONE-23").test("NONE-23 or ONE-234"));
    }

    @Test
    public void assertThatContainsAnyIssueKeyFindsAnIssueKey() {
        assertTrue(containsAnyIssueKey(ImmutableList.of("ONE-1", "JRA-9", "JRA-1330"))
                .test("Is JRA-1330 next?"));
    }

    @Test
    public void assertThatContainsAnyIssueKeyReturnsFalseWithNoKeys() {
        assertFalse(containsAnyIssueKey(ImmutableList.of()).test("Is JRA-1330 next?"));
    }

    @Test
    public void assertThatContainsAnyIssueKeyReturnsFalseWhenNoIssueKeysMatch() {
        assertFalse(containsAnyIssueKey(ImmutableList.of("JRA-9")).test("No issue keys here..."));
    }
}
