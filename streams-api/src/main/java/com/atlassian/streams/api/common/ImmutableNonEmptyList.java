package com.atlassian.streams.api.common;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;

/**
 * An immutable non-empty list implementation. Implements {@code NonEmptyIterable}, and is backed by an
 * {@link com.google.common.collect.ImmutableList}.
 * @param <T> the type of elements stored in the list
 */
public class ImmutableNonEmptyList<T> implements List<T>, NonEmptyIterable<T> {
    private final ImmutableList<T> delegate;

    public ImmutableNonEmptyList(T head) {
        checkNotNull(head, "head");
        this.delegate = ImmutableList.of(head);
    }

    public ImmutableNonEmptyList(T head, Iterable<T> tail) {
        checkNotNull(head, "head");
        this.delegate = ImmutableList.<T>builder().add(head).addAll(tail).build();
    }

    public ImmutableNonEmptyList(NonEmptyIterable<T> items) {
        checkNotNull(items, "items");
        this.delegate = ImmutableList.copyOf(items);
    }

    public static <E> ImmutableNonEmptyList<E> of(E e) {
        return new ImmutableNonEmptyList<>(e);
    }

    public static <E> ImmutableNonEmptyList<E> of(E e1, E e2) {
        return new ImmutableNonEmptyList<>(e1, ImmutableList.of(e2));
    }

    public static <E> ImmutableNonEmptyList<E> of(E e, E... others) {
        return new ImmutableNonEmptyList<>(e, asList(others));
    }

    public static <E> ImmutableNonEmptyList<E> of(E e, Iterable<E> others) {
        return new ImmutableNonEmptyList<>(e, others);
    }

    public static <E> ImmutableNonEmptyList<E> copyOf(NonEmptyIterable<E> items) {
        return (items instanceof ImmutableNonEmptyList)
                ? ((ImmutableNonEmptyList<E>) items)
                : new ImmutableNonEmptyList<>(items);
    }

    @Override
    public boolean add(T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(@Nonnull Collection<? extends T> elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, @Nonnull Collection<? extends T> elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(@CheckForNull Object object) {
        return delegate.contains(object);
    }

    @Override
    public boolean containsAll(@Nonnull Collection<?> collection) {
        return delegate.containsAll(collection);
    }

    @Override
    public T get(int index) {
        return delegate.get(index);
    }

    @Override
    public int indexOf(@CheckForNull Object element) {
        return delegate.indexOf(element);
    }

    @Override
    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    @Override
    @Nonnull
    public Iterator<T> iterator() {
        return delegate.iterator();
    }

    @Override
    public int lastIndexOf(@CheckForNull Object element) {
        return delegate.lastIndexOf(element);
    }

    @Override
    @Nonnull
    public ListIterator<T> listIterator() {
        return delegate.listIterator();
    }

    @Override
    @Nonnull
    public ListIterator<T> listIterator(int index) {
        return delegate.listIterator(index);
    }

    @Override
    public boolean remove(Object object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@Nonnull Collection<?> elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@Nonnull Collection<?> elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    @Nonnull
    public List<T> subList(int fromIndex, int toIndex) {
        return delegate.subList(fromIndex, toIndex);
    }

    @Override
    @Nonnull
    public Object[] toArray() {
        return delegate.toArray();
    }

    @Override
    @Nonnull
    public <U> U[] toArray(@Nonnull U[] array) {
        return delegate.toArray(array);
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    @Override
    public boolean equals(@CheckForNull Object object) {
        return object == this || delegate.equals(object);
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }
}
