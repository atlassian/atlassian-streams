package com.atlassian.streams.api.common;

import java.net.URI;

import static org.apache.commons.lang3.StringUtils.trimToNull;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Option.option;

/**
 * Common function implementations.
 */
@Deprecated
public final class Functions {
    private Functions() {}

    /**
     * @deprecated - use method reference to {@link Integer#valueOf(String)} instead. And don't use Either
     */
    @Deprecated
    public static java.util.function.Function<String, Either<NumberFormatException, Integer>> parseInt() {
        return s -> {
            try {
                return right(Integer.valueOf(s));
            } catch (NumberFormatException e) {
                return left(e);
            }
        };
    }

    @Deprecated
    public static Option<String> trimToNone(String s) {
        return option(trimToNull(s));
    }

    URI toUri(String from) {
        return URI.create(from);
    }
}
