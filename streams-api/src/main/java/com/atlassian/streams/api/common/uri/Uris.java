package com.atlassian.streams.api.common.uri;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMultimap;

import com.atlassian.streams.api.common.Pair;

import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;

import static com.atlassian.streams.api.common.Pair.pair;

public final class Uris {
    private Uris() {
        throw new RuntimeException("UriEncoder cannot be instantiated");
    }

    public static String encode(final String uriComponent) {
        try {
            return URLEncoder.encode(uriComponent, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Funny JVM you have here", e);
        }
    }

    public static String encode(final String uriComponent, String encoding) {
        try {
            return URLEncoder.encode(uriComponent, encoding);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String decode(String uriComponent) {
        try {
            return URLDecoder.decode(uriComponent, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Funny JVM you have here", e);
        }
    }

    public static String decode(String uriComponent, String encoding) {
        try {
            return URLDecoder.decode(uriComponent, encoding);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Collection<String>> getQueryParams(URI uri) {
        if (uri.getQuery() == null) {
            return Collections.emptyMap();
        }
        ImmutableMultimap.Builder<String, String> builder = ImmutableMultimap.builder();
        for (Pair<String, String> param : transform(asList(uri.getQuery().split("&")), asQueryParam())) {
            builder.put(param.first(), param.second());
        }
        return builder.build().asMap();
    }

    /**
     * @deprecated Use method reference to {@link #asQueryParam(String)} instead
     */
    @Deprecated
    private static Function<String, Pair<String, String>> asQueryParam() {
        return AsQueryParam.INSTANCE;
    }

    /**
     * @deprecated Use method reference to {@link #asQueryParam(String)} instead
     */
    @Deprecated
    private enum AsQueryParam implements Function<String, Pair<String, String>> {
        INSTANCE;

        public Pair<String, String> apply(String p) {
            String[] nameValue = p.split("=");
            String name = decode(nameValue[0]);
            String value = nameValue.length == 2 ? decode(nameValue[1]) : "";
            return pair(name, value);
        }
    }

    public static Pair<String, String> asQueryParam(String p) {
        String[] nameValue = p.split("=");
        String name = decode(nameValue[0]);
        String value = nameValue.length == 2 ? decode(nameValue[1]) : "";
        return pair(name, value);
    }
}
