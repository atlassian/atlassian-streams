package com.atlassian.streams.api.common;

import java.util.function.BiFunction;
import java.util.function.Predicate;

import com.google.common.collect.ImmutableList;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

import static com.atlassian.streams.api.common.Fold.foldl;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;

/**
 * Useful methods for working with {@code Pair}s of values.
 */
public final class Pairs {
    /**
     * Make {@code Pair}s out of the values in {@code xs}.  The result will look like
     * {@code [(x0, x1), (x2, x3), ..., (xN-1, xN)]}
     *
     * @param <A> type of {@code xs}
     * @param xs list of values
     * @return pairs of values
     */
    public static <A> Iterable<Pair<A, A>> mkPairs(Iterable<A> xs) {
        return foldl(xs, Pair.pair(ImmutableList.of(), Option.none()), Pairs.<A>mkPairs()::apply)
                .first();
    }

    private static <A>
            BiFunction<A, Pair<Iterable<Pair<A, A>>, Option<A>>, Pair<Iterable<Pair<A, A>>, Option<A>>> mkPairs() {
        return new MkPairs<>();
    }

    private static final class MkPairs<A>
            implements BiFunction<A, Pair<Iterable<Pair<A, A>>, Option<A>>, Pair<Iterable<Pair<A, A>>, Option<A>>> {
        public Pair<Iterable<Pair<A, A>>, Option<A>> apply(A v, Pair<Iterable<Pair<A, A>>, Option<A>> intermediate) {
            for (A a : intermediate.second()) {
                return pair(concat(intermediate.first(), ImmutableList.of(pair(a, v))), Option.none());
            }
            return pair(intermediate.first(), some(v));
        }
    }

    /**
     * Creates an {@code Iterable} containing just the first element of each pair
     *
     * @param <A> type of the first value of the {@code Pair}
     * @param <B> type of the second value of the {@code Pair}
     * @param pairs pairs of values
     * @return {@code Iterable} containing just the first element of each pair
     */
    public static <A, B> Iterable<A> firsts(Iterable<Pair<A, B>> pairs) {
        return transform(pairs, Pair::first);
    }

    /**
     * Creates a {@code Predicate} which evaluates {@code p} with the first value from a {@code Pair}.
     *
     * @param <A> type of the first value of the {@code Pair}
     * @param <B> type of the second value of the {@code Pair}
     * @param p {@code Predicate} to use to evaluate the first value of a {@code Pair}
     * @return {@code Predicate} which evaluates {@code p} with the first value from a {@code Pair}
     */
    public static <A, B> Predicate<Pair<A, B>> withFirst(final Predicate<A> p) {
        return new Predicate<Pair<A, B>>() {
            public boolean test(Pair<A, B> pair) {
                return p.test(pair.first());
            }

            @Override
            public String toString() {
                return String.format("withFirst(%s)", p);
            }
        };
    }

    /**
     * Creates a {@code Predicate} which evaluates {@code p} with the second value from a {@code Pair}.
     *
     * @param <A> type of the first value of the {@code Pair}
     * @param <B> type of the second value of the {@code Pair}
     * @param p {@code Predicate} to use to evaluate the second value of a {@code Pair}
     * @return {@code Predicate} which evaluates {@code p} with the second value from a {@code Pair}
     */
    public static <A, B> Predicate<Pair<A, B>> withSecond(final Predicate<B> p) {
        return new Predicate<Pair<A, B>>() {
            public boolean test(Pair<A, B> pair) {
                return p.test(pair.second());
            }

            @Override
            public String toString() {
                return String.format("withSecond(%s)", p);
            }
        };
    }
}
