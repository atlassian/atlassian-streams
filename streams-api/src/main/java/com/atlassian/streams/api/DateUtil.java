package com.atlassian.streams.api;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.annotation.Nullable;

/**
 * provides utility functions to convert date related objects
 */
public class DateUtil {

    private DateUtil() {}

    public static ZonedDateTime toZonedDate(@Nullable Date dt) {
        if (dt == null) {
            dt = new Date();
        }
        return dt.toInstant().atZone(ZoneId.systemDefault());
    }

    /**
     * convert ZonedDateTime to Date
     * @param zdt
     * @return
     */
    public static Date toDate(@Nullable ZonedDateTime zdt) {
        if (zdt == null) {
            return null;
        }
        return Date.from(zdt.toInstant());
    }
}
