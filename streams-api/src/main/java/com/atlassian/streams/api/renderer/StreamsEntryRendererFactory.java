package com.atlassian.streams.api.renderer;

import java.net.URI;
import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;

/**
 * Factory to create {@link StreamsEntry.Renderer}s for some common types of activity entries.
 */
public interface StreamsEntryRendererFactory {
    /**
     * Creates a {@code StreamsEntry.Renderer} for a comment entry.  Uses {@code comment} as the content of the entry.
     *
     * @param comment comment to use as the content of the entry
     * @return renderer for the comment
     */
    Renderer newCommentRenderer(String comment);

    Renderer newCommentRenderer(Html comment);

    Renderer newCommentRenderer(Function<StreamsEntry, Html> titleRenderer, String message);

    Renderer newCommentRenderer(Function<StreamsEntry, Html> titleRenderer, Html message);

    Renderer newCommentRenderer(Function<StreamsEntry, Html> titleRenderer, Html apply, @Nonnull URI styleLink);

    Function<StreamsEntry, Html> newCommentTitleRendererFunc();

    Function<StreamsEntry, Html> newTitleRendererFunc(String key);

    Function<StreamsEntry, Html> newTitleRendererFunc(
            String key,
            Function<Iterable<UserProfile>, Html> authorsRenderer,
            @Nullable Function<Iterable<ActivityObject>, Option<Html>> activityObjectRenderer,
            @Nullable Function<ActivityObject, Option<Html>> targetRenderer);

    /*
    TODO These methods returning Function can/should be removed completely at some point.
     They serve no real business purpose and seem to be here solely to fulfil wet dreams
     of functional programming zealots.

     Especially nowadays when method reference operator exists.
    */

    Function<Iterable<UserProfile>, Html> newAuthorsRendererFunc();

    Function<Iterable<UserProfile>, Html> newUserProfileRendererFunc();

    Function<Iterable<ActivityObject>, Option<Html>> newActivityObjectsRendererFunc();

    Function<Iterable<ActivityObject>, Option<Html>> newActivityObjectsRendererFunc(
            Function<ActivityObject, Option<Html>> objectRenderer);

    Function<ActivityObject, Option<Html>> newActivityObjectRendererWithSummaryFunc();

    Function<ActivityObject, Option<Html>> newActivityObjectRendererWithoutSummaryFunc();

    <T> Function<Iterable<T>, Option<Html>> newCompoundStatementRendererFunc(Function<T, Option<Html>> render);
}
