package com.atlassian.streams.api.common;

import java.util.regex.Pattern;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Iterables.transform;

@Deprecated
public class Predicates {
    /**
     * @deprecated just kill it with fire
     */
    @Deprecated
    public static java.util.function.Predicate<String> containsAnyIssueKey(Iterable<String> keys) {
        return or(transform(keys, (Function<String, Predicate<String>>) key -> containsIssueKeyPredicate(key)::test));
    }

    /**
     * Creates a predicate that test if Strings contain {@link String} key
     */
    public static java.util.function.Predicate<String> containsIssueKeyPredicate(String key) {
        // Find "ONE-1" but not "CR-ONE-1"
        String regex = "(?<!CR-)\\b" + key + "\\b";
        Pattern pattern = Pattern.compile(regex);

        return input -> pattern.matcher(input).find();
    }
}
