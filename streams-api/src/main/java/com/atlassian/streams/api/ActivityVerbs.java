package com.atlassian.streams.api;

import java.net.URI;

import com.google.common.base.Function;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;

/**
 * Utility methods for working with {@link ActivityVerb}s
 */
public class ActivityVerbs {
    public static final String STANDARD_IRI_BASE = "http://activitystrea.ms/schema/1.0/";
    public static final String ATLASSIAN_IRI_BASE = "http://streams.atlassian.com/syndication/verbs/";

    public static String verb2Key(ActivityVerb verb) {
        return verb.key();
    }

    public static VerbFactory newVerbFactory(String baseIri) {
        return new VerbFactoryImpl(baseIri);
    }

    public interface VerbFactory {
        ActivityVerb newVerb(String key);

        ActivityVerb newVerb(String key, ActivityVerb parent);
    }

    private static final class VerbFactoryImpl implements VerbFactory {
        private final LoadingCache<Pair<String, Option<ActivityVerb>>, ActivityVerb> verbs;

        public VerbFactoryImpl(String baseIri) {
            verbs = CacheBuilder.newBuilder().build(CacheLoader.from(verbFactory(baseIri)));
        }

        public ActivityVerb newVerb(String key) {
            return verbs.getUnchecked(pair(key, none(ActivityVerb.class)));
        }

        public ActivityVerb newVerb(String key, ActivityVerb parent) {
            return verbs.getUnchecked(pair(key, some(parent)));
        }

        private static Function<Pair<String, Option<ActivityVerb>>, ActivityVerb> verbFactory(final String baseIri) {
            return new VerbFactory(baseIri);
        }

        private static final class VerbFactory implements Function<Pair<String, Option<ActivityVerb>>, ActivityVerb> {
            private final String baseIri;

            private VerbFactory(String baseIri) {
                this.baseIri = baseIri;
            }

            public ActivityVerb apply(Pair<String, Option<ActivityVerb>> keyParent) {
                return newVerb(keyParent.first(), URI.create(baseIri + keyParent.first()), keyParent.second());
            }

            public static ActivityVerb newVerb(String key, URI iri, Option<ActivityVerb> parent) {
                return new ActivityVerbTypeImpl(key, iri, parent);
            }
        }

        private static final class ActivityVerbTypeImpl implements ActivityVerb {
            private final String key;
            private final URI iri;
            private final Option<ActivityVerb> parent;

            public ActivityVerbTypeImpl(String key, URI iri, Option<ActivityVerb> parent) {
                this.key = checkNotNull(key, "key");
                this.iri = checkNotNull(iri, "iri");
                this.parent = checkNotNull(parent, "parent");
            }

            public URI iri() {
                return iri;
            }

            public String key() {
                return key;
            }

            public Option<ActivityVerb> parent() {
                return parent;
            }

            @Override
            public String toString() {
                return key;
            }

            @Override
            public int hashCode() {
                return iri.hashCode();
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null) {
                    return false;
                }
                if (!ActivityVerb.class.isAssignableFrom(obj.getClass())) {
                    return false;
                }
                ActivityVerb other = (ActivityVerb) obj;
                return this.iri.equals(other.iri());
            }
        }
    }

    private static final VerbFactory standardVerbs = newVerbFactory(STANDARD_IRI_BASE);

    public static ActivityVerb post() {
        return standardVerbs.newVerb("post");
    }

    public static ActivityVerb update() {
        return standardVerbs.newVerb("update");
    }

    public static ActivityVerb like() {
        return standardVerbs.newVerb("like");
    }
}
