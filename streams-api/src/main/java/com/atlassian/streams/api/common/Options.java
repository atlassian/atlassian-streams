package com.atlassian.streams.api.common;

import java.util.stream.Stream;

import static com.google.common.collect.Iterables.concat;

/**
 * Useful methods for working with {@code Option}
 * @deprecated  It's 2021. Just use {@link java.util.Optional} There's no removal date set
 *  but try not to use {@link com.atlassian.streams.api.common.Option} in the new code.
 *  You should convert it to {@link java.util.Optional} as soon as you get it from this API
 *  (there's {@link Option#toOptional(Option)} method for that).
 */
@Deprecated
public final class Options {
    private Options() {}

    /**
     * Projects out any {@link Option#none()} values from its input iterable,
     * returning the resulting iterable
     * @param as An iterable of optional values
     * @param <A> the type of values
     * @return An iterable containing the elements of {@code as} that were not {@link Option#none()}
     */
    public static <A> Iterable<A> catOptions(Iterable<Option<A>> as) {
        return concat(as);
    }

    /**
     * Returns the first element in the input iterable that is not {@link Option#none()}.
     * @param as An iterable of optional values
     * @param <A> the type of values
     * @return The first element for which {@link Option#isDefined()} was true; {@link Option#none()} if
     *   all of the elements were {@link Option#none()}, or if the iterable was empty
     */
    public static <A> Option<A> find(Iterable<Option<A>> as) {
        for (Option<A> a : as) {
            if (a.isDefined()) {
                return a;
            }
        }
        return Option.<A>none();
    }

    public static <R> Stream<R> stream(final Option<R> option) {
        return option.isDefined() ? Stream.of(option.get()) : Stream.empty();
    }
}
