package com.atlassian.streams.api.common;

import java.util.function.BiFunction;

/**
 * A function of arity-2.
 *
 * @param <T1> type of the first parameter
 * @param <T2> type of the second parameter
 * @param <R> type of the return value
 * @deprecated Use {@link BiFunction} instead
 */
@Deprecated
public interface Function2<T1, T2, R> extends BiFunction<T1, T2, R> {}
