package com.atlassian.streams.jira;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IssueActivityExtractorTest {
    private static final String USERNAME = "username";
    private static final String OTHER_USERNAME = "other";

    @Mock
    private CommentManager commentManager;

    @Mock
    private JiraHelper helper;

    @Mock
    private JiraStreamsActivityProvider.ChangeHistoryQuery changeHistories;

    @Mock
    private ApplicationUser user;

    @Mock
    private ApplicationUser reporter;

    @Mock
    private Issue issue;

    @Mock
    private ChangeHistory changeHistoryOne;

    @Mock
    private ChangeHistory changeHistoryTwo;

    @Mock
    private GenericValue summaryChangeItem;

    @Mock
    private ActivityObjectType activityObjectType;

    @Mock
    private ActivityVerb activityVerb;

    private Predicate<String> inUsers = Predicates.equalTo(USERNAME);
    private Predicate<Date> containsDate = Predicates.alwaysTrue();
    private Predicate<Pair<ActivityObjectType, ActivityVerb>> inJiraActivities = Predicates.alwaysTrue();

    private ImmutableList.Builder<JiraActivityItem> builder = ImmutableList.builder();
    private IssueActivityExtractor issueActivityExtractor;

    @Before
    public void setUp() {
        issueActivityExtractor = new IssueActivityExtractor(
                user, commentManager, helper, builder, changeHistories, inUsers, containsDate, inJiraActivities);

        prepareIssueSummary();
        prepareComments();
        prepareSummaryChangeItem();
    }

    @Test
    public void assertThatNewSummaryIsReturnedIfUpdatedBySameUser() {
        prepareChangeHistories(changeHistoryOne);
        prepareHelperJiraActivity(changeHistoryOne, USERNAME, USERNAME + "key");

        List<JiraActivityItem> activities = activities();

        JiraActivityItem activityItem = activities.get(0);
        assertThat(activityItem.getDisplaySummary(), is(equalTo("new")));
    }

    @Test
    public void assertThatNewSummaryIsReturnedIfUpdatedByAnotherUser() {
        prepareChangeHistories(changeHistoryOne, changeHistoryTwo);
        prepareHelperJiraActivity(changeHistoryOne, USERNAME, USERNAME + "key");
        prepareHelperJiraActivity(changeHistoryTwo, OTHER_USERNAME, OTHER_USERNAME + "key");

        List<JiraActivityItem> activities = activities();

        assertEquals(1, activities.size());

        JiraActivityItem activityItem = activities.get(0);
        assertEquals("new", activityItem.getDisplaySummary());
    }

    private void prepareSummaryChangeItem() {
        when(summaryChangeItem.getString("newstring")).thenReturn("new");
        when(summaryChangeItem.getString("field")).thenReturn("summary");
    }

    private void prepareIssueSummary() {
        when(issue.getSummary()).thenReturn("old");
    }

    private void prepareComments() {
        when(commentManager.getCommentsForUser(issue, user)).thenReturn(Collections.emptyList());
    }

    private void prepareChangeHistories(ChangeHistory... histories) {
        when(changeHistories.sortedChangeHistories(issue)).thenReturn(Arrays.asList(histories));
    }

    // Extra userKey parameter to test handling of renamed users
    private void prepareHelperJiraActivity(ChangeHistory changeHistory, String username, String userKey) {
        ApplicationUser mockUser = mock(ApplicationUser.class);
        when(mockUser.getUsername()).thenReturn(username);
        when(issue.getReporter()).thenReturn(reporter);
        when(changeHistory.getAuthorObject()).thenReturn(mockUser);

        when(changeHistory.getChangeItems()).thenReturn(Collections.singletonList(summaryChangeItem));

        when(helper.jiraActivity(changeHistory)).thenReturn(Option.some(Pair.pair(activityObjectType, activityVerb)));
    }

    private List<JiraActivityItem> activities() {
        issueActivityExtractor.extract(issue);

        return builder.build();
    }
}
