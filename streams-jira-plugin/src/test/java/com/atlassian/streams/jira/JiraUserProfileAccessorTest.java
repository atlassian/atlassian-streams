package com.atlassian.streams.jira;

import java.net.URI;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.jira.JiraTesting.mockUserProfile;

@RunWith(MockitoJUnitRunner.class)
public class JiraUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite {
    private static final URI BASE_URI = URI.create("http://localhost/streams");

    @Mock
    private EmailFormatter emailFormatter;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private UserManager userManager;

    @Mock
    private AvatarManager avatarManager;

    @Mock
    private StreamsI18nResolver i18nResolver;

    @Override
    public String getProfilePathTemplate() {
        return "/secure/ViewProfile.jspa?name={username}";
    }

    @Override
    protected String getProfilePicturePathTemplate() {
        return "/secure/useravatar?avatarId={profilePictureParameter}";
    }

    @Override
    protected String getProfilePicParameter(String username) {
        return "0";
    }

    @Before
    public void createUserProfileBuilder() {
        userProfileAccessor = new JiraUserProfileAccessor(
                getApplicationProperties(),
                emailFormatter,
                authenticationContext,
                userManager,
                avatarManager,
                i18nResolver);
    }

    @Before
    public void setUp() {
        when(emailFormatter.emailVisible(any())).thenReturn(true);
        when(userManager.getUserProfile("user")).thenReturn(mockUserProfile("user", "u@c.com", "User"));
        when(userManager.getUserProfile("user3")).thenReturn(mockUserProfile("user3", "u3@c.com", "User <3&'>"));
    }

    @Test
    public void assertThatEmailIsOnlyAvailableIfItIsVisibleToTheLoggedInUser() {
        when(emailFormatter.emailVisible(any())).thenReturn(false);
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, "user").getEmail(), is(equalTo(none(String.class))));
    }

    @Test
    @Override
    @Ignore("SAL is doing this for us in JIRA, so there's no reason to test it ourselves")
    public void assertThatUsernameIsUriEncodedInProfileUri() {}

    @Test
    @Override
    @Ignore("SAL is doing this for us in JIRA, so there's no reason to test it ourselves")
    public void assertThatUsernameIsUriEncodedInProfilePictureUri() {}

    @Test
    public void profilePictureUriIsTakenFromSal() throws Exception {
        String username = "user";
        URI profileUri = new URI("http://example.test/profile-picture-provided-by-sal");

        UserProfile profile = mockUserProfile(username, profileUri);
        when(userManager.getUserProfile(username)).thenReturn(profile);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, username).getProfilePictureUri();

        assertEquals(Option.some(profileUri), uri);
    }

    @Test
    public void profilePictureUriIsTreatedRelativeToBaseUrl() throws Exception {
        String username = "user";
        URI profileUri = new URI("/path-absolute-profile-picture-provided-by-sal");

        UserProfile profile = mockUserProfile(username, profileUri);
        when(userManager.getUserProfile(username)).thenReturn(profile);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, username).getProfilePictureUri();

        assertEquals(
                Option.some(new URI("http://localhost/streams/path-absolute-profile-picture-provided-by-sal")), uri);
    }

    @Test
    public void profilePictureUriIsNormalizedAfterConstructionFromSal() throws Exception {
        String username = "user";
        URI profileUri = new URI("/path-absolute-profile-picture-provided-by-sal");

        UserProfile profile = mockUserProfile(username, profileUri);
        when(userManager.getUserProfile(username)).thenReturn(profile);

        URI baseUri = URI.create("http://example.test/streams/..");
        Option<URI> uri = userProfileAccessor.getUserProfile(baseUri, username).getProfilePictureUri();

        assertEquals(Option.some(new URI("http://example.test/path-absolute-profile-picture-provided-by-sal")), uri);
    }

    @Test
    public void profilePictureUriIsConstructedWhenSalReturnsNull() throws Exception {
        when(avatarManager.getAnonymousAvatarId()).thenReturn(1L);

        Option<URI> uri =
                userProfileAccessor.getUserProfile(BASE_URI, "I don't exist").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://localhost/streams/secure/useravatar?avatarId=1")), uri);
    }
}
