package com.atlassian.streams.jira;

import java.net.URI;
import java.net.URISyntaxException;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;

class JiraTesting {
    static UserProfile mockUserProfile(
            final String username,
            final String email,
            final String fullName,
            final URI profilePictureUri,
            final URI profilePageUri) {
        return new UserProfile() {
            @Override
            public UserKey getUserKey() {
                return new UserKey(username);
            }

            public String getUsername() {
                return username;
            }

            public String getFullName() {
                return fullName;
            }

            public String getEmail() {
                return email;
            }

            public URI getProfilePictureUri(int width, int height) {
                return getProfilePictureUri();
            }

            public URI getProfilePictureUri() {
                return profilePictureUri;
            }

            public URI getProfilePageUri() {
                return profilePageUri;
            }
        };
    }

    static UserProfile mockUserProfile(final String username, final String email, final String fullName) {
        return mockUserProfile(
                username,
                email,
                fullName,
                URI.create("/secure/useravatar?avatarId=0"),
                URI.create("/secure/ViewProfile.jspa?name=" + username));
    }

    static UserProfile mockUserProfile(String username, URI profileUri) throws URISyntaxException {
        return mockUserProfile(username, "", "", profileUri, new URI(""));
    }
}
