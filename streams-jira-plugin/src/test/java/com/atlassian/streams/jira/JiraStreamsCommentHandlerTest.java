package com.atlassian.streams.jira;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.spi.StreamsCommentHandler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.DELETED_OR_PERMISSION_DENIED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNKNOWN_ERROR;

@RunWith(MockitoJUnitRunner.class)
public class JiraStreamsCommentHandlerTest {
    private static final URI BASE_URI = URI.create("http://localhost/streams");
    private static final String ISSUE_KEY = "TEST-1";
    private static final String COMMENT_TEXT = "comment";

    private SimpleErrorCollection errorCollection;

    @Mock
    private MutableIssue issue;

    @Mock
    private Comment commentObject;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private IssueManager issueManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private CommentService commentService;

    @Mock
    private ApplicationProperties applicationProperties;

    @Spy
    @InjectMocks
    private JiraStreamsCommentHandler commentHandler;

    @Before
    public void prepareJiraStreamsCommentHandler() {
        when(issueManager.getIssueObject(ISSUE_KEY)).thenReturn(issue);
        when(permissionManager.hasPermission(eq(Permissions.COMMENT_ISSUE), eq(issue), isNull()))
                .thenReturn(true);
        errorCollection = new SimpleErrorCollection();
        when(commentHandler.createSimpleErrorCollection()).thenReturn(errorCollection);
        when(commentService.create(isNull(), eq(issue), eq(COMMENT_TEXT), anyBoolean(), eq(errorCollection)))
                .thenReturn(commentObject);
    }

    @Test
    public void assertNullIssueReturnsPostReplyError() {
        when(issueManager.getIssueObject(ISSUE_KEY)).thenReturn(null);

        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult =
                commentHandler.postReply(BASE_URI, ImmutableList.of(ISSUE_KEY), COMMENT_TEXT);

        assertTrue(postReplyResult.isLeft());
        assertThat(postReplyResult.left().get().getType(), is(DELETED_OR_PERMISSION_DENIED));
    }

    @Test
    public void assertUnauthorisedUserReturnsPostReplyError() {
        when(permissionManager.hasPermission(eq(Permissions.COMMENT_ISSUE), eq(issue), isNull()))
                .thenReturn(false);

        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult =
                commentHandler.postReply(BASE_URI, ImmutableList.of(ISSUE_KEY), COMMENT_TEXT);

        assertTrue(postReplyResult.isLeft());
        assertThat(postReplyResult.left().get().getType(), is(UNAUTHORIZED));
    }

    @Test
    public void assertCommentServiceErrorReturnsPostReplyError() {
        errorCollection.addError("comment", "Error message.");

        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult =
                commentHandler.postReply(BASE_URI, ImmutableList.of(ISSUE_KEY), COMMENT_TEXT);

        assertTrue(postReplyResult.isLeft());
        assertThat(postReplyResult.left().get().getType(), is(UNKNOWN_ERROR));
        assertNotNull(postReplyResult.left().get().getCause());
        assertTrue(postReplyResult.left().get().getCause().isDefined());
        assertEquals(
                "Error message.", postReplyResult.left().get().getCause().get().getMessage());
    }

    @Test
    public void assertSuccessfulCommentCreationReturnsURI() {
        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult =
                commentHandler.postReply(BASE_URI, ImmutableList.of(ISSUE_KEY), COMMENT_TEXT);

        assertTrue(postReplyResult.isRight());
        assertEquals(postReplyResult.right().get().toASCIIString(), "http://localhost/streams/browse/TEST-1#action_0");
    }
}
