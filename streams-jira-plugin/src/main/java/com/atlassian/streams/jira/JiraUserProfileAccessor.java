package com.atlassian.streams.jira;

import java.net.URI;
import java.util.Optional;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.sal.api.UrlMode.CANONICAL;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;

/**
 * JIRA implementation of building Atom author constructs
 */
public class JiraUserProfileAccessor implements UserProfileAccessor {
    private static final int AVATAR_PIXELS = Avatar.Size.LARGE.getPixels();

    private final ApplicationProperties applicationProperties;
    private final EmailFormatter emailFormatter;
    private final JiraAuthenticationContext authenticationContext;
    private final UserManager userManager;
    private final AvatarManager avatarManager;
    private final StreamsI18nResolver i18nResolver;

    public JiraUserProfileAccessor(
            ApplicationProperties applicationProperties,
            EmailFormatter emailFormatter,
            JiraAuthenticationContext authenticationContext,
            UserManager userManager,
            AvatarManager avatarManager,
            StreamsI18nResolver i18nResolver) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.emailFormatter = checkNotNull(emailFormatter, "emailFormatter");
        this.authenticationContext = checkNotNull(authenticationContext, "authenticationContext");
        this.userManager = checkNotNull(userManager, "userManager");
        this.avatarManager = checkNotNull(avatarManager, "avatarManager");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public UserProfile getAnonymousUserProfile(URI baseUri) {
        return getAnonymousUserProfile(
                baseUri,
                i18nResolver.getText("streams.jira.authors.unknown.username"),
                i18nResolver.getText("streams.jira.authors.unknown.fullname"));
    }

    @Override
    public UserProfile getUserProfile(final String username) {
        return getUserProfile(URI.create(applicationProperties.getBaseUrl(CANONICAL)), username);
    }

    @Override
    public UserProfile getAnonymousUserProfile() {
        return getAnonymousUserProfile(URI.create(applicationProperties.getBaseUrl(CANONICAL)));
    }

    public UserProfile getUserProfile(URI baseUri, String username) {
        if (username == null) {
            return getAnonymousUserProfile(baseUri);
        }
        return Optional.ofNullable(userManager.getUserProfile(username))
                .map(profile -> getUserProfile(baseUri, profile))
                .orElseGet(() -> getAnonymousUserProfile(baseUri, username, null));
    }

    private UserProfile getUserProfile(URI baseUri, com.atlassian.sal.api.user.UserProfile userProfile) {
        return new UserProfile.Builder(userProfile.getUsername())
                .fullName(userProfile.getFullName())
                .email(option(getEmailForUser(userProfile)))
                .profilePageUri(some(getUserProfileUri(baseUri, userProfile)))
                .profilePictureUri(some(getUserProfilePictureUri(baseUri, userProfile)))
                .build();
    }

    private String getEmailForUser(com.atlassian.sal.api.user.UserProfile user) {
        return emailFormatter.emailVisible(authenticationContext.getLoggedInUser()) ? user.getEmail() : null;
    }

    private URI getUserProfilePictureUri(URI baseUri, com.atlassian.sal.api.user.UserProfile userProfile) {
        URI uri = userProfile.getProfilePictureUri(AVATAR_PIXELS, AVATAR_PIXELS);
        if (uri.isAbsolute()) {
            return uri;
        }
        return combineBaseUriWithPath(baseUri, uri.toString()).normalize();
    }

    private UserProfile getAnonymousUserProfile(URI baseUri, String username, String fullName) {
        return new UserProfile.Builder(username)
                .fullName(fullName)
                .profilePictureUri(some(getAnonymousProfilePictureUri(baseUri)))
                .build();
    }

    private URI getUserProfileUri(URI baseUri, com.atlassian.sal.api.user.UserProfile userProfile) {
        return combineBaseUriWithPath(baseUri, userProfile.getProfilePageUri().toASCIIString());
    }

    private URI getAnonymousProfilePictureUri(URI baseUri) {
        return combineBaseUriWithPath(baseUri, "/secure/useravatar?avatarId=" + avatarManager.getAnonymousAvatarId());
    }

    private URI combineBaseUriWithPath(URI baseUri, String path) {
        final String baseUriStr =
                baseUri == null ? applicationProperties.getBaseUrl(CANONICAL) : baseUri.toASCIIString();
        return URI.create(baseUriStr + path);
    }
}
