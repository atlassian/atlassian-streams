package com.atlassian.streams.jira.builder;

import java.net.URI;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.link.RemoteIssueLink;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.jira.AggregatedJiraActivityItem;
import com.atlassian.streams.jira.JiraActivityItem;
import com.atlassian.streams.jira.JiraHelper;
import com.atlassian.streams.jira.renderer.AttachmentRendererFactory;
import com.atlassian.streams.jira.renderer.IssueUpdateRendererFactory;
import com.atlassian.streams.spi.StreamsI18nResolver;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;

import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.ChangeItems.getChangeItems;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.atlassian.streams.jira.JiraActivityVerbs.link;
import static com.atlassian.streams.jira.JiraActivityVerbs.remoteLink;

class GeneralUpdateEntryBuilder {
    private final JiraHelper helper;
    private final AttachmentRendererFactory attachmentRendererFactory;
    private final IssueUpdateRendererFactory issueUpdateRendererFactory;
    private final StreamsI18nResolver i18nResolver;

    GeneralUpdateEntryBuilder(
            JiraHelper helper,
            AttachmentRendererFactory attachmentRendererFactory,
            IssueUpdateRendererFactory issueUpdateRendererFactory,
            StreamsI18nResolver i18nResolver) {
        this.helper = checkNotNull(helper, "helper");
        this.attachmentRendererFactory = checkNotNull(attachmentRendererFactory, "attachmentRendererFactory");
        this.issueUpdateRendererFactory = checkNotNull(issueUpdateRendererFactory, "issueUpdateRendererFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    Option<StreamsEntry> build(URI baseUri, AggregatedJiraActivityItem aggregatedItem) {
        if (aggregatedItem.getRelatedActivityItems().isDefined()) {
            return buildMultipleActivityItem(baseUri, aggregatedItem);
        }
        return buildSingleActivityItem(baseUri, aggregatedItem.getActivityItem());
    }

    private Option<StreamsEntry> buildSingleActivityItem(URI baseUri, JiraActivityItem item) {
        Iterable<GenericValue> changeItems = filter(getChangeItems(item), helper.validAttachment());
        if (isEmpty(changeItems)) {
            return none();
        }

        if (pair(file(), post()).equals(item.getActivity())) {
            return buildAttachments(item, baseUri, changeItems);
        } else if (pair(issue(), remoteLink()).equals(item.getActivity())) {
            return buildRemoteLink(item, baseUri, changeItems);
        } else {
            return some(new StreamsEntry(
                    (item.getComment().isDefined()
                                    ? helper.newCommentBuilder(baseUri, item)
                                    : helper.newBuilder(item, baseUri))
                            .authors(helper.getUserProfiles(baseUri, item))
                            .addActivityObject(
                                    helper.buildActivityObject(item.getIssue(), baseUri, item.getDisplaySummary()))
                            .verb(update())
                            .renderer(issueUpdateRendererFactory.newRenderer(item, baseUri, changeItems)),
                    i18nResolver));
        }
    }

    private Option<StreamsEntry> buildMultipleActivityItem(URI baseUri, AggregatedJiraActivityItem aggregatedItem) {
        if (pair(issue(), link()).equals(aggregatedItem.getActivity())) {
            JiraActivityItem mainItem = aggregatedItem.getActivityItem();
            return some(new StreamsEntry(
                    helper.newLinkedIssueBuilder(baseUri, mainItem)
                            .authors(helper.getUserProfiles(baseUri, mainItem))
                            .addActivityObject(helper.buildActivityObject(
                                    mainItem.getIssue(), baseUri, mainItem.getDisplaySummary()))
                            .verb(update())
                            .renderer(issueUpdateRendererFactory.newIssueLinkEntryRenderer(baseUri, aggregatedItem)),
                    i18nResolver));
        }
        return none();
    }

    private Option<StreamsEntry> buildAttachments(
            JiraActivityItem item, URI baseUri, Iterable<GenericValue> changeItems) {
        Iterable<Attachment> attachments = helper.extractAttachments(changeItems);

        if (!isEmpty(attachments)) {
            return some(new StreamsEntry(
                    helper.newBuilder(item, baseUri)
                            .authors(helper.getUserProfiles(baseUri, item))
                            .verb(post())
                            .addActivityObjects(helper.buildActivityObjects(baseUri, attachments))
                            .target(some(
                                    helper.buildActivityObject(item.getIssue(), baseUri, item.getDisplaySummary())))
                            .renderer(
                                    attachmentRendererFactory.newAttachmentsEntryRenderer(item, baseUri, attachments)),
                    i18nResolver));
        } else {
            // If attachment has been deleted, we shouldn't display any streams entry (STRM-913)
            return none();
        }
    }

    private Option<StreamsEntry> buildRemoteLink(
            JiraActivityItem item, URI baseUri, Iterable<GenericValue> changeItems) {
        final Option<RemoteIssueLink> remoteLink = helper.extractRemoteIssueLink(changeItems);

        if (remoteLink.isDefined()) {
            return some(new StreamsEntry(
                    helper.newBuilder(item, baseUri)
                            .authors(helper.getUserProfiles(baseUri, item))
                            .addActivityObject(
                                    helper.buildActivityObject(item.getIssue(), baseUri, item.getDisplaySummary()))
                            .verb(update())
                            .renderer(issueUpdateRendererFactory.newRemoteIssueLinkEntryRenderer(
                                    baseUri, item, remoteLink.get())),
                    i18nResolver));
        } else {
            // If remote link has been deleted, we shouldn't display any streams entry (STRM-913)
            return none();
        }
    }
}
