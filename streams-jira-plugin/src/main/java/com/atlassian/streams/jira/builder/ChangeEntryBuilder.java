package com.atlassian.streams.jira.builder;

import java.net.URI;

import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadataManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.jira.AggregatedJiraActivityItem;
import com.atlassian.streams.jira.JiraActivityItem;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.find;

import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.jira.ChangeItems.getChangeItems;
import static com.atlassian.streams.jira.ChangeItems.isStatusUpdate;
import static com.atlassian.streams.jira.JiraActivityVerbs.transition;

public class ChangeEntryBuilder {
    private final HistoryMetadataEntryBuilder historyMetadataEntryBuilder;
    private final HistoryMetadataManager historyMetadataManager;
    private final JiraAuthenticationContext authenticationContext;

    ChangeEntryBuilder(
            HistoryMetadataEntryBuilder historyMetadataEntryBuilder,
            HistoryMetadataManager historyMetadataManager,
            JiraAuthenticationContext authenticationContext) {
        this.historyMetadataEntryBuilder = checkNotNull(historyMetadataEntryBuilder, "historyMetadataEntryBuilder");
        this.historyMetadataManager = checkNotNull(historyMetadataManager, "historyMetadataManager");
        this.authenticationContext = checkNotNull(authenticationContext, "authenticationContext");
    }

    public Option<StreamsEntry> build(AggregatedJiraActivityItem aggregatedItem, ActivityRequest request) {
        fetchMetadata(aggregatedItem);
        URI baseUri = request.getContextUri();
        if (isTransitionVerb(aggregatedItem)) {
            JiraActivityItem item = aggregatedItem.getActivityItem();
            return historyMetadataEntryBuilder.buildStatusChangeEntry(
                    item, baseUri, find(getChangeItems(item), isStatusUpdate()));
        }
        return historyMetadataEntryBuilder.buildGeneralUpdateEntry(baseUri, aggregatedItem);
    }

    private void fetchMetadata(final AggregatedJiraActivityItem aggregatedItem) {
        JiraActivityItem activityItem = aggregatedItem.getActivityItem();
        if (activityItem.getChangeHistory().isDefined()) {
            HistoryMetadataManager.HistoryMetadataResult historyMetadata = historyMetadataManager.getHistoryMetadata(
                    activityItem.getChangeHistory().get(), authenticationContext.getUser());
            activityItem.setHistoryMetadata(option(historyMetadata.getHistoryMetadata()));
        }
    }

    private boolean isTransitionVerb(AggregatedJiraActivityItem aggregatedItem) {
        if (!aggregatedItem.getRelatedActivityItems().isDefined()) {
            ActivityVerb verb = aggregatedItem.getActivity().second();
            if (transition().equals(verb)) {
                return true;
            }
            for (ActivityVerb childVerb : verb.parent()) {
                return transition().equals(childVerb);
            }
        }
        return false;
    }
}
