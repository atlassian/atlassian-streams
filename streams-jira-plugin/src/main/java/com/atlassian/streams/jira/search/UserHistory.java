package com.atlassian.streams.jira.search;

import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import static java.util.Collections.emptyList;

import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;

class UserHistory {
    private final ProjectManager projectManager;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;
    private final ChangeHistoryManager changeHistoryManager;
    private final UserKeyService userKeyService;

    public UserHistory(
            ProjectManager projectManager,
            PermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext,
            ChangeHistoryManager changeHistoryManager,
            UserKeyService userKeyService) {
        this.projectManager = checkNotNull(projectManager, "projectManager");
        this.permissionManager = checkNotNull(permissionManager, "permissionManager");
        this.authenticationContext = checkNotNull(authenticationContext, "authenticationContext");
        this.changeHistoryManager = checkNotNull(changeHistoryManager, "changeHistoryManager");
        this.userKeyService = userKeyService;
    }

    public Set<Issue> find(final ActivityRequest request) {
        return find(request, null);
    }

    public Set<Issue> find(final ActivityRequest request, final Date cutoffPoint) {
        final Collection<Project> projects = ImmutableSet.copyOf(getProjects(request));
        if (projects.isEmpty()) {
            // If there are no projects then that means the user doesn't have permission to browse the requested project
            return ImmutableSet.of();
        }
        return ImmutableSet.copyOf(changeHistoryManager.findUserHistory(
                authenticationContext.getLoggedInUser(),
                getUsers(request),
                projects,
                request.getMaxResults(),
                cutoffPoint));
    }

    private Collection<String> getUsers(ActivityRequest request) {
        Collection<Pair<Operator, Iterable<String>>> filters =
                request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList());
        if (filters.isEmpty()) {
            // ChangeHistoryManager is a bit stupid and will add a "reporter in ()" clause if the user list is
            // not null and empty.  So we return null here to signal it shouldn't add any reporter clause.
            return null;
        }
        Collection<String> result = getIsValues(filters);
        if (result.isEmpty()) {
            // See above
            return null;
        } else {
            final Set<String> keysFromNames = result.stream()
                    .map(userKeyService::getKeyForUsername)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            // See above
            return keysFromNames.isEmpty() ? null : keysFromNames;
        }
    }

    private Iterable<Project> getProjects(final ActivityRequest request) {
        final Set<String> projectKeys =
                getIsValues(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList()));

        if (isEmpty(projectKeys)) {
            return permissionManager.getProjects(
                    ProjectPermissions.BROWSE_PROJECTS, authenticationContext.getLoggedInUser());
        } else {
            return projectKeys.stream()
                    .map(projectManager::getProjectObjByKey)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }
}
