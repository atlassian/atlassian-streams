package com.atlassian.streams.jira;

import java.net.URI;
import java.util.StringJoiner;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.spi.StreamsCommentHandler;

import static com.google.common.collect.Iterables.getOnlyElement;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.DELETED_OR_PERMISSION_DENIED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNKNOWN_ERROR;

public class JiraStreamsCommentHandler implements StreamsCommentHandler {
    private final JiraAuthenticationContext authenticationContext;
    private final IssueManager issueManager;
    private final PermissionManager permissionManager;
    private final CommentService commentService;
    private final ApplicationProperties applicationProperties;

    public JiraStreamsCommentHandler(
            JiraAuthenticationContext authenticationContext,
            IssueManager issueManager,
            PermissionManager permissionManager,
            CommentService commentService,
            ApplicationProperties applicationProperties) {
        this.authenticationContext = authenticationContext;
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.commentService = commentService;
        this.applicationProperties = applicationProperties;
    }

    public Either<PostReplyError, URI> postReply(URI baseUri, Iterable<String> itemPath, String comment)
            throws StreamsException {
        ApplicationUser user = authenticationContext.getLoggedInUser();
        String issueKey = getOnlyElement(itemPath);
        Issue issue = issueManager.getIssueObject(issueKey);
        if (issue == null) {
            return Either.left(new PostReplyError(DELETED_OR_PERMISSION_DENIED));
        } else if (!permissionManager.hasPermission(Permissions.COMMENT_ISSUE, issue, user)) {
            return Either.left(new PostReplyError(UNAUTHORIZED));
        }
        ErrorCollection errorCollection = createSimpleErrorCollection();
        Comment commentObject = commentService.create(user, issue, comment, true, errorCollection);
        if (errorCollection.hasAnyErrors()) {
            Throwable cause = errorCollectionToThrowable(errorCollection);
            return Either.left(new PostReplyError(UNKNOWN_ERROR, cause));
        }
        return Either.right(
                URI.create(baseUri.toASCIIString() + "/browse/" + issueKey + "#action_" + commentObject.getId()));
    }

    @Override
    public Either<PostReplyError, URI> postReply(final Iterable<String> itemPath, final String comment) {
        return postReply(URI.create(applicationProperties.getBaseUrl()), itemPath, comment);
    }

    /**
     * Extracted for testing
     * @return new {@link SimpleErrorCollection} object initialised with no-parameter constructor
     */
    protected SimpleErrorCollection createSimpleErrorCollection() {
        return new SimpleErrorCollection();
    }

    private Throwable errorCollectionToThrowable(ErrorCollection errorCollection) {
        StringJoiner concatenatedErrorMessages = new StringJoiner(" ");
        for (String errorKey : errorCollection.getErrors().keySet()) {
            concatenatedErrorMessages.add(errorCollection.getErrors().get(errorKey));
        }
        return new Throwable(concatenatedErrorMessages.toString());
    }
}
