package com.atlassian.streams.jira.rest.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.streams.jira.JiraInlineActionHandler;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;

/**
 * REST resource for any JIRA-specific inline action tasks.
 */
@Path("/actions")
public class JiraInlineActionResource {
    private final JiraInlineActionHandler inlineActionHandler;

    @Inject
    public JiraInlineActionResource(JiraInlineActionHandler inlineActionHandler) {
        this.inlineActionHandler = checkNotNull(inlineActionHandler, "inlineActionHandler");
    }

    @Path("issue-watch/{issueKey}")
    @Consumes("application/json")
    @POST
    public Response watchIssue(@PathParam("issueKey") String issueKey) {
        if (inlineActionHandler.hasPreviouslyWatched(issueKey)) {
            return Response.status(CONFLICT).build();
        }

        boolean success = inlineActionHandler.startWatching(issueKey);
        if (success) {
            return Response.noContent().build();
        } else {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

    @Path("issue-watch/{issueKey}")
    @Produces("application/json")
    @GET
    public boolean isWatching(@PathParam("issueKey") String issueKey) {
        return inlineActionHandler.hasPreviouslyWatched(issueKey);
    }

    @Path("issue-vote/{issueKey}")
    @Consumes("application/json")
    @POST
    public Response voteIssue(@PathParam("issueKey") String issueKey) {
        if (inlineActionHandler.hasPreviouslyVoted(issueKey)) {
            return Response.status(CONFLICT).build();
        }

        boolean success = inlineActionHandler.voteOnIssue(issueKey);
        if (success) {
            return Response.noContent().build();
        } else {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }
}
