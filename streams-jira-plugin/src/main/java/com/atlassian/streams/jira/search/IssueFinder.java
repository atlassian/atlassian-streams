package com.atlassian.streams.jira.search;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Predicate;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.streams.api.ActivityRequest;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.filter;
import static java.util.Collections.emptyList;

import static com.atlassian.streams.jira.JiraFilterOptionProvider.ISSUE_TYPE;
import static com.atlassian.streams.jira.JiraFilterOptionProvider.PROJECT_CATEGORY;
import static com.atlassian.streams.spi.Filters.inIssueKeys;
import static com.atlassian.streams.spi.Filters.isAndNot;
import static com.atlassian.streams.spi.Filters.isIn;
import static com.atlassian.streams.spi.Filters.notIn;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;

/**
 * Finds recently updated/created issues that should be put in the activity stream.
 */
public class IssueFinder {
    private final UserHistory userHistory;
    private final IssueSearch issueSearch;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;

    public IssueFinder(
            UserHistory userHistory,
            IssueSearch issueSearch,
            PermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext) {
        this.userHistory = checkNotNull(userHistory, "userHistory");
        this.issueSearch = checkNotNull(issueSearch, "issueSearch");
        this.permissionManager = checkNotNull(permissionManager, "permissionManager");
        this.authenticationContext = checkNotNull(authenticationContext, "authenticationContext");
    }

    public Set<Issue> find(final ActivityRequest request) {
        final List<Issue> issues = issueSearch.search(request);
        final int oldestIssueIndex = Math.min(issues.size(), request.getMaxResults()) - 1;
        final Timestamp oldestUpdate =
                oldestIssueIndex == -1 ? null : issues.get(oldestIssueIndex).getUpdated();

        final Set<Issue> userHistoryIssues = userHistory.find(request, oldestUpdate);

        // STRM-1354 STRM-1410 must filter userHistory (JQL already does it)
        final Set<Issue> filteredUserHistory = filter(
                userHistoryIssues,
                and(
                        issueKey(inIssueKeys(request, caseInsensitive(isIn()), caseInsensitive(notIn()))::test),
                        hasBrowsePermission,
                        hasIssueType(request),
                        hasProjectCategory(request),
                        hasProjectKey(request)));

        return Stream.concat(issues.stream(), filteredUserHistory.stream()).collect(Collectors.toSet());
    }

    private java.util.function.Function<Iterable<String>, java.util.function.Predicate<String>> caseInsensitive(
            final java.util.function.Function<Iterable<String>, java.util.function.Predicate<String>> f) {
        return iterable -> {
            Iterable<String> iterableUpper = transform(iterable, String::toUpperCase);
            java.util.function.Predicate<String> predicateForUpper = f.apply(iterableUpper);

            return str -> predicateForUpper.test(str.toUpperCase());
        };
    }

    private Predicate<Issue> issueKey(Predicate<String> issueKeyPredicate) {
        return new IssueKeyPredicateWrapper(issueKeyPredicate);
    }

    private static final class IssueKeyPredicateWrapper implements Predicate<Issue> {
        private final Predicate<String> issueKeyPredicate;

        public IssueKeyPredicateWrapper(Predicate<String> issueKeyPredicate) {
            this.issueKeyPredicate = issueKeyPredicate;
        }

        public boolean apply(Issue issue) {
            return issueKeyPredicate.apply(issue.getKey());
        }
    }
    ;

    private final Predicate<Issue> hasBrowsePermission = new Predicate<Issue>() {
        public boolean apply(Issue issue) {
            return issue != null
                    && permissionManager.hasPermission(
                            Permissions.BROWSE, issue, authenticationContext.getLoggedInUser());
        }
    };

    private Predicate<Issue> hasIssueType(ActivityRequest request) {
        return new HasIssueType(request);
    }

    private class HasIssueType implements Predicate<Issue> {
        private final ActivityRequest request;

        public HasIssueType(ActivityRequest request) {
            this.request = request;
        }

        public boolean apply(Issue issue) {
            return issue != null
                    && isAndNot(request.getProviderFiltersMap().getOrDefault(ISSUE_TYPE, emptyList()))
                            .test(issue.getIssueTypeObject().getId());
        }
    }
    ;

    private Predicate<Issue> hasProjectCategory(ActivityRequest request) {
        return new HasProjectCategory(request);
    }

    private static class HasProjectCategory implements Predicate<Issue> {
        private final ActivityRequest request;

        public HasProjectCategory(ActivityRequest request) {
            this.request = request;
        }

        @Override
        public boolean apply(final Issue issue) {
            if (issue == null) {
                return false;
            }
            ProjectCategory projectCategoryObject = issue.getProjectObject().getProjectCategoryObject();
            // A non-categorized projects should be checked with this filter. That why null should be passed to be
            // handled by the filter.
            final String categoryId =
                    projectCategoryObject != null ? String.valueOf(projectCategoryObject.getId()) : null;
            return isAndNot(request.getProviderFiltersMap().getOrDefault(PROJECT_CATEGORY, emptyList()))
                    .test(categoryId);
        }
    }

    private Predicate<Issue> hasProjectKey(ActivityRequest request) {
        return new HasProjectKey(request);
    }

    private class HasProjectKey implements Predicate<Issue> {
        private final ActivityRequest request;

        public HasProjectKey(ActivityRequest request) {
            this.request = request;
        }

        public boolean apply(Issue issue) {
            return issue != null
                    && isAndNot(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList()))
                            .test(issue.getProjectObject().getKey());
        }
    }
    ;
}
