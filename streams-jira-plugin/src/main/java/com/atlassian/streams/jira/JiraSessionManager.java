package com.atlassian.streams.jira;

import java.util.function.Supplier;

import org.apache.log4j.Logger;

import com.atlassian.jira.util.thread.JiraThreadLocalUtil;
import com.atlassian.streams.spi.SessionManager;

public class JiraSessionManager implements SessionManager {
    private final JiraThreadLocalUtil jiraThreadLocalUtil;

    public JiraSessionManager(JiraThreadLocalUtil jiraThreadLocalUtil) {
        this.jiraThreadLocalUtil = jiraThreadLocalUtil;
    }

    private static final Logger logger = Logger.getLogger(JiraSessionManager.class);

    public <T> T withSession(Supplier<T> s) {
        jiraThreadLocalUtil.preCall();
        try {
            return s.get();
        } finally {
            jiraThreadLocalUtil.postCall(logger, null);
        }
    }
}
