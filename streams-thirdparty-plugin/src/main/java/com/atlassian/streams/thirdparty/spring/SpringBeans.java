package com.atlassian.streams.thirdparty.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.plugins.rest.api.util.RestUrlBuilder;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public RestUrlBuilder importRestUrlBuilder() {
        return importOsgiService(RestUrlBuilder.class);
    }
}
