package com.atlassian.streams.thirdparty;

import java.net.URI;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Qualifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Options;
import com.atlassian.streams.spi.ActivityProviderModuleDescriptor;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.SessionManager;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

class EntityAssociationProviders {
    private final Logger log = LoggerFactory.getLogger(EntityAssociationProviders.class);

    private final PluginAccessor pluginAccessor;
    private final TransactionTemplate transactionTemplate;
    private final SessionManager sessionManager;
    private final UserManager userManager;

    public EntityAssociationProviders(
            PluginAccessor pluginAccessor,
            @Qualifier("sessionManager") SessionManager sessionManager,
            TransactionTemplate transactionTemplate,
            UserManager userManager) {
        this.pluginAccessor = checkNotNull(pluginAccessor, "pluginAccessor");
        this.sessionManager = checkNotNull(sessionManager, "sessionManager");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public Iterable<StreamsEntityAssociationProvider> getProviders() {
        return filter(
                transform(
                        pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class),
                        toAssociationProvider()),
                Predicates.notNull());
    }

    public Iterable<EntityIdentifier> getEntityAssociations(URI targetUri) {
        return Iterables.concat(transform(getProviders(), toAssociations(targetUri)));
    }

    public Option<URI> getEntityURI(EntityIdentifier target) {
        return Options.find(transform(getProviders(), toEntityURI(target)));
    }

    public Option<String> getFilterKey(EntityIdentifier target) {
        return Options.find(transform(getProviders(), toFilterKey(target)));
    }

    public boolean getCurrentUserViewPermission(EntityIdentifier target) {
        return Options.find(transform(getProviders(), toCurrentUserViewPermission(target)))
                .getOrElse(false);
    }

    public boolean getCurrentUserViewPermissionOfTargetlessEntity() {
        return StreamSupport.stream(getProviders().spliterator(), false)
                .map(StreamsEntityAssociationProvider::getCurrentUserViewPermissionForTargetlessEntity)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .reduce((left, right) -> left && right)
                .orElseGet(() -> userManager.getRemoteUser() != null);
        // ^default behaviour in case no EntityAssociationProvider can resolve global user permissions.
    }

    public boolean getCurrentUserEditPermission(EntityIdentifier target) {
        return Options.find(transform(getProviders(), toCurrentUserEditPermission(target)))
                .getOrElse(false);
    }

    private Function<ActivityProviderModuleDescriptor, StreamsEntityAssociationProvider> toAssociationProvider() {
        return toAssociationProviderFunction;
    }

    private final Function<ActivityProviderModuleDescriptor, StreamsEntityAssociationProvider>
            toAssociationProviderFunction =
                    new Function<ActivityProviderModuleDescriptor, StreamsEntityAssociationProvider>() {
                        public StreamsEntityAssociationProvider apply(ActivityProviderModuleDescriptor descriptor) {
                            return descriptor.getEntityAssociationProvider();
                        }
                    };

    private Function<StreamsEntityAssociationProvider, Iterable<EntityIdentifier>> toAssociations(final URI target) {
        return new ProviderSessionScopedFunction<Iterable<EntityIdentifier>>(
                new Function<StreamsEntityAssociationProvider, Iterable<EntityIdentifier>>() {
                    public Iterable<EntityIdentifier> apply(StreamsEntityAssociationProvider provider) {
                        return provider.getEntityIdentifiers(target);
                    }
                },
                new Supplier<Iterable<EntityIdentifier>>() {
                    public Iterable<EntityIdentifier> get() {
                        return ImmutableList.of();
                    }
                });
    }

    private Function<StreamsEntityAssociationProvider, Option<URI>> toEntityURI(final EntityIdentifier identifier) {
        return new ProviderSessionScopedFunction<Option<URI>>(
                new Function<StreamsEntityAssociationProvider, Option<URI>>() {
                    public Option<URI> apply(StreamsEntityAssociationProvider provider) {
                        return provider.getEntityURI(identifier);
                    }
                },
                () -> Option.none());
    }

    private Function<StreamsEntityAssociationProvider, Option<String>> toFilterKey(final EntityIdentifier identifier) {
        return new ProviderSessionScopedFunction<Option<String>>(
                new Function<StreamsEntityAssociationProvider, Option<String>>() {
                    public Option<String> apply(StreamsEntityAssociationProvider provider) {
                        return provider.getFilterKey(identifier);
                    }
                },
                () -> Option.none());
    }

    private Function<StreamsEntityAssociationProvider, Option<Boolean>> toCurrentUserViewPermission(
            final EntityIdentifier identifier) {
        return new ProviderSessionScopedFunction<Option<Boolean>>(
                new Function<StreamsEntityAssociationProvider, Option<Boolean>>() {
                    public Option<Boolean> apply(StreamsEntityAssociationProvider provider) {
                        return provider.getCurrentUserViewPermission(identifier);
                    }
                },
                () -> Option.none());
    }

    private Function<StreamsEntityAssociationProvider, Option<Boolean>> toCurrentUserEditPermission(
            final EntityIdentifier identifier) {
        return new ProviderSessionScopedFunction<Option<Boolean>>(
                new Function<StreamsEntityAssociationProvider, Option<Boolean>>() {
                    public Option<Boolean> apply(StreamsEntityAssociationProvider provider) {
                        return provider.getCurrentUserEditPermission(identifier);
                    }
                },
                () -> Option.none());
    }

    private class ProviderSessionScopedFunction<T> implements Function<StreamsEntityAssociationProvider, T> {
        private final Function<StreamsEntityAssociationProvider, T> getFromProviderFunction;
        private final Supplier<T> defaultSupplier;

        ProviderSessionScopedFunction(
                Function<StreamsEntityAssociationProvider, T> getFromProviderFunction, Supplier<T> defaultSupplier) {
            this.getFromProviderFunction = getFromProviderFunction;
            this.defaultSupplier = defaultSupplier;
        }

        public T apply(final StreamsEntityAssociationProvider provider) {
            final TransactionCallback<T> fetcher = new TransactionCallback<T>() {
                public T doInTransaction() {
                    try {
                        return getFromProviderFunction.apply(provider);
                    } catch (Exception e) {
                        log.error("Error while calling StreamsEntityAssociationProvider method", e);
                        return defaultSupplier.get();
                    }
                }
            };
            return sessionManager.withSession(new Supplier<T>() {
                public T get() {
                    return transactionTemplate.execute(fetcher);
                }
            });
        }
    }
    ;
}
