package com.atlassian.streams.thirdparty.ao;

import net.java.ao.Preload;

@Preload()
public interface ObjectEntity extends ActivityObjEntity {}
