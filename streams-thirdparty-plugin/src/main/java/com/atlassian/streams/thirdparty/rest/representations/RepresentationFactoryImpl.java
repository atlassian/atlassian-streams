package com.atlassian.streams.thirdparty.rest.representations;

import java.net.URI;
import java.util.Date;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import com.atlassian.streams.api.DateUtil;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;
import com.atlassian.streams.thirdparty.rest.LinkBuilder;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.some;

public class RepresentationFactoryImpl implements RepresentationFactory {
    private final LinkBuilder linkBuilder;

    public RepresentationFactoryImpl(LinkBuilder linkBuilder) {
        this.linkBuilder = checkNotNull(linkBuilder, "linkBuilder");
    }

    public ActivityCollectionRepresentation createActivityCollectionRepresentation(
            Iterable<Activity> activities, ActivityQuery query) {
        ImmutableList<ActivityRepresentation> representations =
                ImmutableList.copyOf(Iterables.transform(activities, toActivityRepresentation()));
        return new ActivityCollectionRepresentation(representations, linkBuilder.build(activities, query));
    }

    public ActivityRepresentation createActivityRepresentation(Activity activity) {
        Option<Date> date = some(DateUtil.toDate(activity.getZonedPostedDate()));
        return ActivityRepresentation.builder(
                        createActivityObjectRepresentation(activity.getUser()),
                        createActivityObjectRepresentation(activity.getApplication()))
                .content(activity.getContent())
                .id(activity.getId())
                .icon(activity.getIcon().map(toMediaLinkRepresentation()))
                .title(activity.getTitle())
                .published(date)
                .updated(date)
                .url(activity.getUrl())
                .verb(activity.getVerb())
                .links(some(linkBuilder.build(activity)))
                .build();
    }

    public Function<Activity, ActivityRepresentation> toActivityRepresentation() {
        return toActivityRepresentation;
    }

    private Function<Activity, ActivityRepresentation> toActivityRepresentation =
            new Function<Activity, ActivityRepresentation>() {
                public ActivityRepresentation apply(Activity from) {
                    return createActivityRepresentation(from);
                }
            };

    public ActivityObjectRepresentation createActivityObjectRepresentation(ActivityObject object) {
        return ActivityObjectRepresentation.builder()
                .displayName(object.getDisplayName())
                .id(object.getId())
                .objectType(object.getType())
                .summary(object.getSummary())
                .url(object.getUrl())
                .build();
    }

    public ActivityObjectRepresentation createActivityObjectRepresentation(Application application) {
        return ActivityObjectRepresentation.builder()
                .displayName(some(application.getDisplayName()))
                .id(some(application.getId()))
                .build();
    }

    public ActivityObjectRepresentation createActivityObjectRepresentation(UserProfile userProfile) {
        ActivityObjectRepresentation.Builder builder = ActivityObjectRepresentation.builder()
                .displayName(some(userProfile.getFullName()))
                .idString(some(userProfile.getUsername()))
                .url(userProfile.getProfilePageUri());
        for (URI pictureUri : userProfile.getProfilePictureUri()) {
            builder.image(some(MediaLinkRepresentation.builder(pictureUri).build()));
        }
        return builder.build();
    }

    public MediaLinkRepresentation createMediaLinkRepresentation(Image image) {
        return MediaLinkRepresentation.builder(image.getUrl())
                .height(image.getHeight())
                .width(image.getWidth())
                .build();
    }

    public Function<Image, MediaLinkRepresentation> toMediaLinkRepresentation() {
        return toMediaLinkRepresentation;
    }

    private Function<Image, MediaLinkRepresentation> toMediaLinkRepresentation =
            new Function<Image, MediaLinkRepresentation>() {
                public MediaLinkRepresentation apply(Image from) {
                    return createMediaLinkRepresentation(from);
                }
            };
}
