package com.atlassian.streams.thirdparty;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.Filters;
import com.atlassian.streams.spi.StreamsActivityProvider;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.ActivityService;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;

import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getMaxDate;
import static com.atlassian.streams.spi.Filters.getMinDate;
import static com.atlassian.streams.spi.Filters.getNotValues;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.atlassian.streams.thirdparty.ThirdPartyStreamsFilterOptionProvider.PROVIDER_NAME;

public class ThirdPartyStreamsActivityProvider implements StreamsActivityProvider {
    private final StreamsI18nResolver i18nResolver;
    private final ActivityService activityService;
    private final ThirdPartyStreamsEntryBuilder entryBuilder;

    public ThirdPartyStreamsActivityProvider(
            ActivityService activityService,
            StreamsI18nResolver i18nResolver,
            ThirdPartyStreamsEntryBuilder entryBuilder) {
        this.activityService = checkNotNull(activityService, "activityService");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.entryBuilder = checkNotNull(entryBuilder, "entryBuilder");
    }

    @Override
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest activityRequest) throws StreamsException {
        return new CancellableTask<StreamsFeed>() {
            @Override
            public StreamsFeed call() throws Exception {
                return new StreamsFeed(
                        i18nResolver.getText("streams.thirdparty.title"),
                        getEntries(activityRequest),
                        none(String.class));
            }

            @Override
            public Result cancel() {
                return Result.INTERRUPT;
            }
        };
    }

    @VisibleForTesting
    protected Iterable<StreamsEntry> getEntries(ActivityRequest request) {
        Iterable<StreamsEntry> nextEntries = buildEntries(request, getActivities(request, 0));
        Iterable<StreamsEntry> allEntries = ImmutableList.copyOf(nextEntries);

        for (int size = size(nextEntries);
                !isEmpty(nextEntries) && size < request.getMaxResults();
                size += size(nextEntries)) {
            nextEntries = buildEntries(request, getActivities(request, size));
            if (!isEmpty(nextEntries)) {
                allEntries = concat(allEntries, nextEntries);
            }
        }
        return take(request.getMaxResults(), allEntries);
    }

    private Iterable<StreamsEntry> buildEntries(ActivityRequest request, Iterable<Activity> activities) {
        return transform(
                take(request.getMaxResults(), byPostedDateDescending().sortedCopy(activities)), toStreamsEntry());
    }

    @VisibleForTesting
    protected Iterable<Activity> getActivities(ActivityRequest request, int startIndex) {
        ActivityQuery.Builder query = ActivityQuery.builder()
                .startDate(getMinDate(request))
                .endDate(getMaxDate(request))
                .userNames(getIsValues(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList())))
                .excludeUserNames(
                        getNotValues(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList())))
                .providerKeys(getIsValues(request.getProviderFiltersMap().getOrDefault(PROVIDER_NAME, emptyList())))
                .excludeProviderKeys(
                        getNotValues(request.getProviderFiltersMap().getOrDefault(PROVIDER_NAME, emptyList())))
                .maxResults(request.getMaxResults())
                .startIndex(startIndex);

        for (String projectKey : Filters.getProjectKeys(request)) {
            query.addEntityFilter(PROJECT_KEY, projectKey);
        }
        for (String projectKey : Filters.getNotProjectKeys(request)) {
            query.addExcludeEntityFilter(PROJECT_KEY, projectKey);
        }
        for (String issueKey : Filters.getIssueKeys(request)) {
            query.addEntityFilter(ISSUE_KEY.getKey(), issueKey);
        }
        for (String issueKey : Filters.getNotIssueKeys(request)) {
            query.addExcludeEntityFilter(ISSUE_KEY.getKey(), issueKey);
        }

        return activityService.activities(query.build());
    }

    private Function<Activity, StreamsEntry> toStreamsEntry() {
        return new Function<Activity, StreamsEntry>() {
            public StreamsEntry apply(Activity activity) {
                return entryBuilder.buildStreamsEntry(activity);
            }
        };
    }

    private static final Ordering<Activity> byPostedDateDescending() {
        return byPostedDateDescending;
    }

    private static final Ordering<Activity> byPostedDateDescending = new Ordering<Activity>() {
        @Override
        public int compare(Activity a, Activity b) {
            return b.getZonedPostedDate().compareTo(a.getZonedPostedDate());
        }
    };
}
