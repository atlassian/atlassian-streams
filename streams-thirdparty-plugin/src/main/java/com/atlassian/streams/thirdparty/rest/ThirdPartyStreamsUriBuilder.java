package com.atlassian.streams.thirdparty.rest;

import java.net.URI;

import com.atlassian.plugins.rest.api.util.RestUrlBuilder;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.thirdparty.rest.resources.ThirdPartyStreamsCollectionResource;
import com.atlassian.streams.thirdparty.rest.resources.ThirdPartyStreamsResource;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.thirdparty.api.ActivityQuery.DEFAULT_MAX_RESULTS;
import static com.atlassian.streams.thirdparty.api.ActivityQuery.DEFAULT_START_INDEX;

public final class ThirdPartyStreamsUriBuilder {
    private final ApplicationProperties applicationProperties;
    private final RestUrlBuilder restUrlBuilder;
    private final String ACTIVITIES_BASE = "/rest/activities/1.0";

    public ThirdPartyStreamsUriBuilder(ApplicationProperties applicationProperties, RestUrlBuilder restUrlBuilder) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.restUrlBuilder = checkNotNull(restUrlBuilder, "restUrlBuilder");
    }

    public URI buildActivityUri(Long activityId) {
        return buildActivityUriWithBaseUri(getBaseUri(), activityId);
    }

    public URI buildAbsoluteActivityUri(Long activityId) {
        return buildActivityUriWithBaseUri(getAbsoluteBaseUri(), activityId);
    }

    private URI buildActivityUriWithBaseUri(URI baseUri, Long activityId) {
        return restUrlBuilder.getUrlFor(baseUri, ThirdPartyStreamsResource.class, r -> r.get(activityId));
    }

    public URI buildActivityCollectionUri() {
        return buildActivityCollectionUri(DEFAULT_MAX_RESULTS, DEFAULT_START_INDEX);
    }

    public URI buildActivityCollectionUri(int maxResults, int startIndex) {
        return restUrlBuilder.getUrlFor(
                getBaseUri(),
                ThirdPartyStreamsCollectionResource.class,
                r -> r.fetchActivities(maxResults, startIndex));
    }

    private URI getBaseUri() {
        return URI.create(applicationProperties.getBaseUrl(UrlMode.RELATIVE).concat(ACTIVITIES_BASE));
    }

    private URI getAbsoluteBaseUri() {
        return URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL).concat(ACTIVITIES_BASE));
    }
}
