package com.atlassian.streams.thirdparty.rest.representations;

import com.google.common.base.Function;

import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;

/**
 * Factory for creating REST API representation objects out of Java API objects.
 */
public interface RepresentationFactory {
    ActivityCollectionRepresentation createActivityCollectionRepresentation(
            Iterable<Activity> activities, ActivityQuery query);

    ActivityRepresentation createActivityRepresentation(Activity activity);

    /**
     * @deprecated Use function reference to {@link #createActivityRepresentation(Activity)} instead.
     */
    Function<Activity, ActivityRepresentation> toActivityRepresentation();

    ActivityObjectRepresentation createActivityObjectRepresentation(ActivityObject object);

    ActivityObjectRepresentation createActivityObjectRepresentation(Application application);

    ActivityObjectRepresentation createActivityObjectRepresentation(UserProfile userProfile);

    MediaLinkRepresentation createMediaLinkRepresentation(Image image);

    /**
     * @deprecated Use function reference to {@link #createMediaLinkRepresentation(Image)} instead.
     */
    @Deprecated
    Function<Image, MediaLinkRepresentation> toMediaLinkRepresentation();
}
