package com.atlassian.streams.thirdparty;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.Application;

/**
 * Internal interface for a delegate object containing our ActiveObjects
 * implementation of {@link ActivityService}.  This is necessary in order for us to
 * be able to use the ActiveObjects @Transactional AOP annotation to provide
 * transaction management; unfortunately, AO only respects this annotation if it is
 * on an interface and if the interface methods are not inherited from another
 * interface.
 */
@Transactional
public interface ActivityServiceActiveObjects {
    Activity postActivity(Activity activity);

    Option<Activity> getActivity(long activityId);

    Iterable<Activity> activities(ActivityQuery query);

    boolean delete(long activityId);

    Iterable<Application> applications();
}
