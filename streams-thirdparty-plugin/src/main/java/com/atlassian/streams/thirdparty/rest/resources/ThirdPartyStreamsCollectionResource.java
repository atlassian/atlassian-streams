package com.atlassian.streams.thirdparty.rest.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.google.common.base.Function;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.ValidationErrors;
import com.atlassian.streams.thirdparty.rest.ThirdPartyStreamsUriBuilder;
import com.atlassian.streams.thirdparty.rest.representations.ActivityRepresentation;
import com.atlassian.streams.thirdparty.rest.representations.ErrorRepresentation;
import com.atlassian.streams.thirdparty.rest.representations.RepresentationFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

import static com.atlassian.streams.api.common.Iterables.memoize;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.api.ActivityQuery.all;
import static com.atlassian.streams.thirdparty.rest.MediaTypes.STREAMS_THIRDPARTY_JSON;

@Path("/")
public class ThirdPartyStreamsCollectionResource {
    private final ActivityService activityService;
    private final RepresentationFactory factory;
    private final ThirdPartyStreamsUriBuilder uriBuilder;
    private final UserManager userManager;

    @Inject
    public ThirdPartyStreamsCollectionResource(
            ActivityService activityService,
            RepresentationFactory factory,
            ThirdPartyStreamsUriBuilder uriBuilder,
            UserManager userManager) {
        this.activityService = checkNotNull(activityService, "activityService");
        this.factory = checkNotNull(factory, "factory");
        this.userManager = checkNotNull(userManager, "userManager");
        this.uriBuilder = uriBuilder;
    }

    @GET
    @Produces(STREAMS_THIRDPARTY_JSON)
    public Response fetchActivities(
            @DefaultValue("10") @QueryParam("max-results") Integer maxResults,
            @DefaultValue("0") @QueryParam("start-index") Integer startIndex) {
        ActivityQuery query = ActivityQuery.builder()
                .startIndex(startIndex)
                .maxResults(maxResults)
                .build();
        return Response.ok(factory.createActivityCollectionRepresentation(activityService.activities(query), query))
                .build();
    }

    @DELETE
    @Produces(STREAMS_THIRDPARTY_JSON)
    public Response deleteAllActivities() {
        String user = userManager.getRemoteUsername();
        if (!userManager.isAdmin(user) || !userManager.isSystemAdmin(user)) {
            return Response.status(FORBIDDEN).build();
        }
        return Response.ok(factory.createActivityCollectionRepresentation(
                        concat(memoize(transform(activityService.activities(all()), deletedActivities))), all()))
                .build();
    }

    private Function<Activity, Iterable<Activity>> deletedActivities = new Function<Activity, Iterable<Activity>>() {
        public Iterable<Activity> apply(Activity activity) {
            for (Long id : activity.getActivityId()) {
                if (activityService.delete(id)) {
                    return some(activity);
                }
            }
            return none();
        }
    };

    @POST
    @Consumes(STREAMS_THIRDPARTY_JSON)
    public Response postNewActivity(ActivityRepresentation representation) {
        Either<ValidationErrors, Activity> activityOrError = representation.toActivity(userManager.getRemoteUser());
        if (activityOrError.isLeft()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(STREAMS_THIRDPARTY_JSON)
                    .entity(new ErrorRepresentation(
                            "invalid activity entry",
                            activityOrError.left().get().toString()))
                    .build();
        } else {
            Activity activity =
                    activityService.postActivity(activityOrError.right().get());
            for (Long activityId : activity.getActivityId()) {
                return Response.created(uriBuilder.buildAbsoluteActivityUri(activityId))
                        .type(STREAMS_THIRDPARTY_JSON)
                        .entity(factory.createActivityRepresentation(activity))
                        .build();
            }
            return Response.status(INTERNAL_SERVER_ERROR).build();
        }
    }
}
