package com.atlassian.streams.thirdparty.ao;

import java.net.URI;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload()
public interface MediaLinkEntity extends Entity {
    Integer getDuration();

    void setDuration(Integer duration);

    Integer getHeight();

    void setHeight(Integer height);

    URI getUrl();

    void setUrl(URI url);

    Integer getWidth();

    void setWidth(Integer width);
}
