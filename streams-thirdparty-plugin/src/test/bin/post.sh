#!/bin/sh

baseurl=$1
shift
until [ -z "$1" ]
do
  curl -u admin:admin -H "Content-Type:application/vnd.atl.streams.thirdparty+json" --data-binary @$1 $baseurl/rest/activities/1.0/
  shift
done
