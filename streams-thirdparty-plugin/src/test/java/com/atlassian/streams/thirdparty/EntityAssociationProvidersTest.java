package com.atlassian.streams.thirdparty;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.spi.ActivityProviderModuleDescriptor;
import com.atlassian.streams.spi.SessionManager;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EntityAssociationProvidersTest {
    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private TransactionTemplate transactionTemplate;

    @Mock
    private SessionManager sessionManager;

    @Mock
    private UserManager userManager;

    @InjectMocks
    EntityAssociationProviders entityAssociationProviders;

    @Test
    public void shouldProhibitAccessWhenNoEntityAssociationProvidersAvailableAndUserIsAnonymous() {
        when(userManager.getRemoteUser()).thenReturn(null);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class))
                .thenReturn(Collections.emptyList());

        boolean viewPermitted = entityAssociationProviders.getCurrentUserViewPermissionOfTargetlessEntity();

        assertThat(viewPermitted, is(false));
    }

    @Test
    public void shouldProhibitAccessWhenEntityAssociationProvidersCannotHandleAndUserIsAnonymous() {
        when(userManager.getRemoteUser()).thenReturn(null);
        List<ActivityProviderModuleDescriptor> activityProviderModuleDescriptors = asList(
                createActivityProviderModuleDescriptorForTargetlessEntry(Optional.empty()),
                createActivityProviderModuleDescriptorForTargetlessEntry(Optional.empty()));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class))
                .thenReturn(activityProviderModuleDescriptors);

        boolean viewPermitted = entityAssociationProviders.getCurrentUserViewPermissionOfTargetlessEntity();

        assertThat(viewPermitted, is(false));
    }

    @Test
    public void shouldPermitAccessWhenNoEntityAssociationProvidersAvailableAndUserIsLogged() {
        when(userManager.getRemoteUser()).thenReturn(mock(UserProfile.class));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class))
                .thenReturn(Collections.emptyList());

        boolean viewPermitted = entityAssociationProviders.getCurrentUserViewPermissionOfTargetlessEntity();

        assertThat(viewPermitted, is(true));
    }

    @Test
    public void shouldPermitAccessWhenEntityAssociationProviderPermitsAccess() {
        List<ActivityProviderModuleDescriptor> activityProviderModuleDescriptors =
                asList(createActivityProviderModuleDescriptorForTargetlessEntry(Optional.of(true)));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class))
                .thenReturn(activityProviderModuleDescriptors);

        boolean viewPermitted = entityAssociationProviders.getCurrentUserViewPermissionOfTargetlessEntity();

        assertThat(viewPermitted, is(true));
    }

    @Test
    public void shouldProhibitAccessWhenEntityAssociationProviderProhibitsAccess() {
        List<ActivityProviderModuleDescriptor> activityProviderModuleDescriptors =
                asList(createActivityProviderModuleDescriptorForTargetlessEntry(Optional.of(false)));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class))
                .thenReturn(activityProviderModuleDescriptors);

        boolean viewPermitted = entityAssociationProviders.getCurrentUserViewPermissionOfTargetlessEntity();

        assertThat(viewPermitted, is(false));
    }

    @Test
    public void shouldProhibitAccessWhenEntityAssociationProvidersAreAmbiguousInAccess() {
        List<ActivityProviderModuleDescriptor> activityProviderModuleDescriptors = asList(
                createActivityProviderModuleDescriptorForTargetlessEntry(Optional.of(true)),
                createActivityProviderModuleDescriptorForTargetlessEntry(Optional.of(false)));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class))
                .thenReturn(activityProviderModuleDescriptors);

        boolean viewPermitted = entityAssociationProviders.getCurrentUserViewPermissionOfTargetlessEntity();

        assertThat(viewPermitted, is(false));
    }

    private ActivityProviderModuleDescriptor createActivityProviderModuleDescriptorForTargetlessEntry(
            Optional<Boolean> isPermitted) {
        StreamsEntityAssociationProvider streamsEntityAssociationProvider =
                mock(StreamsEntityAssociationProvider.class);
        when(streamsEntityAssociationProvider.getCurrentUserViewPermissionForTargetlessEntity())
                .thenReturn(isPermitted);

        ActivityProviderModuleDescriptor activityProviderModuleDescriptor =
                mock(ActivityProviderModuleDescriptor.class);
        when(activityProviderModuleDescriptor.getEntityAssociationProvider())
                .thenReturn(streamsEntityAssociationProvider);

        return activityProviderModuleDescriptor;
    }
}
