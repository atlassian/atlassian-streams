package com.atlassian.streams.thirdparty;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.DateUtil;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.Application;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Option.some;

/**
 * Test for {@link ThirdPartyStreamsActivityProvider}
 *
 * @since v5.3.10
 */
@RunWith(MockitoJUnitRunner.class)
public class ThirdPartyStreamsActivityProviderTest {
    @Mock
    private StreamsI18nResolver streamsI18nResolver;

    @Mock
    private ActivityService activityService;

    @Mock
    private ThirdPartyStreamsEntryBuilder entryBuilder;

    @InjectMocks
    private ThirdPartyStreamsActivityProvider thirdPartyStreamsActivityProvider;

    @Test
    public void testGetEntriesPaginatedRequests() throws Exception {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(100);

        ThirdPartyStreamsActivityProvider spy = spy(thirdPartyStreamsActivityProvider);

        URI jacURL = new URI("https://jira.atlassian.com");
        Activity activity = Activity.builder2(
                        Application.application("jira", jacURL), DateUtil.toZonedDate(new Date(1234)), "igrunert")
                .id(jacURL)
                .build()
                .right()
                .get();
        List<Activity> tenActivityItems = Arrays.asList(
                activity, activity, activity, activity, activity, activity, activity, activity, activity, activity);

        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams(), streamsI18nResolver);
        when(entryBuilder.buildStreamsEntry(activity)).thenReturn(streamsEntry);

        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(0));
        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(10));
        doReturn(Collections.emptyList()).when(spy).getActivities(eq(activityRequest), eq(20));

        Iterable<StreamsEntry> entries = spy.getEntries(activityRequest);

        assertThat(entries, Matchers.iterableWithSize(20));
    }

    @Test
    public void testGetEntriesPaginatedRequestsStopsWhenFulfilled() throws Exception {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(11);

        ThirdPartyStreamsActivityProvider spy = spy(thirdPartyStreamsActivityProvider);

        URI jacURL = new URI("https://jira.atlassian.com");
        Activity activity = Activity.builder2(
                        Application.application("jira", jacURL), DateUtil.toZonedDate(new Date(1234)), "igrunert")
                .id(jacURL)
                .build()
                .right()
                .get();
        List<Activity> tenActivityItems = Arrays.asList(
                activity, activity, activity, activity, activity, activity, activity, activity, activity, activity);

        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams(), streamsI18nResolver);
        when(entryBuilder.buildStreamsEntry(activity)).thenReturn(streamsEntry);

        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(0));
        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(10));

        Iterable<StreamsEntry> entries = spy.getEntries(activityRequest);

        assertThat(entries, Matchers.iterableWithSize(11));
    }

    @Test
    public void testGetEntriesDoesNotOvershoot() throws Exception {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(10);

        ThirdPartyStreamsActivityProvider spy = spy(thirdPartyStreamsActivityProvider);

        URI jacURL = new URI("https://jira.atlassian.com");
        Activity activity = Activity.builder2(
                        Application.application("jira", jacURL), DateUtil.toZonedDate(new Date(1234)), "igrunert")
                .id(jacURL)
                .build()
                .right()
                .get();
        List<Activity> nineLotsOfActivityOnTheWall =
                Arrays.asList(activity, activity, activity, activity, activity, activity, activity, activity, activity);

        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams(), streamsI18nResolver);
        when(entryBuilder.buildStreamsEntry(activity)).thenReturn(streamsEntry);

        doReturn(nineLotsOfActivityOnTheWall).when(spy).getActivities(eq(activityRequest), anyInt());

        Iterable<StreamsEntry> entries = spy.getEntries(activityRequest);

        assertThat(entries, Matchers.iterableWithSize(10));
    }

    private StreamsEntry.Parameters<
                    StreamsEntry.HasId,
                    StreamsEntry.HasPostedDate,
                    StreamsEntry.HasAlternateLinkUri,
                    StreamsEntry.HasApplicationType,
                    StreamsEntry.HasRenderer,
                    StreamsEntry.HasVerb,
                    StreamsEntry.HasAuthors>
            newEntryParams() {
        return StreamsEntry.params()
                .id(URI.create("http://example.com"))
                .postedDate(ZonedDateTime.now())
                .alternateLinkUri(URI.create("http://example.com"))
                .applicationType("test")
                .authors(ImmutableNonEmptyList.of(
                        new UserProfile.Builder("someone").fullName("Some One").build()))
                .addActivityObject(new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject.params()
                        .id("activity")
                        .title(some("Some activity"))
                        .alternateLinkUri(URI.create("http://example.com"))
                        .activityObjectType(comment())))
                .verb(post())
                .renderer(newRenderer());
    }

    private StreamsEntry.Renderer newRenderer() {
        return mock(StreamsEntry.Renderer.class);
    }
}
