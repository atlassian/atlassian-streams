package com.atlassian.streams.thirdparty;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import com.google.common.collect.ImmutableList;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.DatabaseUpdater;

import com.atlassian.streams.api.FeedContentSanitizer;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.ao.ActivityEntity;
import com.atlassian.streams.thirdparty.ao.ActivityObjEntity;
import com.atlassian.streams.thirdparty.ao.ActorEntity;
import com.atlassian.streams.thirdparty.ao.MediaLinkEntity;
import com.atlassian.streams.thirdparty.ao.ObjectEntity;
import com.atlassian.streams.thirdparty.ao.TargetEntity;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;

import static com.atlassian.streams.api.common.Option.some;

public class ThirdPartyStreams {
    public static final class ThirdPartyStreamsDatabaseUpdater implements DatabaseUpdater {
        @SuppressWarnings("unchecked")
        @Override
        public void update(EntityManager entityManager) throws Exception {
            entityManager.migrate(
                    ActivityEntity.class,
                    ActivityObjEntity.class,
                    ActorEntity.class,
                    MediaLinkEntity.class,
                    ObjectEntity.class,
                    TargetEntity.class);
        }
    }

    public static final class MockFeedContentSanitizer implements FeedContentSanitizer {
        private int called;

        @Override
        public String sanitize(String taintedInput) {
            called++;
            return taintedInput;
        }

        public int called() {
            return called;
        }
    }

    public static final URI DEFAULT_ACTIVITY_ID = URI.create("http://www.example.org/activity");
    public static final URI DEFAULT_ACTIVITY_ID_2 = URI.create("http://www.example.org/activity2");
    public static final URI DEFAULT_ACTIVITY_ID_3 = URI.create("http://www.example.org/activity3");
    public static final URI DEFAULT_ACTIVITY_OBJ_ID = URI.create("http://www.example.org/activity-object");
    public static final String DEFAULT_OBJECT_TYPE = "object";
    public static final URI DEFAULT_GENERATOR_ID = URI.create("http://www.example.org/generator");
    public static final URI DEFAULT_GENERATOR_ID_2 = URI.create("http://www.example.org/generator2");
    public static final URI DEFAULT_MEDIA_LINK = URI.create("http://www.example.org/media-link");
    public static final String DEFAULT_GENERATOR_NAME = "Default Third Party Provider";
    public static final String DEFAULT_GENERATOR_NAME_2 = "Another Third Party Provider";

    public static Activity.Builder2 activity() {
        return activity(application(), ZonedDateTime.now(), user()).id(DEFAULT_ACTIVITY_ID);
    }

    public static Activity.Builder2 activity(URI id) {
        return activity().id(id);
    }

    public static Activity.Builder2 activity(Application application, ZonedDateTime published, UserProfile user) {
        return new Activity.Builder2(application, published, user);
    }

    public static Activity.Builder2 activity(Application application, ZonedDateTime published, String username) {
        return new Activity.Builder2(application, published, new UserProfile.Builder(username).build());
    }

    public static ActivityObject.Builder activityObject() {
        return new ActivityObject.Builder();
    }

    public static Application application() {
        return Application.application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID);
    }

    public static Application application(String name, URI id) {
        return Application.application(name, id);
    }

    public static UserProfile user() {
        return new UserProfile.Builder("user").build();
    }

    public static ActivityObject.Builder activityObject(URI id) {
        return activityObject().id(some(id));
    }

    public static Image.Builder image() {
        return Image.builder(DEFAULT_MEDIA_LINK);
    }

    public abstract static class Matchers {
        public abstract static class Activities {
            public static Matcher<Iterable<? extends Activity>> hasNoActivities() {
                return emptyIterable();
            }

            public static Matcher<Iterable<? extends Activity>> allActivities(Matcher<Activity> matchers) {
                return everyItem(matchers);
            }

            public static Matcher<? super Iterable<? super Activity>> hasActivity(Matcher<? super Activity> matcher) {
                return hasItem(matcher);
            }

            @SuppressWarnings("unchecked")
            public static Matcher<? super Iterable<? super Activity>> hasActivity(
                    Matcher<? super Activity> m1, Matcher<? super Activity> m2) {
                return hasItem(allOf(m1, m2));
            }

            @SuppressWarnings("unchecked")
            public static Matcher<? super Iterable<? super Activity>> hasActivity(
                    Matcher<? super Activity> m1, Matcher<? super Activity> m2, Matcher<? super Activity> m3) {
                return hasItem(allOf(m1, m2, m3));
            }

            public static Matcher<? super Iterable<? super Activity>> hasActivity(
                    Matcher<? super Activity>... matchers) {
                return hasItem(allOf(matchers));
            }

            public static Matcher<Iterable<? extends Activity>> hasActivities(Matcher<? super Activity> m) {
                return hasActivities(ImmutableList.<Matcher<? super Activity>>of(m));
            }

            public static Matcher<Iterable<? extends Activity>> hasActivities(
                    Matcher<? super Activity> m1, Matcher<? super Activity> m2) {
                return hasActivities(ImmutableList.<Matcher<? super Activity>>of(m1, m2));
            }

            public static Matcher<Iterable<? extends Activity>> hasActivities(
                    Matcher<? super Activity> m1, Matcher<? super Activity> m2, Matcher<? super Activity> m3) {
                return hasActivities(ImmutableList.<Matcher<? super Activity>>of(m1, m2, m3));
            }

            public static Matcher<Iterable<? extends Activity>> hasActivities(
                    Matcher<? super Activity> m1,
                    Matcher<? super Activity> m2,
                    Matcher<? super Activity> m3,
                    Matcher<? super Activity> m4) {
                return hasActivities(ImmutableList.<Matcher<? super Activity>>of(m1, m2, m3, m4));
            }

            public static Matcher<Iterable<? extends Activity>> hasActivities(Matcher<? super Activity>... matchers) {
                return hasActivities(asList(matchers));
            }

            public static Matcher<Iterable<? extends Activity>> hasActivities(
                    List<Matcher<? super Activity>> matchers) {
                return contains(matchers);
            }

            public static Matcher<Activity> withApplication(Matcher<Application> matcher) {
                return new WithApplication(matcher);
            }

            private static final class WithApplication extends TypeSafeDiagnosingMatcher<Activity> {
                private final Matcher<Application> matcher;

                private WithApplication(Matcher<Application> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Activity activity, Description mismatchDescription) {
                    if (!matcher.matches(activity.getApplication())) {
                        mismatchDescription.appendText("application ");
                        matcher.describeMismatch(activity.getApplication(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("application with ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<Activity> withActivityObject(Matcher<ActivityObject> matcher) {
                return new WithActivityObject(matcher);
            }

            private static final class WithActivityObject extends TypeSafeDiagnosingMatcher<Activity> {
                private final Matcher<ActivityObject> matcher;

                private WithActivityObject(Matcher<ActivityObject> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Activity activity, Description mismatchDescription) {
                    Option<ActivityObject> object = activity.getObject();
                    if (!object.isDefined()) {
                        mismatchDescription.appendText("no activity object");
                        return false;
                    }
                    if (!matcher.matches(object.get())) {
                        mismatchDescription.appendText("activityObject ");
                        matcher.describeMismatch(object.get(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("activityObject with ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<Activity> withId(Matcher<? super URI> matcher) {
                return new WithId(matcher);
            }

            private static final class WithId extends TypeSafeDiagnosingMatcher<Activity> {
                private final Matcher<? super URI> matcher;

                private WithId(Matcher<? super URI> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Activity activity, Description mismatchDescription) {
                    if (!activity.getId().isDefined()
                            || !matcher.matches(activity.getId().get())) {
                        mismatchDescription.appendText("id ");
                        matcher.describeMismatch(activity.getId(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("id is ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<Activity> withTitle(Matcher<? super Html> matcher) {
                return new WithTitle(matcher);
            }

            private static final class WithTitle extends TypeSafeDiagnosingMatcher<Activity> {
                private final Matcher<? super Html> matcher;

                private WithTitle(Matcher<? super Html> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Activity activity, Description mismatchDescription) {
                    if (!activity.getTitle().isDefined()
                            || !matcher.matches(activity.getTitle().get())) {
                        mismatchDescription.appendText("title ");
                        matcher.describeMismatch(activity.getTitle(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("title is ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<Activity> withUsername(Matcher<? super String> matcher) {
                return new WithUsername(matcher);
            }

            private static final class WithUsername extends TypeSafeDiagnosingMatcher<Activity> {
                private final Matcher<? super String> matcher;

                private WithUsername(Matcher<? super String> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Activity activity, Description mismatchDescription) {
                    if (!matcher.matches(activity.getUser().getUsername())) {
                        mismatchDescription.appendText("username ");
                        matcher.describeMismatch(activity.getUser().getUsername(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("username is ").appendDescriptionOf(matcher);
                }
            }
        }

        public abstract static class ActivityObjects {
            public static Matcher<Iterable<? extends ActivityObject>> hasActivityObjects(
                    Matcher<? super ActivityObject> m) {
                return hasActivityObjects(ImmutableList.<Matcher<? super ActivityObject>>of(m));
            }

            public static Matcher<Iterable<? extends ActivityObject>> hasActivityObjects(
                    Matcher<? super ActivityObject> m1, Matcher<? super ActivityObject> m2) {
                return hasActivityObjects(ImmutableList.<Matcher<? super ActivityObject>>of(m1, m2));
            }

            public static Matcher<Iterable<? extends ActivityObject>> hasActivityObjects(
                    Matcher<? super ActivityObject> m1,
                    Matcher<? super ActivityObject> m2,
                    Matcher<? super ActivityObject> m3) {
                return hasActivityObjects(ImmutableList.<Matcher<? super ActivityObject>>of(m1, m2, m3));
            }

            public static Matcher<Iterable<? extends ActivityObject>> hasActivityObjects(
                    Matcher<? super ActivityObject> m1,
                    Matcher<? super ActivityObject> m2,
                    Matcher<? super ActivityObject> m3,
                    Matcher<? super ActivityObject> m4) {
                return hasActivityObjects(ImmutableList.<Matcher<? super ActivityObject>>of(m1, m2, m3, m4));
            }

            public static Matcher<Iterable<? extends ActivityObject>> hasActivityObjects(
                    Matcher<? super ActivityObject>... matchers) {
                return hasActivityObjects(asList(matchers));
            }

            public static Matcher<Iterable<? extends ActivityObject>> hasActivityObjects(
                    List<Matcher<? super ActivityObject>> matchers) {
                return contains(matchers);
            }

            public static Matcher<ActivityObject> withId(Matcher<? super URI> matcher) {
                return new WithId(matcher);
            }

            private static final class WithId extends TypeSafeDiagnosingMatcher<ActivityObject> {
                private final Matcher<? super URI> matcher;

                private WithId(Matcher<? super URI> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(ActivityObject object, Description mismatchDescription) {
                    Option<URI> id = object.getId();
                    if (!id.isDefined()) {
                        mismatchDescription.appendText("no id");
                        return false;
                    }
                    if (!matcher.matches(id.get())) {
                        mismatchDescription.appendText("id ");
                        matcher.describeMismatch(id.get(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("id = ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<ActivityObject> withUrl(Matcher<? super URI> matcher) {
                return new WithUrl(matcher);
            }

            private static final class WithUrl extends TypeSafeDiagnosingMatcher<ActivityObject> {
                private final Matcher<? super URI> matcher;

                private WithUrl(Matcher<? super URI> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(ActivityObject object, Description mismatchDescription) {
                    Option<URI> url = object.getUrl();
                    if (!url.isDefined()) {
                        mismatchDescription.appendText("no url");
                        return false;
                    }
                    if (!matcher.matches(url.get())) {
                        mismatchDescription.appendText("url ");
                        matcher.describeMismatch(url.get(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("url = ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<ActivityObject> withDisplayName(Matcher<? super String> matcher) {
                return new WithDisplayName(matcher);
            }

            private static final class WithDisplayName extends TypeSafeDiagnosingMatcher<ActivityObject> {
                private final Matcher<? super String> matcher;

                private WithDisplayName(Matcher<? super String> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(ActivityObject object, Description mismatchDescription) {
                    if (!object.getDisplayName().isDefined()) {
                        mismatchDescription.appendText("no displayName");
                        return false;
                    }
                    if (!matcher.matches(object.getDisplayName().get())) {
                        mismatchDescription.appendText("displayName ");
                        matcher.describeMismatch(object.getDisplayName().get(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("displayName = ").appendDescriptionOf(matcher);
                }
            }
        }

        public static class Applications {
            public static Matcher<Iterable<? extends Application>> hasApplications(
                    Matcher<? super Application>... applications) {
                List<Matcher<? super Application>> itemMatchers = asList(applications);
                return contains(itemMatchers);
            }

            public static Matcher<Application> withDisplayName(Matcher<? super String> matcher) {
                return new WithDisplayName(matcher);
            }

            private static final class WithDisplayName extends TypeSafeDiagnosingMatcher<Application> {
                private final Matcher<? super String> matcher;

                private WithDisplayName(Matcher<? super String> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Application application, Description mismatchDescription) {
                    if (!matcher.matches(application.getDisplayName())) {
                        mismatchDescription.appendText("application displayName ");
                        matcher.describeMismatch(application.getDisplayName(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("application with displayName ").appendDescriptionOf(matcher);
                }
            }

            public static Matcher<Application> withId(Matcher<? super URI> matcher) {
                return new WithId(matcher);
            }

            private static final class WithId extends TypeSafeDiagnosingMatcher<Application> {
                private final Matcher<? super URI> matcher;

                private WithId(Matcher<? super URI> matcher) {
                    this.matcher = matcher;
                }

                @Override
                protected boolean matchesSafely(Application application, Description mismatchDescription) {
                    if (!matcher.matches(application.getId())) {
                        mismatchDescription.appendText("application ID ");
                        matcher.describeMismatch(application.getId(), mismatchDescription);
                        return false;
                    }
                    return true;
                }

                public void describeTo(Description description) {
                    description.appendText("application with ID ").appendDescriptionOf(matcher);
                }
            }
        }
    }
}
