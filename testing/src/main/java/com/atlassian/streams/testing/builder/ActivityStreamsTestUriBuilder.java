package com.atlassian.streams.testing.builder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilder;
import com.atlassian.streams.api.common.Pair;

import static com.google.common.collect.Sets.newHashSet;

/**
 * A {@code StreamsFeedUriBuilder} that can be used in testing contexts (where Streams 4 is not running). This
 * implies a restriction : the auth parameters can't be automatically appended to the URL via {@code addAuthenticationParameterIfLoggedIn};
 * instead, a convenience method {@code addOSUserAuthenticationParameter} is added to add {@code os_authType=basic} to the URL if desired.
 * Getting an FEAUTH token isn't really possible.
 */
public class ActivityStreamsTestUriBuilder implements StreamsFeedUriBuilder {
    private final String baseUrl;
    private final Multimap<String, String> parameters = ArrayListMultimap.create();
    private final Set<String> providers = newHashSet();

    private Integer maxResults;
    private Integer timeout;

    public ActivityStreamsTestUriBuilder(final String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private URI buildUri(String path) {
        final StringBuilder url = new StringBuilder(baseUrl);
        url.append(path);
        String sep = "?";
        for (final Map.Entry<String, String> propEntry : parameters.entries()) {
            url.append(sep).append(encode(propEntry.getKey())).append("=").append(encode(propEntry.getValue()));
            sep = "&";
        }
        if (!providers.isEmpty()) {
            url.append(sep)
                    .append("providers")
                    .append('=')
                    .append(encode(Joiner.on(' ').join(providers)));
        }
        if (maxResults != null) {
            url.append(sep).append("maxResults").append("=").append(maxResults);
        }
        if (timeout != null) {
            url.append(sep).append("timeout").append("=").append(timeout);
        }
        return URI.create(url.toString()).normalize();
    }

    public URI getUri() {
        return buildUri("/activity");
    }

    public URI getServletUri() {
        return buildUri("/plugins/servlet/streams");
    }

    /**
     * Set the max results
     */
    public StreamsFeedUriBuilder setMaxResults(final int maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public StreamsFeedUriBuilder setTimeout(final int timeout) {
        this.timeout = timeout;
        return this;
    }

    public StreamsFeedUriBuilder addApplication(String name) {
        parameters.put("application", name);
        return this;
    }

    public StreamsFeedUriBuilder addLocalOnly(boolean localOnly) {
        parameters.put("local", Boolean.toString(localOnly));
        return this;
    }

    public StreamsFeedUriBuilder addProviderFilter(
            String providerKey, String filterKey, Pair<StreamsFilterType.Operator, Iterable<String>> filter) {
        parameters.put(providerKey, toString(filterKey, filter));
        return this;
    }

    public StreamsFeedUriBuilder addAuthOnly(boolean authOnly) {
        parameters.put("authOnly", Boolean.toString(authOnly));
        return this;
    }

    public StreamsFeedUriBuilder addStandardFilter(String filterKey, StreamsFilterType.Operator op, String value) {
        return addStandardFilter(
                filterKey, Pair.<StreamsFilterType.Operator, Iterable<String>>pair(op, ImmutableList.of(value)));
    }

    public StreamsFeedUriBuilder addStandardFilter(String filterKey, StreamsFilterType.Operator op, Date date) {
        return addStandardFilter(
                filterKey,
                Pair.<StreamsFilterType.Operator, Iterable<String>>pair(
                        op, ImmutableList.of(Long.toString(date.getTime()))));
    }

    public StreamsFeedUriBuilder addStandardFilter(
            String filterKey, StreamsFilterType.Operator op, Iterable<String> values) {
        return addStandardFilter(filterKey, Pair.pair(op, values));
    }

    public StreamsFeedUriBuilder addStandardFilter(
            String filterKey, Pair<StreamsFilterType.Operator, Iterable<String>> filter) {
        parameters.put("streams", toString(filterKey, filter));
        return this;
    }

    private String toString(String filterKey, Pair<StreamsFilterType.Operator, Iterable<String>> filter) {
        return filterKey + " " + filter.first() + " " + Joiner.on(' ').join(filter.second());
    }

    public StreamsFeedUriBuilder addLegacyFilterUser(String filterUser) {
        parameters.put("filterUser", filterUser);
        return this;
    }

    public StreamsFeedUriBuilder addLegacyKey(String key) {
        parameters.put("key", key);
        return this;
    }

    public StreamsFeedUriBuilder setLegacyMaxDate(long maxDate) {
        parameters.put("maxDate", Long.toString(maxDate));
        return this;
    }

    public StreamsFeedUriBuilder addAuthenticationParameterIfLoggedIn() {
        // This method can't really be implemented in a testing environment,
        // as we can't determine whether we want to add an os_authType param
        // or FEAUTH token
        throw new UnsupportedOperationException("Don't use in tests");
    }

    public StreamsFeedUriBuilder addOSUserAuthenticationParameter() {
        parameters.put("os_authType", "basic");
        return this;
    }

    public StreamsFeedUriBuilder setLegacyMinDate(long minDate) {
        parameters.put("minDate", Long.toString(minDate));
        return this;
    }

    public StreamsFeedUriBuilder addProvider(String key) {
        providers.add(key);
        return this;
    }

    public StreamsFeedUriBuilder addUseAcceptLang(boolean useAcceptLang) {
        parameters.put("use-accept-lang", Boolean.toString(useAcceptLang));
        return this;
    }

    private static String encode(final String uriComponent) {
        try {
            return URLEncoder.encode(uriComponent, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("This can't happen", e);
        }
    }
}
