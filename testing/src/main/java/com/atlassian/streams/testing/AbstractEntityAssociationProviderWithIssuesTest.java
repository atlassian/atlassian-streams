package com.atlassian.streams.testing;

import java.net.URI;

import org.hamcrest.Matchers;
import org.junit.Test;

import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StandardStreamsFilterOption;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.EntityIdentifierMatchers.hasNoEntityIdentifiers;

public abstract class AbstractEntityAssociationProviderWithIssuesTest extends AbstractEntityAssociationProviderTest {
    protected static final String ISSUE_ENTITY_KEY = "PROJECT-1";

    protected abstract String getIssueUriPath(String key);

    protected abstract URI getIssueEntityType();

    protected abstract void setIssueExists(String key, boolean exists);

    protected abstract void setIssueViewPermission(String key, boolean permitted);

    protected abstract void setIssueEditPermission(String key, boolean permitted);

    protected URI getIssueUri(String key) {
        return URI.create(BASE_URL + getIssueUriPath(key));
    }

    protected EntityIdentifier getIssueEntityIdentifier(String key) {
        return new EntityIdentifier(getIssueEntityType(), key, getIssueUri(key));
    }

    @Test
    public void assertThatIssueUriReturnsIssueAndProjectEntityIdentifiers() {
        setIssueExists(ISSUE_ENTITY_KEY, true);
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasIssueAndProjectKeys(
                provider.getEntityIdentifiers(getIssueUri(ISSUE_ENTITY_KEY)), ISSUE_ENTITY_KEY, PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatIssueUriWithRequestParametersReturnsIssueAndProjectEntityIdentifiers() {
        setIssueExists(ISSUE_ENTITY_KEY, true);
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasIssueAndProjectKeys(
                provider.getEntityIdentifiers(
                        URI.create(getIssueUri(ISSUE_ENTITY_KEY).toString() + "?req_param=req_value")),
                ISSUE_ENTITY_KEY,
                PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatIssueUriWithHashReturnsIssueAndProjectEntityIdentifiers() {
        setIssueExists(ISSUE_ENTITY_KEY, true);
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasIssueAndProjectKeys(
                provider.getEntityIdentifiers(
                        URI.create(getIssueUri(ISSUE_ENTITY_KEY).toString() + "#myhash")),
                ISSUE_ENTITY_KEY,
                PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatIssueKeyWithoutAbsoluteUriReturnsIssueAndProjectEntityIdentifiers() {
        setIssueExists(ISSUE_ENTITY_KEY, true);
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasIssueAndProjectKeys(
                provider.getEntityIdentifiers(URI.create(ISSUE_ENTITY_KEY)), ISSUE_ENTITY_KEY, PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatUnknownIssueUriReturnsNoIdentifier() {
        setIssueExists(ISSUE_ENTITY_KEY, false);
        setProjectExists(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getEntityIdentifiers(getIssueUri(ISSUE_ENTITY_KEY)), hasNoEntityIdentifiers());
    }

    @Test
    public void assertThatUnknownIssueKeyReturnsNoIdentifier() {
        setIssueExists(ISSUE_ENTITY_KEY, false);
        setProjectExists(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getEntityIdentifiers(URI.create(ISSUE_ENTITY_KEY)), hasNoEntityIdentifiers());
    }

    @Test
    public void assertThatIssueIdentifierReturnsIssueUri() {
        assertThat(
                provider.getEntityURI(getIssueEntityIdentifier(ISSUE_ENTITY_KEY)),
                equalTo(some(getIssueUri(ISSUE_ENTITY_KEY))));
    }

    @Test
    public void assertThatIssueIdentifierReturnsIssueFilterKey() {
        assertThat(
                provider.getFilterKey(getIssueEntityIdentifier(ISSUE_ENTITY_KEY)),
                equalTo(some(StandardStreamsFilterOption.ISSUE_KEY.getKey())));
    }

    private void assertHasIssueAndProjectKeys(
            Iterable<EntityIdentifier> identifiers, String issueKey, String projectKey) {
        assertThat(
                identifiers,
                Matchers.contains(getIssueEntityIdentifier(issueKey), getProjectEntityIdentifier(projectKey)));
    }
}
