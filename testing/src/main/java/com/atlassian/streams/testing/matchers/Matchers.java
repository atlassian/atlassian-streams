package com.atlassian.streams.testing.matchers;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.xml.namespace.QName;

import org.apache.abdera.ext.thread.InReplyTo;
import org.apache.abdera.model.Category;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.ExtensibleElement;
import org.apache.abdera.model.Link;
import org.apache.abdera.model.Person;
import org.apache.abdera.model.Text;
import org.apache.abdera.parser.stax.FOMGenerator;
import org.apache.abdera.util.Constants;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.TypeSafeMatcher;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.google.common.net.MediaType;

import com.atlassian.streams.api.DateUtil;
import com.atlassian.streams.api.StreamsEntry;

import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.size;
import static com.google.common.net.MediaType.ANY_IMAGE_TYPE;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;

import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.ACTIVITY_OBJECT;
import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.ACTIVITY_OBJECT_TYPE;
import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.ACTIVITY_TARGET;
import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.ACTIVITY_VERB;
import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.ATLASSIAN_APPLICATION;
import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.IN_REPLY_TO;
import static com.atlassian.streams.testing.matchers.Matchers.AtomConstants.USR_USERNAME;

public abstract class Matchers {
    public static Matcher<Iterable<? extends Entry>> hasNoEntries() {
        return org.hamcrest.Matchers.emptyIterable();
    }

    public static Matcher<Iterable<? extends Entry>> allEntries(Matcher<Entry> matchers) {
        return everyItem(matchers);
    }

    public static Matcher<? super Iterable<? super Entry>> hasEntry(Matcher<? super Entry> matcher) {
        return hasItem(matcher);
    }

    @SuppressWarnings("unchecked")
    public static Matcher<? super Iterable<? super Entry>> hasEntry(
            Matcher<? super Entry> m1, Matcher<? super Entry> m2) {
        return hasItem(allOf(m1, m2));
    }

    @SuppressWarnings("unchecked")
    public static Matcher<? super Iterable<? super Entry>> hasEntry(
            Matcher<? super Entry> m1, Matcher<? super Entry> m2, Matcher<? super Entry> m3) {
        return hasItem(allOf(m1, m2, m3));
    }

    public static Matcher<? super Iterable<? super Entry>> hasEntry(Matcher<? super Entry>... matchers) {
        return hasItem(allOf(matchers));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntries(Matcher<? super Entry> m) {
        return hasEntries(ImmutableList.of(m));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntries(Matcher<? super Entry> m1, Matcher<? super Entry> m2) {
        return hasEntries(ImmutableList.<Matcher<? super Entry>>of(m1, m2));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntries(
            Matcher<? super Entry> m1, Matcher<? super Entry> m2, Matcher<? super Entry> m3) {
        return hasEntries(ImmutableList.<Matcher<? super Entry>>of(m1, m2, m3));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntries(
            Matcher<? super Entry> m1,
            Matcher<? super Entry> m2,
            Matcher<? super Entry> m3,
            Matcher<? super Entry> m4) {
        return hasEntries(ImmutableList.<Matcher<? super Entry>>of(m1, m2, m3, m4));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntries(Matcher<? super Entry>... matchers) {
        return hasEntries(asList(matchers));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntries(List<Matcher<? super Entry>> matchers) {
        return contains(matchers);
    }

    public static Matcher<Iterable<? extends Element>> hasElements(Matcher<? super Element> matcher) {
        return hasElements(ImmutableList.of(matcher));
    }

    public static Matcher<Iterable<? extends Element>> hasElements(
            Matcher<? super Element> m1, Matcher<? super Element> m2) {
        return hasElements(ImmutableList.<Matcher<? super Element>>of(m1, m2));
    }

    public static Matcher<Iterable<? extends Element>> hasElements(
            Matcher<? super Element> m1, Matcher<? super Element> m2, Matcher<? super Element> m3) {
        return hasElements(ImmutableList.<Matcher<? super Element>>of(m1, m2, m3));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntriesInAnyOrder(Matcher<? super Entry>... matchers) {
        return hasEntriesInAnyOrder(asList(matchers));
    }

    public static Matcher<Iterable<? extends Entry>> hasEntriesInAnyOrder(List<Matcher<? super Entry>> matchers) {
        return containsInAnyOrder(matchers);
    }

    public static Matcher<Iterable<? extends Element>> hasElements(Matcher<? super Element>... matchers) {
        return hasElements(ImmutableList.copyOf(matchers));
    }

    public static Matcher<Iterable<? extends Element>> hasElements(List<Matcher<? super Element>> matchers) {
        return contains(matchers);
    }

    public static Matcher<Iterable<? extends ExtensibleElement>> hasActivityObjects(
            Matcher<? super ExtensibleElement> matcher) {
        return hasActivityObjects(ImmutableList.of(matcher));
    }

    public static Matcher<Iterable<? extends ExtensibleElement>> hasActivityObjects(
            Matcher<? super ExtensibleElement> m1, Matcher<? super ExtensibleElement> m2) {
        return hasActivityObjects(ImmutableList.<Matcher<? super ExtensibleElement>>of(m1, m2));
    }

    public static Matcher<Iterable<? extends ExtensibleElement>> hasActivityObjects(
            Matcher<? super ExtensibleElement> m1,
            Matcher<? super ExtensibleElement> m2,
            Matcher<? super ExtensibleElement> m3) {
        return hasActivityObjects(ImmutableList.<Matcher<? super ExtensibleElement>>of(m1, m2, m3));
    }

    public static Matcher<Iterable<? extends ExtensibleElement>> hasActivityObjects(
            Matcher<? super ExtensibleElement>... matchers) {
        return hasActivityObjects(ImmutableList.copyOf(matchers));
    }

    public static Matcher<Iterable<? extends ExtensibleElement>> hasActivityObjects(
            List<Matcher<? super ExtensibleElement>> matchers) {
        return contains(matchers);
    }

    public static Matcher<Entry> hasTarget(Matcher<? super ExtensibleElement> matcher) {
        return new HasTarget(matcher);
    }

    private static final class HasTarget extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super ExtensibleElement> matcher;

        public HasTarget(Matcher<? super ExtensibleElement> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            Iterable<ExtensibleElement> targets = entry.getExtensions(ACTIVITY_TARGET);
            if (isEmpty(targets)) {
                mismatchDescription.appendText("no activity target");
                return false;
            }
            if (!matcher.matches(getOnlyElement(targets))) {
                mismatchDescription.appendText("target ");
                matcher.describeMismatch(getOnlyElement(targets), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("target where ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> withPublishedDate(Matcher<? super ZonedDateTime> matcher) {
        return new WithPublishedDate(matcher);
    }

    private static final class WithPublishedDate extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super ZonedDateTime> matcher;

        public WithPublishedDate(Matcher<? super ZonedDateTime> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (!matcher.matches(DateUtil.toZonedDate(entry.getPublished()))) {
                mismatchDescription.appendText("publishedDate ");
                matcher.describeMismatch(entry.getPublished(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("publishedDate ").appendDescriptionOf(matcher);
        }
    }

    private static final class IsTitleHtml extends TypeSafeDiagnosingMatcher<Entry> {
        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (entry.getTitleType() == Text.Type.HTML) {
                return true;
            }
            mismatchDescription.appendText("title type is not HTML");
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("title is html");
        }
    }

    public static Matcher<Entry> withTitle(Matcher<? super String> matcher) {
        return new WithTitle(matcher);
    }

    public static Matcher<Entry> withHtmlTitle(Matcher<? super String> matcher) {
        return allOf(new IsTitleHtml(), new WithTitle(matcher));
    }

    public static Matcher<Entry> withCategory(Matcher<Category> matcher) {
        return new WithCategory(matcher);
    }

    public static Matcher<Category> withTerm(Matcher<? super String> matcher) {
        return new WithTerm(matcher);
    }

    public static Matcher<Entry> hasAuthor(Matcher<Element> matcher) {
        return new HasAuthor(matcher);
    }

    private static final class HasAuthor extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<Element> matcher;

        public HasAuthor(Matcher<Element> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (entry.getAuthor() == null) {
                mismatchDescription.appendText("no author");
                return false;
            }
            if (!matcher.matches(entry.getAuthor())) {
                mismatchDescription.appendText("author ");
                matcher.describeMismatch(entry.getAuthor(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("author ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Element> withLink(Matcher<Link> matcher) {
        return new WithLink(matcher);
    }

    private static final class WithLink extends TypeSafeDiagnosingMatcher<Element> {
        private final Matcher<Link> matcher;

        public WithLink(Matcher<Link> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Element element, Description mismatchDescription) {
            Collection<Element> linkElements = filter(element.getElements(), linkPredicate);
            if (linkElements.isEmpty()) {
                mismatchDescription.appendText("link ");
                matcher.describeMismatch(linkElements, mismatchDescription);
                return false;
            }

            for (Element linkElement : linkElements) {
                Link link = (Link) linkElement;
                if (!matcher.matches(link)) {
                    mismatchDescription.appendText("link ");
                    matcher.describeMismatch(link, mismatchDescription);
                    return false;
                }
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("link ").appendDescriptionOf(matcher);
        }
    }

    private static final Predicate<Element> linkPredicate = new Predicate<Element>() {
        public boolean apply(Element element) {
            return element instanceof Link;
        }
    };

    public static Matcher<Link> whereRel(Matcher<? super String> matcher) {
        return new WhereRel(matcher);
    }

    private static final class WhereRel extends TypeSafeDiagnosingMatcher<Link> {
        private final Matcher<? super String> matcher;

        public WhereRel(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Link link, Description mismatchDescription) {
            if (link == null) {
                mismatchDescription.appendText("no link");
                return false;
            }
            if (!matcher.matches(link.getRel())) {
                mismatchDescription.appendText("rel ");
                matcher.describeMismatch(link.getRel(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("rel ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Link> withLinkThatResolvesToAnImage() {
        return new LinkThatResolvesToAnImage();
    }

    private static final class LinkThatResolvesToAnImage extends TypeSafeDiagnosingMatcher<Link> {
        private final UrlThatResolvesToAnImage urlMatcher = new UrlThatResolvesToAnImage();

        @Override
        protected boolean matchesSafely(Link link, Description mismatchDescription) {
            try {
                return urlMatcher.matchesSafely(link.getResolvedHref().toURL(), mismatchDescription);
            } catch (URISyntaxException | MalformedURLException ex) {
                throw new AssertionError(ex);
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("is a link that can be resolved to an image");
        }
    }

    private static final class UrlThatResolvesToAnImage extends TypeSafeDiagnosingMatcher<URL> {
        @Override
        protected boolean matchesSafely(URL url, Description mismatchDescription) {
            try {
                URLConnection connection = url.openConnection();
                try (InputStream ignored = connection.getInputStream()) {
                    MediaType contentType = MediaType.parse(connection.getContentType());
                    if (!contentType.is(ANY_IMAGE_TYPE)) {
                        mismatchDescription
                                .appendText("response content type is non-image content type ")
                                .appendValue(contentType);
                        return false;
                    }
                    if (connection.getContentLength() <= 0) {
                        mismatchDescription
                                .appendText("response content length ")
                                .appendValue(connection.getContentLength());
                        return false;
                    }
                    return true;
                }
            } catch (IOException ex) {
                throw new AssertionError(ex);
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("is a URL that resolves to an image");
        }
    }

    public static Matcher<Link> whereHref(Matcher<? super String> matcher) {
        return new WhereHref(matcher);
    }

    private static final class WhereHref extends TypeSafeDiagnosingMatcher<Link> {
        private final Matcher<? super String> matcher;

        public WhereHref(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Link link, Description mismatchDescription) {
            if (link == null) {
                mismatchDescription.appendText("no link");
                return false;
            }
            if (!matcher.matches(link.getHref().toASCIIString())) {
                mismatchDescription.appendText("href ");
                matcher.describeMismatch(link.getHref(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("href ").appendDescriptionOf(matcher);
        }
    }

    private static final class WithTitle extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        private WithTitle(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (!matcher.matches(entry.getTitle())) {
                mismatchDescription.appendText("title ");
                matcher.describeMismatch(entry.getTitle(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("title is").appendDescriptionOf(matcher);
        }
    }

    private static final class WithCategory extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<Category> matcher;

        private WithCategory(Matcher<Category> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            for (Category category : entry.getCategories()) {
                if (matcher.matches(category)) {
                    return true;
                }
            }

            // this point is only reached upon failures
            mismatchDescription.appendText("category ");
            matcher.describeMismatch(entry.getCategories(), mismatchDescription);
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("category has").appendDescriptionOf(matcher);
        }
    }

    private static final class WithTerm extends TypeSafeDiagnosingMatcher<Category> {
        private final Matcher<? super String> matcher;

        private WithTerm(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Category category, Description mismatchDescription) {
            if (!matcher.matches(category.getTerm())) {
                mismatchDescription.appendText("term ");
                matcher.describeMismatch(category.getTerm(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("term is").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super Element> withText(Matcher<? super String> matcher) {
        return new WithText(matcher);
    }

    private static final class WithText extends TypeSafeDiagnosingMatcher<Element> {
        private final Matcher<? super String> matcher;

        private WithText(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Element element, Description mismatchDescription) {
            if (!matcher.matches(element.getText())) {
                mismatchDescription.appendText("title ");
                matcher.describeMismatch(element.getText(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("text is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super ExtensibleElement> withId(Matcher<? super String> matcher) {
        return new WithId(matcher);
    }

    private static final class WithId extends TypeSafeDiagnosingMatcher<ExtensibleElement> {
        private final Matcher<? super String> matcher;

        private WithId(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ExtensibleElement element, Description mismatchDescription) {
            if (!matcher.matches(element.getSimpleExtension(Constants.ID))) {
                mismatchDescription.appendText("id ");
                matcher.describeMismatch(element.getSimpleExtension(Constants.ID), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("id is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super ExtensibleElement> withType(Matcher<? super String> matcher) {
        return new WithType(matcher);
    }

    private static final class WithType extends TypeSafeDiagnosingMatcher<ExtensibleElement> {
        private final Matcher<? super String> matcher;

        private WithType(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ExtensibleElement element, Description mismatchDescription) {
            if (!matcher.matches(element.getSimpleExtension(ACTIVITY_OBJECT_TYPE))) {
                mismatchDescription.appendText("activity:object-type ");
                matcher.describeMismatch(element.getSimpleExtension(ACTIVITY_OBJECT_TYPE), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("activity:object-type is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super Entry> withSummary(Matcher<? super String> matcher) {
        return new WithSummary(matcher);
    }

    private static final class WithSummary extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        public WithSummary(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (entry.getSummary() == null) {
                mismatchDescription.appendText("no summary");
                return false;
            }
            if (!matcher.matches(entry.getSummary())) {
                mismatchDescription.appendText("summary ");
                matcher.describeMismatch(entry.getSummary(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("summary ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> withEmptyContent() {
        return new WithEmptyContent();
    }

    private static final class WithEmptyContent extends TypeSafeDiagnosingMatcher<Entry> {
        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (entry.getContent() == null) {
                return true;
            }
            mismatchDescription.appendText("content is not empty");
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("content is empty");
        }
    }

    public static Matcher<Entry> withContent(Matcher<? super String> matcher) {
        return new WithContent(matcher);
    }

    private static final class WithContent extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        public WithContent(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (entry.getContent() == null) {
                mismatchDescription.appendText("no content");
                return false;
            }
            if (!matcher.matches(entry.getContent())) {
                mismatchDescription.appendText("content ");
                matcher.describeMismatch(entry.getContent(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("content ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> withUpdatedDate(Matcher<? super Date> matcher) {
        return new WithUpdatedDate(matcher);
    }

    private static final class WithUpdatedDate extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super Date> matcher;

        public WithUpdatedDate(Matcher<? super Date> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (entry.getUpdated() == null) {
                mismatchDescription.appendText("no updated date");
                return false;
            }
            if (!matcher.matches(entry.getUpdated())) {
                mismatchDescription.appendText("updated date ");
                matcher.describeMismatch(entry.getUpdated(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("updated date ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> haveAtomSourceElement() {
        return new HaveAtomSourceElement();
    }

    private static final class HaveAtomSourceElement extends TypeSafeDiagnosingMatcher<Entry> {
        @Override
        protected boolean matchesSafely(Entry item, Description mismatchDescription) {
            if (item.getExtension(Constants.SOURCE) == null) {
                mismatchDescription.appendText("no atom:source element");
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("an atom:source element");
        }
    }

    public static Matcher<Entry> haveAtlassianApplicationElement(Matcher<? super String> matcher) {
        return new HaveAtlassianApplicationElement(matcher);
    }

    private static final class HaveAtlassianApplicationElement extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        private HaveAtlassianApplicationElement(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            String appType = entry.getSimpleExtension(ATLASSIAN_APPLICATION);
            if (appType == null) {
                mismatchDescription.appendText("no atlassian:application element");
                return false;
            } else if (!matcher.matches(appType)) {
                mismatchDescription.appendText("atlassian:application element ");
                matcher.describeMismatch(appType, mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("an atlassian:application element with ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> haveAtomGeneratorElement(Matcher<String> matcher) {
        return new HaveAtomGeneratorElement(matcher);
    }

    private static final class HaveAtomGeneratorElement extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<String> matcher;

        private HaveAtomGeneratorElement(Matcher<String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            String generatorUri = getAtomGeneratorUri(entry).getUri().toASCIIString();
            if (!matcher.matches(generatorUri)) {
                mismatchDescription.appendText("does not contain a generator element with value of ");
                matcher.describeMismatch(generatorUri, mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description
                    .appendText("contains a generator element with value of ")
                    .appendDescriptionOf(matcher);
        }
    }

    private static FOMGenerator getAtomGeneratorUri(Entry entry) {
        return entry.getExtension(Constants.GENERATOR);
    }

    public static Matcher<Entry> haveLink(Matcher<Link> matcher) {
        return new EntryLinkMatcher(hasItem(matcher));
    }

    private static final class EntryLinkMatcher extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<Iterable<? super Link>> matcher;

        public EntryLinkMatcher(Matcher<Iterable<? super Link>> matcher) {
            this.matcher = matcher;
        }

        public void describeTo(Description description) {
            description.appendText("contains a link which is ").appendDescriptionOf(matcher);
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            if (matcher.matches(entry)) {
                return true;
            } else {
                matcher.describeMismatch(entry, mismatchDescription);
                return false;
            }
        }
    }

    public static Matcher<Iterable<? super Link>> hasLink(Matcher<Link> m1, Matcher<Link> m2) {
        return hasLink(ImmutableList.of(m1, m2));
    }

    public static Matcher<Iterable<? super Link>> hasLink(Iterable<Matcher<? super Link>> matchers) {
        return hasItem(allOf(matchers));
    }

    public static Matcher<? super Entry> withAuthorElement(Matcher<? super String> matcher) {
        return new WithAuthorElement(matcher);
    }

    private static final class WithAuthorElement extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        private WithAuthorElement(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            List<Person> people = entry.getAuthors();
            for (Person person : people) {
                if (matcher.matches(person.getSimpleExtension(USR_USERNAME))) {
                    return true;
                }
                mismatchDescription.appendText("user ");
                matcher.describeMismatch(person.getSimpleExtension(USR_USERNAME), mismatchDescription);
            }
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("user is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super Entry> withAuthorLink(Matcher<Link> matcher) {
        return new WithAuthorLink(matcher);
    }

    private static final class WithAuthorLink extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<Link> matcher;

        private WithAuthorLink(Matcher<Link> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            List<Person> people = entry.getAuthors();
            for (Person person : people) {
                Collection<Element> linkElements = filter(person.getElements(), linkPredicate);
                for (Element linkElement : linkElements) {
                    Link link = (Link) linkElement;
                    if (matcher.matches(link)) {
                        return true;
                    }
                }
            }

            mismatchDescription.appendText("link ");
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("user link is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> withVerbElement(Matcher<? super String> matcher) {
        return new WithVerbElement(matcher);
    }

    private static final class WithVerbElement extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        private WithVerbElement(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            List<Element> verbs = entry.getExtensions(ACTIVITY_VERB);
            for (Element verb : verbs) {
                if (matcher.matches(verb.getText())) {
                    return true;
                }
                mismatchDescription.appendText("activity:verb ");
                matcher.describeMismatch(verb.getText(), mismatchDescription);
            }
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("activity:verb is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Entry> withActivityObjectElement(Matcher<? super ExtensibleElement> matcher) {
        return new WithActivityObjectElement(matcher);
    }

    private static final class WithActivityObjectElement extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super ExtensibleElement> matcher;

        private WithActivityObjectElement(Matcher<? super ExtensibleElement> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            List<ExtensibleElement> activityObjects = entry.getExtensions(ACTIVITY_OBJECT);
            for (ExtensibleElement activityObject : activityObjects) {
                if (matcher.matches(activityObject)) {
                    return true;
                }
                mismatchDescription.appendText("activity:object ");
                matcher.describeMismatch(activityObject, mismatchDescription);
            }
            return false;
        }

        public void describeTo(Description description) {
            description.appendText("activity:object ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super Entry> withInReplyToElement(Matcher<? super String> matcher) {
        return new WithInReplyToElement(matcher);
    }

    private static final class WithInReplyToElement extends TypeSafeDiagnosingMatcher<Entry> {
        private final Matcher<? super String> matcher;

        private WithInReplyToElement(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Entry entry, Description mismatchDescription) {
            String inReplyTo = getInReplyToRef(entry);
            if (!matcher.matches(inReplyTo)) {
                mismatchDescription.appendText("in-reply-to ");
                matcher.describeMismatch(inReplyTo, mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("in-reply-to is ").appendDescriptionOf(matcher);
        }
    }

    private static String getInReplyToRef(Entry entry) {
        InReplyTo inReplyTo = entry.getExtension(IN_REPLY_TO);
        if (inReplyTo != null && inReplyTo.getRef() != null) {
            return inReplyTo.getRef().toASCIIString();
        }
        return null;
    }

    public static Matcher<Iterable<Entry>> inDescendingOrder() {
        return new InDescendingOrder();
    }

    private static final class InDescendingOrder extends TypeSafeDiagnosingMatcher<Iterable<Entry>> {
        private static Ordering<Entry> descending = new Ordering<Entry>() {
            public int compare(Entry entry1, Entry entry2) {
                return entry2.getPublished().compareTo(entry1.getPublished());
            }
        };

        @Override
        protected boolean matchesSafely(Iterable<Entry> entries, Description mismatchDescription) {
            if (!descending.isOrdered(entries)) {
                mismatchDescription.appendText("was ").appendValueList("[", ", ", "]", entries);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("dates sorted in descending order");
        }
    }

    public static Matcher<Iterable<Entry>> hasEntryCount(Matcher<? super Integer> matcher) {
        return new EntryCount(matcher);
    }

    private static final class EntryCount extends TypeSafeDiagnosingMatcher<Iterable<Entry>> {
        private final Matcher<? super Integer> matcher;

        public EntryCount(Matcher<? super Integer> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Iterable<Entry> entries, Description mismatchDescription) {
            int size = size(entries);
            if (!matcher.matches(size)) {
                mismatchDescription.appendText("entries count ");
                matcher.describeMismatch(size, mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("entries count is ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<Iterable<? super StreamsEntry.Link>> hasStreamsLink(
            Matcher<? super StreamsEntry.Link> matcher) {
        return hasItem(matcher);
    }

    public static Matcher<StreamsEntry.Link> whereStreamsRel(Matcher<? super String> matcher) {
        return new WhereStreamsRel(matcher);
    }

    private static final class WhereStreamsRel extends TypeSafeDiagnosingMatcher<StreamsEntry.Link> {
        private final Matcher<? super String> matcher;

        public WhereStreamsRel(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(StreamsEntry.Link link, Description mismatchDescription) {
            if (link == null) {
                mismatchDescription.appendText("no link");
                return false;
            }
            if (!matcher.matches(link.getRel())) {
                mismatchDescription.appendText("rel ");
                matcher.describeMismatch(link.getRel(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("rel ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<? super String> matchesRegEx(String regex) {
        return new RegexpMatcher(regex);
    }

    private static final class RegexpMatcher extends TypeSafeMatcher<String> {
        private final String regexp;

        private RegexpMatcher(String regexp) {
            this.regexp = regexp;
        }

        @Override
        protected boolean matchesSafely(String s) {
            return s != null && s.matches(regexp);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("matches='" + regexp + "'");
        }
    }

    static final class AtomConstants {
        // Prefixes
        public static final String ACTIVITY_PREFIX = "activity";
        public static final String ATLASSIAN_PREFIX = "atlassian";
        public static final String USR_PREFIX = "usr";
        public static final String THR_PREFIX = "thr";

        // Namespaces
        public static final String ACTIVITY_NS = "http://activitystrea.ms/spec/1.0/";
        public static final String ATLASSIAN_NS = "http://streams.atlassian.com/syndication/general/1.0";
        public static final String USR_NS = "http://streams.atlassian.com/syndication/username/1.0";
        public static final String THR_NS = "http://purl.org/syndication/thread/1.0";

        // Local parts
        public static final String LN_OBJECT = "object";
        public static final String LN_OBJECT_TYPE = "object-type";
        public static final String LN_TARGET = "target";
        public static final String LN_VERB = "verb";
        public static final String LN_APPLICATION = "application";
        public static final String LN_USERNAME = "username";
        public static final String LN_INREPLYTO = "in-reply-to";

        // Qualified Names
        public static final QName ACTIVITY_VERB = new QName(ACTIVITY_NS, LN_VERB, ACTIVITY_PREFIX);
        public static final QName ACTIVITY_OBJECT = new QName(ACTIVITY_NS, LN_OBJECT, ACTIVITY_PREFIX);
        public static final QName ACTIVITY_TARGET = new QName(ACTIVITY_NS, LN_TARGET, ACTIVITY_PREFIX);
        public static final QName ACTIVITY_OBJECT_TYPE = new QName(ACTIVITY_NS, LN_OBJECT_TYPE, ACTIVITY_PREFIX);
        public static final QName ATLASSIAN_APPLICATION = new QName(ATLASSIAN_NS, LN_APPLICATION, ATLASSIAN_PREFIX);
        public static final QName USR_USERNAME = new QName(USR_NS, LN_USERNAME, USR_PREFIX);
        public static final QName IN_REPLY_TO = new QName(THR_NS, LN_INREPLYTO, THR_PREFIX);
    }
}
