package com.atlassian.streams.spi;

import java.net.URI;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import static org.hamcrest.Matchers.hasItems;

public class EntityIdentifierMatchers {
    public static Matcher<Iterable<EntityIdentifier>> hasEntityIdentifiers(Matcher<EntityIdentifier>... matchers) {
        return hasItems(matchers);
    }

    public static Matcher<Iterable<? extends EntityIdentifier>> hasNoEntityIdentifiers() {
        return Matchers.<EntityIdentifier>emptyIterable();
    }

    public static Matcher<EntityIdentifier> withType(Matcher<? super URI> matcher) {
        return new WithType(matcher);
    }

    private static final class WithType extends TypeSafeDiagnosingMatcher<EntityIdentifier> {
        private final Matcher<? super URI> matcher;

        public WithType(Matcher<? super URI> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(EntityIdentifier identifier, Description mismatchDescription) {
            if (!matcher.matches(identifier.getType())) {
                mismatchDescription.appendText("type ");
                matcher.describeMismatch(identifier.getType(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("type ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<EntityIdentifier> withUri(Matcher<? super URI> matcher) {
        return new WithUri(matcher);
    }

    private static final class WithUri extends TypeSafeDiagnosingMatcher<EntityIdentifier> {
        private final Matcher<? super URI> matcher;

        public WithUri(Matcher<? super URI> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(EntityIdentifier identifier, Description mismatchDescription) {
            if (!matcher.matches(identifier.getUri())) {
                mismatchDescription.appendText("uri ");
                matcher.describeMismatch(identifier.getUri(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("uri ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<EntityIdentifier> withValue(Matcher<? super String> matcher) {
        return new WithValue(matcher);
    }

    private static final class WithValue extends TypeSafeDiagnosingMatcher<EntityIdentifier> {
        private final Matcher<? super String> matcher;

        public WithValue(Matcher<? super String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(EntityIdentifier identifier, Description mismatchDescription) {
            if (!matcher.matches(identifier.getValue())) {
                mismatchDescription.appendText("value ");
                matcher.describeMismatch(identifier.getValue(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("value ").appendDescriptionOf(matcher);
        }
    }
}
