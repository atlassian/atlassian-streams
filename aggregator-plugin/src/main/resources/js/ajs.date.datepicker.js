(function() {

    AJS.$(document).bind('dateLocalizationLoaded.streams', function() {
        // delete the cached defaults
        ActivityStreams.resetDatepickerDefaults();

        AJS.$.datepicker.setDefaults(ActivityStreams.getDatepickerDefaults());
    });

})();