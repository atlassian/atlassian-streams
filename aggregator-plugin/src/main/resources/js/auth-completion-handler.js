var applinkId = AJS.$("#applink-id").val(),
    authAdminUri = AJS.$("#applink-auth-admin-uri").val(),
    success = AJS.$("#applink-authorized").val();

if (window.opener && window.opener.AJS && window.opener.AJS.$) {
    window.opener.AJS.$(window.opener.document).trigger('applinks.auth.completion',
        [applinkId, (success == "true"), authAdminUri]);
    window.close();
}