ActivityStreams = ActivityStreams || {};

/**
 * Helper functions for setting up a Streams gadget, to minimize the amount of js that needs to live in the gadget xml
 */
ActivityStreams.gadget = (function() {
    var prefs = new gadgets.Prefs(),
        loadedDateLibrary = false,
        titleRequired = prefs.getBool("titleRequired"),
        stream;

    if (titleRequired) {
        gadgets.window.setTitle(prefs.getMsg('gadget.activity.stream.title'));
    }

    /**
     * Retrieves an i18n message from the gadget prefs
     * @method getMsg
     * @param {String} key The key of the message to retrieve
     * @return {String} An internationalized message
     */
    function getMsg(key) {
        return prefs.getMsg(key);
    }

    /**
     * It tests whether given dateLocal is in valid format
     * @param {String} dateLocal
     * @returns {boolean}
     */
    function validateLocalDate(dateLocal) {
        // Base on entries in atlassian-streams/streams-jira-plugin/src/main/resources/atlassian-plugin.xml
        // we can assume that we have few formats of dateLocal e.g.
        //
        // en-AU
        // uz-Cyrl-UZ
        // en-MOON
        // kok-IN
        var dateLocalRegex = /^[a-z]{2,3}-([a-z]{2,4}$|[a-z]{1,4}-[a-z]{2,4}$)/gi;

        return dateLocalRegex.test(dateLocal);
    }

    /**
     * Does the heavy-lifting for creating the gadget view
     * @method getTemplate
     * @param {Object} gadget The gadget object
     * @param {Object} args Arguments object passed from the Gadget initialization
     * @param {String} baseUrl The base url
     */
    function getTemplate(gadget, args, baseUrl) {
        if (stream && stream.update) {
            // if stream object already exists, the gadget was probably refreshed so just tell the stream to update itself
            stream.update();
        } else {
            var container = gadget.getView(),
                dateLocal = prefs.getLang() + '-' + prefs.getCountry(),
                localeJsUrl;

            if (validateLocalDate(dateLocal)) {
                localeJsUrl = args.dateLocaleUrl.replace('date-default', 'date-' + dateLocal);
            } else {
                console.warn('Wrong or unknown local date format. Default local date has been set.');
            }

            AJS.$.ajax({
                type: 'get',
                url: baseUrl + '/rest/activity-stream/1.0/preferences',
                global: false,
                dataType: 'json',
                success: function(data, status) {
                    ActivityStreams.setDatePrefs(data);
                    AJS.$(document).trigger('preferencesLoaded.streams');
                    ActivityStreams.gadget.performanceMark('preferencesLoaded');
                },
                complete: function() {
                    if (!loadedDateLibrary) {
                        // load the necessary date.js localized file. the default should already be loaded, so if this fails it will fall back to that
                        AJS.$.ajax({
                            type: 'get',
                            url: localeJsUrl,
                            dataType: 'jsonp',
                            jsonp: 'callback',
                            jsonpCallback: 'ActivityStreams.loadDateJs',
                            global: false,
                            success: function(data) {
                                loadedDateLibrary = true;
                                // trigger this function so that stuff that depends on this localized content knows it can be safely loaded
                                AJS.$(document).trigger('dateLocalizationLoaded.streams');
                                ActivityStreams.gadget.performanceMark('dateLocalizationLoaded');
                            }
                        });
                    }
                }
            });

            // reset view
            gadget.getView().empty();

            gadget.showLoading();
            ActivityStreams.gadget.performanceMark('loadingBarShown');
            ActivityStreams.hijackGadgetConfig(gadget);
            stream = ActivityStreams.stream({
                id: gadget.getPrefs().getModuleId(),
                baseUrl: baseUrl,
                maxResults: gadget.getPref("numofentries"),
                // if gadget prefs can be updated by current user, gadget will have getConfig() fn
                isConfigurable: !!gadget.getConfig,
                container: container,
                title: gadgets.util.unescapeString(gadget.getPref("title")),
                // Hide the header when on "View Issue" screen, or whenever titleRequired is false
                // (eg project page or user profile page)
                hideHeader: gadget.getViewMode() === 'issueTab' || !titleRequired,
                loaded: function() {
                    ActivityStreams.gadget.performanceMark('streamLoaded');
                    gadget.hideLoading();
                },
                resized: function() {
                    gadget.resize();
                }
            });

            //only show the footer if the gadget is configurable.
            if (gadget.getPrefs().getString("isConfigurable") === "false") {
                gadget.getFooter().hide();
            } else {
                // move the footer to the header in DOM, as its positioned in header with css anyway in activity gadget - this is to make it accessible
                var footer = gadget.getFooter();
                if (footer.length && AJS.$('.activity-stream-sparse-header').length) {
                    footer.appendTo('.activity-stream-sparse-header');
                    footer.addClass('moved-to-header');
                }
            }

        }
    }

    /**
     * Creates and returns a descriptor object for initializing the gadget
     * @method getDescriptor
     * @param {Object} gadget The gadget object
     * @param {Object} args Arguments object passed from the Gadget initialization
     * @param {String} baseUrl The base url
     */
    function getDescriptor(gadget, args, baseUrl) {
        // we don't actually use this because we're doing overriding with our own config, but we need it to exist
        // so that the edit link is available in the dropdown
        return  {
            action: baseUrl + "/rest/activity-stream/1.0/validate",
            theme : "gdt",
            fields: []
        };
    }

    return {
        getMsg: getMsg,
        template: getTemplate,
        validateLocalDate: validateLocalDate, // just for tests
        descriptor: getDescriptor,
        /**
         * Use User Timing API to mark certain event in activity streams lifecycle
         *
         * @param name of the mark
         */
        performanceMark: function performanceMark(name) {
            var PREFIX = 'gadgets.activityStream.';
            name = PREFIX + name;

            window.performance && window.performance.mark && window.performance.mark(name);
        }

    };
})();
