/*
 * The relative date functionality is loosely based on John Resig's JavaScript Pretty Date
 * Copyright (c) 2008 John Resig (jquery.com)
 * Licensed under the MIT license.
 *
 */
(function() {

    AJS = AJS || {};
    AJS.Date = AJS.Date || {};
    var timeFormat = 't',
        dateFormat = 'D';


    /**
     * Util function for getting an internationalized string via a unique key
     * @method geti18n
     * @param {String} key Key that corresponds to the desired text string
     * @return {String} Internationalized string
     */
    function geti18n(key) {
        return ActivityStreams.getMsg('gadget.activity.stream.time.' + key);
    }

    /**
     * Given a Date returns the number of milliseconds since midnight Jan 1, 1970 in UTC.
     * @method getUtcTime
     * @param {Date} date date to get the time of in milliseconds in UTC
     * @return number of milliseconds since midnight Jan 1, 1970 in UTC
     */
    function getUtcTime(date) {
        return date.getTime && date.getTime() + date.getTimezoneOffset() * 60;
    }

    /**
     * Returns a relative past timestamp
     * @method getRelativePastDate
     * @param {Date} date Date representation of the time in question
     * @param {Number} diff The number of seconds since the event occurred
     * @param {Number} day_diff The number of days since the event occurred
     * @param {Boolean} isFineGrained Determines the granularity of the relative date (seconds if true, days if false)
     * @return {String} String containing relative past timestamp
     */
    function getRelativePastDate(date, diff, day_diff, isFineGrained) {
        if (day_diff === 0) {
            // date is sometime today
            return (isFineGrained && (
                diff < 60 && geti18n('past.moment') || // Moments ago
                diff < 120 && geti18n('past.minute') || // 1 minute ago
                diff < 3600 && AJS.format(geti18n('past.minutes'), Math.floor( diff / 60 )) || // N minutes ago
                diff < 7200 && geti18n('past.hour') || // 1 hour ago
                diff < 86400 && AJS.format(geti18n('past.hours'), Math.floor( diff / 3600 )) // N hours ago
                )) || geti18n('today');
        } else {
            return day_diff === 1 && (isFineGrained && AJS.format(geti18n('past.dayWithTime'), date.toString(timeFormat)) || geti18n('past.day')) || // Yesterday (at HH:MM)
                   day_diff < 7 && (isFineGrained && AJS.format(geti18n('datetime'), date.toString('dddd'), date.toString(timeFormat)) || date.toString('dddd')) || // Day of week (at HH:MM)
                   day_diff < 365 && (isFineGrained && AJS.format(geti18n('datetime'), date.toString('m'), date.toString(timeFormat)) || date.toString('m')) || // Month DD (at HH:MM)
                   day_diff >= 365 && (isFineGrained && date.toString(dateFormat) || date.toString('y')); // Month DD YYYY (at HH:MM)
        }
    }

    /**
     * Converts date object into localized ISO time string.
     *
     * @param date - date object
     * @returns {string} - localized ISO time string
     */
    function toISOLocal(date) {
        const z  = n => `${n}`.padStart(2, '0');
        const zz = n => `${n}`.padStart(3, '0');
        let off = date.getTimezoneOffset();
        const sign = off > 0 ? '-' : '+';
        off = Math.abs(off);

        return date.getFullYear() + '-'
            + z(date.getMonth() +1 ) + '-' +
            z(date.getDate()) + 'T' +
            z(date.getHours()) + ':'  +
            z(date.getMinutes()) + ':' +
            z(date.getSeconds()) + '.' +
            zz(date.getMilliseconds()) +
            sign + z(off / 60 | 0) + ':' + z(off % 60);
    }

    /*
     * Converts a timezone offset into the number of minutes from UTC
     * @method getTimezoneOffsetInMinutes
     * @param {Number|String} fromOffset The timezone offset to convert from
     * @return {Number} The number of minutes from UTC
     */
    function getTimezoneOffsetInMinutes(offset) {
        var parsed,
            mins = 0;
        if (offset) {
            parsed = offset.toString().match(/([-+])([0-9][0-9])([0-9][0-9])/);
            mins += parsed[2] * 60;
            mins += Number(parsed[3]);
            if (parsed[1] === '-') {
                mins = -mins;
            }
        }
        return mins;
    }

    /*
     * Given two timezone offsets, calculates the difference between them in minutes
     * @method getTimezoneOffsetDifferenceInMinutes
     * @param {Number|String} fromOffset The timezone offset to convert from
     * @param {Number|String} toOffset The timezone offset to convert to
     * @return {Number} The number of minutes to add to convert between the offsets
     */
    function getTimezoneOffsetDifferenceInMinutes(fromOffset, toOffset) {
        return getTimezoneOffsetInMinutes(fromOffset) - getTimezoneOffsetInMinutes(toOffset);
    }

    /*
     * Checks to see if a date object a valid date or not
     * @method isValidDate
     * @param {Date} date A date object
     * @return {Boolean} False if the date doesn't exist or is invalid, true otherwise
     */
    function isValidDate(date) {
        return date && !(date.toString() === "Invalid Date" || date.toString() === 'NaN');
    }

    /*
     * Converts a date in the local (browser) timezone to the equivalent day/time in a timezone with the specified offset
     * @method handleTimezoneOffset
     * @param {Date} date A date object
     * @param {Number|String} offset (optional) The timezone offset, from UTC (eg, -0700)
     * @return {Date} The date object adjusted for the desired timezone.  Note that the date object will still technically
     *                be in the local (browser's) timezone.
     */
    function handleTimezoneOffset(date, offset) {
        var localOffset;
        if (isValidDate(date) && offset) {
            localOffset = date.getUTCOffset();
            date.addMinutes(getTimezoneOffsetDifferenceInMinutes(offset, localOffset));
        }
        return date;
    }

    /*
     * Take a string representation of a date and parses it into a date object
     * @method parseDate
     * @param {String|Number} time A timestamp in any of ISO, UTC, or string format
     * @param {String|Number} timezoneOffset (optional) A numerical offset from UTC time
     * @return {Date} A date object representing the specified timestamp
     */
    function parseDate(time, timezoneOffset) {
        // if time is already a date object, it doesn't need to be parsed
        if (time instanceof Date) {
            return time;
        }
        var exp = /([0-9])T([0-9])/,
            date = new Date(time);
        // IE will return NaN, FF and Webkit will return 'Invalid Date' object
        if (isValidDate(date)) {
            return handleTimezoneOffset(date, timezoneOffset);
        }
        if (time.match(exp)) {
            // ISO time.  We need to do some formatting to be able to parse it into a date object
            time = time
                // ignore any thing less than seconds
                .replace(/\.[0-9]*/g,"")
                // for the Date ctor to use UTC time
                .replace(/Z$/, " -00:00")
                // remove 'T' separator
                .replace(exp,"$1 $2");
        }
        // more formatting to make it parseable
        time = time
            // replace dash-separated dates with forward-slash separated
            .replace(/([0-9]{4})-([0-9]{2})-([0-9]{2})/g,"$1/$2/$3")
            // get rid of semicolon and add space in front of timezone offset (for Safari, Chrome, IE6)
            .replace(/\s?([-\+][0-9]{2}):([0-9]{2})/, ' $1$2');
        date = new Date(time || "");
        return handleTimezoneOffset(date, timezoneOffset);
    }

    /*
     * Given two dates, returns the difference between them in seconds
     * @method getDifference
     * @param {Number|Date} time A timestamp in any of ISO, UTC, or string format
     * @param {Date} now (optional) Date to use as the current date.  Intended primarily for use in unit tests.
     * @return {Number} The difference between the two dates, or null if no difference can be calculated
     */
    function getDifference(time, now) {
        var date = new Date(time),
            diff;
        if (!date) {
            date = parseDate(time);
        }
        now = now || new Date();
        diff = (getUtcTime(now) - getUtcTime(date)) / 1000;
        if (isNaN(diff)) {
            return null;
        }
        return diff;
    }

    /*
     * Takes a timestamp and returns a new Date object with the same day with a time of 00:00:00.000.
     * @method getStartOfDay
     * @param {Number|Date} time A timestamp in any of ISO, UTC, or string format
     * @return {Date} a Date with the same date and the time set to 00:00:00.000
     */
    function getStartOfDay(time) {
        var date = parseDate(time);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    }

    /*
     * Takes a timestamp and returns a relative ("n minutes ago" or "n minutes from now") string representation
     * @method getRelativeDate
     * @param {String|Number} time A timestamp in any of ISO, UTC, or string format
     * @param {Date} now (optional) Date to use as the current date.  Intended primarily for use in unit tests.
     * @return {String} A natural language representation of the timestamp relative to now
     */
    function getRelativeDate(time, now, isFineGrained) {
        now = now || new Date();
        var date = parseDate(time),
            diff = getDifference(date, now),
            day_diff;
        if (diff === null) {
            return null;
        } else if (diff < 0) {
            // if date is in the future, show full date/time
            return AJS.format(geti18n('datetime'), date.toString(dateFormat), date.toString(timeFormat));
        } else {
            // discard time of day when calculating the day difference; even at 1am, 23:59pm @ the prev day is yesterday
            day_diff = getDifference(getStartOfDay(date), getStartOfDay(now)) / 86400;
            return getRelativePastDate(date, diff, day_diff, isFineGrained);
        }
    }

    /*
     * Takes a timestamp and returns a string representation with granularity as fine as seconds
     * @method getFineRelativeDate
     * @param {String|Number} time A timestamp in any of ISO, UTC, or string format
     * @param {Date} now (optional) Date to use as the current date.  Intended primarily for use in unit tests.
     * @return {String} A natural language representation of the timestamp, with up to second granularity
     */
    AJS.Date.getFineRelativeDate = function(time, now){
        return getRelativeDate(time, now, true);
    };

    /*
     * Takes a timestamp and returns a string representation with granularity as fine as days
     * @method getCoarseRelativeDate
     * @param {String|Number} time A timestamp in any of ISO, UTC, or string format
     * @param {Date} now (optional) Date to use as the current date.  Intended primarily for use in unit tests.
     * @return {String} A natural language representation of the timestamp, with up to day granularity
     */
    AJS.Date.getCoarseRelativeDate = function(time, now) {
        return getRelativeDate(time, now, false);
    };

    AJS.Date.parse = parseDate;
    AJS.Date.handleTimezoneOffset = handleTimezoneOffset;

    /*
     * Sets the default date format
     * @method setDateFormat
     * @param {String} format A date format string
     */
    AJS.Date.setDateFormat = function(format) {
        dateFormat = format ? format : dateFormat;
    };

    /*
     * Sets the default time format
     * @method setTimeFormat
     * @param {String} format A time format string
     */
    AJS.Date.setTimeFormat = function(format) {
        // The apps use http://download.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html
        // We need to convert 'a' to 'tt' because that's what date.js expects
        // Then we need to ensure anything in quotes remains as is. This will not work consistently in all cases.
        // The best solution would be to replace date conversions with a REST endpoint, particularly since this JS
        // code is not actually consistent with its documentation (Oracle SimpleDateFormat)
        timeFormat = format ? format.replace(/a/g, 'tt').replace(/'(\w+)'/g, "\\$1") : timeFormat;
    };

    AJS.Date.toISOLocal = toISOLocal;
})();
