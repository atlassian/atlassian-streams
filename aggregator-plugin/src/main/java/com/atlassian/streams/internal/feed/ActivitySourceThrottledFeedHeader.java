package com.atlassian.streams.internal.feed;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link FeedHeader} that indicates which sources exceeded their wallclock budget.
 */
public class ActivitySourceThrottledFeedHeader implements FeedHeader {
    private final String sourceName;

    public ActivitySourceThrottledFeedHeader(@Nonnull String sourceName) {
        this.sourceName = checkNotNull(sourceName);
    }

    @Nonnull
    public String getSourceName() {
        return sourceName;
    }
}
