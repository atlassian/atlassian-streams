package com.atlassian.streams.internal.rest.representations;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

public class StreamsConfigRepresentation {
    @JsonProperty
    final Collection<ProviderFilterRepresentation> filters;

    @JsonCreator
    public StreamsConfigRepresentation(@JsonProperty("filters") Collection<ProviderFilterRepresentation> filters) {
        this.filters = ImmutableList.copyOf(filters);
    }

    public Collection<ProviderFilterRepresentation> getFilters() {
        return filters;
    }
}
