package com.atlassian.streams.internal.feed;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.ActivityProvider.Error;

import static com.google.common.collect.Iterables.getOnlyElement;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_TIMEZONE_OFFSET;
import static com.atlassian.streams.internal.atom.abdera.StreamsAbdera.AtomParsedFeedHeader;

public class FeedAggregator {
    /**
     * Aggregate the given list of feeds into one feed. If provided a single feed whose
     * URI is identical to the aggregate feed URI, that feed will be returned with no
     * changes.  Otherwise, the title, subtitle, and self link will be set, and the
     * entries will be concatenated.
     *
     * @param feedResponses The feeds to aggregate, including errored feed responses
     * @param selfUri The URI for the aggregated feed's self link
     * @param maxResults The maximum number of entries to appear in the output feed
     * @param title The title of the feed
     * @return The aggregated feed
     */
    public FeedModel aggregate(
            final Iterable<Either<Error, FeedModel>> feedResponses,
            final Uri selfUri,
            final int maxResults,
            Option<String> title) {
        if (sizeEquals(feedResponses, 1)) {
            final Either<Error, FeedModel> onlyResponse = getOnlyElement(feedResponses);
            if (onlyResponse.isRight()) {
                final FeedModel feed = onlyResponse.right().get();
                if (feed.getUri().equals(selfUri)) {
                    return feed;
                }
            }
        }

        final List<FeedModel> successfulFeeds = StreamSupport.stream(feedResponses.spliterator(), false)
                .filter(Either::isRight)
                .map(e -> e.right().get())
                .collect(Collectors.toList());

        final List<FeedHeader> successfulFeedHeaders = successfulFeeds.stream()
                .map(FeedModel::getHeaders)
                .flatMap(it -> StreamSupport.stream(it.spliterator(), false))
                .filter(isNonTimezoneOffsetHeader) // The local timezone offset is added to the feed when writing it
                // out, here we filter remote offsets because they aren't relevant
                .collect(Collectors.toList());

        final Optional<ZonedDateTime> maybeLatest = successfulFeeds.stream()
                .map(FeedModel::getUpdatedDate)
                .filter(Objects::nonNull)
                .max(Comparator.naturalOrder());

        final Comparator<FeedEntry> reverseChronological =
                Comparator.comparing(FeedEntry::getEntryZonedDate).reversed();

        final List<FeedEntry> sortedFeedEntries = successfulFeeds.stream()
                .flatMap(feed -> StreamSupport.stream(feed.getEntries().spliterator(), false)
                        .map(feedEntry -> feedEntry.toAggregatedEntry(some(feed))))
                .sorted(reverseChronological)
                .limit(maxResults)
                .collect(Collectors.toList());

        FeedModel.Builder feedModel = FeedModel.builder(selfUri)
                .title(title)
                .addHeaders(createErrorFeedHeaders(feedResponses))
                .addHeaders(successfulFeedHeaders)
                .addEntries(sortedFeedEntries);
        maybeLatest.ifPresent(latest -> feedModel.updated(latest));

        return feedModel.build();
    }

    private List<FeedHeader> createErrorFeedHeaders(Iterable<Either<Error, FeedModel>> feedResponses) {
        return StreamSupport.stream(feedResponses.spliterator(), false)
                .filter(Either::isLeft)
                .map(e -> e.left().get())
                .map(error -> {
                    if (!error.getApplicationLinkName().isDefined()) {
                        return Optional.<FeedHeader>empty();
                    }
                    final String sourceName = error.getApplicationLinkName().get();
                    switch (error.getType()) {
                        case BANNED:
                            return Optional.of(new ActivitySourceBannedFeedHeader(sourceName));
                        case TIMEOUT:
                            return Optional.of(new ActivitySourceTimeOutFeedHeader(sourceName));
                        case THROTTLED:
                            return Optional.of(new ActivitySourceThrottledFeedHeader(sourceName));
                        default:
                            return Optional.<FeedHeader>empty();
                    }
                })
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Predicate<FeedHeader> isNonTimezoneOffsetHeader = header -> {
        if (header instanceof AtomParsedFeedHeader) {
            return !((AtomParsedFeedHeader) header).getElement().getQName().equals(ATLASSIAN_TIMEZONE_OFFSET);
        }
        return true;
    };

    /**
     * We use this auxilary method to calculate the size so we don't have to wait for the whole iterable to be
     * available.  The contents of the iterable may be a remote fetch running in a background thread, and we don't
     * want to wait for all of them to complete before being able to continue.
     */
    private boolean sizeEquals(final Iterable<?> iterable, final int size) {
        final Iterator<?> it = iterable.iterator();
        int i = 0;
        while (it.hasNext() && (i < size)) {
            it.next();
            i++;
        }
        return !it.hasNext() && (i == size);
    }
}
