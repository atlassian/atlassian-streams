package com.atlassian.streams.internal;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation;
import com.atlassian.streams.spi.CancellableTask;

import static com.atlassian.streams.api.common.Option.none;

public interface ActivityProvider {
    /**
     * @param key key to see if the provider is appropriate for handling the request
     * @return <code>true</code> if the provider is the right one to handle the request, <code>false</code> otherwise
     */
    boolean matches(String key);

    String getName();

    String getKey();

    String getBaseUrl();

    String getType();

    CancellableTask<Either<Error, FeedModel>> getActivityFeed(ActivityRequestImpl activityRequest)
            throws StreamsException;

    Either<Error, Iterable<ProviderFilterRepresentation>> getFilters(boolean addApplinkName);

    StreamsKeysRepresentation getKeys();

    boolean allKeysAreValid(Iterable<String> keys);

    class Error {
        private final Type type;
        private final Option<Exception> cause;
        private final Option<String> applicationLinkName;
        private final Option<ActivityProvider> activityProvider;

        private Error(Type type, ActivityProvider activityProvider) {
            this(
                    type,
                    Option.option(activityProvider.getName()),
                    none(Exception.class),
                    Option.option(activityProvider));
        }

        private Error(
                Type type,
                Option<String> applicationLinkName,
                Option<Exception> cause,
                Option<ActivityProvider> activityProvider) {
            this.type = type;
            this.cause = cause;
            this.applicationLinkName = applicationLinkName;
            this.activityProvider = activityProvider;
        }

        public Type getType() {
            return type;
        }

        public Option<Exception> getCause() {
            return cause;
        }

        public Option<String> getApplicationLinkName() {
            return applicationLinkName;
        }

        public Option<ActivityProvider> getActivityProvider() {
            return activityProvider;
        }

        public static Error timeout(ActivityProvider activityProvider) {
            return new Error(Type.TIMEOUT, activityProvider);
        }

        public static Error credentialsRequired(
                ActivityProvider activityProvider, Option<CredentialsRequiredException> e) {
            return new Error(
                    Type.CREDENTIALS_REQUIRED,
                    none(String.class),
                    Option.<Exception>option(e.get()),
                    Option.option(activityProvider));
        }

        public static Error credentialsRequired(ActivityProvider activityProvider) {
            return new Error(Type.CREDENTIALS_REQUIRED, activityProvider);
        }

        public static Error unauthorized(final ActivityProvider activityProvider) {
            return new Error(Type.UNAUTHORIZED, activityProvider);
        }

        public static Error other(ActivityProvider activityProvider) {
            return new Error(Type.OTHER, activityProvider);
        }

        /**
         * This should only be used when we have a timeout inside the StreamsCompletionService and we lose the context of the ActivityProvider.
         *
         * In these cases, we handle the timeout and register the failure by hooking into the Future's .cancel()
         */
        public static Error timeout() {
            return new Error(Type.TIMEOUT, none(String.class), none(Exception.class), none(ActivityProvider.class));
        }

        /**
         * This should only be used if we have a failure but *don't* want the failure to be logged into the failure cache
         */
        public static Error other() {
            return new Error(Type.OTHER, none(String.class), none(Exception.class), none(ActivityProvider.class));
        }

        public static Error banned(final ActivityProvider activityProvider) {
            return new Error(Type.BANNED, activityProvider);
        }

        public static Error throttled(final ActivityProvider activityProvider) {
            return new Error(Type.THROTTLED, activityProvider);
        }

        @Override
        public String toString() {
            return type.name().toLowerCase();
        }

        public enum Type {
            TIMEOUT,
            OTHER,
            CREDENTIALS_REQUIRED,
            BANNED,
            THROTTLED,
            UNAUTHORIZED;
        }
    }
}
