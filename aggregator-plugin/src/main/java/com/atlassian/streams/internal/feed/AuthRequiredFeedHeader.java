package com.atlassian.streams.internal.feed;

import java.net.URI;

/**
 * A {@link FeedHeader} that indicates authorization is required (or at least
 * suggested) for an application link.  The front end displays this as a
 * "more information may be available, please authenticate" banner.
 */
public class AuthRequiredFeedHeader extends AbstractApplicationSpecificFeedHeader {
    private final URI authUri;

    public AuthRequiredFeedHeader(String applicationId, String applicationName, URI applicationUri, URI authUri) {
        super(applicationId, applicationName, applicationUri);
        this.authUri = authUri;
    }

    /**
     * The URI for initiating interactive authentication/authorization for
     * this applink.
     */
    public URI getAuthUri() {
        return authUri;
    }
}
