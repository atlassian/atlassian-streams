package com.atlassian.streams.internal;

import org.apache.commons.lang3.time.StopWatch;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.analytics.StreamStatsEvent;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation;
import com.atlassian.streams.spi.CancellableTask;

import static java.util.Objects.requireNonNull;

public class ActivityProviderWithAnalytics implements ActivityProvider {

    private final ActivityProvider delegate;
    private final EventPublisher eventPublisher;

    public ActivityProviderWithAnalytics(final ActivityProvider delegate, final EventPublisher eventPublisher) {
        this.delegate = requireNonNull(delegate, "delegate");
        this.eventPublisher = requireNonNull(eventPublisher, "eventPublisher");
    }

    public ActivityProvider getDelegate() {
        return delegate;
    }

    @Override
    public CancellableTask<Either<Error, FeedModel>> getActivityFeed(final ActivityRequestImpl request) {
        final CancellableTask<Either<Error, FeedModel>> delegateTask = delegate.getActivityFeed(request);

        return new CancellableTask<Either<Error, FeedModel>>() {
            @Override
            public Either<Error, FeedModel> call() throws Exception {
                final StopWatch stopWatch = StopWatch.createStarted();
                boolean requestSuccessful = true;
                try {
                    final Either<Error, FeedModel> result = delegateTask.call();
                    requestSuccessful = result.isRight();
                    return result;
                } catch (final Exception ex) {
                    requestSuccessful = false;
                    throw ex;
                } finally {
                    final long processingTime = stopWatch.getTime();

                    eventPublisher.publish(new StreamStatsEvent(
                            request.getRequestId(),
                            processingTime,
                            delegate instanceof LocalActivityProvider,
                            requestSuccessful,
                            delegate.getName().toLowerCase(),
                            request.getMaxResults(),
                            request.getTimeout()));
                }
            }

            @Override
            public Result cancel() {
                return delegateTask.cancel();
            }
        };
    }

    @Override
    public boolean matches(final String key) {
        return delegate.matches(key);
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public String getKey() {
        return delegate.getKey();
    }

    @Override
    public String getBaseUrl() {
        return delegate.getBaseUrl();
    }

    @Override
    public String getType() {
        return delegate.getType();
    }

    @Override
    public Either<Error, Iterable<ProviderFilterRepresentation>> getFilters(final boolean addApplinkName) {
        return delegate.getFilters(addApplinkName);
    }

    @Override
    public StreamsKeysRepresentation getKeys() {
        return delegate.getKeys();
    }

    @Override
    public boolean allKeysAreValid(final Iterable<String> keys) {
        return delegate.allKeysAreValid(keys);
    }
}
