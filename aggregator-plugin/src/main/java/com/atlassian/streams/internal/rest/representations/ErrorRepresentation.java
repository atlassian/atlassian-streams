package com.atlassian.streams.internal.rest.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Provides a JSON representation of the error response.
 */
public class ErrorRepresentation {
    @JsonProperty
    private final String errorMessage;

    @JsonProperty
    private final String subCode;

    /**
     * Constructor for use by Jackson
     */
    @JsonCreator
    public ErrorRepresentation(
            @JsonProperty("errorMessage") String errorMessage, @JsonProperty("subCode") String subCode) {
        this.errorMessage = errorMessage;
        this.subCode = subCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getSubCode() {
        return subCode;
    }
}
