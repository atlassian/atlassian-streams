package com.atlassian.streams.internal.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;

@EventName("atlassian.streams.stats")
@Internal
public class StreamStatsEvent {
    private final long requestId;
    private final long processingTimeMillis;
    private final boolean localProvider;
    private final boolean requestSuccessful;
    private final String product;
    private final long maxResults;
    private final long timeout;

    public StreamStatsEvent(
            final long requestId,
            final long processingTimeMillis,
            final boolean localProvider,
            final boolean requestSuccessful,
            final String product,
            final long maxResults,
            final long timeout) {
        this.requestId = requestId;
        this.processingTimeMillis = processingTimeMillis;
        this.localProvider = localProvider;
        this.requestSuccessful = requestSuccessful;
        this.product = product;
        this.maxResults = maxResults;
        this.timeout = timeout;
    }

    public long getRequestId() {
        return requestId;
    }

    public long getProcessingTimeMillis() {
        return processingTimeMillis;
    }

    public boolean isLocalProvider() {
        return localProvider;
    }

    public boolean isRequestSuccessful() {
        return requestSuccessful;
    }

    public String getProduct() {
        return product;
    }

    public long getMaxResults() {
        return maxResults;
    }

    public long getTimeout() {
        return timeout;
    }
}
