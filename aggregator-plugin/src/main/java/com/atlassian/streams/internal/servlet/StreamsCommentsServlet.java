package com.atlassian.streams.internal.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.sal.api.xsrf.XsrfHeaderValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.MissingModuleKeyException;
import com.atlassian.streams.internal.NoSuchModuleException;
import com.atlassian.streams.internal.PostReplyHandler;
import com.atlassian.streams.internal.RemotePostReplyException;
import com.atlassian.streams.internal.RemotePostValidationException;
import com.atlassian.streams.spi.StreamsCommentHandler;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.CONFLICT;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNKNOWN_ERROR;

@UnrestrictedAccess
public class StreamsCommentsServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(StreamsCommentsServlet.class);
    private final PostReplyHandler handler;

    /**
     * Used to validate the XSRF token that should be submitted to this servlet as a cookie
     * header within the POST request. Never null.
     */
    private final XsrfTokenValidator xsrfTokenValidator;

    private final XsrfHeaderValidator xsrfHeaderValidator;

    public StreamsCommentsServlet(
            PostReplyHandler handler, XsrfTokenValidator xsrfTokenValidator, XsrfHeaderValidator xsrfHeaderValidator) {
        this.handler = checkNotNull(handler, "handler");
        this.xsrfTokenValidator = checkNotNull(xsrfTokenValidator, "XSRF token validator");
        this.xsrfHeaderValidator = checkNotNull(xsrfHeaderValidator, "XSRF header validator");
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        // If we're not XSRF-safe, we could be under attack, or maybe our XSRF token has simply gone AWOL because
        // of cookie expiry or something. In any case, we'll send an error back to the user, and ask them to refresh
        // the page. If its a deliberate request by an authenticated user, they'll get a nice new XSRF token when
        // they refresh the page; and any attack will be thwarted.
        if (!isXsrfSafe(req)) {
            resp.setContentType("application/json");
            final PostReplyError.Type errorType = CONFLICT;
            resp.setStatus(errorType.getStatusCode());
            out.print(errorType.asJsonString());
            out.close();
            return;
        }

        // We're not under XSRF attack, so continue processing of the request.

        String pathInfo = req.getPathInfo();
        final String comment = req.getParameter("comment");

        if (comment == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No comment specified");
            return;
        }
        // See if there is a reply to
        String replyTo = req.getParameter("replyTo");
        try {
            Either<StreamsCommentHandler.PostReplyError, URI> locationOrError =
                    handler.postReply(pathInfo, comment, replyTo);

            if (locationOrError.isLeft()) {
                StreamsCommentHandler.PostReplyError error =
                        locationOrError.left().get();
                if (error.getType().equals(UNKNOWN_ERROR)) {
                    if (error.getCause().isDefined()) {
                        log.error(
                                "Unknown error while posting comment",
                                error.getCause().get());
                    } else {
                        log.error("Unknown error while posting comment. No exception details available");
                    }
                } else {
                    log.warn("Logged an error while posting comment: "
                            + error.getType().toString());
                }
                resp.setContentType("application/json");
                resp.setStatus(error.getType().getStatusCode());
                out.print(error.asJsonString());
                out.flush();
            } else {
                resp.setStatus(HttpServletResponse.SC_CREATED);
                resp.setHeader("Location", locationOrError.right().get().toASCIIString());
            }
        } catch (MissingModuleKeyException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No module key in URI");
        } catch (NoSuchModuleException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No module with key " + e.getKey() + " installed");
        } catch (RemotePostReplyException e) {
            resp.sendError(HttpServletResponse.SC_BAD_GATEWAY, e.getMessage());
        } catch (RemotePostValidationException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, Joiner.on('\n').join(e.getErrors()));
        } finally {
            out.close();
        }
    }

    /**
     * Checks to see if the given request has a valid XSRF token.
     *
     * @param req the HTTP servlet request that should contain a valid XSRF token
     * @return true if the request has a valid XSRF token; false if the token is missing or invalid
     */
    @VisibleForTesting
    boolean isXsrfSafe(HttpServletRequest req) {
        // Skip token checking if there is valid header
        if (xsrfHeaderValidator.requestHasValidXsrfHeader(req)) {
            return true;
        }

        XsrfAwareRequest xsrfAwareReq = new XsrfAwareRequest(req, xsrfTokenValidator.getXsrfParameterName());
        return xsrfTokenValidator.validateFormEncodedToken(xsrfAwareReq);
    }
}
