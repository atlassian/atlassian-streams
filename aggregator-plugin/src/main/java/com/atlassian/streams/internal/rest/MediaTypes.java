package com.atlassian.streams.internal.rest;

import javax.ws.rs.core.MediaType;

public class MediaTypes {
    /**
     * Content type for streams filter resource
     */
    public static final String STREAMS_JSON = "application/vnd.atl.streams+json";

    public static final MediaType STREAMS_JSON_TYPE = new MediaType("application", "vnd.atl.streams+json");
}
