package com.atlassian.streams.internal.feed;

import java.io.IOException;
import java.io.Writer;
import java.net.URI;

/**
 * An object that can render output from a {@link FeedModel}.
 */
public interface FeedRenderer {
    /**
     * Writes feed data to a Writer.
     */
    void writeFeed(URI baseUri, FeedModel feed, Writer writer, FeedRendererContext context) throws IOException;

    /**
     * Returns the MIME type corresponding to the format {@link #writeFeed(URI, FeedModel, Writer, FeedRendererContext)}
     * will use.
     */
    String getContentType();
}
