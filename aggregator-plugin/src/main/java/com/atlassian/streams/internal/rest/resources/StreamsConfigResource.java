package com.atlassian.streams.internal.rest.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.streams.internal.ConfigRepresentationBuilder;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.internal.rest.MediaTypes.STREAMS_JSON;

@Path("/config")
@UnrestrictedAccess
public class StreamsConfigResource {
    private static final CacheControl NO_CACHE = new CacheControl();

    static {
        NO_CACHE.setNoStore(true);
        NO_CACHE.setNoCache(true);
    }

    private final ConfigRepresentationBuilder representationBuilder;

    @Inject
    public StreamsConfigResource(ConfigRepresentationBuilder representationBuilder) {
        this.representationBuilder = checkNotNull(representationBuilder, "representationBuilder");
    }

    @GET
    @Produces(STREAMS_JSON)
    public Response getFilters(@QueryParam("local") boolean local) {
        return Response.ok(representationBuilder.getConfigRepresentation(local))
                .type(STREAMS_JSON)
                .cacheControl(NO_CACHE)
                .build();
    }
}
