package com.atlassian.streams.internal.servlet;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;

import com.atlassian.streams.api.common.Option;

/**
 * An HTTP servlet request wrapper that publishes a 'shadow' request parameter, which is a parameter that may not
 * exist in the wrapped request, but is synthesised so that it appears to be in the request. It has the same value
 * as the cross-product XSRF token parameter.
 * <p>
 * The name of the shadow parameter is provided in the constructor. Calls to the usual getParameter-ish methods
 * will return the shadow parameter as if it exists in the wrapped request. In fact, the shadow parameter
 * is synthesised by application of the Decorator (Gamma et al) pattern; however, if there is already a parameter
 * in the wrapped request with the same name as the shadow parameter, then the value of that existing parameter
 * is preserved - it is not overridden.
 * <p>
 * An instance of this class can be passed into Service Access Layer (SAL) implementations, which may expect an
 * XSRF token parameter to be present in the request under a product-specific name.
 * <p>
 * This class is required because the JavaScript for the Activity Streams gadget, being a cross-platform component,
 * does not know the name of the request parameter in which a given product expects the XSRF token to be held.
 *
 * @since v5.2
 */
final class XsrfAwareRequest extends HttpServletRequestWrapper {
    /**
     * The name of the cross-product XSRF token parameter.
     * <p>
     * <b>The value specified here must correspond to that used in the form, which is constructed in JavaScript
     * contained in a file called {@code comment.js} in the {@code streams-inline-actions-plugin}.</b>
     */
    public static final String CROSS_PRODUCT_TOKEN_PARAM = "xsrfToken";

    /**
     * The name of the request parameter that will be published by this XSRF-aware request, having
     * the same value as that of the cross-product XSRF token parameter.
     */
    private final String shadowParamName;

    /**
     * Constructs an XSRF-aware request that wraps the given HTTP servlet request and publishes a 'shadow'
     * request parameter having the same value as the cross-product XSRF token parameter.
     *
     * @param req             the undecorated HTTP servlet request. Must not be null.
     * @param shadowParamName the name of the parameter to be published as a shadow of the
     *                        cross-product XSRF token parameter. Must not be null.
     * @throws IllegalArgumentException if the request is null
     * @throws NullPointerException     if the validator is null
     */
    XsrfAwareRequest(HttpServletRequest req, String shadowParamName) {
        super(req);
        this.shadowParamName = Preconditions.checkNotNull(shadowParamName);
    }

    @Override
    public String getParameter(String name) {
        final String NULL_STRING = null; // to avoid a cast
        return name.equals(shadowParamName) && !shadowProvided()
                ? xsrfToken().getOrElse(NULL_STRING)
                : super.getParameter(name);
    }

    @Override
    public String[] getParameterValues(String name) {
        return name.equals(shadowParamName) && !shadowProvided()
                ? xsrfTokenAsStringArray()
                : super.getParameterValues(name);
    }

    /**
     * This implementation returns a new enumeration containing the parameter names
     * found in the wrapped request, as well as the name of the shadow parameter.
     */
    @Override
    public Enumeration<String> getParameterNames() {
        List<String> nameList = Collections.list(super.getParameterNames());
        nameList.add(shadowParamName);
        return Collections.enumeration(nameList);
    }

    /**
     * This implementation returns a new map that contains all of the entries in the parameter map of the
     * wrapped request plus a new entry representing the shadow parameter.
     */
    @Override
    public Map<String, String[]> getParameterMap() {
        // This cast is safe because the JavaDoc of ServletRequest specifies that the keys
        // in the parameter map are of type String, and the values in the parameter map are
        // of type String array.
        final Map<String, String[]> paramMap = super.getParameterMap();

        if (shadowProvided()) {
            return paramMap;
        }

        final Map<String, String[]> enhancedParamMap = new HashMap<String, String[]>(paramMap);
        enhancedParamMap.put(shadowParamName, paramMap.get(CROSS_PRODUCT_TOKEN_PARAM));
        return enhancedParamMap;
    }

    /**
     * Determines if the wrapped HTTP servlet request has a parameter with the same
     * name as the shadow parameter.
     *
     * @return true if there is already a parameter with the same name as the shadow parameter
     */
    private boolean shadowProvided() {
        return getRequest().getParameter(shadowParamName) != null;
    }

    /**
     * Obtains the XSRF token from the wrapped request.
     *
     * @return an Option wrapped around the XSRF token
     */
    private Option<String> xsrfToken() {
        return Option.option(getParameter(CROSS_PRODUCT_TOKEN_PARAM));
    }

    /**
     * Returns the value of the XSRF token as a string array.
     *
     * @return a string array, or null if the cross-product XSRF token request parameter doesn't exist
     */
    private String[] xsrfTokenAsStringArray() {
        // A function that produces a string array containing its string argument.
        final Function<String, String[]> stringToStringArray = new Function<String, String[]>() {
            public String[] apply(@Nullable final String from) {
                return new String[] {from};
            }
        };
        final String[] NULL_STRINGS = null; // to avoid a cast
        return xsrfToken().map(stringToStringArray).getOrElse(NULL_STRINGS);
    }
}
