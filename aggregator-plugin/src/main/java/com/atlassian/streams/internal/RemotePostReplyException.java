package com.atlassian.streams.internal;

import com.atlassian.streams.api.StreamsException;

public class RemotePostReplyException extends StreamsException {
    private final int statusCode;

    public RemotePostReplyException(int statusCode, String statusText) {
        super(statusText);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
