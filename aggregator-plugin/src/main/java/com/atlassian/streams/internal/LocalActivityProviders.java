package com.atlassian.streams.internal;

import java.time.Duration;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Qualifier;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Supplier;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.spi.ActivityProviderModuleDescriptor;
import com.atlassian.streams.spi.SessionManager;
import com.atlassian.streams.spi.StreamsI18nResolver;

import static com.google.common.base.Preconditions.checkNotNull;

class LocalActivityProviders implements Supplier<Iterable<ActivityProvider>> {
    private final PluginAccessor pluginAccessor;
    private final StreamsI18nResolver i18nResolver;
    private final TransactionTemplate transactionTemplate;
    private final SessionManager sessionManager;
    private final ApplicationProperties applicationProperties;
    private final EventPublisher eventPublisher;

    private final CallThrottler callThrottler =
            new CallThrottler(Duration.ofSeconds(30), getAllowedWallClockPercentage());

    LocalActivityProviders(
            PluginAccessor pluginAccessor,
            StreamsI18nResolver i18nResolver,
            @Qualifier("sessionManager") SessionManager sessionManager,
            TransactionTemplate transactionTemplate,
            ApplicationProperties applicationProperties,
            EventPublisher eventPublisher) {
        this.pluginAccessor = checkNotNull(pluginAccessor, "pluginAccessor");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.sessionManager = checkNotNull(sessionManager, "sessionManager");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.eventPublisher = checkNotNull(eventPublisher, "eventPublisher");
    }

    @Override
    public Iterable<ActivityProvider> get() {
        return pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class).stream()
                .map(descriptor -> {
                    final LocalActivityProvider provider = new LocalActivityProvider(
                            descriptor,
                            sessionManager,
                            transactionTemplate,
                            i18nResolver,
                            applicationProperties,
                            callThrottler);
                    return new ActivityProviderWithAnalytics(provider, eventPublisher);
                })
                .collect(Collectors.toList());
    }

    private static int getAllowedWallClockPercentage() {
        final String property = System.getProperty(
                "com.atlassian.streams.internal.LocalActivityProviders.allowed.wallclock.percentage");
        if (StringUtils.isBlank(property)) {
            return 10;
        }
        return Integer.parseInt(property);
    }
}
