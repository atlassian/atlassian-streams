package com.atlassian.streams.internal.completion;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Function;

import com.atlassian.failurecache.failures.FailureCache;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.ActivityProvider;
import com.atlassian.streams.internal.ActivityProviderCallable;
import com.atlassian.streams.spi.CancellableTask;

import static io.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static java.util.Objects.requireNonNull;

/**
 * Has a cached thread pool executor service with a thread factory that
 * names threads StreamsCompletionService:n as well as a completer for running the jobs
 */
public class Execution {
    private static final int MAX_POOL_SIZE = Integer.getInteger("streams.completion.service.pool.max", 32);

    private final ExecutorService executorService;
    private final Completer completer;
    private final FailureCache failureCache;
    private static final Logger logger = LoggerFactory.getLogger(Execution.class);

    public Execution(final ThreadLocalDelegateExecutorFactory factory, final FailureCache failureCache) {
        this.failureCache = requireNonNull(failureCache, "failureCache can't be null");
        executorService = factory.createExecutorService(
                newLimitedCachedThreadPool(namedThreadFactory("StreamsCompletionService:"), MAX_POOL_SIZE));
        completer = new Completer(executorService, cancellingCompletionServiceFactory(failureCache));
    }

    private ExecutorService newLimitedCachedThreadPool(final ThreadFactory threadFactory, final int limit) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                limit, limit, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), threadFactory);
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }

    public <T> Iterable<Either<ActivityProvider.Error, T>> invokeAll(
            final Iterable<? extends ActivityProviderCallable<Either<ActivityProvider.Error, T>>> jobs) {
        return completer.invokeAll(jobs);
    }

    public <T> Iterable<Either<ActivityProvider.Error, T>> invokeAll(
            final Iterable<? extends ActivityProviderCallable<Either<ActivityProvider.Error, T>>> jobs,
            long time,
            TimeUnit unit) {
        return completer.invokeAll(jobs, time, unit);
    }

    public void close() {
        executorService.shutdownNow();
    }

    private static Completer.ExecutorCompletionServiceFactory cancellingCompletionServiceFactory(
            FailureCache failureCache) {
        return new CancellingCompletionServiceFactory(failureCache);
    }

    private static class CancellingCompletionServiceFactory implements Completer.ExecutorCompletionServiceFactory {
        private final FailureCache failureCache;

        private CancellingCompletionServiceFactory(final FailureCache failureCache) {
            this.failureCache = failureCache;
        }

        @Override
        public <T> Function<Executor, CompletionService<Either<ActivityProvider.Error, T>>> create() {
            return new Function<Executor, CompletionService<Either<ActivityProvider.Error, T>>>() {
                @Override
                public CompletionService<Either<ActivityProvider.Error, T>> apply(Executor e) {
                    return new CancellingCompletionService<Either<ActivityProvider.Error, T>>(e, failureCache);
                }
            };
        }
    }

    private static final class CancellingCompletionService<T> implements CompletionService<T> {
        private final CompletionService<T> delegate;
        private final FailureCache failureCache;
        private final ConcurrentHashMap<Future<T>, Future<T>> originalFutureToWrappedFuture;

        CancellingCompletionService(Executor executor, FailureCache failureCache) {
            this.delegate = new ExecutorCompletionService<T>(executor);
            this.failureCache = failureCache;
            this.originalFutureToWrappedFuture = new ConcurrentHashMap<Future<T>, Future<T>>();
        }

        @Override
        public Future<T> poll() {
            return wrappedFuture(delegate.poll());
        }

        @Override
        public Future<T> poll(long timeout, TimeUnit unit) throws InterruptedException {
            return wrappedFuture(delegate.poll(timeout, unit));
        }

        @Override
        public Future<T> submit(final Callable<T> task) {
            logger.debug(
                    "Submitting task stream provider {}",
                    ((ActivityProviderCallable) task).getActivityProvider().getName());

            final Future<T> f = delegate.submit(task);
            Future<T> wrappedFuture = new Future<T>() {
                volatile boolean cancelled = false;

                @Override
                public boolean cancel(boolean mayInterrupt) {
                    if (task instanceof CancellableTask) {
                        CancellableTask.Result r = ((CancellableTask<T>) task).cancel();
                        switch (r) {
                            case CANCELLED:
                                cancelled = true;
                                break;
                            case CANNOT_CANCEL:
                                cancelled = false;
                                break;
                            case INTERRUPT:
                                cancelled = f.cancel(mayInterrupt);
                                break;
                            default:
                                throw new IllegalStateException(
                                        "Unknown result type '" + r + "' returned from CancellableTask.cancel");
                        }
                    } else {
                        cancelled = f.cancel(mayInterrupt);
                    }

                    logger.warn(
                            "Registering failure for stream provider {} due to cancellation (timeout)",
                            ((ActivityProviderCallable) task)
                                    .getActivityProvider()
                                    .getName());
                    failureCache.registerFailure(((ActivityProviderCallable) task).getActivityProvider());

                    originalFutureToWrappedFuture.remove(f);

                    return cancelled;
                }

                @Override
                public T get() throws InterruptedException, ExecutionException {
                    logger.debug(
                            "Attempting get from stream provider {}",
                            ((ActivityProviderCallable) task)
                                    .getActivityProvider()
                                    .getName());
                    T value = f.get();
                    failureCache.registerSuccess(((ActivityProviderCallable) task).getActivityProvider());
                    return value;
                }

                @Override
                public T get(long t, TimeUnit u) throws InterruptedException, ExecutionException, TimeoutException {
                    logger.debug("Attempting get from stream provider {} with timeout {} {}", new Object[] {
                        ((ActivityProviderCallable) task).getActivityProvider().getName(),
                        String.valueOf(t),
                        u.toString()
                    });
                    T value = f.get(t, u);
                    failureCache.registerSuccess(((ActivityProviderCallable) task).getActivityProvider());
                    return value;
                }

                @Override
                public boolean isCancelled() {
                    return cancelled;
                }

                @Override
                public boolean isDone() {
                    return cancelled || f.isDone();
                }
            };

            originalFutureToWrappedFuture.put(f, wrappedFuture);
            return wrappedFuture;
        }

        @Override
        public Future<T> submit(Runnable task, T result) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Future<T> take() throws InterruptedException {
            return wrappedFuture(delegate.take());
        }

        private Future<T> wrappedFuture(Future<T> future) {
            if (future == null) {
                return future;
            }
            return originalFutureToWrappedFuture.remove(future);
        }
    }
}
