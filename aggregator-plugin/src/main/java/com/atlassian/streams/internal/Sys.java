package com.atlassian.streams.internal;

import static com.atlassian.plugin.internal.util.PluginUtils.ATLASSIAN_DEV_MODE;

final class Sys {
    public static boolean inDevMode() {
        return Boolean.getBoolean(ATLASSIAN_DEV_MODE);
    }
}
