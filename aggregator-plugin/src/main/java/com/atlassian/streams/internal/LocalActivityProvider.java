package com.atlassian.streams.internal;

import java.net.URI;
import java.util.Objects;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.feed.builder.FeedFetcher;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation;
import com.atlassian.streams.spi.ActivityProviderModuleDescriptor;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.SessionManager;
import com.atlassian.streams.spi.StreamsActivityProvider;
import com.atlassian.streams.spi.StreamsCommentHandler;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.StreamsKeyProvider;
import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;
import com.atlassian.streams.spi.StreamsValidator;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

/**
 * Local activity providers are providers that come from plugins that have registered an {@code <activity-streams-provider>}.
 */
public final class LocalActivityProvider implements ActivityProvider {
    public static final String ACCEPT_LANGUAGE_KEY = "Accept-Language";
    private static final Logger logger = LoggerFactory.getLogger(LocalActivityProvider.class);

    private final String key;
    private final String completeKey;
    private CallThrottler callThrottler;
    private final String name;
    private final StreamsActivityProvider activityProvider;
    private final StreamsFilterOptionProvider filterOptionProvider;
    private final StreamsKeyProvider keyProvider;
    private final StreamsValidator validator;
    private final StreamsCommentHandler commentHandler;
    private final TransactionTemplate transactionTemplate;
    private final StreamsI18nResolver i18nResolver;
    private final SessionManager sessionManager;
    private final ApplicationProperties applicationProperties;

    public LocalActivityProvider(
            ActivityProviderModuleDescriptor descriptor,
            SessionManager sessionManager,
            TransactionTemplate transactionTemplate,
            StreamsI18nResolver i18nResolver,
            ApplicationProperties applicationProperties,
            CallThrottler callThrottler) {
        this.key = requireNonNull(descriptor.getKey(), "key");
        this.name = requireNonNull(
                descriptor.getI18nNameKey() != null
                        ? i18nResolver.getText(descriptor.getI18nNameKey())
                        : descriptor.getName(),
                "name");
        completeKey = descriptor.getCompleteKey();
        this.activityProvider = requireNonNull(descriptor.getModule(), "activityProvider");
        this.sessionManager = requireNonNull(sessionManager, "sessionManager");
        this.transactionTemplate = requireNonNull(transactionTemplate, "transactionTemplate");
        this.i18nResolver = requireNonNull(i18nResolver, "i18nResolver");
        this.applicationProperties = requireNonNull(applicationProperties, "applicationProperties");
        this.filterOptionProvider = descriptor.getFilterOptionProvider();
        this.keyProvider = descriptor.getKeyProvider();
        this.validator = descriptor.getValidator();
        this.commentHandler = descriptor.getCommentHandler();
        this.callThrottler = callThrottler;
    }

    public boolean matches(String key) {
        return getKey().equals(key);
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public String getBaseUrl() {
        return applicationProperties.getBaseUrl();
    }

    public String getType() {
        return applicationProperties.getDisplayName();
    }

    @Override
    public CancellableTask<Either<Error, FeedModel>> getActivityFeed(final ActivityRequestImpl request) {
        final CancellableTask<StreamsFeed> task = activityProvider.getActivityFeed(request);
        return new CancellableTask<Either<Error, FeedModel>>() {
            @Override
            public Either<Error, FeedModel> call() {
                if (callThrottler.isBudgetExceeded()) {
                    logger.info(
                            "Local activity exceeded the allowed budget of {}% time in the past {}",
                            callThrottler.getAllowedWallClockPercentage(), callThrottler.getTimeWindow());
                    if (logger.isTraceEnabled()) {
                        logger.trace(
                                "Per-provider Activity Stream statistics for the last {}: {}",
                                callThrottler.getTimeWindow(),
                                callThrottler.getStats());
                    }
                    return Either.left(Error.throttled(LocalActivityProvider.this));
                }
                try (final CallThrottler.TrackedCall tracker = callThrottler.startTracking(completeKey)) {
                    return sessionManager.withSession(() -> transactionTemplate.execute(fetchFeed(request, task)));
                }
            }

            @Override
            public Result cancel() {
                return task.cancel();
            }
        };
    }

    @Override
    public Either<Error, Iterable<ProviderFilterRepresentation>> getFilters(final boolean addApplinkName) {
        if (filterOptionProvider == null) {
            return Either.right(emptyList());
        }
        return Either.<Error, Iterable<ProviderFilterRepresentation>>right(
                ImmutableList.of(new ProviderFilterRepresentation(key, name, "", filterOptionProvider, i18nResolver)));
    }

    @Override
    public StreamsKeysRepresentation getKeys() {
        if (keyProvider == null) {
            return new StreamsKeysRepresentation(ImmutableList.<StreamsKey>of());
        }
        return new StreamsKeysRepresentation(keyProvider.getKeys());
    }

    @Override
    public boolean allKeysAreValid(final Iterable<String> keys) {
        if (validator == null) {
            return false;
        }
        return StreamSupport.stream(keys.spliterator(), false).allMatch(validator::isValidKey);
    }

    public Either<StreamsCommentHandler.PostReplyError, URI> postReply(
            URI baseUri, Iterable<String> itemPath, String comment) {
        return commentHandler.postReply(baseUri, itemPath, comment);
    }

    private FeedFetcher fetchFeed(ActivityRequest request, CancellableTask<StreamsFeed> task) {
        return new FeedFetcher(i18nResolver, request, task, this);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final LocalActivityProvider that = (LocalActivityProvider) o;
        return Objects.equals(key, that.key) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, name);
    }
}
