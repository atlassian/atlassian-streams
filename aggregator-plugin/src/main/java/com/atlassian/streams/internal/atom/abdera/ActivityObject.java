package com.atlassian.streams.internal.atom.abdera;

import java.net.URI;
import javax.xml.namespace.QName;

import org.apache.abdera.factory.Factory;
import org.apache.abdera.model.Content;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.ExtensibleElementWrapper;
import org.apache.abdera.model.IRIElement;
import org.apache.abdera.model.Link;
import org.apache.abdera.model.Source;
import org.apache.abdera.model.Text;

import com.atlassian.streams.api.ActivityObjectType;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.abdera.util.Constants.LN_ALTERNATE;

import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_OBJECT_TYPE;

public class ActivityObject extends ExtensibleElementWrapper {
    public ActivityObject(Element internal) {
        super(internal);
    }

    public ActivityObject(Factory factory, QName qname) {
        super(factory, qname);
    }

    public void setId(String id) {
        checkNotNull(id, "id");
        IRIElement idElem = getFactory().newID();
        idElem.setValue(id);
        addExtension(idElem);
    }

    public void setTitle(String title) {
        checkNotNull(title, "title");
        Text titleElem = getFactory().newTitle();
        titleElem.setValue(title);
        addExtension(titleElem);
    }

    public void setSummary(String summary) {
        checkNotNull(summary, "summary");
        Text summaryElem = getFactory().newSummary();
        summaryElem.setValue(summary);
        addExtension(summaryElem);
    }

    public void setContent(String content) {
        checkNotNull(content, "content");
        Content contentElem = getFactory().newContent(Content.Type.HTML);
        contentElem.setValue(content);
        addExtension(contentElem);
    }

    public void setAlternateLink(URI link) {
        checkNotNull(link, "link");
        Link linkElem = getFactory().newLink();
        linkElem.setRel(LN_ALTERNATE);
        linkElem.setHref(link.toASCIIString());
        addExtension(linkElem);
    }

    public void setObjectType(ActivityObjectType objectType) {
        addSimpleExtension(
                ACTIVITY_OBJECT_TYPE,
                checkNotNull(objectType, "objectType").iri().toASCIIString());
    }

    public void setSource(Source source) {
        addExtension(source);
    }
}
