package com.atlassian.streams.internal.atom.abdera;

import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.namespace.QName;

import org.apache.abdera.Abdera;
import org.apache.abdera.ext.thread.InReplyTo;
import org.apache.abdera.model.Content;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.ExtensibleElement;
import org.apache.abdera.model.Feed;
import org.apache.abdera.model.Person;
import org.apache.abdera.parser.stax.FOMFactory;
import org.apache.abdera.parser.stax.FOMGenerator;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.DateUtil;
import com.atlassian.streams.api.FeedContentSanitizer;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.atom.abdera.StreamsAbdera.AtomParsedFeedEntry;
import com.atlassian.streams.internal.atom.abdera.StreamsAbdera.AtomParsedFeedHeader;
import com.atlassian.streams.internal.feed.ActivitySourceBannedFeedHeader;
import com.atlassian.streams.internal.feed.ActivitySourceThrottledFeedHeader;
import com.atlassian.streams.internal.feed.ActivitySourceTimeOutFeedHeader;
import com.atlassian.streams.internal.feed.AuthRequiredFeedHeader;
import com.atlassian.streams.internal.feed.FeedEntry;
import com.atlassian.streams.internal.feed.FeedHeader;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.feed.FeedRenderer;
import com.atlassian.streams.internal.feed.FeedRendererContext;
import com.atlassian.streams.spi.FormatPreferenceProvider;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static org.apache.abdera.ext.thread.ThreadConstants.IN_REPLY_TO;
import static org.apache.abdera.model.Text.Type.HTML;
import static org.apache.abdera.util.Constants.GENERATOR;
import static org.apache.abdera.util.Constants.LN_ALTERNATE;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.internal.Predicates.blank;
import static com.atlassian.streams.internal.Predicates.isAbsolute;
import static com.atlassian.streams.internal.Predicates.linkHref;
import static com.atlassian.streams.internal.Predicates.linkRel;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_OBJECT;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_OBJECT_TYPE;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_STREAMS_PERSON_TYPE;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_TARGET;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_VERB;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_APPLICATION;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_AUTHORISATION_MESSAGE;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_BANNED_ACTIVITY_SOURCE;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_BANNED_ACTIVITY_SOURCE_LIST;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_THROTTLED_ACTIVITY_SOURCE;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_THROTTLED_ACTIVITY_SOURCE_LIST;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_TIMED_OUT_ACTIVITY_SOURCE;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_TIMED_OUT_ACTIVITY_SOURCE_LIST;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_TIMEZONE_OFFSET;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.MEDIA_HEIGHT;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.MEDIA_WIDTH;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.USR_USERNAME;

/**
 * Implementation of {@link FeedRenderer} that generates an Atom XML feed
 * using Apache Abdera.
 */
public class AbderaAtomFeedRenderer implements FeedRenderer {
    private static final String FEED_CONTENT_TYPE = "application/atom+xml";

    private final Abdera abdera = StreamsAbdera.getAbdera();
    private final FOMFactory factory = new FOMFactory(abdera);

    private final FormatPreferenceProvider formatPreferenceProvider;
    private final DateTimeFormatter timeZoneFormatter = DateTimeFormatter.ofPattern("Z");
    private final FeedContentSanitizer sanitizer;

    public AbderaAtomFeedRenderer(FormatPreferenceProvider formatPreferenceProvider, FeedContentSanitizer sanitizer) {
        this.formatPreferenceProvider = checkNotNull(formatPreferenceProvider, "formatPreferenceProvider");
        this.sanitizer = checkNotNull(sanitizer, "sanitizer");
    }

    @Override
    public void writeFeed(URI baseUri, FeedModel feed, Writer writer, FeedRendererContext context) throws IOException {
        Feed outputFeed;
        if (feed.getEncodedContent().isDefined()) {
            outputFeed = (Feed) feed.getEncodedContent().get();
        } else {
            outputFeed = new AtomFeedBuilder(baseUri, context).build(feed);
        }

        org.apache.abdera.writer.Writer abderaWriter = (context.isDeveloperMode())
                ? abdera.getWriterFactory().getWriter("PrettyXML")
                : abdera.getWriterFactory().getWriter();
        outputFeed.writeTo(abderaWriter, writer);
    }

    @Override
    public String getContentType() {
        return FEED_CONTENT_TYPE;
    }

    private final class AtomFeedBuilder {
        private final FeedRendererContext context;
        private final Feed outputFeed;
        private final URI baseUri;

        AtomFeedBuilder(URI baseUri, FeedRendererContext context) {
            this.baseUri = baseUri;
            this.context = context;
            this.outputFeed = abdera.newFeed();
        }

        Feed build(FeedModel feed) {
            outputFeed.setId(feed.getUri().toString());
            outputFeed.addLink(feed.getUri().toString(), "self");

            outputFeed.setTitle(feed.getTitle().getOrElse(context.getDefaultFeedTitle()));
            addTimeZoneOffset(outputFeed, ZonedDateTime.now());
            for (String subtitle : feed.getSubtitle()) {
                outputFeed.setSubtitle(subtitle);
            }

            outputFeed.setUpdated(DateUtil.toDate(feed.getUpdatedDate()));

            for (FeedHeader header : feed.getHeaders()) {
                addHeaderExtension(outputFeed, header);
            }

            for (Map.Entry<String, Uri> link : feed.getLinks().entries()) {
                outputFeed.addLink(link.getValue().toString(), link.getKey());
            }

            for (FeedEntry entry : feed.getEntries()) {
                Entry atomEntry;
                if (entry instanceof AtomParsedFeedEntry) {
                    atomEntry = ((AtomParsedFeedEntry) entry).getAtomEntry();
                } else {
                    atomEntry = getAtomEntry(entry.getStreamsEntry());
                }

                // Set the time zone offset field *every* time we output this entry - even if it was
                // an existing Atom entry that's being aggregated - because we want to use the time
                // zone for the local aggregating app (STRM-1724).
                addTimeZoneOffset(atomEntry, entry.getEntryZonedDate());
                for (FeedModel sourceFeed : entry.getSourceFeed()) {
                    for (Object sourceAtomFeed : sourceFeed.getEncodedContent()) {
                        atomEntry.addExtension(((Feed) sourceAtomFeed).getAsSource());
                    }
                }

                outputFeed.addEntry(atomEntry);
            }

            // Atom requires a feed to have an <author> element only if there are no entries.
            if (outputFeed.getEntries().isEmpty()) {
                outputFeed.addAuthor(context.getDefaultFeedAuthor());
            }

            return outputFeed;
        }

        private void addHeaderExtension(Feed outputFeed, FeedHeader header) {
            if (header instanceof AtomParsedFeedHeader) {
                outputFeed.addExtension(((AtomParsedFeedHeader) header).getElement());
            }
            if (header instanceof AuthRequiredFeedHeader) {
                addAuthRequiredExtension(outputFeed, (AuthRequiredFeedHeader) header);
            }
            if (header instanceof ActivitySourceTimeOutFeedHeader) {
                addTimedOutSource(outputFeed, ((ActivitySourceTimeOutFeedHeader) header).getSourceName());
            }
            if (header instanceof ActivitySourceThrottledFeedHeader) {
                addThrottledSource(outputFeed, ((ActivitySourceThrottledFeedHeader) header).getSourceName());
            }
            if (header instanceof ActivitySourceBannedFeedHeader) {
                addBannedSource(outputFeed, ((ActivitySourceBannedFeedHeader) header).getSourceName());
            }
        }

        private void addAuthRequiredExtension(Feed outputFeed, AuthRequiredFeedHeader header) {
            AuthorisationMessage authRequest = outputFeed.addExtension(ATLASSIAN_AUTHORISATION_MESSAGE);
            authRequest.setApplicationId(header.getApplicationId());
            authRequest.setApplicationName(header.getApplicationName());
            authRequest.setApplicationUri(header.getApplicationUri());
            authRequest.setAuthorisationUri(header.getAuthUri());
        }

        private Entry getAtomEntry(StreamsEntry streamEntry) {
            Entry atomEntry = abdera.newEntry();

            final Html title = streamEntry.renderTitleAsHtml();
            final Option<Html> summary = streamEntry.renderSummaryAsHtml();
            final Option<Html> content = streamEntry.renderContentAsHtml();

            atomEntry.setId(streamEntry.getId().toASCIIString());
            atomEntry.setTitle(title.toString(), HTML);

            for (Html s : summary) {
                atomEntry.setSummary(sanitizer.sanitize(s.toString()), HTML);
            }

            for (Html c : content) {
                atomEntry.setContent(sanitizer.sanitize(c.toString()), Content.Type.HTML);
            }

            for (Person person : buildAuthors(streamEntry)) {
                atomEntry.addAuthor(person);
            }

            atomEntry.setPublished(DateUtil.toDate(streamEntry.getPostedZonedDate()));
            atomEntry.setUpdated(DateUtil.toDate(streamEntry.getPostedZonedDate()));

            // add <category> elements
            addCategories(atomEntry, streamEntry.getCategories());

            if (isAbsolute(streamEntry.getAlternateLink())) {
                atomEntry.addLink(streamEntry.getAlternateLink().toASCIIString(), LN_ALTERNATE);
            }

            List<Link> linkValues = streamEntry.getLinksMap().values().stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            for (Link link : filter(linkValues, and(linkHref(isAbsolute()), linkRel(not(equalTo(LN_ALTERNATE)))))) {
                if (link.getTitle().isDefined()) {
                    atomEntry.addLink(factory.newLink()
                            .setHref(link.getHref().toASCIIString())
                            .setRel(link.getRel())
                            .setTitle(link.getTitle().get()));
                } else {
                    atomEntry.addLink(link.getHref().toASCIIString(), link.getRel());
                }
            }

            // add <thr:in-reply-to> element
            for (URI uri : filter(streamEntry.getInReplyTo(), isAbsolute())) {
                InReplyTo inReplyTo = atomEntry.addExtension(IN_REPLY_TO);
                inReplyTo.setRef(uri.toASCIIString());
            }

            // add <atom:generator> element
            atomEntry.addExtension(buildGenerator(baseUri.toASCIIString()));

            // add <atlassian:application> element
            atomEntry.addSimpleExtension(ATLASSIAN_APPLICATION, streamEntry.getApplicationType());

            addActivityVerbs(atomEntry, streamEntry.getVerb());
            addActivityObjectElements(atomEntry, streamEntry.getActivityObjects());
            addActivityTargetElement(atomEntry, streamEntry.getTarget());

            return atomEntry;
        }

        private void addTimeZoneOffset(ExtensibleElement element, ZonedDateTime updated) {
            String offsetValue = timeZoneFormatter
                    .withZone(formatPreferenceProvider.getUserTimeZoneId())
                    .format(updated.toInstant());
            Element existingOffset = element.getExtension(ATLASSIAN_TIMEZONE_OFFSET);
            if (existingOffset == null) {
                element.addSimpleExtension(ATLASSIAN_TIMEZONE_OFFSET, offsetValue);
            } else {
                existingOffset.setText(offsetValue);
            }
        }

        private void addTimedOutSource(ExtensibleElement element, String sourceName) {
            ExtensibleElement timedOutSourceList = element.getExtension(ATLASSIAN_TIMED_OUT_ACTIVITY_SOURCE_LIST);
            if (timedOutSourceList == null) {
                timedOutSourceList = element.addExtension(ATLASSIAN_TIMED_OUT_ACTIVITY_SOURCE_LIST);
            }
            timedOutSourceList.addSimpleExtension(ATLASSIAN_TIMED_OUT_ACTIVITY_SOURCE, sourceName);
        }

        private void addThrottledSource(ExtensibleElement element, String sourceName) {
            ExtensibleElement timedOutSourceList = element.getExtension(ATLASSIAN_THROTTLED_ACTIVITY_SOURCE_LIST);
            if (timedOutSourceList == null) {
                timedOutSourceList = element.addExtension(ATLASSIAN_THROTTLED_ACTIVITY_SOURCE_LIST);
            }
            timedOutSourceList.addSimpleExtension(ATLASSIAN_THROTTLED_ACTIVITY_SOURCE, sourceName);
        }

        private void addBannedSource(ExtensibleElement element, String sourceName) {
            ExtensibleElement bannedSourceList = element.getExtension(ATLASSIAN_BANNED_ACTIVITY_SOURCE_LIST);
            if (bannedSourceList == null) {
                bannedSourceList = element.addExtension(ATLASSIAN_BANNED_ACTIVITY_SOURCE_LIST);
            }
            bannedSourceList.addSimpleExtension(ATLASSIAN_BANNED_ACTIVITY_SOURCE, sourceName);
        }

        private void addCategories(Entry atomEntry, Iterable<String> categories) {
            if (categories != null) {
                for (String category : categories) {
                    atomEntry.addCategory(category);
                }
            }
        }

        private void addActivityVerbs(Entry atomEntry, ActivityVerb verb) {
            for (ActivityVerb parent : verb.parent()) {
                addActivityVerbs(atomEntry, parent);
            }
            atomEntry.addSimpleExtension(ACTIVITY_VERB, verb.iri().toASCIIString());
        }

        private void addActivityObjectElements(
                Entry atomEntry, Iterable<? extends StreamsEntry.ActivityObject> activityObjects) {
            for (StreamsEntry.ActivityObject object : activityObjects) {
                addActivityObjectAsElement(atomEntry, object, ACTIVITY_OBJECT);
            }
        }

        private void addActivityTargetElement(Entry atomEntry, Option<StreamsEntry.ActivityObject> target) {
            for (StreamsEntry.ActivityObject o : target) {
                addActivityObjectAsElement(atomEntry, o, ACTIVITY_TARGET);
            }
        }

        private void addActivityObjectAsElement(Entry atomEntry, StreamsEntry.ActivityObject object, QName element) {
            ActivityObject ao = atomEntry.addExtension(element);
            for (String id : object.getId()) {
                ao.setId(id);
            }
            for (String title : filter(object.getTitle(), not(blank()))) {
                ao.setTitle(title);
            }
            for (Html titleAsHtml : object.getTitleAsHtml()) {
                ao.setTitle(titleAsHtml.toString());
            }
            for (String summary : filter(object.getSummary(), not(blank()))) {
                ao.setSummary(summary);
            }
            for (URI uri : object.getAlternateLinkUri()) {
                if (isAbsolute(uri)) {
                    ao.setAlternateLink(uri);
                }
            }
            for (ActivityObjectType type : object.getActivityObjectType()) {
                ao.setObjectType(type);
            }
        }

        private List<Person> buildAuthors(final StreamsEntry entry) {
            final List<Person> people = new ArrayList<Person>();
            for (final UserProfile author : entry.getAuthors()) {
                Person person = buildPerson(author, entry.getApplicationType());
                people.add(person);
            }
            if (people.isEmpty()) {
                people.add(buildAnonPerson(entry.getApplicationType()));
            }
            return people;
        }

        private Person buildPerson(UserProfile profile, String application) {
            Person person = abdera.getFactory().newAuthor();
            person.setName(profile.getFullName() != null ? profile.getFullName() : profile.getUsername());

            for (String email : filter(profile.getEmail(), not(blank()))) {
                person.setEmail(email);
            }

            for (URI uri : profile.getProfilePageUri()) {
                person.setUri(uri.toASCIIString());
            }

            addProfilePictureLinks(person, profile.getProfilePictureUri(), application);

            // add <usr:username> element
            person.addSimpleExtension(USR_USERNAME, profile.getUsername());

            // add <activity:object-type>http://activitystrea.ms/schema/1.0/person</activity:object-type> element
            person.addSimpleExtension(ACTIVITY_OBJECT_TYPE, ACTIVITY_STREAMS_PERSON_TYPE);

            return person;
        }

        private FOMGenerator buildGenerator(String uri) {
            FOMGenerator generator = abdera.getFactory().newExtensionElement(GENERATOR);
            generator.setUri(uri);
            return generator;
        }

        private Person buildAnonPerson(String application) {
            Person person = abdera.getFactory().newAuthor();
            person.setName(context.getAnonymousUserName());
            addProfilePictureLinks(person, none(URI.class), application);
            return person;
        }

        private void addProfilePictureLinks(Person person, Option<URI> pictureUri, String application) {
            // add profile picture link(s)
            for (int size : context.getDefaultUserPictureSizes()) {
                Option<URI> maybeLinkUri = context.getUserPictureUri(pictureUri, size, application);
                for (URI linkUri : maybeLinkUri) {
                    org.apache.abdera.model.Link link = abdera.getFactory().newLink();
                    link.setRel("photo");
                    link.setHref(linkUri.toASCIIString());
                    String sizeStr = String.valueOf(size);
                    link.setAttributeValue(MEDIA_HEIGHT, sizeStr);
                    link.setAttributeValue(MEDIA_WIDTH, sizeStr);
                    person.addExtension(link);
                }
            }
        }
    }
}
