package com.atlassian.streams.internal;

import java.net.URI;
import java.util.Map;

import com.google.common.base.Predicate;

import com.atlassian.streams.api.StreamsEntry.Link;

import static org.apache.commons.lang3.StringUtils.isBlank;

public final class Predicates {
    public static <K, V> Predicate<Map.Entry<K, V>> whereMapEntryKey(Predicate<K> p) {
        return new WhereKey<K, V>(p);
    }

    private static final class WhereKey<K, V> implements Predicate<Map.Entry<K, V>> {
        private final Predicate<K> p;

        public WhereKey(Predicate<K> p) {
            this.p = p;
        }

        public boolean apply(Map.Entry<K, V> e) {
            return p.apply(e.getKey());
        }
    }

    public static Predicate<String> blank() {
        return Blank.INSTANCE;
    }

    private enum Blank implements Predicate<String> {
        INSTANCE;

        public boolean apply(String s) {
            return isBlank(s);
        }
    }

    public static Predicate<URI> isAbsolute() {
        return IsAbsolute.INSTANCE;
    }

    public static boolean isAbsolute(URI uri) {
        return isAbsolute().apply(uri);
    }

    private enum IsAbsolute implements Predicate<URI> {
        INSTANCE;

        public boolean apply(URI uri) {
            return uri != null && uri.isAbsolute();
        }
    }

    public static Predicate<Link> linkHref(Predicate<URI> p) {
        return new LinkHref(p);
    }

    private static final class LinkHref implements Predicate<Link> {
        private final Predicate<URI> p;

        public LinkHref(Predicate<URI> p) {
            this.p = p;
        }

        public boolean apply(Link link) {
            return p.apply(link.getHref());
        }
    }

    public static Predicate<Link> linkRel(Predicate<String> p) {
        return new LinkRel(p);
    }

    private static final class LinkRel implements Predicate<Link> {
        private final Predicate<String> p;

        public LinkRel(Predicate<String> p) {
            this.p = p;
        }

        public boolean apply(Link link) {
            return p.apply(link.getRel());
        }
    }
}
