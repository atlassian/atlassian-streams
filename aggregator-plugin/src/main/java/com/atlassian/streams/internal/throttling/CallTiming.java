package com.atlassian.streams.internal.throttling;

import java.time.Instant;
import java.util.function.Supplier;

public abstract class CallTiming {
    protected final Instant callStart;
    protected final Instant callEnd;

    public abstract long getCallDurationMs();

    public abstract boolean mayEndAfter(Instant instant);

    public abstract boolean endedBefore(Instant instant);

    public abstract CallTiming end();

    private static final class FinishedCallTiming extends CallTiming {
        private FinishedCallTiming(final Instant callStart, final Instant callEnd) {
            super(callStart, callEnd);
        }

        @Override
        public long getCallDurationMs() {
            return callEnd.toEpochMilli() - callStart.toEpochMilli();
        }

        @Override
        public CallTiming end() {
            throw new IllegalStateException("Attempted to finish a call that already ended");
        }

        @Override
        public boolean mayEndAfter(final Instant instant) {
            return callEnd.isAfter(instant);
        }

        @Override
        public boolean endedBefore(final Instant instant) {
            return callEnd.isBefore(instant);
        }
    }

    public static final class RunningCallTiming extends CallTiming {
        private final Supplier<Instant> clock;

        private RunningCallTiming(final Supplier<Instant> clock) {
            super(clock.get(), null);
            this.clock = clock;
        }

        @Override
        public long getCallDurationMs() {
            final Instant endOrNow = callEnd == null ? clock.get() : callEnd;
            return endOrNow.toEpochMilli() - callStart.toEpochMilli();
        }

        @Override
        public CallTiming end() {
            return new FinishedCallTiming(callStart, clock.get());
        }

        @Override
        public boolean mayEndAfter(final Instant instant) {
            return true;
        }

        @Override
        public boolean endedBefore(final Instant instant) {
            return false;
        }
    }

    private CallTiming(final Instant callStart, final Instant callEnd) {
        this.callStart = callStart;
        this.callEnd = callEnd;
    }

    public static CallTiming start(final Supplier<Instant> clock) {
        return new RunningCallTiming(clock);
    }
}
