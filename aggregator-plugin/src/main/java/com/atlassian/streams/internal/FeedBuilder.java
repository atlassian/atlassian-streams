package com.atlassian.streams.internal;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.ActivityProvider.Error;
import com.atlassian.streams.internal.feed.FeedAggregator;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.feed.builder.ToFeedCallable;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.internal.ActivityProvider.Error.banned;

public final class FeedBuilder {
    private final Logger log = LoggerFactory.getLogger(FeedBuilder.class);

    private final ActivityProviders activityProviders;
    private final FeedAggregator aggregator;
    private final StreamsCompletionService completionService;
    private final ApplicationProperties applicationProperties;
    private final UserManager userManager;

    public FeedBuilder(
            ActivityProviders activityProviders,
            FeedAggregator aggregator,
            StreamsCompletionService completionService,
            ApplicationProperties applicationProperties,
            UserManager userManager) {
        this.applicationProperties = applicationProperties;
        this.activityProviders = checkNotNull(activityProviders, "activityProviders");
        this.aggregator = checkNotNull(aggregator, "aggregator");
        this.completionService = checkNotNull(completionService, "completionService");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public FeedModel getFeed(Uri self, String contextPath, HttpParameters parameters, String requestLanguages) {
        if (parameters.allowOnlyAuthorized() && userManager.getRemoteUserKey() == null) {
            Set<Either<Error, FeedModel>> empty = Collections.emptySet();
            return aggregator.aggregate(empty, self, 0, parameters.getTitle());
        }

        Iterable<ActivityProvider> providers = activityProviders.get(
                ImmutableSet.of(parameters.module(), parameters.fetchLocalOnly(), parameters.isSelectedProvider()));
        final ImmutableSet<ActivityProvider> banned =
                ImmutableSet.copyOf(filter(providers, not(completionService.reachable())));
        final Iterable<ActivityProvider> notBannedProviders = filter(providers, testedProvider -> {
            for (ActivityProvider bannedProvider : banned) {
                // fallback -> old behaviour
                if (bannedProvider.equals(testedProvider)) {
                    return false;
                }

                if (bannedProvider instanceof ActivityProviderWithAnalytics) {
                    ActivityProvider bannedInternalProvider =
                            ((ActivityProviderWithAnalytics) bannedProvider).getDelegate();
                    if (testedProvider instanceof ActivityProviderWithAnalytics) {
                        ActivityProvider testedInternalProvider =
                                ((ActivityProviderWithAnalytics) testedProvider).getDelegate();
                        if (bannedInternalProvider.equals(testedInternalProvider)) {
                            return false;
                        }
                    }
                }
            }

            return true;
        });

        final Iterable<ActivityProviderCancellableTask<Either<Error, FeedModel>>> callables = transform(
                notBannedProviders,
                toFeedCallable(
                        pair(self, parameters),
                        parameters.calculateContextUrl(applicationProperties, contextPath),
                        requestLanguages,
                        RandomUtils.nextLong()));

        final Iterable<Either<Error, FeedModel>> results;
        if (Sys.inDevMode() && !parameters.isTimeoutTest()) {
            results = completionService.execute(callables);
        } else {
            results = completionService.execute(callables, ActivityRequestImpl.DEFAULT_TIMEOUT, MILLISECONDS);
        }
        return aggregator.aggregate(
                transformForAnonymous(results, banned),
                self,
                parameters.parseMaxResults(ActivityRequestImpl.DEFAULT_MAX_RESULTS),
                parameters.getTitle());
    }

    private List<Either<Error, FeedModel>> transformForAnonymous(
            Iterable<Either<Error, FeedModel>> results, ImmutableSet<ActivityProvider> banned) {
        Stream<Either<Error, FeedModel>> resultsStream = stream(results.spliterator(), false);
        if (userManager.getRemoteUserKey() == null) {
            // for anonymous users we are showing only correct results - see BSP-2720
            return resultsStream.filter(Either::isRight).collect(toList());
        } else {
            Stream<Either<Error, FeedModel>> bannedStream =
                    banned.stream().map(activityProvider -> left(banned(activityProvider)));

            return Stream.concat(resultsStream, bannedStream).collect(toList());
        }
    }

    private final Function<ActivityProvider, ActivityProviderCancellableTask<Either<Error, FeedModel>>> toFeedCallable(
            final Pair<Uri, HttpParameters> feedParameters,
            final URI baseUri,
            final String requestLanguages,
            final long requestId) {
        return new ToFeedCallable(feedParameters, baseUri, requestLanguages, requestId);
    }
}
