package com.atlassian.streams.internal.rest.representations;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import com.atlassian.plugins.rest.api.model.Status;

/**
 * A custom extension of the Jackson {@code ObjectMapper} that adds a custom object mapping from a {@code Status}
 * object to a custom {@code JsonSerializer}. The JSON Serializer will serialize an {@code ErrorRepresentation} object
 * from the mapped {@code Status} object.
 */
public class ErrorResponseStatusObjectMapper extends ObjectMapper {
    public ErrorResponseStatusObjectMapper() {
        // Create a custom serializer factory to handle com.atlassian.plugins.rest.api.model.Status response entities
        SimpleModule module = new SimpleModule();
        module.addSerializer(Status.class, new StatusToErrorRepresentationSerializer());
        registerModule(module);
    }

    /**
     * Implementation of a JSON serializer that generates an {@code ErrorRepresentation} object from a {@code Status}
     * object. This class is needed for Jackson to properly serialize {@code Status} objects returned from the
     * response entity. The {@code Status} object must be of type {@code CLIENT_ERROR} or {@code SERVER_ERROR}
     */
    private static final class StatusToErrorRepresentationSerializer extends JsonSerializer<Status> {
        /*
         * (non-Javadoc)
         *
         * @see com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object,
         * com.fasterxml.jackson.core.JsonGenerator,
         * com.fasterxml.jackson.databind.SerializerProvider)
         */
        @Override
        public void serialize(Status value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeObject(new ErrorRepresentation(value.getMessage(), "stream.error.unexpected.error"));
        }
    }
}
