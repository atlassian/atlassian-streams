package com.atlassian.streams.internal;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.streams.internal.throttling.CallTiming;

public final class CallThrottler {
    private static final Logger logger = LoggerFactory.getLogger(CallThrottler.class);

    private final List<TrackedCall> calls = new CopyOnWriteArrayList<>();

    private final Supplier<Instant> clock;

    private final Duration maxAllowedWallclockTime;
    private final Duration timeWindow;
    private final int allowedWallClockPercentage;

    public CallThrottler(final Duration timeWindow, final int allowedWallClockPercentage) {
        this(Instant::now, timeWindow, allowedWallClockPercentage);
    }

    public CallThrottler(
            final Supplier<Instant> clock, final Duration timeWindow, final int allowedWallClockPercentage) {
        this.clock = clock;
        this.timeWindow = timeWindow;
        this.allowedWallClockPercentage = allowedWallClockPercentage;
        this.maxAllowedWallclockTime = Duration.ofMillis(
                Runtime.getRuntime().availableProcessors() * timeWindow.toMillis() * allowedWallClockPercentage / 100);
    }

    public boolean isBudgetExceeded() {
        final Instant timeWindowStart = getTimeWindowStart();

        final long consumedWallclockMs = calls.stream()
                .map(TrackedCall::getCallTiming)
                .filter(t -> t.mayEndAfter(timeWindowStart))
                .mapToLong(CallTiming::getCallDurationMs)
                .map(duration -> Math.min(timeWindow.toMillis(), duration))
                .sum();

        if (logger.isDebugEnabled()) {
            final long maxAllowedWallclockTimeMs = maxAllowedWallclockTime.toMillis();
            final long budgetUtilisation =
                    maxAllowedWallclockTimeMs <= 0 ? 100 : 100 * consumedWallclockMs / maxAllowedWallclockTimeMs;
            logger.debug(
                    "Tracking {} calls, since {}, budget utilisation: {}%",
                    calls.size(), timeWindowStart, budgetUtilisation);
        }
        return Duration.ofMillis(consumedWallclockMs).compareTo(maxAllowedWallclockTime) > 0;
    }

    public Map<String, Duration> getStats() {
        return calls.stream()
                .collect(Collectors.toMap(
                        TrackedCall::getContext,
                        e -> Duration.ofMillis(e.getCallTiming().getCallDurationMs()),
                        Duration::plus));
    }

    public final class TrackedCall implements AutoCloseable {
        private final String context;
        private volatile CallTiming callTiming;

        private TrackedCall(final String context) {
            this.context = context;
            callTiming = CallTiming.start(clock);
        }

        @Override
        public void close() {
            callTiming = callTiming.end();
        }

        public String getContext() {
            return context;
        }

        public CallTiming getCallTiming() {
            return callTiming;
        }
    }

    public TrackedCall startTracking(final String context) {
        removeCallsOlderThan(getTimeWindowStart());

        final TrackedCall trackedCall = new TrackedCall(context);
        calls.add(trackedCall);
        return trackedCall;
    }

    public int getAllowedWallClockPercentage() {
        return allowedWallClockPercentage;
    }

    public Duration getTimeWindow() {
        return timeWindow;
    }

    private void removeCallsOlderThan(final Instant instant) {
        calls.removeIf(e -> e.getCallTiming().endedBefore(instant));
    }

    private Instant getTimeWindowStart() {
        return clock.get().minus(timeWindow);
    }
}
