package com.atlassian.streams.testing;

public final class StreamsTestGroups {
    public static final String BAMBOO = "bamboo";
    public static final String CONFLUENCE = "confluence";
    public static final String FECRU = "fecru";
    public static final String JIRA = "jira";
    public static final String APPLINKS = "applinks";
    public static final String APPLINKS_OAUTH = "applinks-oauth";
    public static final String APPLINKS_STREAMS3 = "applinks-streams3";
}
