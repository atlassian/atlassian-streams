package com.atlassian.streams.testing;

import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.containsString;

import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class StreamsImportingProgressBarPage extends ConfluenceAbstractPage {

    @ElementBy(id = "taskCurrentStatus")
    private PageElement statusLabel;

    @Override
    public String getUrl() {
        return "/admin/restore.action";
    }

    public void waitTillImportComplete() {
        waitUntilTrue(
                "Import status label should be visible", statusLabel.timed().isVisible());
        waitUntil(
                "Import should have completed",
                statusLabel.timed().getText(),
                containsString("Complete"),
                by(180, SECONDS));
    }
}
