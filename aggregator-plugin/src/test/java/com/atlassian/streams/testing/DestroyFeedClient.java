package com.atlassian.streams.testing;

import com.google.common.base.Function;
import com.google.inject.Injector;

import com.atlassian.integrationtesting.runner.CompositeTestRunner.AfterTestClass;

public final class DestroyFeedClient implements Function<AfterTestClass, Void> {
    private final Injector injector;

    public DestroyFeedClient(Injector injector) {
        this.injector = injector;
    }

    public Void apply(AfterTestClass from) {
        injector.getInstance(FeedClient.class).destroy();
        return null;
    }
}
