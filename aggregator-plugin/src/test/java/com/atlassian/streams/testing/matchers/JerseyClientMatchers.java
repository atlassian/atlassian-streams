package com.atlassian.streams.testing.matchers;

import javax.ws.rs.core.Response;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

public final class JerseyClientMatchers {
    private JerseyClientMatchers() {}

    public static Matcher<? super Response> ok() {
        return new StatusMatcher(Response.Status.OK);
    }

    public static Matcher<? super Response> accepted() {
        return new StatusMatcher(ACCEPTED);
    }

    public static Matcher<? super Response> badRequest() {
        return new StatusMatcher(BAD_REQUEST);
    }

    public static Matcher<? super Response> notFound() {
        return new StatusMatcher(NOT_FOUND);
    }

    public static Matcher<? super Response> conflict() {
        return new StatusMatcher(CONFLICT);
    }

    public static Matcher<? super Response> badProxy() {
        return new StatusMatcher(502);
    }

    public static Matcher<? super Response> forbidden() {
        return new StatusMatcher(FORBIDDEN);
    }

    public static Matcher<? super Response> unauthorized() {
        return new StatusMatcher(UNAUTHORIZED);
    }

    public static Matcher<? super Response> location(Matcher<String> matcher) {
        return new HeaderMatcher("Location", matcher);
    }

    private static final class HeaderMatcher extends TypeSafeDiagnosingMatcher<Response> {
        private final String headerName;
        private final Matcher<String> matcher;

        public HeaderMatcher(String headerName, Matcher<String> matcher) {
            this.headerName = headerName;
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(Response Response, Description description) {
            String val = Response.getStringHeaders().getFirst(headerName);
            if (!matcher.matches(val)) {
                description
                        .appendText("A response with a \"" + headerName + "\" header whose value was ")
                        .appendValue(val);
                return false;
            }
            return true;
        }

        @Override
        public void describeTo(Description description) {
            description
                    .appendText("A response with a \"" + headerName + "\" header matching ")
                    .appendDescriptionOf(matcher);
        }
    }

    private static final class StatusMatcher extends TypeSafeDiagnosingMatcher<Response> {
        private final int expected;

        public StatusMatcher(int status) {
            expected = status;
        }

        public StatusMatcher(Response.Status status) {
            this(status.getStatusCode());
        }

        @Override
        protected boolean matchesSafely(Response item, Description mismatchDescription) {
            if (item.getStatus() != expected) {
                mismatchDescription.appendText("status was ").appendValue(item.getStatus());
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("status is ").appendValue(expected);
        }
    }
}
