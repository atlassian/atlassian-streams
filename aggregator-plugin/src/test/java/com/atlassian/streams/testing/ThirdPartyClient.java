package com.atlassian.streams.testing;

import java.net.URI;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.thirdparty.ThirdPartyJsonProvider;
import com.atlassian.streams.thirdparty.rest.representations.ActivityCollectionRepresentation;
import com.atlassian.streams.thirdparty.rest.representations.ActivityRepresentation;

import static com.atlassian.streams.thirdparty.rest.MediaTypes.STREAMS_THIRDPARTY_JSON;

/**
 * Simple client for the third-party activity REST API.
 * <p>
 * This currently uses generic JSON objects rather than the API-specific representation classes from
 * streams-thirdparty-plugin.
 */
public final class ThirdPartyClient {
    private static final String DEFAULT_USERNAME = "admin";
    private static final String DEFAULT_PASSWORD = "admin";

    private final Client client;
    private final ApplicationProperties applicationProperties;

    public ThirdPartyClient(ApplicationProperties applicationProperties) {
        this(applicationProperties, true);
    }

    public ThirdPartyClient(ApplicationProperties applicationProperties, boolean login) {
        this(applicationProperties, login, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public ThirdPartyClient(ApplicationProperties applicationProperties, String username, String password) {
        this(applicationProperties, true, username, password);
    }

    private ThirdPartyClient(
            ApplicationProperties applicationProperties, boolean login, String username, String password) {
        this.applicationProperties = applicationProperties;

        ClientBuilder builder = ClientBuilder.newBuilder()
                .register(ThirdPartyJsonProvider.class)
                .property(ClientProperties.FOLLOW_REDIRECTS, Boolean.FALSE);
        if (login) {
            builder.register(HttpAuthenticationFeature.basic(username, password));
        }
        client = builder.build();
    }

    public ThirdPartyClient as(String username, String password) {
        return new ThirdPartyClient(applicationProperties, username, password);
    }

    public void destroy() {
        client.close();
    }

    public Response postActivity(ActivityRepresentation rep) {
        WebTarget target = client.target(collectionUri());
        return target.request().post(Entity.entity(rep, STREAMS_THIRDPARTY_JSON));
    }

    public Iterable<URI> getActivityUris() {
        WebTarget target = client.target(collectionUri()).register(ThirdPartyJsonProvider.class);
        ActivityCollectionRepresentation response = target.request().get(ActivityCollectionRepresentation.class);
        ImmutableList.Builder<URI> ret = ImmutableList.builder();
        for (ActivityRepresentation item : response.getItems()) {
            ret.add(item.getLinks().get("self"));
        }
        return ret.build();
    }

    public Response deleteActivity(URI activityUri) {
        WebTarget target = client.target(fullActivityUri(activityUri));
        return target.request().delete();
    }

    public Response deleteAllActivities() {
        WebTarget target = client.target(collectionUri());
        return target.request().delete();
    }

    private URI collectionUri() {
        return UriBuilder.fromUri(applicationProperties.getBaseUrl(UrlMode.CANONICAL))
                .path("/rest/activities/1.0/")
                .build();
    }

    private URI fullActivityUri(URI activityUri) {
        String basePath =
                URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).getPath();
        return UriBuilder.fromUri(applicationProperties.getBaseUrl(UrlMode.CANONICAL))
                .path(activityUri.toASCIIString().substring(basePath.length()))
                .build();
    }
}
