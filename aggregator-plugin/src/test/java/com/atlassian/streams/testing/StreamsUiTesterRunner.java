package com.atlassian.streams.testing;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.runners.model.InitializationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.spi.Message;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.client.apache4.config.DefaultApacheHttpClient4Config;
import com.sun.jersey.multipart.impl.MultiPartWriter;

import com.atlassian.confluence.test.ConfluenceApacheHttpClient4;
import com.atlassian.confluence.test.ConfluenceBaseUrlSelector;
import com.atlassian.confluence.test.rest.api.BackupRestoreRest;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.VersionedRpcBaseResolver;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.CompositeTestRunner;
import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.integrationtesting.ui.Bamboo;
import com.atlassian.integrationtesting.ui.CompositeUiTester.Backup;
import com.atlassian.integrationtesting.ui.CompositeUiTester.Login;
import com.atlassian.integrationtesting.ui.CompositeUiTester.WebSudoLogin;
import com.atlassian.integrationtesting.ui.Confluence;
import com.atlassian.integrationtesting.ui.FeCru;
import com.atlassian.integrationtesting.ui.Jira;
import com.atlassian.integrationtesting.ui.RunningTestGroup;
import com.atlassian.integrationtesting.ui.UiRunListener.OutputDirectory;
import com.atlassian.integrationtesting.ui.UiTestRunner;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.integrationtesting.ui.UiTesterFunctionProvider;
import com.atlassian.integrationtesting.ui.UiTesters;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.sal.api.ApplicationProperties;

import static org.apache.commons.lang3.StringUtils.isBlank;

import static com.atlassian.integrationtesting.ui.UiTesters.logInByGoingTo;

public class StreamsUiTesterRunner extends CompositeTestRunner {
    public StreamsUiTesterRunner(Class<?> klass) throws InitializationError {
        super(klass, compose());
    }

    public static Composer compose() {
        Injector injector = Guice.createInjector(new StreamsModule());
        return CompositeTestRunner.compose()
                .from(UiTestRunner.compose(injector))
                .afterTestClass(new DestroyClients(injector));
    }

    static final class StreamsModule extends AbstractModule {
        @Override
        protected void configure() {}

        @Provides
        @Singleton
        @RunningTestGroup
        String getRunningTestGroup() {
            if (isBlank(TestGroupRunner.getRunningTestGroup())) {
                throw new ConfigurationException(
                        ImmutableList.of(
                                new Message(
                                        "No testGroup configured - can't figure out which UiTester to use without a test group in the form {application}-v{version}")));
            }
            return TestGroupRunner.getRunningTestGroup();
        }

        @Provides
        @Singleton
        ApplicationProperties getApplicationProperties() {
            return ApplicationPropertiesImpl.getStandardApplicationProperties();
        }

        @Provides
        @Singleton
        @OutputDirectory
        File outputDirectory(@RunningTestGroup String runningTestGroup) {
            return new File("target", runningTestGroup + "-htmlunit");
        }

        @Provides
        @Singleton
        RestTester getRestTester(ApplicationProperties applicationProperties) {
            return new RestTester(applicationProperties);
        }

        @Provides
        @Singleton
        UiTester getUiTester(ApplicationProperties applicationProperties, @RunningTestGroup String runningTestGroup) {
            return Testers.valueOf(runningTestGroup.toUpperCase()).get(applicationProperties);
        }

        @Provides
        @Singleton
        StreamsUiTester getStreamsUiTester(UiTester uiTester) {
            return new StreamsUiTester(uiTester);
        }

        @Provides
        @Singleton
        ThirdPartyClient getThirdPartyClient(ApplicationProperties applicationProperties) {
            return new ThirdPartyClient(applicationProperties);
        }

        enum Testers {
            BAMBOO(Bamboo.v5_7),
            CONFLUENCE(ConfluenceWithUiRestore.v5_6),
            FECRU(FeCru.v3_6),
            JIRA(JiraWithUiRestore.v6_3);

            private final UiTesterFunctionProvider provider;

            Testers(UiTesterFunctionProvider provider) {
                this.provider = provider;
            }

            public UiTester get(ApplicationProperties applicationProperties) {
                return UiTesters.newUiTester(applicationProperties, provider);
            }
        }
    }

    /**
     * Because we have a hacked up version of Fisheye and the dashboard is broken we need a customized login
     * that will redirect to somewhere other than the dashboard, which currently returns a 500.
     */
    private enum FeCruWithCustomizedLogIn implements UiTesterFunctionProvider {
        v2_3(FeCru.v2_3);

        private final UiTesterFunctionProvider delegate;

        FeCruWithCustomizedLogIn(UiTesterFunctionProvider delegate) {
            this.delegate = delegate;
        }

        public Function<UiTester, String> getLoggedInUser() {
            return delegate.getLoggedInUser();
        }

        public Function<UiTester, Boolean> isOnLogInPage() {
            return delegate.isOnLogInPage();
        }

        public Function<Login, HtmlPage> logIn() {
            return logInByGoingTo("login?origUrl=/streams/browse")
                    .formName("loginform")
                    .userInputName("username")
                    .passwordInputName("password")
                    .build();
        }

        public Function<UiTester, Void> logout() {
            return delegate.logout();
        }

        public Function<Backup, Void> restore() {
            return delegate.restore();
        }

        public Function<WebSudoLogin, HtmlPage> webSudoLogIn() {
            return delegate.webSudoLogIn();
        }
    }

    private enum ConfluenceWithUiRestore implements UiTesterFunctionProvider {
        v5_6(Confluence.v5_6);

        private static final Logger log = LoggerFactory.getLogger(ConfluenceWithUiRestore.class);

        private final UiTesterFunctionProvider delegate;
        private final ConfluenceRpcClient rpcClient;
        private final BackupRestoreRest backupRestoreRest;

        ConfluenceWithUiRestore(UiTesterFunctionProvider delegate) {
            this.delegate = delegate;
            ConfluenceBaseUrlSelector baseUrlSelector = new ConfluenceBaseUrlSelector();
            ClientConfig clientConfig = new DefaultApacheHttpClient4Config();
            clientConfig.getClasses().add(MultiPartWriter.class);
            ConfluenceRestClient restClient =
                    new ConfluenceRestClient(baseUrlSelector, ConfluenceApacheHttpClient4.create(clientConfig));
            rpcClient = new ConfluenceRpcClient(baseUrlSelector, VersionedRpcBaseResolver.V2);
            backupRestoreRest = restClient.getAdminSession().getBackupRestoreRestComponent();
        }

        @Override
        public Function<Login, HtmlPage> logIn() {
            return delegate.logIn();
        }

        @Override
        public Function<WebSudoLogin, HtmlPage> webSudoLogIn() {
            return delegate.webSudoLogIn();
        }

        @Override
        public Function<UiTester, String> getLoggedInUser() {
            return delegate.getLoggedInUser();
        }

        @Override
        public Function<UiTester, Boolean> isOnLogInPage() {
            return delegate.isOnLogInPage();
        }

        @Override
        public Function<UiTester, Void> logout() {
            return delegate.logout();
        }

        @Override
        public Function<Backup, Void> restore() {
            return backup -> {
                try {
                    log.info("Restoring Confluence state from {}", backup);
                    backupRestoreRest.performSyncSiteRestore(new File(backup.data.toURI()));
                    rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
                    log.info("Restoring Confluence state FINISHED");
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(ex);
                } catch (IOException | URISyntaxException e) {
                    throw new RuntimeException(e);
                }
                return null;
            };
        }
    }

    private enum JiraWithUiRestore implements UiTesterFunctionProvider {
        v6_3(Jira.v6_3);

        private static final Logger log = LoggerFactory.getLogger(JiraWithUiRestore.class);

        private final UiTesterFunctionProvider delegate;
        private final Backdoor backdoor;

        JiraWithUiRestore(UiTesterFunctionProvider delegate) {
            this.delegate = delegate;
            this.backdoor = new Backdoor(new TestKitLocalEnvironmentData());
        }

        @Override
        public Function<Login, HtmlPage> logIn() {
            return delegate.logIn();
        }

        @Override
        public Function<WebSudoLogin, HtmlPage> webSudoLogIn() {
            return delegate.webSudoLogIn();
        }

        @Override
        public Function<UiTester, String> getLoggedInUser() {
            return delegate.getLoggedInUser();
        }

        @Override
        public Function<UiTester, Boolean> isOnLogInPage() {
            return delegate.isOnLogInPage();
        }

        @Override
        public Function<UiTester, Void> logout() {
            return delegate.logout();
        }

        @Override
        public Function<Backup, Void> restore() {
            return backup -> {
                // Backup file is loaded via ClassLoader#getResource(),
                // therefore a file path has to be trimmed to be relative
                final String filePath = backup.data.getFile().split("target/test-classes/")[1];
                log.info("Restoring Jira state from {}", filePath);
                // Backups override base URL with `@base-url@` value which causes some tests to fail
                final String jiraBaseURLProp = "jira.baseurl";
                final String baseURL = backdoor.applicationProperties().getString(jiraBaseURLProp);
                backdoor.restoreDataFromResource(filePath);
                backdoor.applicationProperties().setString(jiraBaseURLProp, baseURL);
                log.info("Restoring Jira state FINISHED");
                return null;
            };
        }
    }

    private static final class DestroyClients implements Function<AfterTestClass, Void> {
        private final Injector injector;

        public DestroyClients(Injector injector) {
            this.injector = injector;
        }

        public Void apply(AfterTestClass from) {
            injector.getInstance(FeedClient.class).destroy();
            injector.getInstance(RestTester.class).destroy();
            return null;
        }
    }
}
