package com.atlassian.streams.testing;

import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.internal.util.collection.ImmutableMultivaluedMap;
import org.glassfish.jersey.internal.util.collection.StringKeyIgnoreCaseMultivaluedMap;

public final class PreferenceMapBuilder {
    private static final String PREF_TITLE = "title";
    private static final String PREF_KEYS = "keys";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_NUMOFENTRIES = "numofentries";

    private final MultivaluedMap<String, String> preferences;

    private PreferenceMapBuilder() {
        preferences = new StringKeyIgnoreCaseMultivaluedMap<>();
        title("Default Title");
        maxResults("10");
    }

    public static PreferenceMapBuilder builder() {
        return new PreferenceMapBuilder();
    }

    public PreferenceMapBuilder title(String title) {
        return putValue(PREF_TITLE, title);
    }

    public PreferenceMapBuilder keys(String keys) {
        return putValue(PREF_KEYS, keys);
    }

    public PreferenceMapBuilder usernames(String usernames) {
        return putValue(PREF_USERNAME, usernames);
    }

    public PreferenceMapBuilder maxResults(String maxResults) {
        return putValue(PREF_NUMOFENTRIES, maxResults);
    }

    private PreferenceMapBuilder putValue(String key, String value) {
        if (value == null) {
            preferences.remove(key);
        } else {
            preferences.putSingle(key, value);
        }
        return this;
    }

    public MultivaluedMap<String, String> build() {
        return new ImmutableMultivaluedMap<>(preferences);
    }
}
