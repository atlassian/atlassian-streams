package com.atlassian.streams.testing;

import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Iterables.filter;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringEscapeUtils.escapeXml;
import static org.apache.commons.lang3.StringUtils.capitalize;

import static com.atlassian.streams.testing.App.CRUCIBLE;

/**
 * Utility which automates configuring applinks.  It is expected that each app will already be configured to
 * trust the others before this is done (if there was a good cross product way to setup trusted apps then we could
 * put that in here too, but I haven't been able to find any).
 */
public class ConfigureApplinks {
    static Client client = ClientBuilder.newBuilder()
            .register(HttpAuthenticationFeature.basic("admin", "admin"))
            .build();

    public static void main(String[] args) {
        for (App app : filter(asList(App.values()), not(equalTo(CRUCIBLE)))) {
            configureApplinks(app);
        }
    }

    /**
     * Assumes all other apps have been configured to "trust" requests from {@code app}.
     */
    private static void configureApplinks(App app) {
        System.out.println("Configuring " + app);
        for (App other : filter(asList(App.values()), not(or(equalTo(app), in(app.excludes()))))) {
            if (addInstanceLink(app, other)) {
                addProjectLink(app, other);
            }
        }
    }

    private static boolean addInstanceLink(App from, App to) {
        System.out.print("  adding instance link to " + to);
        Form form = new Form();
        form.param("application", to.name());
        form.param("name", to.toString());
        form.param("url", "http://localhost:" + to.port() + "/streams");

        Response response = client.target(
                        "http://localhost:" + from.port() + "/streams/plugins/servlet/applinks/instance")
                .request(MediaType.APPLICATION_FORM_URLENCODED)
                .post(Entity.form(form));
        if (response.getStatus() != 200) {
            System.out.println(" - failed with status " + response.getStatus());
            return false;
        } else {
            return true;
        }
    }

    private static void addProjectLink(App from, App to) {
        System.out.print(" - adding project link to " + to);

        String xml = "<com.atlassian.applinks.core.DefaultApplicationLink>" + "<name>"
                + to + "</name>" + "<remoteKey>"
                + to.project() + "</remoteKey>" + "<url>"
                + escapeXml("http://localhost:" + to.port() + "/streams") + "</url>"
                + "</com.atlassian.applinks.core.DefaultApplicationLink>";
        Response response = client.target(
                        "http://localhost:" + from.port() + "/streams/plugins/servlet/applinks/projectlinks?key="
                                + from.project() + "&application=" + to.name())
                .request(MediaType.APPLICATION_XML)
                .post(Entity.xml(xml));
        if (response.getStatus() != 200) {
            System.out.println(" - failed with status " + response.getStatus());
        } else {
            System.out.println(" - success!");
        }
    }
}

enum App {
    JIRA(2990, "ONE"),
    BAMBOO(6990, "ONE"),
    CONFLUENCE(1990, "ONE"),
    FISHEYE(3990, "ONE"),
    CRUCIBLE(3990, "CR-ONE");

    private final int port;
    private final String project;

    App(int port, String project) {
        this.port = port;
        this.project = project;
    }

    int port() {
        return port;
    }

    String project() {
        return project;
    }

    Set<App> excludes() {
        switch (this) {
            case FISHEYE:
                return ImmutableSet.of(CRUCIBLE);
            case CRUCIBLE:
                return ImmutableSet.of(FISHEYE);
            default:
                return ImmutableSet.of();
        }
    }

    @Override
    public String toString() {
        return this == JIRA ? name() : capitalize(name().toLowerCase());
    }
}
