package com.atlassian.streams.testing;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import com.google.common.collect.ImmutableList;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import static com.atlassian.streams.confluence.ConfluenceStreamsActivityProvider.PROVIDER_KEY;

public class Confluence {
    public static final String CONFLUENCE_PROVIDER = "wiki";

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities) {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities) {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity) {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(
            Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2) {
        return activities(op, ImmutableList.of(a1, a2));
    }

    /**
     * Timestamp for "My First Blog"
     */
    public static final ZonedDateTime MY_FIRST_BLOG = ZonedDateTime.of(2010, 11, 11, 14, 28, 26, 0, ZoneOffset.UTC);

    // 2011-06-29T21:42:52.165Z
    public static final ZonedDateTime STRM_769_ADD_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 42, 52, 165 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:03.043Z
    public static final ZonedDateTime STRM_769_EDIT_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 3, 43 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:10.273Z
    public static final ZonedDateTime STRM_975_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 10, 273 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:18.605Z
    public static final ZonedDateTime STRM_1426_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 18, 605 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:26.253Z
    public static final ZonedDateTime STRM_1486_BLOG =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 26, 253 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:33.492Z
    public static final ZonedDateTime STRM_930_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 33, 492 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:45.303Z
    public static final ZonedDateTime MIN_DATE_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 45, 303 * 1000000, ZoneOffset.UTC);

    // 2011-06-29T21:43:52.004Z
    public static final ZonedDateTime MAX_DATE_PAGE =
            ZonedDateTime.of(2011, 6, 29, 14, 43, 52, 4 * 1000000, ZoneOffset.UTC);
}
