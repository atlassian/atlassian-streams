package com.atlassian.streams.testing;

import org.slf4j.LoggerFactory;

import com.atlassian.confluence.webdriver.pageobjects.AjaxTracingDefaultWebDriverTester;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.LoggerModule;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.pageobjects.elements.ElementModule;
import com.atlassian.pageobjects.elements.timeout.TimeoutsModule;
import com.atlassian.webdriver.WebDriverModule;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * TestedProduct for tests that don't require a specific product to run, for example QUnit tests
 */
@Defaults(instanceId = "", contextPath = "/streams", httpPort = 3990)
public class StreamsTestedProduct implements TestedProduct<WebDriverTester> {

    private final WebDriverTester webDriverTester;
    private final ProductInstance productInstance;
    private final PageBinder pageBinder;

    public StreamsTestedProduct(
            TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance) {
        checkNotNull(productInstance);
        this.webDriverTester = testerFactory == null ? new AjaxTracingDefaultWebDriverTester() : testerFactory.create();
        this.productInstance = productInstance;
        this.pageBinder = new InjectPageBinder(
                productInstance,
                webDriverTester,
                new StandardModule(this),
                new WebDriverModule(this),
                new ElementModule(),
                new TimeoutsModule(),
                new LoggerModule(LoggerFactory.getLogger(ConfluenceTestedProduct.class)));
    }

    @Override
    public <P extends Page> P visit(Class<P> pageClass, Object... args) {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    @Override
    public PageBinder getPageBinder() {
        return pageBinder;
    }

    @Override
    public ProductInstance getProductInstance() {
        return productInstance;
    }

    @Override
    public WebDriverTester getTester() {
        return webDriverTester;
    }
}
