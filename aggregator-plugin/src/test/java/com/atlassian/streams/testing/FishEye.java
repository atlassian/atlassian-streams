package com.atlassian.streams.testing;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.apache.abdera.model.Entry;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import com.google.common.collect.ImmutableList;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

import static com.atlassian.streams.fisheye.FishEyeStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.testing.AbstractFeedClient.module;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withSummary;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

public class FishEye {
    public static final String FISHEYE_PROVIDER = "source";

    /**
     * Provides matchers for entries that will appear in the activity stream when started with the
     * streams-fecru-home.zip or fecru-streams-applinks-test-resources.zip as the home directory.
     */
    public static final class Data {
        // Changeset 1 was committed at 2007-11-18T23:22:05Z
        public static final ZonedDateTime CHANGESET_1 = ZonedDateTime.of(2007, 11, 18, 23, 22, 5, 0, ZoneOffset.UTC);

        // Changeset 2 was committed at 2007-11-18T23:23:20Z
        public static final ZonedDateTime CHANGESET_2 = ZonedDateTime.of(2007, 11, 18, 23, 23, 20, 0, ZoneOffset.UTC);

        // Changeset 3 was committed at 2010-11-22T23:57:16Z
        public static final ZonedDateTime CHANGESET_3 = ZonedDateTime.of(2010, 11, 22, 23, 57, 16, 0, ZoneOffset.UTC);

        // Changeset 4 was committed at 2010-11-22T23:58:23Z
        public static final ZonedDateTime CHANGESET_4 = ZonedDateTime.of(2010, 11, 22, 23, 58, 23, 0, ZoneOffset.UTC);

        // Changeset 5 was committed at 2011-03-02T18:21:04Z
        public static final ZonedDateTime CHANGESET_5 = ZonedDateTime.of(2011, 3, 2, 21, 04, 0, 0, ZoneOffset.UTC);

        // Changeset 6 was committed at 2011-03-02T18:24:58Z
        public static final ZonedDateTime CHANGESET_6 = ZonedDateTime.of(2011, 3, 2, 18, 24, 58, 0, ZoneOffset.UTC);

        // Changeset 7 was committed at 2012-10-22T04:09:11Z
        public static final ZonedDateTime CHANGESET_7 = ZonedDateTime.of(2012, 10, 22, 4, 9, 11, 0, ZoneOffset.UTC);

        public static Matcher<? super Entry> changeSet1() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("detkin"),
                            containsString("committed changeset"),
                            containsString("cs=1"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(withContent(containsString("Initial import")));
        }

        public static Matcher<Entry> changeSet2() {
            return Matchers.<Entry>both(withTitle(allOf(
                            containsString("detkin"),
                            containsString("committed changeset"),
                            containsString("cs=2"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(withContent(containsString("added a comment to the file")))
                    .and(withContent(containsString("TST-1")));
        }

        public static Matcher<Entry> changeSet3() {
            return Matchers.<Entry>both(withTitle(allOf(
                            containsString("rtalusan"),
                            containsString("committed changeset"),
                            containsString("cs=3"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(withContent(containsString("added file1")));
        }

        public static Matcher<Entry> changeSet4() {
            return Matchers.<Entry>both(withTitle(allOf(
                            containsString("rtalusan"),
                            containsString("committed changeset"),
                            containsString("cs=4"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(withContent(containsString("wow, that's a lot of files..")));
        }

        public static Matcher<Entry> changeSet5() {
            return Matchers.<Entry>both(withTitle(allOf(
                            containsString("rtalusan"),
                            containsString("committed changeset"),
                            containsString("cs=5"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(withContent(containsString("removed blank files")));
        }

        public static Matcher<Entry> changeSet6() {
            return Matchers.<Entry>both(withTitle(allOf(
                            containsString("rtalusan"),
                            containsString("committed changeset"),
                            containsString("cs=6"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(withContent(containsString("added non-blank multiple files")));
        }

        public static Matcher<Entry> changeSet7() {
            return Matchers.<Entry>both(withTitle(allOf(
                            containsString("mminns"),
                            containsString("committed changeset"),
                            containsString("cs=7"),
                            containsString("to the"),
                            containsString("Test Repository"))))
                    .and(
                            withSummary(
                                    allOf(
                                            containsString("add a file to test XSS comment"),
                                            containsString(
                                                    "&lt;script type=&ldquo;text/javascript&rdquo;&gt;alert('Xss!');&lt;/script&gt;"))));
        }
    }

    public static Parameter fisheyeModule() {
        return module(PROVIDER_KEY);
    }

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities) {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities) {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity) {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(
            Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2) {
        return activities(op, ImmutableList.of(a1, a2));
    }
}
