package com.atlassian.streams.testing;

import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.apache.abdera.model.Feed;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;

import com.atlassian.streams.api.DateUtil;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;
import com.atlassian.streams.thirdparty.rest.representations.ActivityObjectRepresentation;
import com.atlassian.streams.thirdparty.rest.representations.ActivityRepresentation;

import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.testing.AbstractFeedClient.module;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.providerFilter;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasThirdPartyFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withFilterProviderName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionKey;
import static com.atlassian.streams.testing.matchers.JerseyClientMatchers.badRequest;
import static com.atlassian.streams.testing.matchers.JerseyClientMatchers.location;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withPublishedDate;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.thirdparty.ThirdPartyStreamsEntryBuilder.DEFAULT_ACTIVITY_ICON;

/**
 * Abstract base class for third-party activity tests to be run in all products.
 */
public abstract class ThirdPartyTests {

    protected static final String DEFAULT_ACTIVITY_ID = "http://activity";
    protected static final String GENERATOR_ID = "http://bitbucket.com";
    protected static final String GENERATOR_NAME = "BitBucket";
    protected static final String GENERATOR_ID_2 = "http://www.atlassian.com/GreenHopper";
    protected static final String GENERATOR_NAME_2 = "GreenHopper";
    protected static final String GENERATOR_KEY = GENERATOR_ID + "@" + GENERATOR_NAME;
    protected static final String USER_NAME = "admin";
    protected static final String UNKNOWN_USER_NAME = "panada";
    protected static final URI AVATAR_URL =
            URI.create("http://www.rockfuse.com/blog/wp-content/uploads/2011/04/panda.jpg");
    protected static final String USER_DISPLAY_NAME = "Joe Schmoe";
    protected static final String PROVIDER_NAME_FILTER = "provider_name";

    @Inject
    protected static FeedClient client;

    @Inject
    protected static RestTester restTester;

    @Inject
    protected static ThirdPartyClient thirdPartyClient;

    protected String defaultTitle = this.getClass().getSimpleName();

    @Before
    public void setUp() {
        thirdPartyClient.deleteAllActivities();
        assertTrue(isEmpty(thirdPartyClient.getActivityUris()));
    }

    @After
    public void tearDown() {
        thirdPartyClient.deleteAllActivities();
    }

    protected abstract String getValidProjectKey();

    protected abstract String getInvalidProjectKey();

    @Test
    public void assertThatFilterResourceHasThirdPartyFilterKey() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasThirdPartyFilters());
    }

    @Test
    public void assertThatFilterHasThirdPartyAsDisplayName() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasThirdPartyFilters(withFilterProviderName(equalTo("Third Parties"))));
    }

    @Test
    public void assertThatFilterHasProviderNameFilterOption() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasThirdPartyFilters(withOption(withOptionKey(PROVIDER_NAME_FILTER))));
    }

    @Test
    public void canDeleteActivity() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());
        assertThat(feed.getEntries().size(), equalTo(1));

        Iterable<URI> activityUris = thirdPartyClient.getActivityUris();
        assertThat(size(activityUris), equalTo(1));

        thirdPartyClient.deleteActivity(Iterables.getOnlyElement(activityUris));

        Feed feed2 = client.getAs("admin", thirdPartyModule());
        assertThat(feed2.getEntries().size(), equalTo(0));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void activityPublishedDateDefaultsToNowIfOmitted() {
        ZonedDateTime now = ZonedDateTime.now();
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());

        assertThat(feed.getEntries(), hasEntries(withPublishedDate(greaterThanOrEqualTo(now))));
    }

    @Test
    public void activityPublishedDateUsesSpecifiedDate() {
        ZonedDateTime date = ZonedDateTime.of(2010, 1, 1, 12, 0, 0, 0, ZoneId.systemDefault());
        postActivityAndAssertSuccess(
                activity(actorWithId(USER_NAME), generator()).published(some(DateUtil.toDate(date))));

        Feed feed = client.getAs("admin", thirdPartyModule());

        assertThat(feed.getEntries(), hasEntries(withPublishedDate(equalTo(date))));
    }

    @Test
    public void activityHasGenerator() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(haveAtlassianApplicationElement(equalTo(GENERATOR_ID))));
    }

    @Test
    public void activityIsIncludedByGeneratorFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs(
                "admin", thirdPartyModule(), providerFilter("thirdparty", PROVIDER_NAME_FILTER, IS, GENERATOR_KEY));

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(haveAtlassianApplicationElement(equalTo(GENERATOR_ID))));
    }

    @Test
    public void activityWithWrongGeneratorIsExcludedByGeneratorFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator(GENERATOR_ID_2, GENERATOR_NAME_2)));

        Feed feed = client.getAs(
                "admin", thirdPartyModule(), providerFilter("thirdparty", PROVIDER_NAME_FILTER, IS, GENERATOR_KEY));

        assertThat(feed.getEntries().size(), equalTo(0));
    }

    @Test
    public void activityFromRegisteredUserHasUserName() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(withAuthorElement(equalTo(USER_NAME))));
    }

    @Test
    public void activityWithRegisteredUserIsIncludedByUsernameFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule(), user(IS, USER_NAME));

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(withAuthorElement(equalTo(USER_NAME))));
    }

    @Test
    public void activityWithoutIconHasDefaultIcon() {
        postActivityAndAssertSuccess(activity(actorWithDisplayName(USER_DISPLAY_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());
        assertThat(
                feed,
                hasEntry(withLink(
                        (allOf(whereHref(endsWith(DEFAULT_ACTIVITY_ICON)), whereRel(equalTo(ICON_LINK_REL)))))));
    }

    @Test
    public void postingActivityHasResponseWithCorrectLocationHeader() {
        Response response = thirdPartyClient.postActivity(
                activity(actorWithId(USER_NAME), generator()).build());
        assertThat(response, is(location(startsWith(client.getBaseUrl() + "/rest/activities/1.0/"))));
    }

    @Test
    public void postingInvalidActivityCausesValidationError() {
        // Omitting generator name is one way for an activity to be invalid.  We're testing the
        // other validity constraints in more detail in the streams-thirdparty-plugin unit
        // tests; this test just verifies that validation errors are reported as HTTP 400,
        // rather than HTTP 500 (STRM-1856).
        Response response = thirdPartyClient.postActivity(activity(
                        actorWithId(USER_NAME),
                        ActivityObjectRepresentation.builder().idString(some(GENERATOR_ID)))
                .build());
        assertThat(response, is(badRequest()));
    }

    @Test
    public void activityWithProjectKeyIsIncludedByProjectFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                .title(some(html(defaultTitle)))
                .target(some(ActivityObjectRepresentation.builder()
                        .urlString(some(getValidProjectKey()))
                        .build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), key(IS, getValidProjectKey()));

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(withTitle(equalTo(defaultTitle))));
    }

    @Test
    public void activityWithoutProjectKeyIsExcludedByProjectFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule(), issueKey(IS, getValidProjectKey()));

        assertThat(feed.getEntries().size(), equalTo(0));
    }

    @Test
    public void activityWithWrongProjectKeyIsExcludedByProjectFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                .title(some(html(defaultTitle)))
                .target(some(ActivityObjectRepresentation.builder()
                        .urlString(some(getInvalidProjectKey()))
                        .build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), key(IS, getValidProjectKey()));

        assertThat(feed.getEntries().size(), equalTo(0));
    }

    @Test
    public void activityWithoutTargetQueriedByAnonymousUserIsExcluded() {
        postActivityAndAssertSuccess(
                activity(actorWithId(USER_NAME), generator()).title(some(html(defaultTitle))));

        Feed feed = client.get(thirdPartyModule());

        assertThat(feed.getEntries(), Matchers.hasSize(0));
    }

    protected static ActivityRepresentation.Builder activity(
            ActivityObjectRepresentation.Builder actor, ActivityObjectRepresentation.Builder generator) {
        return ActivityRepresentation.builder(actor.build(), generator.build()).idString(some(DEFAULT_ACTIVITY_ID));
    }

    protected static ActivityObjectRepresentation.Builder actorWithId(String id) {
        return ActivityObjectRepresentation.builder().idString(some(id));
    }

    protected static ActivityObjectRepresentation.Builder actorWithDisplayName(String name) {
        return ActivityObjectRepresentation.builder().displayName(some(name));
    }

    protected static ActivityObjectRepresentation.Builder generator() {
        return generator(GENERATOR_ID, GENERATOR_NAME);
    }

    protected static Parameter thirdPartyModule() {
        return module("thirdparty");
    }

    protected static ActivityObjectRepresentation.Builder generator(String id, String name) {
        return ActivityObjectRepresentation.builder().idString(some(id)).displayName(some(name));
    }

    protected void postActivityAndAssertSuccess(ActivityRepresentation.Builder builder) {
        Response response = thirdPartyClient.postActivity(builder.build());
        if (response.getStatus() != HttpServletResponse.SC_CREATED) {
            fail("Unexpected error response (" + response.getStatus() + "): " + response.readEntity(String.class));
        }
    }
}
