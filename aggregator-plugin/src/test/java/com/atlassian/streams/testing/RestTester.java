package com.atlassian.streams.testing;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.internal.rest.representations.I18nTranslations;
import com.atlassian.streams.internal.rest.representations.JsonProvider;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.internal.rest.resources.ConfigurationPreferencesResource;
import com.atlassian.streams.internal.rest.resources.I18nResource;
import com.atlassian.streams.internal.rest.resources.StreamsConfigResource;
import com.atlassian.streams.internal.rest.resources.StreamsValidationResource;
import com.atlassian.streams.internal.rest.resources.UrlProxyResource;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

import static com.atlassian.streams.internal.rest.MediaTypes.STREAMS_JSON;

public class RestTester {
    private static final String DEFAULT_USERNAME = "admin";
    private static final String DEFAULT_PASSWORD = "admin";

    private final Client client;
    private final ApplicationProperties applicationProperties;

    public RestTester(ApplicationProperties applicationProperties) {
        this(applicationProperties, true);
    }

    public RestTester(ApplicationProperties applicationProperties, boolean login) {
        this(applicationProperties, login, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public RestTester(ApplicationProperties applicationProperties, String username, String password) {
        this(applicationProperties, true, username, password);
    }

    private RestTester(ApplicationProperties applicationProperties, boolean login, String username, String password) {
        this.applicationProperties = applicationProperties;
        ClientBuilder builder = ClientBuilder.newBuilder()
                .register(JsonProvider.class)
                .property(ClientProperties.FOLLOW_REDIRECTS, Boolean.FALSE);
        if (login) {
            builder.register(HttpAuthenticationFeature.basic(username, password));
        }
        client = builder.build();
    }

    public void destroy() {
        client.close();
    }

    public StreamsConfigRepresentation getStreamsConfigRepresentation() {
        WebTarget target = client.target(newBaseUriBuilder().path(StreamsConfigResource.class));
        return target.request(STREAMS_JSON).accept(STREAMS_JSON).get(StreamsConfigRepresentation.class);
    }

    public ConfigPreferencesRepresentation getConfigPreferencesRepresentation() {
        WebTarget target = client.target(newBaseUriBuilder().path(ConfigurationPreferencesResource.class));
        return target.request(STREAMS_JSON).accept(STREAMS_JSON).get(ConfigPreferencesRepresentation.class);
    }

    public String getI18nValue(String key, String... parameters) {
        UriBuilder uriBuilder =
                newBaseUriBuilder().path(I18nResource.class).path("key").path(key);
        if (parameters.length > 0) {
            uriBuilder.queryParam("parameters", new Object[] {parameters});
        }
        WebTarget target = client.target(uriBuilder);
        return target.request(TEXT_PLAIN).accept(TEXT_PLAIN).get(String.class);
    }

    public I18nTranslations getI18nValues(String prefix) {
        WebTarget target = client.target(
                newBaseUriBuilder().path(I18nResource.class).path("prefix").path(prefix));
        return target.request(APPLICATION_JSON).accept(APPLICATION_JSON).get(I18nTranslations.class);
    }

    public Response getProxiedResponse(String url) {
        WebTarget target =
                client.target(newBaseUriBuilder().path(UrlProxyResource.class).queryParam("url", url));
        return target.request().get();
    }

    public Response validatePreferences(MultivaluedMap<String, String> preferences) {
        WebTarget target = client.target(newBaseUriBuilder().path(StreamsValidationResource.class));
        target = addQueryParams(target, preferences);

        return target.request(STREAMS_JSON).accept(STREAMS_JSON).get();
    }

    protected UriBuilder newBaseUriBuilder() {
        return UriBuilder.fromUri(applicationProperties.getBaseUrl(UrlMode.CANONICAL))
                .path("/rest/activity-stream/1.0");
    }

    WebTarget addQueryParams(WebTarget webTarget, MultivaluedMap<String, String> params) {
        for (String key : params.keySet()) {
            for (String value : params.get(key)) {
                webTarget = webTarget.queryParam(key, value);
            }
        }
        return webTarget;
    }
}
