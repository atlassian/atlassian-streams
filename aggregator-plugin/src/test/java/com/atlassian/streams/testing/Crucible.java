package com.atlassian.streams.testing;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import org.apache.abdera.model.Entry;
import org.hamcrest.Matcher;
import com.google.common.collect.ImmutableList;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.testing.AbstractFeedClient.module;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

public class Crucible {
    public static final String CRUCIBLE_PROVIDER = "reviews";

    public static final class Data {
        // CR-1 was created 2009-10-20T04:27:25Z
        public static final ZonedDateTime CR_1_CREATE = ZonedDateTime.of(2009, 10, 20, 4, 27, 25, 0, ZoneOffset.UTC);

        // CR-1 was was started 2009-10-20T04:28:28Z
        public static final ZonedDateTime CR_1_START = ZonedDateTime.of(2009, 10, 20, 4, 28, 28, 0, ZoneOffset.UTC);

        // CR-3 was created 2010-11-13T00:27:47Z
        public static final ZonedDateTime CR_3_CREATE = ZonedDateTime.of(2010, 11, 13, 0, 27, 47, 0, ZoneOffset.UTC);

        // CR-4 was created 2010-11-13T00:28:09Z
        public static final ZonedDateTime CR_4_CREATE = ZonedDateTime.of(2010, 11, 13, 0, 28, 9, 0, ZoneOffset.UTC);

        // CR-4 was closed 2010-11-13T00:28:30Z
        public static final ZonedDateTime CR_4_CLOSE = ZonedDateTime.of(2010, 11, 13, 0, 28, 30, 0, ZoneOffset.UTC);

        // CR-7 was created 2010-11-13T00:30:26Z
        public static final ZonedDateTime CR_7_CREATE =
                ZonedDateTime.of(2010, 11, 13, 0, 30, 26, 425 * 1000000, ZoneOffset.UTC);

        // CR-7 was commented 2010-11-13T00:30:44Z
        public static final ZonedDateTime CR_7_COMMENT = ZonedDateTime.of(2010, 11, 13, 0, 30, 44, 0, ZoneOffset.UTC);

        // CR-7 comment was commented 2010-12-03T19:01:38Z
        public static final ZonedDateTime CR_7_THREAD_COMMENT =
                ZonedDateTime.of(2010, 12, 3, 19, 1, 38, 0, ZoneOffset.UTC);

        // CR-8 was created 2010-12-08T19:04:11Z
        public static final ZonedDateTime CR_8_CREATE = ZonedDateTime.of(2010, 12, 8, 19, 4, 11, 0, ZoneOffset.UTC);

        // CR-8 was abandoned 2010-12-08T19:04:32Z
        public static final ZonedDateTime CR_8_ABANDON = ZonedDateTime.of(2010, 12, 8, 19, 4, 32, 0, ZoneOffset.UTC);

        // CR-9 was created 2010-12-08T19:04:36Z
        public static final ZonedDateTime CR_9_CREATE = ZonedDateTime.of(2010, 12, 8, 19, 4, 36, 0, ZoneOffset.UTC);

        // CR-9 was started 2010-12-08T19:08:41Z
        public static final ZonedDateTime CR_9_START = ZonedDateTime.of(2010, 12, 8, 19, 8, 41, 0, ZoneOffset.UTC);

        public static List<Matcher<? super Entry>> allCrucibleEntries() {
            return ImmutableList.<Matcher<? super Entry>>builder()
                    .add(reviewStrm1Created())
                    .addAll(allCrucibleDefaultProjectEntries())
                    .build();
        }

        public static List<Matcher<? super Entry>> allCrucibleDefaultProjectEntries() {
            return ImmutableList.<Matcher<? super Entry>>of(
                    review9Created(),
                    review8Abandoned(),
                    review8Created(),
                    review7CommentCommented(),
                    review7Commented(),
                    review7Created(),
                    review6Commented(),
                    review6ReOpened(),
                    review6Closed(),
                    review6Created(),
                    review5Closed(),
                    review5Created(),
                    review4Closed(),
                    review4Created(),
                    review3Summarized(),
                    review3Created(),
                    review2Created(),
                    review1Created());
        }

        public static Matcher<? super Entry> reviewStrm1Created() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-STRM-1")));
        }

        public static Matcher<? super Entry> review1Created() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-1"),
                    containsString("A Sample Review")));
        }

        public static Matcher<? super Entry> review2Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-2"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review2Started() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("started review"),
                    not(containsString("created")),
                    containsString("CR-2")));
        }

        public static Matcher<? super Entry> review3Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-3"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review3CreatedOnly() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created review"),
                            containsString("CR-3"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review3Summarized() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"), containsString("summarised review"), containsString("CR-3")));
        }

        public static Matcher<? super Entry> review4Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-4"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<Entry> review4Summarized() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"), containsString("summarised review"), containsString("CR-4")));
        }

        public static Matcher<? super Entry> review4Closed() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised and closed review"),
                    containsString("CR-4")));
        }

        public static Matcher<? super Entry> review5Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-5"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review5Summarized() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"), containsString("summarised review"), containsString("CR-5")));
        }

        public static Matcher<? super Entry> review5Closed() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised and closed review"),
                    containsString("CR-5")));
        }

        public static Matcher<? super Entry> review6Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-6"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review6Closed() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised and closed review"),
                    containsString("CR-6")));
        }

        public static Matcher<? super Entry> review6ReOpened() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"), containsString("reopened review"), containsString("CR-6")));
        }

        public static Matcher<? super Entry> review6Commented() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("commented on review"),
                            containsString("CR-6"))))
                    .and(withContent(containsString("We're commenting on this review")));
        }

        public static Matcher<? super Entry> review6CommentThatHasBeenDeleted() {
            return org.hamcrest.Matchers.<Entry>both(
                            withTitle(allOf(containsString("commented on review"), containsString("CR-6"))))
                    .and(withContent(containsString("comment to be deleted")));
        }

        public static Matcher<? super Entry> review7Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-7"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review7Commented() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("commented on review"),
                            containsString("CR-7"))))
                    .and(withContent(containsString("this is my comment")));
        }

        public static Matcher<? super Entry> review7CommentCommented() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("commented on review"),
                            containsString("CR-7"))))
                    .and(withContent(containsString("reply to a comment")));
        }

        public static Matcher<? super Entry> review8Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created review"),
                            containsString("CR-8"))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 0"),
                            containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review8Abandoned() {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"), containsString("abandoned review"), containsString("CR-8")));
        }

        public static Matcher<? super Entry> review9Created() {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                            containsString("A. D. Ministrator"),
                            containsString("created and started review"),
                            containsString("CR-9"),
                            containsString("wow, that's a lot of files.."))))
                    .and(withContent(allOf(
                            containsString("Default Project"),
                            containsString("Files: 9"),
                            containsString("Reviewers: None"))));
        }
    }

    public static Parameter crucibleModule() {
        return module(PROVIDER_KEY);
    }

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities) {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities) {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity) {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(
            Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2) {
        return activities(op, ImmutableList.of(a1, a2));
    }
}
