package com.atlassian.streams.internal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ArrayListMultimap;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;

import static com.google.common.collect.Iterables.getOnlyElement;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ActivityRequestImplTest {
    private static final String PROVIDER_KEY = "myProvider";

    @Mock
    ActivityProvider provider;

    @Before
    public void setUp() {
        when(provider.matches(PROVIDER_KEY)).thenReturn(true);
    }

    @Test
    public void assertFilterValuesAreUnescaped() {
        ArrayListMultimap<String, String> parameters = ArrayListMultimap.create();
        parameters.put(PROVIDER_KEY, "user IS user_123");
        ActivityRequest request = builder().build(HttpParameters.parameters(parameters), provider);
        Pair<Operator, Iterable<String>> filter =
                getOnlyElement(request.getProviderFiltersMap().getOrDefault("user", emptyList()));
        assertThat(filter.second(), contains("user 123"));

        parameters = ArrayListMultimap.create();
        parameters.put(PROVIDER_KEY, "user IS user\\_123");
        request = builder().build(HttpParameters.parameters(parameters), provider);
        filter = getOnlyElement(request.getProviderFiltersMap().getOrDefault("user", emptyList()));
        assertThat(filter.second(), contains("user_123"));
    }

    @Test
    public void assertFilterValuesAreSplitOnSpaces() {
        ArrayListMultimap<String, String> parameters = ArrayListMultimap.create();
        parameters.put(PROVIDER_KEY, "myFilter IS abc def ghi");

        ActivityRequest request = builder().build(HttpParameters.parameters(parameters), provider);
        Pair<Operator, Iterable<String>> filter =
                getOnlyElement(request.getProviderFiltersMap().getOrDefault("myFilter", emptyList()));
        assertThat(filter.second(), contains("abc", "def", "ghi"));
    }

    @Test
    public void assertFilterValuesAreUnescapedAfterBeingSplit() {
        ArrayListMultimap<String, String> parameters = ArrayListMultimap.create();
        parameters.put(PROVIDER_KEY, "myFilter IS ab_c d\\_ef ghi");

        ActivityRequest request = builder().build(HttpParameters.parameters(parameters), provider);
        Pair<Operator, Iterable<String>> filter =
                getOnlyElement(request.getProviderFiltersMap().getOrDefault("myFilter", emptyList()));
        assertThat(filter.second(), contains("ab c", "d_ef", "ghi"));
    }

    private ActivityRequestImpl.Builder builder() {
        return ActivityRequestImpl.builder(Uri.parse("http://localhost:3990/streams"));
    }
}
