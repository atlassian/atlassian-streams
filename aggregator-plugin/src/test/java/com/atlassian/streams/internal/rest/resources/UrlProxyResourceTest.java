package com.atlassian.streams.internal.rest.resources;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.internal.ActivityProviders;
import com.atlassian.streams.internal.AppLinksActivityProvider;
import com.atlassian.streams.internal.rest.resources.whitelist.Whitelist;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UrlProxyResourceTest {
    @Mock
    private ActivityProviders activityProviders;

    @Mock
    private Whitelist whitelist;

    @InjectMocks
    private UrlProxyResource urlProxyResource;

    @Test
    public void urlProxyResourceShouldNotFollowRedirects() throws Exception {
        Request request = mockRequestWithOkResponse();
        configureWithSingleProvider(activityProviders, mockApplinksActivityProviderWithRequest(request));
        allowEverything(whitelist);

        urlProxyResource.get("http://example.com/nowhere");

        verify(request).setFollowRedirects(eq(false));
    }

    private static void configureWithSingleProvider(
            ActivityProviders activityProviders, AppLinksActivityProvider mockProvider) {
        when(activityProviders.getRemoteProviderForUri(any())).thenReturn(Option.some(mockProvider));
    }

    private static void allowEverything(Whitelist whitelist) {
        when(whitelist.allows(any(URI.class))).thenReturn(true);
    }

    @SuppressWarnings("unchecked")
    private static AppLinksActivityProvider mockApplinksActivityProviderWithRequest(Request request)
            throws CredentialsRequiredException {
        AppLinksActivityProvider mockProvider = mock(AppLinksActivityProvider.class);
        when(mockProvider.createRequest(any(String.class), any(Request.MethodType.class)))
                .thenReturn(request);
        return mockProvider;
    }

    @SuppressWarnings("unchecked")
    private static Request mockRequestWithOkResponse() throws ResponseException {
        Request request = mock(Request.class);
        Response response = mockResponseWithOkStatus();
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);
        return request;
    }

    private static Response mockResponseWithOkStatus() {
        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(200);
        return response;
    }
}
