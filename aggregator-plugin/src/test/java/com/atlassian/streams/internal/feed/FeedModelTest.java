package com.atlassian.streams.internal.feed;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.testing.StreamsTestData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.testing.StreamsTestData.FEED_DATE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_SUBTITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_TITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_URI;

@RunWith(MockitoJUnitRunner.class)
public class FeedModelTest {
    @Mock
    I18nResolver i18nResolver;

    @Test
    public void simpleProperties() {
        FeedModel feed = FeedModel.builder(FEED_URI)
                .title(some(FEED_TITLE))
                .subtitle(some(FEED_SUBTITLE))
                .updated(FEED_DATE)
                .build();

        assertThat(feed.getUri(), equalTo(FEED_URI));
        assertThat(feed.getTitle(), equalTo(some(FEED_TITLE)));
        assertThat(feed.getSubtitle(), equalTo(some(FEED_SUBTITLE)));
        assertThat(feed.getUpdatedDate(), equalTo(FEED_DATE));
        assertThat(Iterables.size(feed.getEntries()), equalTo(0));
        assertThat(Iterables.size(feed.getHeaders()), equalTo(0));
    }

    @Test
    public void addEntries() {
        FeedEntry[] entries = new FeedEntry[4];
        for (int i = 0; i < entries.length; i++) {
            entries[i] = FeedEntry.fromStreamsEntry(new StreamsEntry(StreamsTestData.newEntryParams(), i18nResolver));
        }

        FeedModel feed = FeedModel.builder(FEED_URI)
                .addEntries(ImmutableList.of(entries[0], entries[1]))
                .addEntries(ImmutableList.of(entries[2], entries[3]))
                .build();

        assertThat(feed.getEntries(), contains(entries));
    }

    @Test
    public void addHeaders() {
        FeedHeader[] headers = new FeedHeader[3];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = StreamsTestData.emptyFeedHeader();
        }

        FeedModel feed = FeedModel.builder(FEED_URI)
                .addHeaders(ImmutableList.of(headers[0]))
                .addHeaders(ImmutableList.of(headers[1], headers[2]))
                .build();

        assertThat(feed.getHeaders(), contains(headers));
    }

    public void addMultiLinks() {
        String REL = "foo";
        Uri LINK_1 = Uri.parse("http://link/1");
        Uri LINK_2 = Uri.parse("http://link/2");

        FeedModel feed = FeedModel.builder(FEED_URI)
                .addLink(REL, LINK_1)
                .addLink(REL, LINK_2)
                .build();

        assertThat(feed.getLinks().get(REL), contains(LINK_1, LINK_2));
    }

    public void replaceLink() {
        String REL_1 = "next";
        String REL_2 = "last";
        Uri LINK_1 = Uri.parse("http://link/1");
        Uri LINK_2 = Uri.parse("http://link/2");

        FeedModel feed = FeedModel.builder(FEED_URI)
                .replaceLink(REL_1, LINK_2)
                .replaceLink(REL_1, LINK_1)
                .replaceLink(REL_2, LINK_2)
                .build();

        assertThat(feed.getLinks().get(REL_1), contains(LINK_1));
        assertThat(feed.getLinks().get(REL_2), contains(LINK_2));
    }

    public void addLinkIfNotPresent() {
        String REL_1 = "next";
        String REL_2 = "last";
        Uri LINK_1 = Uri.parse("http://link/1");
        Uri LINK_2 = Uri.parse("http://link/2");

        FeedModel feed = FeedModel.builder(FEED_URI)
                .addLinkIfNotPresent(REL_1, LINK_2)
                .addLinkIfNotPresent(REL_1, LINK_1)
                .addLinkIfNotPresent(REL_2, LINK_1)
                .build();

        assertThat(feed.getLinks().get(REL_1), contains(LINK_2));
        assertThat(feed.getLinks().get(REL_2), contains(LINK_1));
    }
}
