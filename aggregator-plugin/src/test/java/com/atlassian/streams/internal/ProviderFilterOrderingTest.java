package com.atlassian.streams.internal;

import org.junit.Test;
import com.google.common.collect.ImmutableList;

import com.atlassian.streams.internal.rest.representations.FilterOptionRepresentation;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;

public class ProviderFilterOrderingTest {
    private final ProviderFilterRepresentation streams = newProviderFilter(STANDARD_FILTERS_PROVIDER_KEY);
    private final ProviderFilterRepresentation localConf = newLocalProviderFilter("wiki");
    private final ProviderFilterRepresentation localConfThirdParty = newLocalThirdPartyProviderFilter("wiki");
    private final ProviderFilterRepresentation jira = newProviderFilter("issues");
    private final ProviderFilterRepresentation conf = newProviderFilter("wiki");
    private final ProviderFilterRepresentation fe = newProviderFilter("source");
    private final ProviderFilterRepresentation jiraThirdParty = newThirdPartyProviderFilter("issues");
    private final ProviderFilterRepresentation confThirdParty = newThirdPartyProviderFilter("wiki");
    private final ProviderFilterRepresentation feThirdParty = newThirdPartyProviderFilter("source");

    @Test
    public void assertThatNoPrioritizedProvidersSortsNormally() {
        assertThat(
                ProviderFilterOrdering.prioritizing()
                        .sortedCopy(ImmutableList.of(streams, fe, jira, conf, confThirdParty)),
                contains(streams, jira, fe, conf, confThirdParty));
    }

    @Test
    public void assertThatSingleLocalPrioritizedProviderComesFirst() {
        assertThat(
                ProviderFilterOrdering.prioritizing("wiki").sortedCopy(ImmutableList.of(streams, fe, jira, localConf)),
                contains(streams, localConf, jira, fe));
    }

    @Test
    public void assertThatMultipleLocalPrioritizedProvidersComeFirst() {
        assertThat(
                ProviderFilterOrdering.prioritizing("wiki", "Third Parties")
                        .sortedCopy(ImmutableList.of(streams, fe, jira, localConf, localConfThirdParty)),
                contains(streams, localConf, localConfThirdParty, jira, fe));
    }

    @Test
    public void assertThatRemoteProductProviderComesBeforeRemoteThirdPartyProviders() {
        assertThat(
                ProviderFilterOrdering.prioritizing()
                        .sortedCopy(ImmutableList.of(
                                streams, jira, conf, fe, jiraThirdParty, confThirdParty, feThirdParty)),
                contains(streams, jira, jiraThirdParty, fe, feThirdParty, conf, confThirdParty));
    }

    private ProviderFilterRepresentation newProviderFilter(String name) {
        return new ProviderFilterRepresentation(name, name, name, ImmutableList.<FilterOptionRepresentation>of(), null);
    }

    private ProviderFilterRepresentation newThirdPartyProviderFilter(String name) {
        return new ProviderFilterRepresentation(
                "thirdparty@" + name, "Third Parties", name, ImmutableList.<FilterOptionRepresentation>of(), null);
    }

    private ProviderFilterRepresentation newLocalProviderFilter(String name) {
        return new ProviderFilterRepresentation(name, name, "", ImmutableList.<FilterOptionRepresentation>of(), null);
    }

    private ProviderFilterRepresentation newLocalThirdPartyProviderFilter(String name) {
        return new ProviderFilterRepresentation(
                "thirdparty@" + name, "Third Parties", "", ImmutableList.<FilterOptionRepresentation>of(), null);
    }
}
