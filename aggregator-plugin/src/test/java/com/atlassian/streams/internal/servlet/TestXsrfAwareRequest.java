package com.atlassian.streams.internal.servlet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link XsrfAwareRequest} class.
 *
 * @since v5.2
 */
public class TestXsrfAwareRequest {
    /**
     * Tests that the XSRF token is to be found under the product-specific name
     * when fetched by the {@link HttpServletRequest#getParameter(String)} method.
     */
    @Test
    public void getParameter() {
        final HttpServletRequest req = mockReq();
        assertNull(req.getParameter("shadow"));
        final XsrfAwareRequest xsrfAwareRequest = new XsrfAwareRequest(req, "shadow");
        assertEquals("tzu", xsrfAwareRequest.getParameter("shadow"));
    }

    /**
     * Tests that the XSRF token is to be found under the product-specific name
     * when fetched by the {@link HttpServletRequest#getParameterValues(String)} method.
     */
    @Test
    public void getParameterValues() {
        final XsrfAwareRequest xsrfAwareRequest = new XsrfAwareRequest(mockReq(), "shadow");
        assertArrayEquals(new String[] {"tzu"}, xsrfAwareRequest.getParameterValues("shadow"));
    }

    /**
     * Tests that the product-specific XSRF token parameter name is included
     * in the list of parameter names.
     */
    @Test
    public void getParameterNames() {
        final XsrfAwareRequest xsrfAwareRequest = new XsrfAwareRequest(mockReq(), "shadow");
        final Enumeration<String> parameterNames = xsrfAwareRequest.getParameterNames();
        assertThat(Collections.list(parameterNames), hasItem("shadow"));
    }

    /**
     * Tests that the parameter map includes the product-specific XSRF parameter
     */
    @Test
    public void getParameterMap() {
        final XsrfAwareRequest xsrfAwareRequest = new XsrfAwareRequest(mockReq(), "shadow");
        final Map<String, String[]> parameterMap = xsrfAwareRequest.getParameterMap();
        assertArrayEquals(new String[] {"tzu"}, parameterMap.get("shadow"));
    }

    /**
     * Tests that we don't override the value of any parameter in the wrapped HTTP servlet request
     * that happens to have the same name as the shadow parameter.
     */
    @Test
    public void noShadowOverride() {
        final XsrfAwareRequest xsrfAwareRequest = new XsrfAwareRequest(mockReqWithShadow(), "shadow");
        assertEquals("abc", xsrfAwareRequest.getParameter("shadow"));
        assertArrayEquals(new String[] {"abc"}, xsrfAwareRequest.getParameterValues("shadow"));
        final Map<String, String[]> parameterMap = xsrfAwareRequest.getParameterMap();
        assertArrayEquals(new String[] {"abc"}, parameterMap.get("shadow"));
    }

    /**
     * Creates a mock HTTP servlet request resembling the requests that are typically wrapped by XsrfAwareRequest
     * instances.
     * The request returned by this method has these characteristics:
     * <ul>
     *     <li>Its {@code getParameterMap} method returns a mapping like this:
     *          <ul>
     *              <li>{@code "a" --> "foo"}</li>
     *              <li>{@code "b" --> "bar"}</li>
     *              <li>{@code <T> --> "tzu"}</li>
     *          </ul>
     *          ... where {@code <T>} is the cross-platform parameter name
     *              ({@link XsrfAwareRequest#CROSS_PRODUCT_TOKEN_PARAM})
     *     </li>
     *     <li>Its {@code getParameterNames} method returns the names in the above map</li>
     *     <li>Its {@code getParameter} method returns {@code String} values as per the above map</li>
     *     <li>Its {@code getParameterNames} method returns {@code String[]} values as per the above map</li>
     * </ul>
     *
     * @return a mock HTTP servlet request
     */
    private static HttpServletRequest mockReq() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getParameterNames()).thenReturn(Collections.enumeration(sampleParamNames()));
        when(req.getParameterMap()).thenReturn(sampleParamMap());
        when(req.getParameter("a")).thenReturn("foo");
        when(req.getParameter("b")).thenReturn("bar");
        when(req.getParameter(XsrfAwareRequest.CROSS_PRODUCT_TOKEN_PARAM)).thenReturn("tzu");
        when(req.getParameterValues("a")).thenReturn(new String[] {"foo"});
        when(req.getParameterValues("b")).thenReturn(new String[] {"bar"});
        when(req.getParameterValues(XsrfAwareRequest.CROSS_PRODUCT_TOKEN_PARAM)).thenReturn(new String[] {"tzu"});
        return req;
    }

    private static HttpServletRequest mockReqWithShadow() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getParameterNames()).thenReturn(Collections.enumeration(sampleParamNamesWithShadow()));
        when(req.getParameterMap()).thenReturn(sampleParamMapWithShadow());
        when(req.getParameter("a")).thenReturn("foo");
        when(req.getParameter("b")).thenReturn("bar");
        when(req.getParameter(XsrfAwareRequest.CROSS_PRODUCT_TOKEN_PARAM)).thenReturn("tzu");
        when(req.getParameter("shadow")).thenReturn("abc");
        when(req.getParameterValues("a")).thenReturn(new String[] {"foo"});
        when(req.getParameterValues("b")).thenReturn(new String[] {"bar"});
        when(req.getParameterValues(XsrfAwareRequest.CROSS_PRODUCT_TOKEN_PARAM)).thenReturn(new String[] {"tzu"});
        when(req.getParameterValues("shadow")).thenReturn(new String[] {"abc"});
        return req;
    }

    private static List<String> sampleParamNames() {
        return Arrays.asList("a", "b", XsrfAwareRequest.CROSS_PRODUCT_TOKEN_PARAM);
    }

    private static List<String> sampleParamNamesWithShadow() {
        List<String> names = new ArrayList<String>(sampleParamNames());
        names.add("shadow");
        return names;
    }

    private static Map<String, String[]> sampleParamMap() {
        final Map<String, String[]> paramMap = new HashMap<String, String[]>();
        paramMap.put("a", new String[] {"foo"});
        paramMap.put("b", new String[] {"bar"});
        paramMap.put(XsrfAwareRequest.CROSS_PRODUCT_TOKEN_PARAM, new String[] {"tzu"});
        return paramMap;
    }

    private static Map<String, String[]> sampleParamMapWithShadow() {
        final Map<String, String[]> paramMap = new HashMap<String, String[]>(sampleParamMap());
        paramMap.put("shadow", new String[] {"abc"});
        return paramMap;
    }
}
