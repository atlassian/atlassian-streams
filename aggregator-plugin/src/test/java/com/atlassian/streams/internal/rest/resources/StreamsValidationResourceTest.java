package com.atlassian.streams.internal.rest.resources;

import javax.ws.rs.core.Response;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.streams.internal.ProjectKeyValidator;
import com.atlassian.streams.internal.rest.representations.ValidationErrorCollectionRepresentation;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StreamsValidationResourceTest {
    private static final String VALID_KEY = "validKey";
    private static final String INVALID_KEY = "invalidKey";

    @Mock
    private ProjectKeyValidator validator;

    @InjectMocks
    private StreamsValidationResource streamsValidationResource;

    @Before
    public void setUp() {
        when(validator.allKeysAreValid(ImmutableList.of(VALID_KEY), false)).thenReturn(true);
    }

    @Test
    public void assertThatValidateReturnsOkWhenOneKeyProviderAcceptsKey() {
        assertThat(
                streamsValidationResource
                        .validate("title", VALID_KEY, "user", "1", false)
                        .getStatus(),
                is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatValidateReturnsErrorWhenNoKeyProviderAcceptsKey() {
        final Response response = streamsValidationResource.validate("title", INVALID_KEY, "user", "1", false);
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
        final Object entity = response.getEntity();
        assertThat(entity, instanceOf(ValidationErrorCollectionRepresentation.class));
        final ValidationErrorCollectionRepresentation errorCollection =
                (ValidationErrorCollectionRepresentation) entity;
        assertThat(errorCollection.getErrors(), hasItem(errorInField("keys")));
    }

    Matcher<ValidationErrorCollectionRepresentation.ValidationErrorEntry> errorInField(final String field) {
        return new FeatureMatcher<ValidationErrorCollectionRepresentation.ValidationErrorEntry, String>(
                is(field), "an error whose field is", "field") {
            @Override
            protected String featureValueOf(final ValidationErrorCollectionRepresentation.ValidationErrorEntry error) {
                return error.getField();
            }
        };
    }

    @Test
    public void assertThatValidateReturnsOkForUnknownUser() {
        assertThat(
                streamsValidationResource
                        .validate("title", VALID_KEY, "unknownuser", "1", false)
                        .getStatus(),
                is(equalTo(SC_OK)));
    }
}
