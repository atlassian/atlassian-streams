package com.atlassian.streams.internal;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CallThrottlerTest {
    private final AtomicReference<Instant> clock = new AtomicReference<>(Instant.now());
    private static final int allowedWallClockPercentage = 10;
    private final Duration timeWindow = Duration.ofSeconds(1);
    private final CallThrottler callThrottler = new CallThrottler(clock::get, timeWindow, allowedWallClockPercentage);

    @Test
    public void budgetNotExceededWhenUnderThreshold() {
        runCalls(Runtime.getRuntime().availableProcessors());
        assertFalse(callThrottler.isBudgetExceeded());
    }

    @Test
    public void retrievesStats() {
        Stream.of("a", "b", "c", "a").forEach(callThrottler::startTracking);

        final Duration elapsedTime = Duration.ofMillis(100);
        clock.getAndUpdate(c -> c.plus(elapsedTime));

        {
            final Map<String, Duration> stats = callThrottler.getStats();
            assertThat(
                    stats,
                    equalTo(ImmutableMap.of(
                            "a", elapsedTime.multipliedBy(2),
                            "b", elapsedTime,
                            "c", elapsedTime)));
        }

        callThrottler.startTracking("c");
        assertThat(callThrottler.getStats().get("c"), equalTo(elapsedTime));
        clock.getAndUpdate(c -> c.plus(elapsedTime));
        {
            final Map<String, Duration> stats = callThrottler.getStats();
            assertThat(
                    stats,
                    equalTo(ImmutableMap.of(
                            "a", elapsedTime.multipliedBy(2).multipliedBy(2),
                            "b", elapsedTime.plus(elapsedTime),
                            "c", elapsedTime.plus(elapsedTime.multipliedBy(2)))));
        }
    }

    @Test
    public void budgetExceededWhenOverThreshold() {
        runCalls(Runtime.getRuntime().availableProcessors() + 1);
        assertTrue(callThrottler.isBudgetExceeded());
    }

    @Test
    public void recoversAfterBudgetExceeded() {
        // the extra +1 call will go over the budget
        runCalls(Runtime.getRuntime().availableProcessors() + 1);
        assertTrue(callThrottler.isBudgetExceeded());

        clock.getAndUpdate(c -> c.plus(timeWindow));
        assertFalse(callThrottler.isBudgetExceeded());

        runCalls(Runtime.getRuntime().availableProcessors());
        assertFalse(callThrottler.isBudgetExceeded());
    }

    private void runCalls(final int count) {
        // this call duration, when repeated num processor times, should put us right under the throttling threshold
        final Duration callDuration = Duration.ofMillis(timeWindow.toMillis() * 10 / 100 - 1);

        final List<CallThrottler.TrackedCall> calls = IntStream.range(0, count)
                .mapToObj(i -> callThrottler.startTracking("context"))
                .collect(Collectors.toList());

        clock.getAndUpdate(c -> c.plus(callDuration));

        calls.forEach(CallThrottler.TrackedCall::close);
    }
}
