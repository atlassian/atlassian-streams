package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import com.google.common.collect.ImmutableList;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;

import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.changeset;
import static com.atlassian.streams.testing.Crucible.Data.review1Created;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.FishEye.Data.changeSet2;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.Jira.setUserLanguage;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasProviderFilter;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withProviderKey;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withType;

@Ignore("FeCru is not compatible with Platform 5")
@TestGroups({APPLINKS, APPLINKS_OAUTH})
public class JiraToFeCruApplinksTest extends JiraApplinksTest {
    protected static String fisheye;
    protected static String crucible;

    @BeforeClass
    public static void setUpFeCruProviderKeys() {
        fisheye = getProviderKeyStartingWith(streamsConfigRepresentation, "source");
        crucible = getProviderKeyStartingWith(streamsConfigRepresentation, "reviews");
    }

    @Test
    public void shouldContainFisheyeActivityInFeed() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(fisheye));

        assertThat(feed.getEntries(), hasEntry(projectOneChangeSet2()));
    }

    @Test
    public void shouldContainCrucibleActivityInFeed() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(crucible));

        assertThat(feed.getEntries(), hasEntry(createdReviewOne1()));
    }

    @Test
    public void shouldContainOnlyEntriesFromCrucibleProviderInFeedWhenCrucibleProviderIsSpecified() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(crucible));
        assertThat(feed.getEntries(), hasEntry(createdReviewOne1()));

        assertThat(
                feed.getEntries(),
                allEntries(allOf(
                        haveAtlassianApplicationElement(containsString("com.atlassian.fisheye")),
                        not(withActivityObjectElement(
                                withType(equalTo(changeset().iri().toASCIIString())))))));
    }

    @Test
    public void shouldOnlyEntriesFromFisheyeRepositoryOneBeInFeedWhenOneKeyIsSpecified() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(IS, "ONE"), provider(fisheye));
        assertThat(
                feed.getEntries(),
                allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                        hasEntry(projectOneChangeSet2()), not(hasEntry(changeSet2())))));
    }

    @Test
    public void shouldContainOnlyEntriesFromCrucibleProjectCrOneInFeedWhenOneKeyIsSpecified() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(IS, "ONE"), provider(crucible));

        assertThat(
                feed.getEntries(),
                allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                        hasEntry(createdReviewOne1()), not(hasEntry(review1Created())))));
    }

    @Test
    public void shouldNotContainEntriesFromFisheyeRepositoryOneInFeedWhenOneKeyIsFiltered() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(NOT, "ONE"), provider(fisheye));
        assertThat(feed.getEntries(), not(hasEntry(projectOneChangeSet2())));
    }

    @Test
    public void shouldContainEntriesFromFisheyeRepositoryTstInFeedWhenOneKeyIsFiltered() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(NOT, "ONE"), provider(fisheye));
        assertThat(feed.getEntries(), hasEntry(changeSet2()));
    }

    @Test
    public void shouldNotContainEntriesFromCrucibleProjectCrOneInFeedWhenOneKeyIsFiltered() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(NOT, "ONE"), provider(crucible));
        assertThat(
                feed.getEntries(),
                allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                        not(hasEntry(createdReviewOne1())), hasEntry(review1Created()))));
    }

    @Test
    public void shouldContainAppLinksProviderFiltersFromFisheyeInFilterResource() {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksFisheyeFilters());
    }

    @Test
    public void shouldContainAppLinksProviderFiltersFromCrucibleInFilterResource() {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksCrucibleFilters());
    }

    //////////

    @Test
    public void shouldRetrievingFeedFromDisabledFeCruUserNotReturnPrivateCrucibleEntry() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(not(hasEntry(createdReviewOne1())), hasEntry(createdOne1())));
    }

    @Test
    public void shouldRetrievingFeedFromDisabledFeCruUserReturnPublicCrucibleEntry() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), hasEntry(review1Created()));
    }

    @Test
    public void shouldRetrievingFeedFromDisabledFeCruUserNotReturnPrivateFishEyeEntry() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(not(hasEntry(projectTst2ChangeSet1())), hasEntry(createdOne1())));
    }

    @Test
    public void shouldRetrievingFeedFromDisabledFeCruUserReturnPublicFishEyeEntry() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), hasEntry(projectOneChangeSet2()));
    }

    @Test
    public void shouldRetrievingFeedFromFeCruDoesntIncludeXssVulnerabilities() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

        assertThat(
                feed.getEntries(),
                hasEntry(withTitle(containsString("&lt;script&gt;alert('fecru xss');&lt;/script&gt;"))));
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void shouldBlogCreatedEntryBeValidInFrench() {
        uiTester.logInAs(ADMIN_USER);
        try {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(blogInFrench()));
        } finally {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void shouldCommentedIssueEntryBeValidInFrench() {
        uiTester.logInAs(ADMIN_USER);
        try {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(commentedIssueInFrench()));
        } finally {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void shouldCreatedIssueEntryBeValidInFrench() {
        uiTester.logInAs(ADMIN_USER);
        try {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(createdIssueInFrench()));
        } finally {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void shouldPageEditedEntryBeValidInFrench() {
        uiTester.logInAs(ADMIN_USER);
        try {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(editedInFrench()));
        } finally {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void shouldJiraIgnoreAcceptLanguageHeader() {
        Feed feed = client.getAs(ADMIN_USER, "fr-FR, fr;q=0.9", maxResults(MAX_RESULTS));

        assertThat(
                feed.getEntries(),
                allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                        not(hasEntry(blogInFrench())),
                        not(hasEntry(commentedIssueInFrench())),
                        not(hasEntry(createdIssueInFrench())),
                        not(hasEntry(editedInFrench())))));
    }

    // STRM-2339; call JIRA with a user that exists in in JIRA but not in FeCru - should omit content that's anonymously
    // accessible in FeCru
    @Test
    public void shouldNotExposeNonLocalAnonymousContentToNonUser() {
        Feed feed = client.getAs("jirauser", maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntry(createdReview("CR-1"))));
        assertThat(feed.getEntries(), hasEntry(withTitle(containsString("ONE-2"))));
    }

    // STRM-2339; call JIRA with an anonymous user - should omit content that's anonymously accessible in FeCru
    @Test
    public void shouldNotExposeNonRemoteAnonymousContentToAnonymous() {
        Feed feed = client.get(maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntry(createdReview("CR-1"))));
    }

    // STRM-2339: valid case, user exists on both, should see item
    @Test
    public void shouldShowRemoteContentToValidUser() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), hasEntry(createdReview("CR-1")));
    }

    @Test
    public void shouldWhitelistUrlProxyResourceToAccessFeCru() {
        assertThat(
                restTester.getProxiedResponse("http://localhost:3990/streams/").getStatus(),
                anyOf(equalTo(OK.getStatusCode()), equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
    }

    @Test
    public void shouldUsingAdminUserFilterNotReturnBuildEntriesPerformedByRwallace() {
        Feed feed = client.get(user(IS, ADMIN_USER), maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntries(withAuthorElement(equalTo("rwallace")))));
    }

    /*
     * Fecru Matchers
     */

    protected final Matcher<? super Entry> blogInFrench() {
        return withTitle(containsString("le blogue"));
    }

    protected final Matcher<? super Entry> commentedIssueInFrench() {
        return withTitle(containsString("comment\u00e9"));
    }

    protected final Matcher<? super Entry> createdIssueInFrench() {
        return withTitle(containsString("cr\u00e9\u00e9"));
    }

    protected final Matcher<? super Entry> editedInFrench() {
        return withTitle(containsString("ajout\u00e9"));
    }

    protected final Matcher<? super Entry> projectOneChangeSet2() {
        return withTitle(allOf(
                containsString("rwallace"),
                containsString("committed"),
                containsString("cs=2"),
                containsString("Project ONE")));
    }

    protected final Matcher<? super Entry> projectTst2ChangeSet1() {
        return withTitle(allOf(
                containsString("pkaeding"),
                containsString("committed"),
                containsString("cs=1"),
                containsString("TST2")));
    }

    protected final Matcher<? super Entry> createdReviewOne1() {
        return createdReview("CR-ONE-1");
    }

    protected final Matcher<? super Entry> createdReview(final String reviewKey) {
        return withTitle(allOf(containsString("created"), containsString("review"), containsString(reviewKey)));
    }

    private Matcher<? super StreamsConfigRepresentation> hasAppLinksFisheyeFilters() {
        return hasProviderFilter(withProviderKey(startsWith("source")));
    }

    protected final Matcher<? super StreamsConfigRepresentation> hasAppLinksCrucibleFilters() {
        return hasProviderFilter(withProviderKey(startsWith("reviews")));
    }
}
