package it.com.atlassian.streams.applinks.fisheye.external;

import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import it.com.atlassian.streams.applinks.FecruApplinksTest;

import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.fisheye.external.activity.ExternalActivityItemFactory;
import com.atlassian.streams.fisheye.external.activity.RomeExternalActivityItemFactory;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;
import com.atlassian.streams.testing.FeedClient;

import static it.com.atlassian.streams.applinks.fisheye.external.ExternalActivityItemMatchers.hasActivityItem;
import static it.com.atlassian.streams.applinks.fisheye.external.ExternalActivityItemMatchers.withSummary;
import static it.com.atlassian.streams.applinks.fisheye.external.ExternalActivityItemMatchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.CombinableMatcher.CombinableBothMatcher;

import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_STREAMS3;

@RunWith(FecruApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_STREAMS3})
@Ignore("FeCru is not compatible with Platform 5")
public class ExternalActivityItemApplinkedRestTest {
    /** This needs to be high enough so all the content from each application will be in the stream **/
    static final int MAX_RESULTS = 500;

    static final ExternalActivityItemFactory itemFactory = new RomeExternalActivityItemFactory();

    @Inject
    static FeedClient feedClient;

    @Test
    public void assertThatFisheyeActivityIsInFeed() {
        Iterable<ExternalActivityItem> items = getItems(maxResults(MAX_RESULTS));
        assertThat(items, hasActivityItem(projectOneChangeSet2()));
    }

    @Test
    public void assertThatCrucibleActivityIsInFeed() {
        Iterable<ExternalActivityItem> items = getItems(maxResults(MAX_RESULTS));
        assertThat(items, hasActivityItem(createdReviewOne1()));
    }

    @Test
    public void assertThatJiraActivityIsInFeed() {
        Iterable<ExternalActivityItem> items = getItems(maxResults(MAX_RESULTS));
        assertThat(items, hasActivityItem(createdOne1()));
    }

    @Test
    public void assertThatFisheyeActivityProvidesJiraIssueLinks() {
        Iterable<ExternalActivityItem> items = getItems(maxResults(MAX_RESULTS));

        // JIRA Issue type string can be found and linked. NB Requires JIRA to allow remote API calls.
        assertThat(items, hasActivityItem(projectOneChangeSet5()));

        // JIRA Issue type string can be found but NOT linked
        assertThat(items, hasActivityItem(testRepositoryChangeSet2()));
    }

    @Test
    public void assertThatFisheyeActivityProvidesMultipleJiraIssueLinks() {
        Iterable<ExternalActivityItem> items = getItems(maxResults(MAX_RESULTS));

        // JIRA Issue type strings can be found and linked. NB Requires JIRA to allow remote API calls.
        assertThat(items, hasActivityItem(projectOneChangeSet6()));
    }

    private static Iterable<ExternalActivityItem> getItems(Parameter... parameters) {
        return itemFactory.getItems(feedClient.getAs("admin", String.class, parameters));
    }

    private static Matcher<ExternalActivityItem> projectOneChangeSet2() {
        return withTitle(allOf(containsString("committed"), containsString("cs=2"), containsString("Project ONE")));
    }

    private static Matcher<ExternalActivityItem> projectOneChangeSet5() {
        return allOf(
                withTitle(allOf(containsString("committed"), containsString("cs=5"), containsString("Project ONE"))),
                withSummary(containsString("/streams/action/jira-issue.do?key=ONE-2\">ONE-2</a>")));
    }

    private static Matcher<ExternalActivityItem> projectOneChangeSet6() {
        return allOf(
                withTitle(allOf(containsString("committed"), containsString("cs=6"), containsString("Project ONE"))),
                withSummary(allOf(
                        containsString("/streams/action/jira-issue.do?key=ONE-2\">ONE-2</a>"),
                        containsString("/streams/action/jira-issue.do?key=ONE-1\">ONE-1</a>"))));
    }

    private static Matcher<ExternalActivityItem> testRepositoryChangeSet2() {
        return allOf(
                withTitle(
                        allOf(containsString("committed"), containsString("cs=2"), containsString("Test Repository"))),
                withSummary(containsString("TST-1")));
    }

    private static Matcher<ExternalActivityItem> createdReviewOne1() {
        return withTitle(allOf(containsString("created"), containsString("review"), containsString("CR-ONE-1")));
    }

    private static Matcher<ExternalActivityItem> createdOne1() {
        CombinableBothMatcher<String> containsCreated = both(containsString("created"));
        return withTitle(containsCreated.and(containsString("ONE-1")));
    }
}
