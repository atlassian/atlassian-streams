package it.com.atlassian.streams.jira;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/custom-field.xml")
public class CustomFieldUpdateTest {
    @Inject
    static FeedClient client;

    @Test
    public void assertThatFeedContainsUpdateOfCustomField() throws Exception {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-1"));
        assertThat(
                feed.getEntries(),
                hasEntry(withTitle(allOf(
                        containsString("updated the Status"),
                        not(containsString("This is a custom status field")),
                        containsString("ONE-1")))));
    }

    @Test
    public void assertThatFeedContainsIssueResolvedEntry() throws Exception {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(containsString("resolved"), containsString("ONE-1")))));
    }
}
