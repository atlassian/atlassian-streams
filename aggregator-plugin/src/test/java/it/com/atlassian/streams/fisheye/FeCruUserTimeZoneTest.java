package it.com.atlassian.streams.fisheye;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.matchesRegEx;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class FeCruUserTimeZoneTest {
    @Inject
    static ApplicationProperties applicationProperties;

    @Test
    public void assertThatHongKongUserTimeZoneIsCorrect() {
        RestTester userRestTester = new RestTester(applicationProperties, "hongkong_user", "hongkong_user");
        ConfigPreferencesRepresentation prefs = userRestTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeZone(), is(equalTo("+0800")));
    }

    @Test
    public void assertThatUserTimeZoneIsCorrectWhenUsingADifferentUser() {
        RestTester userRestTester = new RestTester(applicationProperties, "oburn", "oburn");
        ConfigPreferencesRepresentation prefs = userRestTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeZone(), is(equalTo("-0400")));
    }

    @Test
    public void assertThatNotNullUserTimeZoneIsReturnedWhenThereIsNoLoggedInUser() {
        RestTester anonRestTester = new RestTester(applicationProperties, false);
        ConfigPreferencesRepresentation prefs = anonRestTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeZone(), matchesRegEx("[+-]\\d{4}"));
    }
}
