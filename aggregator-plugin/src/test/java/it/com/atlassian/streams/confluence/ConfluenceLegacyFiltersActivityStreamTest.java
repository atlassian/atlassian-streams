package it.com.atlassian.streams.confluence;

import java.io.IOException;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.LegacyFeedClient;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;

import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.Confluence.MY_FIRST_BLOG;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.LegacyFeedClient.itemKey;
import static com.atlassian.streams.testing.LegacyFeedClient.key;
import static com.atlassian.streams.testing.LegacyFeedClient.maxDate;
import static com.atlassian.streams.testing.LegacyFeedClient.minDate;
import static com.atlassian.streams.testing.LegacyFeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasTarget;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withId;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/base.zip")
public class ConfluenceLegacyFiltersActivityStreamTest {
    @Inject
    static LegacyFeedClient feedClient;

    @Inject
    static RestTester restTester;

    @Test
    public void assertThatFeedWithAuthorFilterOnlyContainsEntriesFromThatAuthor() throws Exception {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), user("admin"));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("admin"))));
    }

    @Test
    public void assertThatFeedWithSpaceFilterOnlyContainsEntriesFromThatSpace() throws Exception {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), key("newspace"));
        assertThat(
                feed.getEntries(),
                allEntries(anyOf(
                        withTitle(containsString("newspace")),
                        withContent(containsString("New Space")),
                        hasTarget(withId(containsString("newspace"))))));
    }

    @Test
    public void assertThatFeedWithDateBeforeAndAfterParametersFindsEntriesInMiddle() throws IOException {
        // entry will exist twice due to confluence 4.0 migration's revision
        Feed feed = feedClient.getAs(
                "admin",
                provider(CONFLUENCE_PROVIDER),
                maxDate(MY_FIRST_BLOG.plusSeconds(1)),
                minDate(MY_FIRST_BLOG.minusSeconds(1)));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("My First Blog"))));
    }

    @Test
    public void assertThatWithLabelParametersFindsCorrectEntries() throws IOException {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), itemKey("homepage"));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("Home"))));
    }
}
