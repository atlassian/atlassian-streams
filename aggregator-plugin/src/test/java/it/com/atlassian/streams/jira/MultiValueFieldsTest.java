package it.com.atlassian.streams.jira;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/edit-multi-value-fields.xml")
public class MultiValueFieldsTest {
    @Inject
    static FeedClient client;

    public static Matcher<? super Entry> issueOne3() {
        return withTitle(allOf(containsString("ONE-3"), containsString("Gee, Your Hair Smells Terrific")));
    }

    @Test
    public void assertThatAddingSingleComponentShowsValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(), withTitle(allOf(containsString("added the Component"), containsString("JIRA"))))));
    }

    @Test
    public void assertThatAddingSingleAffectsVersionShowsValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("added the Affects Version"), containsString("1.0"))))));
    }

    @Test
    public void assertThatAddingSingleFixVersionShowsValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("added the Fix Version"), containsString("1.2"))))));
    }

    @Test
    public void assertThatAddingSingleComponentHasValueWithLinkInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(
                                containsString("added the Component"),
                                containsString("JIRA"),
                                containsString(client.getBaseUrl() + "/browse/ONE/component/10000"))))));
    }

    @Test
    public void assertThatAddingSingleAffectsVersionHasValueWithLinkInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(
                                containsString("added the Affects Version"),
                                containsString("1.0"),
                                containsString(client.getBaseUrl() + "/browse/ONE/fixforversion/10000"))))));
    }

    @Test
    public void assertThatAddingSingleFixVersionHasValueWithLinkInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(
                                containsString("added the Fix Version"),
                                containsString("1.2"),
                                containsString(client.getBaseUrl() + "/browse/ONE/fixforversion/10002"))))));
    }

    @Test
    public void assertThatAddingMultipleComponentShowsFieldNameUpdatedWithoutValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("updated the Component"), not(containsString("Bamboo")))))));
    }

    @Test
    public void assertThatAddingMultipleAffectsVersionShowsFieldNameUpdatedWithoutValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("updated the Affects Version"), not(containsString("2.0")))))));
    }

    @Test
    public void assertThatAddingMultipleFixVersionShowsFieldNameUpdatedWithoutValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("updated the Fix Version"), not(containsString("2.1")))))));
    }

    @Test
    public void assertThatAddingMultipleComponentShowsValueWithLinkInContent() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withContent(allOf(
                                containsString("Added the Component"),
                                containsString("Bamboo"),
                                containsString("Confluence"),
                                containsString(client.getBaseUrl() + "/browse/ONE/component/10001"),
                                containsString(client.getBaseUrl() + "/browse/ONE/component/10002"))))));
    }

    @Test
    public void assertThatAddingMultipleAffectsVersionHasValueWithLinkInContent() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), maxResults(20));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withContent(allOf(
                                containsString("Added the Affects Version"),
                                containsString("2.0"),
                                containsString("3.0"),
                                containsString(client.getBaseUrl() + "/browse/ONE/fixforversion/10003"),
                                containsString(client.getBaseUrl() + "/browse/ONE/fixforversion/10005"))))));
    }

    @Test
    public void assertThatAddingMultipleFixVersionHasValueWithLinkInContent() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withContent(allOf(
                                containsString("Added the Fix Version"),
                                containsString("2.1"),
                                containsString("3.2"),
                                containsString(client.getBaseUrl() + "/browse/ONE/fixforversion/10004"),
                                containsString(client.getBaseUrl() + "/browse/ONE/fixforversion/10007"))))));
    }

    @Test
    public void assertThatAddingAndRemovingMultipleComponentShowsValueInContent() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withContent(allOf(
                                containsString("Added the Component"),
                                containsString("Removed the Component"),
                                containsString("Bamboo"),
                                containsString("JIRA"),
                                containsString("FeCru"))))));
    }

    @Test
    public void assertThatAddingAndRemovingMultipleAffectsVersionHasValueInContent() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withContent(allOf(
                                containsString("Added the Affects Version"),
                                containsString("Removed the Affects Version"),
                                containsString("1.0"),
                                containsString("2.0"),
                                containsString("3.1"))))));
    }

    @Test
    public void assertThatAddingAndRemovingMultipleFixVersionHasValueInContent() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withContent(allOf(
                                containsString("Added the Fix Version"),
                                containsString("Removed the Fix Version"),
                                containsString("2.0"),
                                containsString("2.1"),
                                containsString("1.2"))))));
    }

    @Test
    public void assertThatRemovingSingleComponentShowsValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("removed the Component"), containsString("FeCru"))))));
    }

    @Test
    public void assertThatRemovingSingleAffectsVersionShowsValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("removed the Affects Version"), containsString("3.0"))))));
    }

    @Test
    public void assertThatRemovingSingleFixVersionShowsValueInTitle() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("removed the Fix Version"), containsString("2.0"))))));
    }
}
