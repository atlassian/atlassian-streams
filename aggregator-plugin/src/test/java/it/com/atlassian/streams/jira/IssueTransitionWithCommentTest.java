package it.com.atlassian.streams.jira;

import java.io.IOException;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.atlassian.streams.jira.JiraActivityVerbs.reopen;
import static com.atlassian.streams.jira.JiraActivityVerbs.resolve;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.Jira.activities;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/issue-transitioned-with-comments.xml")
public class IssueTransitionWithCommentTest {
    @Inject
    static FeedClient client;

    @Test
    public void assertThatResolvedIssueHasComment() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), resolve())));
        assertThat(
                feed.getEntries(),
                hasEntry(
                        withTitle(allOf(containsString("resolved"), containsString("ONE-1"))),
                        withContent(containsString("I won't be fixing this issue!"))));
    }

    @Test
    public void assertThatReopenedIssueHasComment() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), reopen())));
        assertThat(
                feed.getEntries(),
                hasEntry(
                        withTitle(allOf(containsString("reopened"), containsString("ONE-1"))),
                        withContent(containsString("You should fix this issue!"))));
    }
}
