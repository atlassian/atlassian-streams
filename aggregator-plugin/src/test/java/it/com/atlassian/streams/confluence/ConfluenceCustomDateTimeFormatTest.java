package it.com.atlassian.streams.confluence;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/custom-datetime-formats.zip")
public class ConfluenceCustomDateTimeFormatTest {
    @Inject
    static RestTester restTester;

    @Test
    public void assertThatTheCustomDateFormatIsCorrect() {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateFormat(), is(equalTo("'new date format'")));
    }

    @Test
    public void assertThatTheCustomTimeFormatIsCorrect() {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeFormat(), is(equalTo("'new time format'")));
    }

    @Test
    public void assertThatTheCustomDateTimeFormatIsCorrect() {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateTimeFormat(), is(equalTo("'new datetime format'")));
    }
}
