package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.model.InitializationError;

import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.AppLinksTestRunner;

import static java.util.Arrays.asList;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.Jira.Data.createdTwo1;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasProviderFilter;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withProviderKey;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;

public abstract class JiraApplinksTest extends AppLinksTests {
    public static final class Runner extends AppLinksTestRunner {
        public Runner(Class<?> klass) throws InitializationError {
            super(klass, "jira");
        }
    }

    protected static final String JIRA_APPLICATION = "com.atlassian.jira";
    protected static String jira;

    @BeforeClass
    public static void setUpJiraProviderKey() {
        jira = getProviderKeyStartingWith(streamsConfigRepresentation, "issues");
    }

    // STRM-1739 - proxy is now forbidden for local requests
    @Test
    public void shouldNotWhitelistUrlProxyResourceToAccessJira() {
        assertThat(
                restTester.getProxiedResponse("http://localhost:2990/streams/").getStatus(),
                equalTo(HttpServletResponse.SC_FORBIDDEN));
    }

    @Test
    @Override
    public void shouldSortFilterProviders() {
        shouldSortFilterProvidersWithLocalProvidersFirst("Jira", "Third Parties");
    }

    @Test
    public void shouldContainJiraActivityInFeed() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(jira));

        assertThat(feed.getEntries(), hasEntry(createdOne1()));
    }

    @Test
    public void shouldContainOnlyEntriesFromJiraProviderInFeedWhenJiraProviderIsSpecified() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(jira));

        assertThat(feed.getEntries(), hasEntry(createdOne1()));
        assertThat(feed.getEntries(), allEntries(haveAtlassianApplicationElement(containsString(JIRA_APPLICATION))));
    }

    @Test
    public void shouldContainOnlyEntriesFromJiraProjectOneInFeedWhenOneKeyIsSpecified() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(IS, "ONE"), provider(jira));

        assertThat(feed.getEntries(), allOf(asList(hasEntry(createdOne1()), not(hasEntry(createdTwo1())))));
    }

    @Test
    public void shouldContainAppLinksProviderFiltersFromJiraInFilterResource() {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksJiraFilters());
    }

    @Test
    @Ignore("STRM-1739 - proxy is now forbidden for local requests")
    public void shouldWhitelistUrlProxyResourceToAccessJira() {
        assertThat(
                restTester.getProxiedResponse("http://localhost:2990/streams/").getStatus(),
                anyOf(equalTo(OK.getStatusCode()), equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
    }

    private Matcher<? super StreamsConfigRepresentation> hasAppLinksJiraFilters() {
        return hasProviderFilter(withProviderKey(startsWith("issues")));
    }
}
