package it.com.atlassian.streams.jira;

import java.io.IOException;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.atlassian.streams.jira.JiraActivityVerbs.close;
import static com.atlassian.streams.jira.JiraActivityVerbs.open;
import static com.atlassian.streams.jira.JiraActivityVerbs.reopen;
import static com.atlassian.streams.jira.JiraActivityVerbs.resolve;
import static com.atlassian.streams.jira.JiraActivityVerbs.start;
import static com.atlassian.streams.jira.JiraActivityVerbs.stop;
import static com.atlassian.streams.jira.JiraActivityVerbs.transition;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.Jira.activities;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withCategory;
import static com.atlassian.streams.testing.matchers.Matchers.withTerm;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/issue-transitioned.xml")
public class IssueTransitionTest {
    @Inject
    static FeedClient client;

    @Test
    public void assertThatOne1EntryHasTransitionVerbWhenIssueStatusIsChanged() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(
                feed.getEntries(),
                hasEntry(withVerbElement(equalTo(transition().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedContainsStartVerbForStartingProgressOnAnIssue() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), start())));
        assertThat(
                feed.getEntries(),
                hasEntry(withVerbElement(equalTo(start().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedContainsStopVerbForStoppingProgressOnAnIssue() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), stop())));
        assertThat(
                feed.getEntries(), hasEntry(withVerbElement(equalTo(stop().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedContainsResolveVerbForResolvingAnIssue() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), resolve())));
        assertThat(
                feed.getEntries(),
                hasEntry(withVerbElement(equalTo(resolve().iri().toASCIIString()))));
    }

    @Test
    public void assertThatResolvedIssueEntryDoesNotHaveResolveLinkStyleClassIfIssueIsStillOpen() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), resolve())));
        assertThat(
                feed.getEntries(),
                hasEntry(withTitle(allOf(
                        containsString("resolved"), containsString("ONE-1"), not(containsString("resolved-link"))))));
    }

    @Test
    public void assertThatResolvedIssueEntryHasResolution() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), resolve())));
        assertThat(
                feed.getEntries(),
                hasEntry(
                        withTitle(containsString("resolved")),
                        withTitle(containsString("ONE-1")),
                        withTitle(containsString("as 'Fixed'"))));
    }

    @Test
    public void assertThatNonResolvedIssueEntryDoesNotHaveResolveLinkStyleClass() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), start())));
        assertThat(
                feed.getEntries(),
                hasEntry(withTitle(containsString("ONE-1")), not(withTitle(containsString("resolved-link")))));
    }

    @Test
    public void assertThatFeedContainsReopenVerbForReopeningAnIssue() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), reopen())));
        assertThat(
                feed.getEntries(),
                hasEntry(withVerbElement(equalTo(reopen().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedContainsCloseVerbForClosingAnIssue() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), close())));
        assertThat(
                feed.getEntries(),
                hasEntry(withVerbElement(equalTo(close().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedContainsOpenVerbForOpeningAnIssue() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-3"), activities(IS, pair(issue(), open())));
        assertThat(
                feed.getEntries(), hasEntry(withVerbElement(equalTo(open().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedContainsTransitionVerbForIssuesUpdatedToCustomTransition() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                hasEntry(withVerbElement(equalTo(transition().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedContainsEntriesWithStatusChangedToCustomTransition()
            throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(feed.getEntries(), hasEntries(withTitle(containsString("changed the status to Under Review"))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedDoesNotContainEntriesWithStartVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                        withVerbElement(equalTo(start().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedDoesNotContainEntriesWithStopVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                        withVerbElement(equalTo(stop().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedDoesNotContainEntriesWithResolveVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                        withVerbElement(equalTo(resolve().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedDoesNotContainEntriesWithReopenVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                        withVerbElement(equalTo(reopen().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedDoesNotContainEntriesWithCloseVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                        withVerbElement(equalTo(close().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFilteringByIssuesTransitionedDoesNotContainEntriesWithOpenVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), activities(IS, pair(issue(), transition())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                        withVerbElement(equalTo(open().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFeedWithFilterNotIssuesClosedDoesNotContainEntriesWithCloseVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(NOT, pair(issue(), close())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(withVerbElement(equalTo(close().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedWithFilterNotIssuesProgressStartedDoesNotContainEntriesWithStartVerb()
            throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(NOT, pair(issue(), start())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(withVerbElement(equalTo(start().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedWithFilterNotIssuesProgressStoppedDoesNotContainEntriesWithStopVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(NOT, pair(issue(), stop())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(withVerbElement(equalTo(stop().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedWithFilterNotIssuesReopenedDoesNotContainEntriesWithReopenVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(NOT, pair(issue(), reopen())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(withVerbElement(equalTo(reopen().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedWithFilterNotIssuesResolvedDoesNotContainEntriesWithResolveVerb() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(NOT, pair(issue(), resolve())));
        assertThat(
                feed.getEntries(),
                not(hasEntry(withVerbElement(equalTo(resolve().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedWithCustomTransitionHasTransitionNameAsCategoryTerm() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(withCategory(withTerm(equalTo("Under Review")))));
    }

    @Test
    public void assertThatFeedWithCustomTransitionWithResolutionHasResolutionInTitle() throws IOException {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-2"));
        assertThat(
                feed.getEntries(),
                hasEntry(withTitle(allOf(
                        containsString("changed the status to Done"),
                        containsString("with a resolution of 'Fixed'")))));
    }
}
