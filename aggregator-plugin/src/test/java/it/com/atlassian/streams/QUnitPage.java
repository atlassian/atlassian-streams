package it.com.atlassian.streams;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import com.google.common.collect.Iterables;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

/**
 * Pageobject for handling QUnit results. Streams QUnit tests require the product to be running and use a plugin (js-tests-plugin)
 * to expose the results. This page object will wait until the tests are finished (tested by verifying the state of the
 * qunit banner). Afterwards the results can be obtained by calling {@link QUnitPage#getQUnitResults()}.
 *
 */
public class QUnitPage implements Page {

    @ElementBy(id = "qunit-banner")
    protected PageElement qunitBanner;

    @ElementBy(cssSelector = "#qunit-tests > li")
    protected Iterable<PageElement> testResults;

    private final QUnitTestSet qUnitTestSet;

    public QUnitPage() {
        this(QUnitTestSet.ALL_TESTS);
    }

    public QUnitPage(QUnitTestSet qUnitTestSet) {
        this.qUnitTestSet = qUnitTestSet;
    }

    @Override
    public String getUrl() {
        return "plugins/servlet/jstests/" + qUnitTestSet.testPagePath;
    }

    /**
     * @return parsed results of QUnit tests
     */
    public List<QUnitResult> getQUnitResults() {
        return StreamSupport.stream(testResults.spliterator(), false)
                .map(tr -> {
                    final String className = Iterables.getOnlyElement(tr.getCssClasses());
                    final String testName = tr.find(By.className("test-name")).getText();
                    final String testMessage = tr.findAll(By.className("test-message")).stream()
                            .map(PageElement::getText)
                            .collect(Collectors.joining("\n"));
                    return new QUnitResult(testName, Result.valueOf(className.toUpperCase()), testMessage);
                })
                .collect(Collectors.toList());
    }

    @WaitUntil
    @SuppressWarnings("unused")
    public void waitUntilTestsFinished() {
        Poller.waitUntil(
                qunitBanner.timed().getCssClasses(),
                Matchers.anyOf(Matchers.hasItem("qunit-pass"), Matchers.hasItem("qunit-fail")),
                Poller.by(20, TimeUnit.SECONDS));
    }

    /**
     * Represents the result of a QUnit test
     */
    public static final class QUnitResult {
        private final String testName;
        private final Result result;
        private final String testMessage;

        QUnitResult(String testName, Result result, String testMessage) {
            this.testName = testName;
            this.result = result;
            this.testMessage = testMessage;
        }

        /**
         * @return the name of the executed test
         */
        String testName() {
            return testName;
        }

        /**
         * @return the result of the QUnit test
         */
        Result result() {
            return result;
        }

        /**
         * @return the message, usually representing the cause of a test's failure
         */
        String testMessage() {
            return testMessage;
        }
    }

    /**
     * The result of a QUnit test
     */
    public enum Result {
        PASS,
        FAIL
    }

    /**
     * The possible sets of QUnit tests that this page object can interact with.
     */
    public enum QUnitTestSet {
        ACTIVITY_STREAM_GADGET("activity-stream.gadget-test"),
        ALL_TESTS("all-tests"),
        DATE_TEST("date-test"),
        JATOM_TEST("jatom-test"); // sample-test is not included as it does not set the state of the qunit-banner

        private final String testPagePath;

        QUnitTestSet(String testPagePath) {
            this.testPagePath = testPagePath;
        }
    }
}
