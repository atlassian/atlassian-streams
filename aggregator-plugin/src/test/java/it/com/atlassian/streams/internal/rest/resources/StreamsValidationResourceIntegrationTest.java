package it.com.atlassian.streams.internal.rest.resources;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;

@RunWith(StreamsUiTesterRunner.class)
public class StreamsValidationResourceIntegrationTest {
    @Inject
    static RestTester rest;

    @Test
    public void assertThatResponseIsBadRequestIfStreamsTitleParameterIsNull() {
        Response response = rest.validatePreferences(builder().title(null).build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsTitleParameterIsBlank() {
        Response response = rest.validatePreferences(builder().title("").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfStreamsHasInvalidUsernameParameter() {
        // Following CSTRM-87, we don't validate user names any more, as this leaks account information
        Response response =
                rest.validatePreferences(builder().usernames("invalidusername").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsNull() {
        Response response = rest.validatePreferences(builder().maxResults(null).build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsEmpty() {
        Response response = rest.validatePreferences(builder().maxResults("").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsNotANumber() {
        Response response = rest.validatePreferences(builder().maxResults("ten").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsLessThanZero() {
        Response response = rest.validatePreferences(builder().maxResults("-10").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsZero() {
        Response response = rest.validatePreferences(builder().maxResults("0").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsGreaterThan100() {
        Response response = rest.validatePreferences(builder().maxResults("101").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysNotDefined() {
        Response response = rest.validatePreferences(builder().build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameDefined() {
        Response response =
                rest.validatePreferences(builder().usernames("admin").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }
}
