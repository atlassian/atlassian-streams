package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.AppLinksTestRunner;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;

import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;

@RunWith(FecruApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_OAUTH})
@Ignore("FeCru is not compatible with Platform 5")
public class FecruApplinksTest extends AppLinksTests {
    public static final class Runner extends AppLinksTestRunner {
        public Runner(Class<?> klass) throws InitializationError {
            super(klass, "fecru");
        }
    }

    @Test
    @Ignore
    public void shouldContainOnlyEntriesFromCrucibleProjectCrOneInFeedWhenOneKeyIsSpecified() {
        fail("Making a request to FECRU for anything where key=ONE only includes the repository entries because"
                + "crucible and fisheye aren't actually linked in any meaningful way");
    }

    // STRM-1349 : Make sure there's no self linked provider in crucible
    @Test
    public void shouldNotContainCrucibleProviderFilterInFecruFilterResource() {
        assertThat(restTester.getStreamsConfigRepresentation(), not(hasSelfLinkedCrucibleFilters()));
    }

    @Test
    @Override
    public void shouldSortFilterProviders() {
        shouldSortFilterProvidersWithLocalProvidersFirst("Crucible", "FishEye", "Third Parties");
    }

    // STRM-1739 - proxy is now forbidden for local requests
    @Test
    public void shouldNotWhitelistUrlProxyResourceToAccessFeCru() {
        assertThat(
                restTester.getProxiedResponse("http://localhost:3990/streams/").getStatus(),
                equalTo(HttpServletResponse.SC_FORBIDDEN));
    }
}
