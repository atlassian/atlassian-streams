package it.com.atlassian.streams.applinks.fisheye.external;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import com.atlassian.fisheye.activity.ExternalActivityItem;

import static org.hamcrest.Matchers.hasItem;

public abstract class ExternalActivityItemMatchers {
    public static Matcher<? super Iterable<? super ExternalActivityItem>> hasActivityItem(
            Matcher<ExternalActivityItem> matcher) {
        return hasItem(matcher);
    }

    public static Matcher<ExternalActivityItem> withTitle(Matcher<String> matcher) {
        return new WithTitle(matcher);
    }

    private static final class WithTitle extends TypeSafeDiagnosingMatcher<ExternalActivityItem> {
        private final Matcher<String> matcher;

        private WithTitle(Matcher<String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ExternalActivityItem item, Description mismatchDescription) {
            if (!matcher.matches(item.getTitle())) {
                mismatchDescription.appendText("title ");
                matcher.describeMismatch(item.getTitle(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("title is").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<ExternalActivityItem> withSummary(Matcher<String> matcher) {
        return new WithSummary(matcher);
    }

    private static final class WithSummary extends TypeSafeDiagnosingMatcher<ExternalActivityItem> {
        private final Matcher<String> matcher;

        private WithSummary(Matcher<String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ExternalActivityItem item, Description mismatchDescription) {
            if (!matcher.matches(item.getSummary())) {
                mismatchDescription.appendText("summary ");
                matcher.describeMismatch(item.getSummary(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("summary is").appendDescriptionOf(matcher);
        }
    }
}
