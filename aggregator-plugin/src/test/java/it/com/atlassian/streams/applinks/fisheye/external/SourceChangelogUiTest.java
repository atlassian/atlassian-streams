package it.com.atlassian.streams.applinks.fisheye.external;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import be.roam.hue.doj.Doj;
import it.com.atlassian.streams.applinks.FecruApplinksTest;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.ui.UiTester;

import static it.com.atlassian.streams.applinks.fisheye.external.HtmlUnitMatchers.hasElement;
import static it.com.atlassian.streams.applinks.fisheye.external.HtmlUnitMatchers.withText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_STREAMS3;

/**
 * UI tests for the Activity tab on a FishEye Source project.
 */
@RunWith(FecruApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_STREAMS3})
@Ignore("FeCru is not compatible with Platform 5")
public class SourceChangelogUiTest {
    @Inject
    static UiTester uiTester;

    private Doj externalStreamElements;

    @BeforeClass
    public static void logIn() {
        uiTester.logInAs("admin");
    }

    @Before
    public void loadStream() {
        uiTester.gotoPage("changelog/ONE"); // view anything about project ONE regardless of the author
        externalStreamElements = uiTester.currentPage().getById("stream").getByClass("article-jira");
    }

    @Test
    public void assertThatProjectPageIsLimitedToSpecifiedProject() {
        assertThat(
                externalStreamElements.allElements(),
                not(hasElement(withText(allOf(containsString("TWO-1"), containsString("created"))))));
    }

    @Test
    public void assertThatCrucibleEventsDoNotAppearDuplicatedAsRemoteEvents() {
        assertThat(externalStreamElements.allElements(), not(hasElement(withText(containsString("review")))));
    }

    @Test
    public void assertThatFisheyeEventsDoNotAppearDuplicatedAsRemoteEvents() {
        assertThat(externalStreamElements.allElements(), not(hasElement(withText(containsString("committed")))));
    }

    @Test
    public void assertThatOtherUsersEventDoesNotAppearInSingleUserFecruUi() {
        uiTester.gotoPage(""); // view this specific user's feed
        externalStreamElements = uiTester.currentPage().getById("stream").getByClass("stream-jira");

        assertThat(
                externalStreamElements.allElements(),
                not(hasElement(withText(allOf(containsString("ONE-2"), containsString("created"))))));
    }

    /**
     * External activity items have pre-formatted titles and thus do not include fecru's elements.
     */
    @Test
    public void assertThatActivityTitlesDoNotHaveVerbElement() {
        assertThat(externalStreamElements.getByClass("title").getByClass("verb"), is(equalTo(Doj.EMPTY)));
    }

    /**
     * External activity items have pre-formatted titles and thus do not include fecru's elements.
     */
    @Test
    public void assertThatActivityTitlesDoNotHaveLinkElement() {
        assertThat(externalStreamElements.getByClass("title").getByClass("crulinkspan"), is(equalTo(Doj.EMPTY)));
    }
}
