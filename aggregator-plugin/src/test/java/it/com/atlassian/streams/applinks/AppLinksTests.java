package it.com.atlassian.streams.applinks;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.BeforeClass;
import org.junit.Test;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.streams.internal.ProviderFilterOrdering;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.RestTester;

import static org.junit.Assert.assertTrue;

public abstract class AppLinksTests {
    protected static final String ADMIN_USER = "admin";
    protected static final String USER_USER = "user";

    /** This needs to be high enough so all the content from each application will be in the stream **/
    protected static final int MAX_RESULTS = 500;

    @Inject
    protected static FeedClient client;

    @Inject
    protected static RestTester restTester;

    @Inject
    protected static UiTester uiTester;

    protected static StreamsConfigRepresentation streamsConfigRepresentation;

    @BeforeClass
    public static void setUpStreamsConfiguration() {
        streamsConfigRepresentation = restTester.getStreamsConfigRepresentation();
    }

    protected static String getProviderKeyStartingWith(StreamsConfigRepresentation config, final String keyPrefix) {
        return config.getFilters().stream()
                .filter(filter -> filter.getKey().startsWith(keyPrefix))
                .findFirst()
                .map(ProviderFilterRepresentation::getKey)
                .orElseThrow(NoSuchElementException::new);
    }

    @Test
    public abstract void shouldSortFilterProviders();

    protected final void shouldSortFilterProvidersWithLocalProvidersFirst(String... localProviders) {
        assertTrue(ProviderFilterOrdering.prioritizing(localProviders)
                .isOrdered(restTester.getStreamsConfigRepresentation().getFilters()));
    }

    protected final Matcher<? super StreamsConfigRepresentation> hasSelfLinkedCrucibleFilters() {
        return new TypeSafeDiagnosingMatcher<StreamsConfigRepresentation>() {
            @Override
            protected boolean matchesSafely(StreamsConfigRepresentation rep, Description mismatchDescription) {
                StreamsConfigRepresentation config = restTester.getStreamsConfigRepresentation();
                Optional<ProviderFilterRepresentation> reviewProviders = config.getFilters().stream()
                        .filter(filter -> filter.getKey().startsWith("reviews"))
                        .findAny();

                // the local provider exists
                if (reviewProviders.isPresent()) {
                    mismatchDescription.appendText("review provider filter count was not zero");
                    return false;
                }
                return true;
            }

            public void describeTo(Description description) {
                description.appendText("self linked crucible filter exists");
            }
        };
    }
}
