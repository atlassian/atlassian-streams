package it.com.atlassian.streams.thirdparty;

import org.apache.abdera.model.Feed;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.StreamsUiTesterRunner;
import com.atlassian.streams.testing.ThirdPartyTests;
import com.atlassian.streams.thirdparty.rest.representations.ActivityObjectRepresentation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorLink;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
@Ignore("FeCru has an old version of ActiveObjects SPI that can't be overridden")
public class FeCruThirdPartyTest extends ThirdPartyTests {
    private static final String ANON_AVATAR_URL = "/streams/avatar/";
    private static final String REPO_KEY = "TST";

    @Override
    public String getValidProjectKey() {
        return "CR";
    }

    @Override
    public String getInvalidProjectKey() {
        return "CR-STRM";
    }

    @Test
    public void activityWithRepositoryKeyIsIncludedByProjectFilter() {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                .title(some(html(defaultTitle)))
                .target(some(ActivityObjectRepresentation.builder()
                        .urlString(some(REPO_KEY))
                        .build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), key(IS, REPO_KEY));

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(withTitle(equalTo(defaultTitle))));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void activityWithUnrecognizedUserAndNoAvatarSpecifiedInRequestHasAnonymousAvatar() {
        postActivityAndAssertSuccess(activity(actorWithId(UNKNOWN_USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        withAuthorElement(equalTo(UNKNOWN_USER_NAME)),
                        withAuthorLink(whereHref(containsString(ANON_AVATAR_URL))))));
    }
}
