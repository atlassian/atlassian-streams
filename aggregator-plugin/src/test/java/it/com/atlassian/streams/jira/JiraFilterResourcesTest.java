package it.com.atlassian.streams.jira;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.PreferenceMapBuilder;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import static com.atlassian.streams.jira.JiraFilterOptionProvider.PROJECT_CATEGORY;
import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasJiraFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasStandardFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withFilterProviderName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionKey;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionValueKeys;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/base.xml")
public class JiraFilterResourcesTest {
    @Inject
    static RestTester restTester;

    @Test
    public void assertThatFilterResourceHasJiraApplicationFilterKey() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasJiraFilters());
    }

    @Test
    public void assertThatJiraFilterHasJiraAsDisplayName() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasJiraFilters(withFilterProviderName(equalTo("Jira"))));
    }

    @Test
    public void assertThatJiraFilterHasJiraSpecificFilterOptions() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasJiraFilters(withOption(withOptionKey("issue_type"))));
    }

    @Test
    public void assertThatJiraFilterHasProjectCategoryFilterOptions() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasJiraFilters(withOption(withOptionKey(PROJECT_CATEGORY))));
    }

    @Test
    public void assertThatFilterResourceHasKeyFilterNamedProductSpecific() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionName("Project"))));
    }

    @Test
    public void assertThatFilterResourceHasJiraProjectsAsKeyOptionValues() {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(
                representation,
                hasStandardFilters(withOption(withOptionKey("key"), withOptionValueKeys("ONE", "TWO"))));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsHasInvalidKeyParameter() {
        Response response =
                restTester.validatePreferences(builder().keys("INVALIDKEY").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithKeysDefined() {
        Response response = restTester.validatePreferences(
                PreferenceMapBuilder.builder().keys("ONE").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysDefined() {
        Response response = restTester.validatePreferences(
                PreferenceMapBuilder.builder().usernames("admin").keys("ONE").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithMultipleKeysDefined() {
        Response response = restTester.validatePreferences(
                PreferenceMapBuilder.builder().keys("ONE,TWO").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndMultipleKeysDefined() {
        Response response = restTester.validatePreferences(PreferenceMapBuilder.builder()
                .usernames("admin")
                .keys("ONE,TWO")
                .build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatTheDefaultDateFormatIsCorrect() {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateFormat(), is(equalTo("dd/MMM/yy")));
    }

    @Test
    public void assertThatTheDefaultTimeFormatIsCorrect() {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeFormat(), is(equalTo("h:mm a")));
    }

    @Test
    public void assertThatTheDefaultDateTimeFormatIsCorrect() {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateTimeFormat(), is(equalTo("dd/MMM/yy h:mm a")));
    }
}
