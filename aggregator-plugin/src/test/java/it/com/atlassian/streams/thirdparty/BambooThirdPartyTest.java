package it.com.atlassian.streams.thirdparty;

import org.junit.Test;
import org.junit.runner.RunWith;

import net.java.ao.schema.Ignore;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.StreamsUiTesterRunner;
import com.atlassian.streams.testing.ThirdPartyTests;

import static com.atlassian.streams.testing.StreamsTestGroups.BAMBOO;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(BAMBOO)
public class BambooThirdPartyTest extends ThirdPartyTests {
    @Override
    public String getValidProjectKey() {
        return "AA";
    }

    @Override
    public String getInvalidProjectKey() {
        return "AP";
    }

    @Test
    @Ignore
    public void activityWithUnrecognizedUserAndNoAvatarSpecifiedInRequestHasAnonymousAvatar() {
        // STRM-1954 Bamboo doesn't have avatars for unknown users
    }
}
