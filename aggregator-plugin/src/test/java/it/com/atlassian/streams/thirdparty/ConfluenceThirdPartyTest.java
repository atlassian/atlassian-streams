package it.com.atlassian.streams.thirdparty;

import org.junit.runner.RunWith;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.StreamsUiTesterRunner;
import com.atlassian.streams.testing.ThirdPartyTests;

import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/base.zip")
public class ConfluenceThirdPartyTest extends ThirdPartyTests {
    @Override
    public String getValidProjectKey() {
        return "ds";
    }

    @Override
    public String getInvalidProjectKey() {
        return "bad";
    }
}
