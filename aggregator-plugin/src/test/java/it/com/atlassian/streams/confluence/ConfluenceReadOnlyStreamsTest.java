package it.com.atlassian.streams.confluence;

import javax.ws.rs.core.Response;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.PreferenceMapBuilder;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.Confluence.activities;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasAuthor;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.hasTarget;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtomGeneratorElement;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withId;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/base.zip")
public class ConfluenceReadOnlyStreamsTest {
    @Inject
    static FeedClient feedClient;

    @Inject
    static RestTester restTester;

    @Test
    public void assertThatFeedUpdatedDateIsSameAsFirstEntryPublishedDate() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getUpdated(), is(equalTo(feed.getEntries().get(0).getPublished())));
    }

    @Test
    public void assertThatActivityStreamDoesNotContainTargetsOfOrphanedSpaces() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), allEntries(not(hasTarget(withId(containsString("fourohfour.action"))))));
    }

    @Test
    public void assertThatActivityStreamContainsAtomGeneratorModule() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), allEntries(haveAtomGeneratorElement(containsString(feedClient.getBaseUrl()))));
    }

    @Test
    public void assertThatAuthorsContainProfilePictureUris() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), allEntries(hasAuthor(withLink(whereRel(equalTo("photo"))))));
    }

    @Test
    public void assertThatAllActivityStreamEntriesContainsAtlassianApplicationElement() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(
                feed.getEntries(),
                allEntries(haveAtlassianApplicationElement(containsString("com.atlassian.confluence"))));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsHasInvalidKeyParameter() {
        Response response =
                restTester.validatePreferences(builder().keys("INVALIDKEY").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithKeysDefined() {
        Response response = restTester.validatePreferences(
                PreferenceMapBuilder.builder().keys("ds").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysDefined() {
        Response response = restTester.validatePreferences(
                PreferenceMapBuilder.builder().usernames("admin").keys("ds").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatRevisionCommentIsBlockquoted() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        withTitle(allOf(
                                containsString("admin"), containsString("edited"), containsString("My Streams Page"))),
                        withContent(containsString("<blockquote>Migrated to Confluence 4.0</blockquote>")))));
    }

    @Test
    public void assertThatPageCommentIsBlockQuoted() {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        withTitle(allOf(
                                containsString("admin"),
                                containsString("commented on"),
                                containsString("My Streams Page"))),
                        withContent(allOf(
                                containsString("<blockquote>"),
                                containsString("Cool stream!"),
                                containsString("</blockquote>"))))));
    }

    @Test
    public void assertThatOlderRevisionsLinkToNewerVersions() {
        // the blog post has been edited after the initial post
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(article(), post())));
        String expectedUrlOld = "/display/ds/2010/11/11/My+First+Blog";
        String expectedUrlNew = "/pages/viewpage.action?pageId=688137";

        assertThat(
                feed.getEntries(),
                allEntries(allOf(
                        withTitle(anyOf(containsString(expectedUrlOld), containsString(expectedUrlNew))),
                        withActivityObjectElement(withLink(
                                whereHref(anyOf(containsString(expectedUrlOld), containsString(expectedUrlNew))))))));
    }
}
