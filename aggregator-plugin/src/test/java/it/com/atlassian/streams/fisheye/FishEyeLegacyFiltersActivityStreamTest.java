package it.com.atlassian.streams.fisheye;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.FeCruInstanceController;
import com.atlassian.streams.testing.LegacyFeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;

import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_1;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_2;
import static com.atlassian.streams.testing.FishEye.Data.changeSet1;
import static com.atlassian.streams.testing.FishEye.Data.changeSet2;
import static com.atlassian.streams.testing.FishEye.Data.changeSet3;
import static com.atlassian.streams.testing.FishEye.Data.changeSet4;
import static com.atlassian.streams.testing.FishEye.Data.changeSet5;
import static com.atlassian.streams.testing.FishEye.Data.changeSet6;
import static com.atlassian.streams.testing.FishEye.Data.changeSet7;
import static com.atlassian.streams.testing.FishEye.fisheyeModule;
import static com.atlassian.streams.testing.LegacyFeedClient.filter;
import static com.atlassian.streams.testing.LegacyFeedClient.itemKey;
import static com.atlassian.streams.testing.LegacyFeedClient.key;
import static com.atlassian.streams.testing.LegacyFeedClient.maxDate;
import static com.atlassian.streams.testing.LegacyFeedClient.minDate;
import static com.atlassian.streams.testing.LegacyFeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasNoEntries;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class FishEyeLegacyFiltersActivityStreamTest {
    @Inject
    static LegacyFeedClient feedClient;

    @Test
    public void assertThatFishEyeActivityStreamReturnsEntriesMatchingUser() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user("detkin"));
        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatFishEyeActivityStreamReturnsEntriesMatchingEitherUser() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user("detkin"), user("rtalusan"));
        assertThat(
                feed.getEntries(),
                hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet6(), changeSet5(), changeSet4(), changeSet3(), changeSet2(), changeSet1())));
    }

    @Test
    public void assertThatFishEyeActivityStreamReturnsNoEntriesWhenNoChangeSetsMatchUser() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user("noone"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFishEyeActivityStreamForTestRepositoryContainsChangeSets() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), key("TST"));
        assertThat(
                feed.getEntries(),
                hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet7(),
                        changeSet6(),
                        changeSet5(),
                        changeSet4(),
                        changeSet3(),
                        changeSet2(),
                        changeSet1())));
    }

    @Test
    public void assertThatFishEyeActivityStreamForNonExistentRepositoryIsEmpty() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), key("NONE"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFishEyeActivityStreamIsFiltered() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), filter("TST-1"));
        assertThat(feed.getEntries(), hasEntries(changeSet2()));
    }

    @Test
    public void assertThatFishEyeActivityStreamIsFilteredWithNonExistentFilter() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), filter("NONE-NONE"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithMinDateContainsEntriesAfterTheMinDate() throws Exception {
        // make sure all repos are loaded and so we can get the final  entry.
        // Nasty hardcoding of the control port, but then localhost is hardcoded everywhere...
        FeCruInstanceController ic = new FeCruInstanceController(39901);
        ic.reindexRepositories();

        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), minDate(CHANGESET_2.minusSeconds(1)));

        assertThat(
                feed.getEntries(),
                hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet7(), changeSet6(), changeSet5(), changeSet4(), changeSet3(), changeSet2())));
    }

    @Test
    public void assertThatFeedWithMaxDateContainsEntriesBeforeTheMaxDate() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), maxDate(CHANGESET_1.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet1()));
    }

    @Test
    public void assertThatFeedWithMaxDateAndKeyContainsEntriesBeforeTheMaxDateForRepository() throws Exception {
        Feed feed =
                feedClient.getAs("admin", fisheyeModule(), local(), key("TST"), maxDate(CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndKeyContainsEntriesWithinTheDateRangeForRepository()
            throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                key("TST"),
                minDate(CHANGESET_1.minusSeconds(1)),
                maxDate(CHANGESET_1.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet1()));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateContainsEntriesWithinTheDateRange() throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                minDate(CHANGESET_1.minusSeconds(1)),
                maxDate(CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndMaxResultsContainsMaximumAmountOfEntriesWithinTheDateRange()
            throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                maxResults(1),
                minDate(CHANGESET_1.minusSeconds(1)),
                maxDate(CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet2()));
    }

    @Test
    public void assertThatFeedWithItemKeyParameterContainsOnlyEntriesForChangeset() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), itemKey("TST-1"));
        assertThat(feed.getEntries(), hasEntries(changeSet2()));
    }

    @Test
    public void assertThatFeedWithInvalidItemKeyParameterContainsNoEntries() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), itemKey("ABC-1"));
        assertThat(feed.getEntries(), hasNoEntries());
    }
}
