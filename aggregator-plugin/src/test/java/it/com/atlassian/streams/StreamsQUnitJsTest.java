package it.com.atlassian.streams;

import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

import com.atlassian.pageobjects.DefaultProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.testing.StreamsTestedProduct;

import static it.com.atlassian.streams.QUnitPage.Result.PASS;

@RunWith(StreamsQUnitJsTest.JsQUnitRunner.class)
public class StreamsQUnitJsTest {
    private static final String BASE_URL = System.getProperty("baseurl", "http://localhost:3990/streams/");

    protected static RunNotifier runNotifier;

    private static StreamsTestedProduct testedProduct;

    static {
        testedProduct = TestedProductFactory.create(
                StreamsTestedProduct.class, new DefaultProductInstance(BASE_URL, "", 3990, "streams"), null);
    }

    @Test
    public void runQUnitJsTests() {
        QUnitPage qunitPage = testedProduct.visit(QUnitPage.class);
        notifyTestResults(qunitPage.getQUnitResults());
    }

    private void notifyTestResults(Iterable<QUnitPage.QUnitResult> results) {
        for (QUnitPage.QUnitResult result : results) {
            Description test = Description.createTestDescription(StreamsQUnitJsTest.class, result.testName());
            runNotifier.fireTestStarted(test);
            if (result.result() == PASS) {
                pass(test);
            } else {
                fail(test, result.testMessage());
            }
        }
    }

    /**
     * Finishing current QUnit testcase as passed.
     */
    private static void pass(Description test) {
        runNotifier.fireTestFinished(test);
    }

    /**
     * Finishing current QUnit testcase as failed.
     */
    private static void fail(Description test, String testMessage) {
        Failure failure = new Failure(test, new RuntimeException(testMessage));
        runNotifier.fireTestFailure(failure);
    }

    private String ensureTrailingSlash(String url) {
        return url.endsWith("/") ? url : url + "/";
    }

    /**
     * Override the default Junit4 runner class to get the {RunNotifier} before testing.
     */
    public static class JsQUnitRunner extends BlockJUnit4ClassRunner {
        public JsQUnitRunner(Class<?> klass) throws InitializationError {
            super(klass);
        }

        @Override
        public void run(RunNotifier notifier) {
            runNotifier = notifier;
            super.run(notifier);
        }
    }
}
