package it.com.atlassian.streams.fisheye;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.FeCruInstanceController;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.changeset;
import static com.atlassian.streams.fisheye.FishEyeActivityVerbs.push;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_1;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_2;
import static com.atlassian.streams.testing.FishEye.Data.changeSet1;
import static com.atlassian.streams.testing.FishEye.Data.changeSet2;
import static com.atlassian.streams.testing.FishEye.Data.changeSet3;
import static com.atlassian.streams.testing.FishEye.Data.changeSet4;
import static com.atlassian.streams.testing.FishEye.Data.changeSet5;
import static com.atlassian.streams.testing.FishEye.Data.changeSet6;
import static com.atlassian.streams.testing.FishEye.Data.changeSet7;
import static com.atlassian.streams.testing.FishEye.activities;
import static com.atlassian.streams.testing.FishEye.fisheyeModule;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.hasNoEntries;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class FishEyeFiltersActivityStreamTest {
    @Inject
    static FeedClient feedClient;

    @Test
    public void assertThatFeedWithFilterIsPushChangesetActivityOnlyContainsEntriesMatchingThatActivity()
            throws Exception {
        Feed feed = feedClient.getAs("admin", activities(IS, pair(changeset(), push())), fisheyeModule());
        assertThat(
                feed.getEntries(),
                allEntries(allOf(
                        withVerbElement(equalTo(push().iri().toASCIIString())),
                        withActivityObjectElement(
                                withType(equalTo(changeset().iri().toASCIIString()))))));
    }

    @Test
    public void assertThatFeedWithFilterNotPushChangesetActivityOnlyContainsEntriesNotMatchingThatActivity()
            throws Exception {
        Feed feed = feedClient.getAs("admin", activities(NOT, pair(changeset(), push())), fisheyeModule());
        assertThat(feed.getEntries(), hasNoEntries()); // pushing changesets is the only supported activity
    }

    @Test
    public void assertThatFishEyeActivityStreamReturnsEntriesMatchingUser() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user(IS, "detkin"));
        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatFishEyeActivityStreamReturnsEntriesMatchingEitherUser() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user(IS, "detkin", "rtalusan"));
        assertThat(
                feed.getEntries(),
                hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet6(), changeSet5(), changeSet4(), changeSet3(), changeSet2(), changeSet1())));
    }

    @Test
    public void assertThatFishEyeActivityStreamReturnsNoEntriesWhenNoChangeSetsMatchUser() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user(IS, "noone"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFishEyeActivityStreamDoesNotReturnEntriesMatchingNottedUser() throws Exception {
        // make sure all repos are loaded and so we can get the final  entry.
        // Nasty hardcoding of the control port, but then localhost is hardcoded everywhere...
        FeCruInstanceController ic = new FeCruInstanceController(39901);
        ic.reindexRepositories();

        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), user(NOT, "detkin"));
        assertThat(
                feed.getEntries(),
                allOf(
                        hasEntries(changeSet7(), changeSet6(), changeSet5(), changeSet4(), changeSet3()),
                        not(hasEntries(changeSet2(), changeSet1()))));
    }

    @Test
    public void assertThatFishEyeActivityStreamForTestRepositoryContainsChangeSets() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), key(IS, "TST"));
        assertThat(
                feed.getEntries(),
                hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet7(),
                        changeSet6(),
                        changeSet5(),
                        changeSet4(),
                        changeSet3(),
                        changeSet2(),
                        changeSet1())));
    }

    @Test
    public void assertThatFishEyeActivityStreamForNonExistentRepositoryIsEmpty() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), key(IS, "NONE"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFishEyeActivityStreamNotForTestRepositoryDoesntContainChangeSets() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), key(NOT, "TST"));
        assertThat(
                feed.getEntries(),
                not(hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet6(), changeSet5(), changeSet4(), changeSet3(), changeSet2(), changeSet1()))));
    }

    @Test
    public void assertThatFishEyeActivityStreamIsFilteredByIssueKey() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), issueKey(IS, "TST-1"));
        assertThat(feed.getEntries(), hasEntries(changeSet2()));
    }

    @Test
    public void assertThatFishEyeActivityStreamIsFilteredWithNonExistentIssueKey() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), issueKey(IS, "NONE-NONE"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFishEyeActivityStreamIsFilteredByNotIssueKey() {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), issueKey(NOT, "TST-1"));
        assertThat(feed.getEntries(), not(hasEntry(changeSet2())));
    }

    @Test
    public void assertThatFeedWithMinDateContainsEntriesAfterTheMinDate() throws Exception {
        // make sure all repos are loaded and so we can get the final  entry.
        // Nasty hardcoding of the control port, but then localhost is hardcoded everywhere...
        FeCruInstanceController ic = new FeCruInstanceController(39901);
        ic.reindexRepositories();

        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), updateDate(AFTER, CHANGESET_2.minusSeconds(1)));

        assertThat(
                feed.getEntries(),
                hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                        changeSet7(), changeSet6(), changeSet5(), changeSet4(), changeSet3(), changeSet2())));
    }

    @Test
    public void assertThatFeedWithMaxDateContainsEntriesBeforeTheMaxDate() throws Exception {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), updateDate(BEFORE, CHANGESET_1.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet1()));
    }

    @Test
    public void assertThatFeedWithMaxDateAndKeyContainsEntriesBeforeTheMaxDateForRepository() throws Exception {
        Feed feed = feedClient.getAs(
                "admin", fisheyeModule(), local(), key(IS, "TST"), updateDate(BEFORE, CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndKeyContainsEntriesWithinTheDateRangeForRepository()
            throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                key(IS, "TST"),
                updateDate(AFTER, CHANGESET_1.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_1.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet1()));
    }

    @Test
    public void assertThatFeedWithDateRangeAndKeyContainsEntriesWithinTheDateRangeForRepository() throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                key(IS, "TST"),
                updateDate(BETWEEN, CHANGESET_1.minusSeconds(1), CHANGESET_1.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet1()));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateContainsEntriesWithinTheDateRange() throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                updateDate(AFTER, CHANGESET_1.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndMaxResultsContainsMaximumAmountOfEntriesWithinTheDateRange()
            throws Exception {
        Feed feed = feedClient.getAs(
                "admin",
                fisheyeModule(),
                local(),
                maxResults(1),
                updateDate(AFTER, CHANGESET_1.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(changeSet2()));
    }
}
