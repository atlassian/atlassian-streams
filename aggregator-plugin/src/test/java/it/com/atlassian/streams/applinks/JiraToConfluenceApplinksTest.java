package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;

import static java.util.Arrays.asList;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.testing.FeedClient.activities;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasProviderFilter;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withProviderKey;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;

@TestGroups({APPLINKS, APPLINKS_OAUTH})
@RunWith(JiraApplinksTest.Runner.class)
public class JiraToConfluenceApplinksTest extends JiraApplinksTest {

    protected static final String CONFLUENCE_APPLICATION = "com.atlassian.confluence";
    protected static final String POST_VERB = "http://activitystrea.ms/schema/1.0/post";
    protected static final String UPDATE_VERB = "http://activitystrea.ms/schema/1.0/update";

    protected static String confluence;

    @BeforeClass
    public static void setUpConfluenceProviderKey() {
        confluence = getProviderKeyStartingWith(streamsConfigRepresentation, "wiki");
    }

    // STRM-2339; call Jira with a user that exists in Jira but not in Confluence - should omit content that's
    // anonymously accessible in Confluence
    @Test
    public void shouldNotExposeNonLocalAnonymousContentToNonUser() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntry(createdConfluenceBlog())));
        assertThat(feed.getEntries(), hasEntry(withTitle(containsString("ONE-2"))));
    }

    // STRM-2339; call Jira with an anonymous user - should omit content that's anonymously accessible in Confluence
    @Test
    public void shouldNotExposeNonRemoteAnonymousContentToAnonymous() {
        Feed feed = client.get(maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntry(createdConfluenceBlog())));
    }

    // STRM-2339: valid case, user exists on both, should see item
    @Test
    public void shouldShowRemoteContentToValidUser() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), hasEntry(createdConfluenceBlog()));
    }

    @Test
    public void shouldContainConfluenceActivityInStream() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(confluence));

        assertThat(feed.getEntries(), hasEntry(addedConfluencePageWithAttachments()));
    }

    @Test
    public void shouldContainOnlyEntriesFromConfluenceProviderInFeedWhenConfluenceProviderIsSpecified() {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(confluence));

        assertThat(
                feed.getEntries(), allEntries(haveAtlassianApplicationElement(containsString(CONFLUENCE_APPLICATION))));
    }

    @Test
    public void shouldContainOnlyEntriesFromConfluenceInFeedWhenArticleIsSpecified() {
        Feed feed = client.getAs(
                ADMIN_USER,
                maxResults(MAX_RESULTS),
                activities(confluence, IS, asList(pair(article(), post()))),
                provider(confluence));

        assertThat(feed.getEntries(), allOf(hasEntry(createdConfluenceBlog()), not(hasEntry(createdOne1()))));
    }

    @Test
    public void shouldContainNoUnmatchedEntriesFromConfluenceInFeedWhenUnmatched() {
        Feed feed = client.getAs(
                ADMIN_USER,
                maxResults(MAX_RESULTS),
                activities(confluence, IS, asList(pair(article(), post()))),
                provider(confluence));

        assertThat(
                feed.getEntries(),
                allOf(
                        not(hasEntry(updatedConfluenceBlog())),
                        not(hasEntry(addedConfluencePageWithAttachments())),
                        not(hasEntry(updatedConfluencePageWithAttachments()))));
    }

    @Test
    public void shouldContainMatchedEntriesFromConfluenceInFeedWhenMatched() {
        Feed feed = client.getAs(
                ADMIN_USER,
                maxResults(MAX_RESULTS),
                activities(confluence, IS, asList(pair(article(), post()))),
                provider(confluence));

        assertThat(feed.getEntries(), hasEntry(createdConfluenceBlog()));
    }

    @Test
    public void shouldContainAppLinksProviderFiltersFromConfluenceInFilterResource() {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksConfluenceFilters());
    }

    @Test
    public void shouldWhitelistUrlProxyResourceToAccessConfluence() {
        assertThat(
                restTester.getProxiedResponse("http://localhost:1990/streams/").getStatus(),
                anyOf(equalTo(OK.getStatusCode()), equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
    }

    @Test
    public void shouldNotReturnPrivateConfluenceEntryWhenRetrievingFeedFromDisabledConfluenceUser() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(not(hasEntry(createdConfluenceBlog())), hasEntry(createdOne1())));
    }

    // Confluence Matchers
    protected final Matcher<? super Entry> addedConfluencePageWithAttachments() {

        return allOf(
                haveAtlassianApplicationElement(equalTo(CONFLUENCE_APPLICATION)),
                withTitle(containsString("Page with Attachments")),
                withVerbElement(equalTo(POST_VERB)));
    }

    protected final Matcher<? super Entry> updatedConfluencePageWithAttachments() {
        return allOf(
                haveAtlassianApplicationElement(equalTo(CONFLUENCE_APPLICATION)),
                withTitle(containsString("Page with Attachments")),
                withVerbElement(equalTo(UPDATE_VERB)));
    }

    protected final Matcher<? super Entry> createdConfluenceBlog() {
        return allOf(
                haveAtlassianApplicationElement(equalTo(CONFLUENCE_APPLICATION)),
                withTitle(containsString("My First Blog")),
                withVerbElement(equalTo(POST_VERB)));
    }

    protected final Matcher<? super Entry> updatedConfluenceBlog() {
        return allOf(
                haveAtlassianApplicationElement(equalTo(CONFLUENCE_APPLICATION)),
                withTitle(containsString("My First Blog")),
                withVerbElement(equalTo(UPDATE_VERB)));
    }

    private Matcher<? super StreamsConfigRepresentation> hasAppLinksConfluenceFilters() {
        return hasProviderFilter(withProviderKey(startsWith("wiki")));
    }
}
