package it.com.atlassian.streams.jira;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.inject.Inject;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/issue-worklog.xml")
public class IssueWorklogTest {
    @Inject
    static FeedClient client;

    public static Matcher<? super Entry> issueOne3() {
        return withTitle(allOf(
                not(containsString("updated 2 fields of")),
                containsString("ONE-3"),
                containsString("You should track your time!")));
    }

    public static Matcher<? super Entry> issueOne3MultipleUpdate() {
        return withTitle(allOf(
                containsString("updated 2 fields of"),
                containsString("ONE-3"),
                containsString("You should track your time!")));
    }

    @Test
    public void assertThatWorklogIdUpdateDoesNotShowUpInAnyEntry() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(feed.getEntries(), not(hasEntry(withContent(containsString("Worklog Id")))));
    }

    @Test
    public void assertThatRemainingEstimateFieldUpdateChangesSecondsToPrettyDate() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(
                                allOf(containsString("changed the Remaining Estimate"), containsString("5 hours"))))));
    }

    @Test
    public void assertThatRemainingEstimateInMultipleFieldsUpdateChangesSecondsToPrettyDate() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3MultipleUpdate(),
                        withContent(
                                allOf(containsString("Changed the Remaining Estimate"), containsString("4 hours"))))));
    }

    @Test
    public void assertThatOriginalEstimateFieldUpdateChangesSecondsToPrettyDate() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(
                                containsString("changed the Original Estimate"), containsString("1 day, 1 hour"))))));
    }

    @Test
    public void assertThatOriginalEstimateInMultipleFieldsUpdateChangesSecondsToPrettyDate() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3MultipleUpdate(),
                        withContent(allOf(containsString("Changed the Original Estimate"), containsString("1 day"))))));
    }

    @Test
    public void assertThatTimeSpentFieldUpdateChangesSecondsToPrettyDate() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(issueOne3(), withTitle(allOf(containsString("logged"), containsString("1 hour"))))));
    }

    @Test
    public void assertThatTimeSpentInMultipleFieldsUpdateChangesSecondsToPrettyDate() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3MultipleUpdate(),
                        withContent(allOf(containsString("Logged"), containsString("4 hours"))))));
    }

    @Test
    public void assertThatTimeSpentFieldUpdateLogsTimeLoggedAndNotTotalTime() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(issueOne3(), withTitle(allOf(containsString("logged"), containsString("2 hours"))))));
    }

    @Test
    public void assertThatTimeSpentFieldUpdateShowsCommentIfItExists() {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(
                feed.getEntries(),
                hasEntry(allOf(
                        issueOne3(),
                        withTitle(allOf(containsString("logged"), containsString("2 hours"))),
                        withContent(containsString("doing a bug fix")))));
    }
}
