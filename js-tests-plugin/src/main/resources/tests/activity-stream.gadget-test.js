// If you are doing amps:run from within a product
// (e.g., JIRA, you may access these tests via http://localhost:3990/streams/plugins/servlet/jstests/all-tests)
AJS.toInit(function() {
    module("Activity stream gadget QUnit Test");

    test("Set of correct dates in az-AZ format", function() {
        ok(ActivityStreams.gadget.validateLocalDate("de-DE"));
        ok(ActivityStreams.gadget.validateLocalDate("en-AU"));
        ok(ActivityStreams.gadget.validateLocalDate("pl-PL"));
    });

    test("Set of correct dates in az-azaz-Az format", function () {
        ok(ActivityStreams.gadget.validateLocalDate("sr-Cyrl-BA"));
        ok(ActivityStreams.gadget.validateLocalDate("uz-Latn-UZ"));
        ok(ActivityStreams.gadget.validateLocalDate("az-Latn-AZ"));
    });

    test("Set of correct dates in different formats", function () {
        ok(ActivityStreams.gadget.validateLocalDate("en-MOON"));
        ok(ActivityStreams.gadget.validateLocalDate("kok-IN"));
    });

    test("Set of incorrect dates in various formats", function () {
        equal(ActivityStreams.gadget.validateLocalDate("../../../../"), false);
        equal(ActivityStreams.gadget.validateLocalDate("a1-az"), false);
        equal(ActivityStreams.gadget.validateLocalDate("<script>alert(1)</script>"), false);
        equal(ActivityStreams.gadget.validateLocalDate("22-33"), false);
        equal(ActivityStreams.gadget.validateLocalDate(""), false);
    });
});
