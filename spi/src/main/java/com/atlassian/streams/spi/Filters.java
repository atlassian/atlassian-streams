package com.atlassian.streams.spi;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableSet;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityObjectTypes;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Function2;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;

import static com.google.common.base.Predicates.alwaysTrue;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.intersection;
import static com.google.common.collect.Sets.union;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Fold.foldl;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pairs.firsts;
import static com.atlassian.streams.api.common.Pairs.mkPairs;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_OBJECT_VERB_SEPARATOR;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.UPDATE_DATE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;

/**
 * An assortment of methods for creating {@link Predicate}s and {@link Function}s useful when applying filtering
 * parameters.
 */
public final class Filters {
    /**
     * Creates a {@code Predicate} from the {@code IS} and {@code NOT} operators in the list of filters.  The returned
     * {@code Predicate} uses the {@link Function}s returned by {@link #isIn} and {@link #notIn} to construct the sub
     * {@code Predicate}s for each {@code Operator}, and then {@code and}s them all together.
     *
     * @param filters filters to create a predicate for
     * @return {@code Predicate} from the {@code IS} and {@code NOT} filters
     * @deprecated - use java streams instead
     */
    @Deprecated
    public static Predicate<String> isAndNot(Iterable<Pair<Operator, Iterable<String>>> filters) {
        return isAndNot(filters, isIn(), notIn());
    }

    /**
     * Creates a {@code Predicate} from the {@code IS} and {@code NOT} operators in the list of filters.  The returned
     * {@code Predicate} uses the {@link Function}s {@code is} and {@code not} to construct the sub
     * {@code Predicate}s for each {@code Operator}, and then {@code and}s them all together.
     *
     * @param filters filters to create a predicate for
     * @return {@code Predicate} from the {@code IS} and {@code NOT} filters
     */
    public static Predicate<String> isAndNot(
            Iterable<Pair<Operator, Iterable<String>>> filters,
            Function<Iterable<String>, Predicate<String>> is,
            Function<Iterable<String>, Predicate<String>> not) {
        Predicate<String> alwaysTrue = Predicates.alwaysTrue();
        if (filters == null) {
            return alwaysTrue;
        }
        return foldl(filters, alwaysTrue, new IsAndNot(is, not));
    }

    /**
     * @return a {@code Function} which takes a list of {@code String}s and returns a {@code Predicate} which determines
     * if a {@code String} is in that list of values.
     * @deprecated use method reference to relevant contains() method
     */
    @Deprecated
    public static Function<Iterable<String>, Predicate<String>> isIn() {
        return IsIn.INSTANCE;
    }

    /**
     * @return a {@code Function} which takes a list of {@code String}s and returns a {@code Predicate} which determines
     * if a {@code String} is not in that list of values.
     * @deprecated use method reference to relevant contains() method and negation
     */
    @Deprecated
    public static Function<Iterable<String>, Predicate<String>> notIn() {
        return NotIn.INSTANCE;
    }

    @Deprecated
    private enum IsIn implements Function<Iterable<String>, Predicate<String>> {
        INSTANCE;

        public Predicate<String> apply(final Iterable<String> xs) {
            return el -> com.google.common.collect.Iterables.contains(xs, el);
        }
    }

    @Deprecated
    private enum NotIn implements Function<Iterable<String>, Predicate<String>> {
        INSTANCE;

        public Predicate<String> apply(final Iterable<String> xs) {
            return IsIn.INSTANCE.apply(xs).negate();
        }
    }

    private static final class IsAndNot
            implements Function2<Pair<Operator, Iterable<String>>, Predicate<String>, Predicate<String>> {
        private final Function<Iterable<String>, Predicate<String>> is;
        private final Function<Iterable<String>, Predicate<String>> not;

        IsAndNot(Function<Iterable<String>, Predicate<String>> is, Function<Iterable<String>, Predicate<String>> not) {
            this.is = is;
            this.not = not;
        }

        public Predicate<String> apply(Pair<Operator, Iterable<String>> filter, Predicate<String> predicate) {
            switch (filter.first()) {
                case IS:
                    return predicate.and(is.apply(filter.second()));
                case NOT:
                    return predicate.and(not.apply(ImmutableSet.copyOf(filter.second())));
                default:
                    return predicate;
            }
        }
    }

    /**
     * Creates a {@code Predicate} to determine if any of several usernames match the filters from the request.
     * If the request does not contain any author filters, the returned {@code Predicate} will return {@code true}
     * for every username. Only a single match is needed for the {@code Predicate} to return true.
     *
     * @param request request to generate the user predicate from
     * @return {@code Predicate} to determine if a set of usernames match the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static java.util.function.Predicate<Iterable<String>> anyInUsers(ActivityRequest request) {
        if (isEmpty(getIsValues(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList())))) {
            return alwaysTrue();
        }
        return new AnyInUsers(request);
    }

    private static final class AnyInUsers implements Predicate<Iterable<String>> {
        private final Predicate<String> inUsers;

        public AnyInUsers(ActivityRequest request) {
            inUsers = inUsers(request);
        }

        @Override
        public boolean test(Iterable<String> users) {
            return StreamSupport.stream(users.spliterator(), false).anyMatch(inUsers);
        }
    }

    /**
     * Creates a {@code Predicate} to determine if a user name matches the filters from the request.  If the request
     * does not contain any user filters, the returned {@code Predicate} will return {@code true} for every user name.
     *
     * @param request request to generate the user name predicate from
     * @return {@code Predicate} to determine if a user name matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static Predicate<String> inUsers(final ActivityRequest request) {
        return isAndNot(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList()));
    }

    /**
     * {@code Predicate} for removing any {@code StreamsEntry}s that match notted "filterUser" filters.
     *
     * @param request the {@code ActivityRequest} containing the filters
     * @return the {@code Predicate}
     * @deprecated just use java streams API
     */
    @Deprecated
    public static java.util.function.Predicate<Iterable<String>> notInUsers(ActivityRequest request) {
        return new NotInUsers(request);
    }

    private static final class NotInUsers implements Predicate<Iterable<String>> {
        private final Iterable<String> nottedUsers;

        public NotInUsers(ActivityRequest request) {
            nottedUsers = getAllValues(NOT, request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList()));
        }

        @Override
        public boolean test(Iterable<String> users) {
            return intersection(ImmutableSet.copyOf(nottedUsers), ImmutableSet.copyOf(users))
                    .isEmpty();
        }
    }

    /**
     * Creates a {@code Predicate} to determine if a project key matches the filters from the request.  If the request
     * does not contain any project key filters, the returned {@code Predicate} will return {@code true} for every
     * project key.
     *
     * @param request request to generate the project key predicate from
     * @return {@code Predicate} to determine if a project key matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static Predicate<String> inProjectKeys(ActivityRequest request) {
        return isAndNot(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList()));
    }

    /**
     * Creates a {@code Predicate} to determine if an issue key matches the filters from the request.  If the request
     * does not contain any issue key filters, the returned {@code Predicate} will return {@code true} for every issue
     * key.
     *
     * @param request request to generate the issue key predicate from
     * @return {@code Predicate} to determine if an issue key matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static Predicate<String> inIssueKeys(ActivityRequest request) {
        return isAndNot(request.getStandardFiltersMap().getOrDefault(ISSUE_KEY.getKey(), emptyList()));
    }

    /**
     * Creates a {@code Predicate} to determine if an issue key matches the filters from the request.  If the request
     * does not contain any issue key filters, the returned {@code Predicate} will return {@code true} for every issue
     * key.
     *
     * @param request request to generate the issue key predicate from
     * @param is {@code Function} which is used to construct the {@code Predicate} for checking if an issue key is in
     *           the list of issue keys from the filter
     * @param not {@code Function}s which is used to construct the {@code Predicate} for checking if an issue key is not
     *           in the list of issue keys from the filter
     * @return {@code Predicate} to determine if an issue key matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static Predicate<String> inIssueKeys(
            ActivityRequest request,
            Function<Iterable<String>, Predicate<String>> is,
            Function<Iterable<String>, Predicate<String>> not) {
        return isAndNot(request.getStandardFiltersMap().getOrDefault(ISSUE_KEY.getKey(), emptyList()), is, not);
    }

    /**
     * Creates a {@code Predicate} to determine if an {@code ActivityObjectType}/{@code ActivityVerb} pair matches the
     * activity filters from the request. If the request does not contain any activity filters, the returned
     * {@code Predicate} will return {@code true} for every combination.
     *
     * @param request request to generate the activity predicate from
     * @return {@code Predicate} to determine if a activity matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static java.util.function.Predicate<Pair<ActivityObjectType, ActivityVerb>> inActivities(
            ActivityRequest request) {
        if (!providerFilterExists(request, ACTIVITY_KEY)) {
            return alwaysTrue();
        }
        return new InActivities(request.getProviderFiltersMap().getOrDefault(ACTIVITY_KEY, emptyList()));
    }

    private static final class InActivities implements Predicate<Pair<ActivityObjectType, ActivityVerb>> {
        private final Predicate<String> inActivities;

        public InActivities(Collection<Pair<Operator, Iterable<String>>> activities) {
            this.inActivities = isAndNot(activities)::test;
        }

        @Override
        public boolean test(Pair<ActivityObjectType, ActivityVerb> activity) {
            return inActivities.test(activity.first().key()
                    + ACTIVITY_OBJECT_VERB_SEPARATOR
                    + activity.second().key());
        }
    }

    /**
     * Creates a {@code Predicate} to determine if an {@code ActivityObjectType}/{@code ActivityVerb} pair
     * matches the activity filters from the request. If the request does not contain any activity filters, the returned
     * {@code Predicate} will return {@code true} for every combination.
     *
     * @param request request to generate the activity predicate from
     * @return {@code Predicate} to determine if a activity matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static java.util.function.Predicate<Option<Pair<ActivityObjectType, ActivityVerb>>> inOptionActivities(
            ActivityRequest request) {
        if (!providerFilterExists(request, ACTIVITY_KEY)) {
            return alwaysTrue();
        }
        return new InOptionActivities(request.getProviderFiltersMap().getOrDefault(ACTIVITY_KEY, emptyList()));
    }

    private static final class InOptionActivities implements Predicate<Option<Pair<ActivityObjectType, ActivityVerb>>> {
        private final Predicate<String> inActivities;

        public InOptionActivities(Collection<Pair<Operator, Iterable<String>>> activities) {
            this.inActivities = isAndNot(activities)::test;
        }

        @Override
        public boolean test(Option<Pair<ActivityObjectType, ActivityVerb>> activity) {
            for (Pair<ActivityObjectType, ActivityVerb> activityPair : activity) {
                return inActivities.test(activityPair.first().key()
                        + ACTIVITY_OBJECT_VERB_SEPARATOR
                        + activityPair.second().key());
            }

            return false;
        }

        @Override
        public String toString() {
            return String.format("inOptionActivities(%s)", inActivities);
        }
    }

    /**
     * Creates a {@code Predicate} to determine if a {@code StreamsEntry}'s object-type/verb matches the activity
     * filters from the request.  If the request does not contain any activity filters, the returned {@code Predicate}
     * will return {@code true} for every activity combination.
     *
     * @param request request to generate the activity predicate from
     * @return {@code Predicate} to determine if a activity matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static Predicate<StreamsEntry> entriesInActivities(ActivityRequest request) {
        if (!providerFilterExists(request, ACTIVITY_KEY)) {
            return alwaysTrue();
        }
        return new EntriesInActivities(request.getProviderFiltersMap().getOrDefault(ACTIVITY_KEY, emptyList()));
    }

    private static final class EntriesInActivities implements Predicate<StreamsEntry> {
        private final Predicate<Pair<ActivityObjectType, ActivityVerb>> inActivities;

        public EntriesInActivities(Collection<Pair<Operator, Iterable<String>>> activities) {
            this.inActivities = new InActivities(activities);
        }

        @Override
        public boolean test(StreamsEntry entry) {
            final ActivityVerb verb = entry.getVerb();

            return StreamSupport.stream(
                            ActivityObjectTypes.getActivityObjectTypes(entry.getActivityObjects())
                                    .spliterator(),
                            false)
                    .map(activityObjectType -> Pair.pair(activityObjectType, verb))
                    .anyMatch(inActivities);
        }
    }

    private static boolean providerFilterExists(ActivityRequest request, String filterKey) {
        Map<String, Collection<Pair<Operator, Iterable<String>>>> providerFilters = request.getProviderFiltersMap();
        Collection<Pair<Operator, Iterable<String>>> fieldFilters =
                providerFilters.getOrDefault(filterKey, emptyList());
        return !fieldFilters.isEmpty();
    }

    /**
     * Determines the authors based on the {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the authors to be searched
     */
    public static Iterable<String> getAuthors(ActivityRequest request) {
        return getIsValues(request.getStandardFiltersMap().getOrDefault(USER.getKey(), emptyList()));
    }

    /**
     * Determines the project keys based on the {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the project keys to be searched
     */
    public static Iterable<String> getProjectKeys(ActivityRequest request) {
        return getIsValues(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList()));
    }

    /**
     * Determines the project keys to exclude based on the {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the project keys to be excluded
     */
    public static Iterable<String> getNotProjectKeys(ActivityRequest request) {
        return getNotValues(request.getStandardFiltersMap().getOrDefault(PROJECT_KEY, emptyList()));
    }

    /**
     * Determines the issue keys based on the {@code ActivityRequest}
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the issue keys to be searched
     */
    public static Iterable<String> getIssueKeys(ActivityRequest request) {
        return getIsValues(request.getStandardFiltersMap().getOrDefault(ISSUE_KEY.getKey(), emptyList()));
    }

    /**
     * Determines the issue keys to exclude based on the {@code ActivityRequest}
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the issue keys to be excluded
     */
    public static Iterable<String> getNotIssueKeys(ActivityRequest request) {
        return getNotValues(request.getStandardFiltersMap().getOrDefault(ISSUE_KEY.getKey(), emptyList()));
    }

    /**
     * Determines the minimum date filter based on the {@code ActivityRequest}. Returns null if none is specified.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the minimum date
     */
    public static Option<Date> getMinDate(ActivityRequest request) {
        Collection<Pair<Operator, Iterable<String>>> filters =
                request.getStandardFiltersMap().getOrDefault(UPDATE_DATE.getKey(), emptyList());

        // first try with BEFORE operator
        Option<Long> minDate = parseLongSafely(getFirstValue(AFTER, filters));

        if (!minDate.isDefined() && size(filters) > 0) {
            return getDateRange(filters).map(Pair::first);
        }
        return minDate.map(toDate);
    }

    /**
     * Determines the requested activity object types based on the {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @param activities the set of possible activities
     * @return the {@link ActivityObjectType}s to be searched
     */
    public static Iterable<ActivityObjectType> getRequestedActivityObjectTypes(
            ActivityRequest request, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities) {
        return firsts(filter(activities, inActivities(request)::test));
    }

    private static final Function<Long, Date> toDate = Date::new;

    /**
     * Determines the maximum date filter based on the {@code ActivityRequest}. Returns null if none is specified.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the maximum date
     */
    public static Option<Date> getMaxDate(ActivityRequest request) {
        Collection<Pair<Operator, Iterable<String>>> filters =
                request.getStandardFiltersMap().getOrDefault(UPDATE_DATE.getKey(), emptyList());

        // first try with BEFORE operator
        Option<Long> maxDate = parseLongSafely(getFirstValue(BEFORE, filters));

        if (!maxDate.isDefined() && size(filters) > 0) {
            return getDateRange(filters).map(Pair::second);
        }
        return maxDate.map(toDate);
    }

    /**
     * Given a collection of date filters, extract the first date range.
     *
     * @param filters the date filters
     * @return the first date range found
     */
    private static Option<Pair<Date, Date>> getDateRange(Collection<Pair<Operator, Iterable<String>>> filters) {
        Pair<Operator, Iterable<String>> firstFilter = get(filters, 0);
        Iterable<Date> dates = transform(firstFilter.second(), Filters::toDate);

        if (size(dates) < 2) {
            // no range exists
            return none();
        }

        Iterable<Pair<Date, Date>> ranges = mkPairs(dates);
        return some(get(ranges, 0));
    }

    private static Option<Long> parseLongSafely(Option<String> minDate) {
        for (String min : minDate) {
            return some(Long.parseLong(min));
        }
        return none();
    }

    /**
     * Creates a {@code Predicate} to determine if a date matches the filters from the request.  If the request
     * does not contain any update date filters, the returned {@code Predicate} will return {@code true} for every date.
     *
     * @param request request to generate the updated date predicate from
     * @return {@code Predicate} to determine if a date matches the filters from the request
     * @deprecated just use java streams API
     */
    @Deprecated
    public static java.util.function.Predicate<Date> inDateRange(ActivityRequest request) {
        Predicate<Date> alwaysTrue = Predicates.alwaysTrue();
        return foldl(
                request.getStandardFiltersMap().getOrDefault(UPDATE_DATE.getKey(), emptyList()),
                alwaysTrue,
                ContainsDate.INSTANCE);
    }
    /**
     * @deprecated just use java streams API
     */
    @Deprecated
    private enum ContainsDate implements Function2<Pair<Operator, Iterable<String>>, Predicate<Date>, Predicate<Date>> {
        INSTANCE;

        public Predicate<Date> apply(Pair<Operator, Iterable<String>> filter, Predicate<Date> predicate) {
            List<Date> dates = StreamSupport.stream(filter.second().spliterator(), false)
                    .map(Filters::toDate)
                    .collect(toList());
            switch (filter.first()) {
                case BEFORE:
                    return predicate.and(dates.stream()
                            .map(ToBeforePredicate.INSTANCE)
                            .reduce(Predicates.alwaysFalse(), Predicate::or));
                case AFTER:
                    return predicate.and(dates.stream()
                            .map(ToAfterPredicate.INSTANCE)
                            .reduce(Predicates.alwaysFalse(), Predicate::or));
                case BETWEEN:
                    Stream<Pair<Date, Date>> ranges =
                            StreamSupport.stream(mkPairs(dates).spliterator(), false);
                    return predicate.and(
                            ranges.map(ToBetweenPredicate.INSTANCE).reduce(Predicates.alwaysFalse(), Predicate::or));
                default:
                    return predicate;
            }
        }
    }

    private enum ToBeforePredicate implements Function<Date, Predicate<Date>> {
        INSTANCE;

        @Override
        public Predicate<Date> apply(Date date) {
            return new BeforePredicate(date);
        }

        private static final class BeforePredicate implements Predicate<Date> {
            private final Date date;

            public BeforePredicate(Date date) {
                this.date = date;
            }

            @Override
            public boolean test(Date input) {
                return input.before(date);
            }
        }
    }

    private enum ToAfterPredicate implements Function<Date, Predicate<Date>> {
        INSTANCE;

        @Override
        public Predicate<Date> apply(Date date) {
            return new AfterPredicate(date);
        }

        private static final class AfterPredicate implements Predicate<Date> {
            private final Date date;

            public AfterPredicate(Date date) {
                this.date = date;
            }

            @Override
            public boolean test(Date input) {
                return input.after(date);
            }
        }
    }

    private enum ToBetweenPredicate implements Function<Pair<Date, Date>, Predicate<Date>> {
        INSTANCE;

        @Override
        public Predicate<Date> apply(Pair<Date, Date> range) {
            return new BetweenPredicate(range);
        }

        private static final class BetweenPredicate implements Predicate<Date> {
            private final Pair<Date, Date> range;

            public BetweenPredicate(Pair<Date, Date> range) {
                this.range = range;
            }

            @Override
            public boolean test(Date input) {
                return input.after(range.first()) && input.before(range.second());
            }
        }
    }

    /**
     * Extracts the set of values to include from the request.  It does this by getting the set of values that should be
     * included - those with the "is" operator - and the set of values that should not be included - those with the
     * "is not" operator - and taking the difference.
     *
     * Useful only when you want to know the set of values to include.  The result of this method does not tell you
     * anything about which values *not* to include.
     */
    public static Set<String> getIsValues(Iterable<Pair<Operator, Iterable<String>>> filters) {
        Pair<Set<String>, Set<String>> isAndNotValues = getIsAndNotValues(filters);
        return difference(isAndNotValues.first(), isAndNotValues.second());
    }

    public static Set<String> getNotValues(Iterable<Pair<Operator, Iterable<String>>> filters) {
        Pair<Set<String>, Set<String>> isAndNotValues = getIsAndNotValues(filters);
        return isAndNotValues.second();
    }

    public static Pair<Set<String>, Set<String>> getIsAndNotValues(Iterable<Pair<Operator, Iterable<String>>> filters) {
        Pair<Set<String>, Set<String>> emptySets = Pair.pair(ImmutableSet.of(), ImmutableSet.of());
        return foldl(filters, emptySets, ExtractIsAndNotValues.INSTANCE::apply);
    }

    private enum ExtractIsAndNotValues
            implements
                    BiFunction<
                            Pair<Operator, Iterable<String>>,
                            Pair<Set<String>, Set<String>>,
                            Pair<Set<String>, Set<String>>> {
        INSTANCE;

        public Pair<Set<String>, Set<String>> apply(
                Pair<Operator, Iterable<String>> current, Pair<Set<String>, Set<String>> intermediate) {
            switch (current.first()) {
                case IS:
                    return Pair.pair(
                            union(intermediate.first(), ImmutableSet.copyOf(current.second())), intermediate.second());
                case NOT:
                    return Pair.pair(
                            intermediate.first(), union(intermediate.second(), ImmutableSet.copyOf(current.second())));
                default:
                    return intermediate;
            }
        }
    }

    public static Date toDate(String date) {
        return new Date(Long.parseLong(date));
    }

    /**
     * Extracts all the values from the {@code filters} for a particular {@code Operation}.
     *
     * @param op operation to extract all the values for
     * @param filters filters to extract values from
     * @return all the values from the {@code filters} for a particular {@code Operation}
     */
    public static Iterable<String> getAllValues(Operator op, Collection<Pair<Operator, Iterable<String>>> filters) {
        return foldl(
                        filters,
                        ImmutableList.builder(),
                        (Pair<Operator, Iterable<String>> filter, Builder<String> builder) -> {
                            if (filter.first().equals(op)) {
                                builder.addAll(filter.second());
                            }
                            return builder;
                        })
                .build();
    }

    /**
     * Extracts only the first value encountered in the {@code filters} with the given {@code op}.
     *
     * @param op operation to find the first value of
     * @param filters filters to extract the value from
     * @return the first value encountered in the {@code filters} with the given {@code op}
     */
    public static Option<String> getFirstValue(Operator op, Iterable<Pair<Operator, Iterable<String>>> filters) {
        try {
            Iterable<String> values = StreamSupport.stream(filters.spliterator(), false)
                    .filter(withOp(op))
                    .findFirst()
                    .map(Pair::second)
                    .orElse(emptyList());
            if (!isEmpty(values)) {
                return some(get(values, 0));
            }
        } catch (NoSuchElementException e) {
            // ignore and return null
        }
        return none();
    }

    private static Predicate<Pair<Operator, Iterable<String>>> withOp(Operator op) {
        return new WithOperator(op);
    }

    private static final class WithOperator implements Predicate<Pair<Operator, Iterable<String>>> {
        private final Operator op;

        public WithOperator(Operator op) {
            this.op = op;
        }

        @Override
        public boolean test(Pair<Operator, Iterable<String>> input) {
            return input.first().equals(op);
        }
    }
}
