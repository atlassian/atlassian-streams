package com.atlassian.streams.spi;

import java.util.function.Function;

/**
 * An optional {@link Function} for evicting instances from the application's Hibernate session.
 */
public interface Evictor<T> extends Function<T, Void> {}
