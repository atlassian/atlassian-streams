package com.atlassian.streams.spi;

import java.io.IOException;
import java.net.URI;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.atlassian.annotations.PublicApi;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.option;

/**
 * Interface for handling comments on streams items
 */
@PublicApi
public interface StreamsCommentHandler {
    /**
     * Post a reply to an activity item
     *
     * @param itemPath path of the item to comment on
     * @param comment The comment for the item
     * @return An {@code Either} containing an error or the URL of the newly added comment
     * @throws StreamsException If an error occured, such as if the item doesn't exist, or the user doesn't have
     * permission to reply to that item
     */
    Either<PostReplyError, URI> postReply(Iterable<String> itemPath, String comment);

    /**
     * Post a reply to an activity item
     *
     * @param baseUri the root URI of the application in the appropriate context. This would be a relative path for
     *                links within the application, and a canonical path for emails and such.
     * @param itemPath path of the item to comment on
     * @param comment The comment for the item
     * @return An {@code Either} containing an error or the URL of the newly added comment
     * @throws StreamsException If an error occured, such as if the item doesn't exist, or the user doesn't have
     * permission to reply to that item
     */
    Either<PostReplyError, URI> postReply(URI baseUri, Iterable<String> itemPath, String comment);

    @JsonDeserialize(using = PostReplyErrorDeserializer.class)
    class PostReplyError {
        private static final Logger log = LoggerFactory.getLogger(PostReplyError.class);

        final Type type;
        final Option<Throwable> cause;

        public PostReplyError(Type type) {
            this(type, null);
        }

        public PostReplyError(Type type, Throwable cause) {
            this.type = checkNotNull(type, "type");
            this.cause = option(cause);
        }

        public Type getType() {
            return type;
        }

        public Option<Throwable> getCause() {
            return cause;
        }

        public String asJsonString() {
            if (getCause().isDefined()) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode node = null;
                try {
                    node = mapper.readTree(getType().asJsonString());
                    ((ObjectNode) node).put("causeMessage", getCause().get().getMessage());
                    return mapper.writeValueAsString(node);
                } catch (IOException e) {
                    log.debug("An error occurred when serializing PostReplyError to JSON.", e);
                    return getType().asJsonString();
                }
            }
            return getType().asJsonString();
        }

        @JsonSerialize(using = PostReplyErrorTypeSerializer.class)
        @JsonDeserialize(using = PostReplyErrorTypeDeserializer.class)
        public enum Type {
            DELETED_OR_PERMISSION_DENIED(404, "comment.deleted.or.denied"),
            UNAUTHORIZED(401, "unauthorized"),
            FORBIDDEN(403, "forbidden"),
            CONFLICT(409, "conflict"),
            REMOTE_POST_REPLY_ERROR(500, "remote.error"),
            UNKNOWN_ERROR(500, "unknown.error");

            private final int statusCode;
            private final String subCode;

            Type(int statusCode, String subCode) {
                this.statusCode = statusCode;
                this.subCode = "streams.comment.action." + subCode;
            }

            public int getStatusCode() {
                return statusCode;
            }

            public String getSubCode() {
                return subCode;
            }

            public String asJsonString() {
                try {
                    return new ObjectMapper().writeValueAsString(this);
                } catch (IOException e) {
                    log.debug("An error occurred when serializing PostReplyError.Type to JSON.", e);
                    return "";
                }
            }
        }

        static class PostReplyErrorTypeSerializer extends JsonSerializer<Type> {

            public void serialize(Type type, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
                    throws IOException {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeFieldName("subCode");
                jsonGenerator.writeString(type.subCode);
                jsonGenerator.writeEndObject();
            }
        }

        static class PostReplyErrorTypeDeserializer extends JsonDeserializer<Type> {
            @Override
            public Type deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                    throws IOException {
                JsonNode node = jsonParser.getCodec().readTree(jsonParser);
                String subCode = node.get("subCode").asText();
                return Stream.of(Type.values())
                        .filter(enumValue -> enumValue.getSubCode().equals(subCode))
                        .findFirst()
                        .orElseThrow(() -> new IllegalArgumentException("SubCode " + subCode + " is not recognized"));
            }
        }
    }

    class PostReplyErrorDeserializer extends JsonDeserializer<PostReplyError> {
        @Override
        public PostReplyError deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            PostReplyError.Type errorType = new ObjectMapper().readValue(node.toString(), PostReplyError.Type.class);
            String causeMessage = node.get("causeMessage").asText();
            return new PostReplyError(errorType, new Throwable(causeMessage));
        }
    }
}
