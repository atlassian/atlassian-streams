package com.atlassian.streams.spi;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.atlassian.streams.api.common.Option;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.streams.api.common.Option.option;

public abstract class OptionalService<T> implements InitializingBean, DisposableBean {
    private final Class<T> type;
    private final BundleContext bundleContext;

    private ServiceTracker tracker;

    public OptionalService(final Class<T> type, final BundleContext bundleContext) {
        this.type = checkNotNull(type, "type");
        this.bundleContext = bundleContext;
    }

    protected final Option<T> getService() {
        return option(type.cast(tracker.getService()));
    }

    @Override
    public final void afterPropertiesSet() throws Exception {
        tracker = new ServiceTracker(bundleContext, type.getName(), null);
        tracker.open();
    }

    @Override
    public final void destroy() throws Exception {
        tracker.close();
    }
}
