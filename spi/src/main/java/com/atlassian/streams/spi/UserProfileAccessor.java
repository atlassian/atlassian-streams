package com.atlassian.streams.spi;

import java.net.URI;

import com.atlassian.annotations.PublicApi;
import com.atlassian.streams.api.UserProfile;

@PublicApi
public interface UserProfileAccessor {
    /**
     * Gets the user profile for the given username.
     * @param username The username to retrieve the profile for.
     * @return A populated UserProfile.
     */
    UserProfile getUserProfile(String username);

    /**
     * Gets the user profile for the anonymous user.
     * @return A populated anonymous UserProfile.
     */
    UserProfile getAnonymousUserProfile();

    /**
     * Gets the user profile for the given username.
     * @param baseUri The baseUri of the application. This is used for getting
     *                the context path for links in the UserProfile.
     * @param username The username to retrieve the profile for.
     * @return A populated UserProfile.
     */
    UserProfile getUserProfile(URI baseUri, String username);

    /**
     * Gets the user profile for the anonymous user.
     * @param baseUri The baseUri of the application. This is used for getting
     *                the context path for links in the UserProfile.
     * @return A populated anonymous UserProfile.
     */
    UserProfile getAnonymousUserProfile(URI baseUri);
}
