package com.atlassian.streams.spi;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNKNOWN_ERROR;

@RunWith(MockitoJUnitRunner.class)
public class PostReplyErrorTest {
    StreamsCommentHandler.PostReplyError.Type errorType = UNKNOWN_ERROR;
    String ERROR_CAUSE_MESSAGE = "message";

    @Mock
    Throwable errorThrowable;

    StreamsCommentHandler.PostReplyError postReplyError;

    @Before
    public void preparePostReplyErrorMock() {
        postReplyError = new StreamsCommentHandler.PostReplyError(errorType, errorThrowable);
        when(errorThrowable.getMessage()).thenReturn(ERROR_CAUSE_MESSAGE);
    }

    @Test
    public void errorTypeSerialisesToJson() {
        String errorTypeJson = errorType.asJsonString();
        assertContainsBackwardsCompatibleErrorTypeInformation(errorTypeJson);
    }

    @Test
    public void errorTypeSerialisesToJsonAfterRefactor() throws IOException {
        String errorTypeJson = errorType.asJsonString();

        StreamsCommentHandler.PostReplyError.Type deserialisedErrorType =
                new ObjectMapper().readValue(errorTypeJson, StreamsCommentHandler.PostReplyError.Type.class);
        assertThat(deserialisedErrorType.getStatusCode(), is(UNKNOWN_ERROR.getStatusCode()));
        assertThat(deserialisedErrorType.getSubCode(), is(UNKNOWN_ERROR.getSubCode()));
    }

    @Test
    public void postReplyErrorSerialisesToJson() throws IOException {
        String postReplyErrorJson = postReplyError.asJsonString();

        StreamsCommentHandler.PostReplyError deserialisedPostReplyError =
                new ObjectMapper().readValue(postReplyErrorJson, StreamsCommentHandler.PostReplyError.class);
        assertThat(deserialisedPostReplyError.getType().getStatusCode(), is(UNKNOWN_ERROR.getStatusCode()));
        assertThat(deserialisedPostReplyError.getType().getSubCode(), is(UNKNOWN_ERROR.getSubCode()));
        assertTrue(deserialisedPostReplyError.getCause().isDefined());
        assertThat(deserialisedPostReplyError.getCause().get().getMessage(), is(ERROR_CAUSE_MESSAGE));
    }

    @Test
    public void postReplyErrorJsonIsBackwardsCompatible() {
        String postReplyErrorJson = postReplyError.asJsonString();
        assertContainsBackwardsCompatibleErrorTypeInformation(StringUtils.deleteWhitespace(postReplyErrorJson));
    }

    private void assertContainsBackwardsCompatibleErrorTypeInformation(String errorTypeJson) {
        assertThat(errorTypeJson, containsString("\"subCode\":\"streams.comment.action.unknown.error\""));
        assertThat(StringUtils.countMatches(errorTypeJson, "{"), is(1));
        assertThat(StringUtils.countMatches(errorTypeJson, "}"), is(1));
        assertTrue(errorTypeJson.startsWith("{"));
    }
}
