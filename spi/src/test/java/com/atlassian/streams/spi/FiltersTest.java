package com.atlassian.streams.spi;

import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.HasAlternateLinkUri;
import com.atlassian.streams.api.StreamsEntry.HasApplicationType;
import com.atlassian.streams.api.StreamsEntry.HasAuthors;
import com.atlassian.streams.api.StreamsEntry.HasId;
import com.atlassian.streams.api.StreamsEntry.HasPostedDate;
import com.atlassian.streams.api.StreamsEntry.HasRenderer;
import com.atlassian.streams.api.StreamsEntry.HasVerb;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.StreamsFilterOptionProvider.ActivityOption;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.spi.Filters.getAuthors;
import static com.atlassian.streams.spi.Filters.getMaxDate;
import static com.atlassian.streams.spi.Filters.getMinDate;
import static com.atlassian.streams.spi.Filters.getNotProjectKeys;
import static com.atlassian.streams.spi.Filters.getProjectKeys;
import static com.atlassian.streams.spi.Filters.inDateRange;
import static com.atlassian.streams.spi.Filters.notInUsers;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.UPDATE_DATE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;

@RunWith(MockitoJUnitRunner.class)
public class FiltersTest {
    @Mock
    private I18nResolver i18nResolver;

    @Mock
    private ActivityRequest request;

    @Mock
    private Map<String, Collection<Pair<Operator, Iterable<String>>>> standardFilters;

    @Mock
    private Map<String, Collection<Pair<Operator, Iterable<String>>>> providerFilters;

    @Before
    public void setup() {
        when(request.getStandardFiltersMap()).thenReturn(standardFilters);
        when(request.getProviderFiltersMap()).thenReturn(providerFilters);
    }

    @Test
    public void testMinDateIsNotSpecified() {
        when(standardFilters.getOrDefault(anyString(), anyCollection())).thenReturn(emptyList());

        assertThat(getMinDate(request), is(equalTo(none(Date.class))));
    }

    @Test
    public void testMinDateIsSpecified() {
        Long dateVal = Instant.now().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(AFTER, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        assertThat(getMinDate(request), is(equalTo(some(new Date(dateVal)))));
    }

    @Test
    public void testMaxDateIsNotSpecified() {
        when(standardFilters.getOrDefault(anyString(), anyCollection())).thenReturn(emptyList());

        assertThat(getMaxDate(request), is(equalTo(none(Date.class))));
    }

    @Test
    public void testMaxDateIsSpecified() {
        Long dateVal = ZonedDateTime.now().toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BEFORE, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        assertThat(getMaxDate(request).get(), is(equalTo(new Date(dateVal))));
    }

    @Test
    public void testMinDateIsSpecifiedFromRange() {
        Long startVal = ZonedDateTime.now().withYear(2009).toInstant().toEpochMilli();
        Long endVal = ZonedDateTime.now().withYear(2010).toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(startVal.toString(), endVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BETWEEN, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        assertThat(getMinDate(request).get(), is(equalTo(new Date(startVal))));
    }

    @Test
    public void testMaxDateIsSpecifiedFromRange() {
        Long startVal = ZonedDateTime.now().withYear(2009).toInstant().toEpochMilli();
        Long endVal = ZonedDateTime.now().withYear(2010).toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(startVal.toString(), endVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BETWEEN, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        assertThat(getMaxDate(request).get(), is(equalTo(new Date(endVal))));
    }

    @Test
    public void testMaxDateWhenMinDateIsSpecified() {
        Long dateVal = ZonedDateTime.now().toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(AFTER, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        assertThat(getMaxDate(request), is(equalTo(none(Date.class))));
    }

    @Test
    public void testMinDateWhenMaxDateIsSpecified() {
        Long dateVal = ZonedDateTime.now().toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BEFORE, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        assertThat(getMinDate(request), is(equalTo(none(Date.class))));
    }

    @Test
    public void testContainsDateBetweenOperatorIncludesTimeBetween() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long startVal = rightNow.toInstant().toEpochMilli();
        Long endVal = rightNow.plusSeconds(5).toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(startVal.toString(), endVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BETWEEN, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.plusSeconds(1).toInstant());
        assertThat(inDateRange(request).test(toTest), is(true));
    }

    @Test
    public void testContainsDateBetweenOperatorDoesNotIncludeTimeJustSecondsBeforeStart() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long startVal = rightNow.toInstant().toEpochMilli();
        Long endVal = rightNow.plusSeconds(5).toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(startVal.toString(), endVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BETWEEN, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.minusSeconds(1).toInstant());
        assertThat(inDateRange(request).test(toTest), is(false));
    }

    @Test
    public void testContainsDateBetweenOperatorDoesNotIncludeTimeJustSecondsAfterEnd() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long startVal = rightNow.toInstant().toEpochMilli();
        Long endVal = rightNow.plusSeconds(5).toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(startVal.toString(), endVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BETWEEN, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.plusSeconds(6).toInstant());
        assertThat(inDateRange(request).test(toTest), is(false));
    }

    @Test
    public void testContainsDateBeforeOperatorIncludesTimeJustSecondsBefore() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long dateVal = rightNow.toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BEFORE, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.minusSeconds(1).toInstant());
        assertThat(inDateRange(request).test(toTest), is(true));
    }

    @Test
    public void testContainsDateBeforeOperatorDoesNotIncludeTimeJustSecondsAfter() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long dateVal = rightNow.toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(BEFORE, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.plusSeconds(1).toInstant());
        assertThat(inDateRange(request).test(toTest), is(false));
    }

    @Test
    public void testContainsDateAfterOperatorIncludesTimeJustSecondsAfter() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long dateVal = rightNow.toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(AFTER, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.plusSeconds(1).toInstant());
        assertThat(inDateRange(request).test(toTest), is(true));
    }

    @Test
    public void testContainsDateAfterOperatorDoesNotIncludeTimeJustSecondsBefore() {
        ZonedDateTime rightNow = ZonedDateTime.now();
        Long dateVal = rightNow.toInstant().toEpochMilli();
        Iterable<String> dates = ImmutableList.of(dateVal.toString());
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(AFTER, dates));

        when(standardFilters.getOrDefault(eq(UPDATE_DATE.getKey()), anyCollection()))
                .thenReturn(values);

        Date toTest = Date.from(rightNow.minusSeconds(1).toInstant());
        assertThat(inDateRange(request).test(toTest), is(false));
    }

    @Test
    public void testAuthorsOfIses() {
        String[] is = new String[] {"user1", "user2"};
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(IS, iterable(is)));

        when(standardFilters.getOrDefault(eq(USER.getKey()), anyCollection())).thenReturn(values);

        assertThat(getAuthors(request), onlyHasItems(is));
    }

    /**
     * I wouldn't expect this to ever be executed in real life, but I just wanted to make
     * sure that NOT usernames were removed from the list.
     */
    @Test
    public void testAuthorsOfIsesAndNots() {
        String[] is = new String[] {"user1", "user2"};
        String[] not = new String[] {"user1"};
        ImmutableList<Pair<Operator, Iterable<String>>> values =
                ImmutableList.of(pair(IS, iterable(is)), pair(NOT, iterable(not)));

        when(standardFilters.getOrDefault(eq(USER.getKey()), anyCollection())).thenReturn(values);

        assertThat(getAuthors(request), onlyHasItems("user2"));
    }

    @Test
    public void testAuthorsOfNots() {
        String[] not = new String[] {"user1"};
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(NOT, iterable(not)));

        when(standardFilters.getOrDefault(eq(USER.getKey()), anyCollection())).thenReturn(values);

        StreamsEntry entry1 = new StreamsEntry(
                newEntryParams().authors(ImmutableNonEmptyList.of(newUserProfile("user1"))), i18nResolver);
        StreamsEntry entry2 = new StreamsEntry(
                newEntryParams().authors(ImmutableNonEmptyList.of(newUserProfile("user2"))), i18nResolver);
        StreamsEntry entry3 = new StreamsEntry(
                newEntryParams().authors(ImmutableNonEmptyList.of(newUserProfile("user1"), newUserProfile("user2"))),
                i18nResolver);
        Iterable<StreamsEntry> entries = ImmutableList.of(entry1, entry2, entry3);

        assertThat(filter(entries, entryAuthors(notInUsers(request)::test)), onlyHasItems(entry2));
    }

    private UserProfile newUserProfile(String username) {
        return new UserProfile.Builder(username).build();
    }

    Predicate<StreamsEntry> entryAuthors(Predicate<Iterable<String>> authorPredicate) {
        return entry -> authorPredicate.apply(transform(entry.getAuthors(), UserProfile::getUsername));
    }

    @Test
    public void testProjectKeysOfIses() {
        String[] is = new String[] {"space1", "space2"};
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(IS, iterable(is)));

        when(standardFilters.getOrDefault(eq(PROJECT_KEY), anyCollection())).thenReturn(values);

        assertThat(getProjectKeys(request), onlyHasItems(is));
    }

    @Test
    public void testProjectKeysOfNots() {
        String[] isNot = new String[] {"space1", "space2"};
        ImmutableList<Pair<Operator, Iterable<String>>> values = ImmutableList.of(pair(NOT, iterable(isNot)));

        when(standardFilters.getOrDefault(eq(PROJECT_KEY), anyCollection())).thenReturn(values);

        assertThat(getNotProjectKeys(request), onlyHasItems(isNot));
    }

    @Test
    public void testGetRequestedActivityObjectTypesForIs() {
        Iterable<Pair<ActivityObjectType, ActivityVerb>> activities =
                ImmutableList.of(pair(article(), post()), pair(article(), update()), pair(file(), post()));
        ActivityObjectType[] expected = new ActivityObjectType[] {article(), file()};
        ImmutableList<ActivityOption> is = ImmutableList.of(
                new ActivityOption("article posted", article(), post()),
                new ActivityOption("file posted", file(), post()));
        ImmutableList<Pair<Operator, Iterable<String>>> filters =
                ImmutableList.of(pair(IS, transform(is, ActivityOption::toActivityOptionKey)));

        when(providerFilters.getOrDefault(eq(ACTIVITY_KEY), anyCollection())).thenReturn(filters);

        assertThat(Filters.getRequestedActivityObjectTypes(request, activities), onlyHasItems(expected));
    }

    @Test
    public void testGetRequestedActivityObjectTypesForNot() {
        Iterable<Pair<ActivityObjectType, ActivityVerb>> activities = ImmutableList.of(
                pair(article(), post()), pair(article(), update()), pair(comment(), update()), pair(file(), post()));
        ActivityObjectType[] expected = new ActivityObjectType[] {article(), comment()};
        ImmutableList<ActivityOption> not = ImmutableList.of(
                new ActivityOption("article posted", article(), post()),
                new ActivityOption("file posted", file(), post()));
        ImmutableList<Pair<Operator, Iterable<String>>> filters =
                ImmutableList.of(pair(NOT, transform(not, ActivityOption::toActivityOptionKey)));

        when(providerFilters.getOrDefault(eq(ACTIVITY_KEY), anyCollection())).thenReturn(filters);

        assertThat(Filters.getRequestedActivityObjectTypes(request, activities), onlyHasItems(expected));
    }

    private StreamsEntry.Parameters<
                    HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors>
            newEntryParams() {
        return StreamsEntry.params()
                .id(URI.create("http://example.com"))
                .postedDate(ZonedDateTime.now())
                .alternateLinkUri(URI.create("http://example.com"))
                .applicationType("test")
                .authors(ImmutableNonEmptyList.of(
                        new UserProfile.Builder("someone").fullName("Some One").build()))
                .addActivityObject(new ActivityObject(ActivityObject.params()
                        .id("activity")
                        .title(some("Some activity"))
                        .alternateLinkUri(URI.create("http://example.com"))
                        .activityObjectType(comment())))
                .verb(post())
                .baseUri(URI.create("http://example.com"))
                .renderer(mock(Renderer.class));
    }

    /**
     * Verifies that the specified elements exist and that no other elements do.
     */
    private static <T> Matcher<Iterable<T>> onlyHasItems(T... ts) {
        return allOf(size(ts), hasItems(ts));
    }

    /**
     * Need to break this out into separate function in order to properly cast generics type
     */
    private static <T> Matcher<Iterable<T>> size(T... ts) {
        return iterableWithSize(ts.length);
    }

    /**
     * Another workaround for generics/casting compilation problem
     */
    private static <T> Iterable<T> iterable(T[] array) {
        return ImmutableList.copyOf(array);
    }
}
