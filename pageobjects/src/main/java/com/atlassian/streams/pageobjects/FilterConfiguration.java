package com.atlassian.streams.pageobjects;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.utils.element.WebDriverPoller;

import static com.google.common.collect.Iterables.get;

import static com.atlassian.webdriver.utils.element.ElementConditions.isVisible;

public class FilterConfiguration {
    @Inject
    WebDriverPoller poller;

    @Inject
    PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    private WebElement configIdElement;
    private final String configId;

    public FilterConfiguration(String configId) {
        this.configId = configId;
    }

    @WaitUntil
    public void waitUntilFilterConfigurationIsVisible() {
        configIdElement = elementFinder.find(By.id(configId)).asWebElement();
        poller.waitUntil(isVisible(By.className("smart-config"), configIdElement));
    }

    public Filter getFirstFilter() {
        return get(getFilters(), 0);
    }

    private Iterable<Filter> getFilters() {
        return Lists.transform(configIdElement.findElements(By.className("config-row")), toFilter());
    }

    private Function<WebElement, Filter> toFilter() {
        return toFilterFunction;
    }

    private final Function<WebElement, Filter> toFilterFunction = new Function<WebElement, Filter>() {
        public Filter apply(WebElement e) {
            return pageBinder.bind(Filter.class, e);
        }
    };
}
