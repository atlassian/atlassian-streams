package com.atlassian.streams.pageobjects;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class Matchers {
    public static Matcher<ActivityItem> withSummary(Matcher<String> matcher) {
        return new HasSummaryMatcher(matcher);
    }

    public static Matcher<ActivityItem> hasSummary(Matcher<String> matcher) {
        return new HasSummaryMatcher(matcher);
    }

    private static final class HasSummaryMatcher extends TypeSafeDiagnosingMatcher<ActivityItem> {
        private final Matcher<String> matcher;

        public HasSummaryMatcher(Matcher<String> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ActivityItem item, Description mismatchDescription) {
            if (!matcher.matches(item.getSummaryText())) {
                mismatchDescription.appendText("summary ");
                matcher.describeMismatch(item.getSummaryText(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("summary ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<ActivityItem> hasVisibleInlineActions(Matcher<Boolean> matcher) {
        return new HasInlineActionsMatcher(matcher);
    }

    private static final class HasInlineActionsMatcher extends TypeSafeDiagnosingMatcher<ActivityItem> {
        private final Matcher<Boolean> matcher;

        public HasInlineActionsMatcher(Matcher<Boolean> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ActivityItem item, Description mismatchDescription) {
            if (!matcher.matches(item.getInlineActionsContainer().isDisplayed())) {
                mismatchDescription.appendText("inline actions ");
                matcher.describeMismatch(item.getInlineActions(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description) {
            description.appendText("inline actions ").appendDescriptionOf(matcher);
        }
    }
}
