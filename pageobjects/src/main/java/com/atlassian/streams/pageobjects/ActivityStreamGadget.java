package com.atlassian.streams.pageobjects;

import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.streams.api.common.Option;
import com.atlassian.webdriver.utils.MouseEvents;
import com.atlassian.webdriver.utils.element.WebDriverPoller;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.transform;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Option.toOptional;
import static com.atlassian.streams.pageobjects.Waits.hasClass;
import static com.atlassian.streams.pageobjects.Waits.not;
import static com.atlassian.streams.pageobjects.Waits.or;

public class ActivityStreamGadget {
    private static final String STREAM_CONTAINER_CLASSNAME = "activity-stream-container";
    private static final By STREAM_CONTAINER = By.className(STREAM_CONTAINER_CLASSNAME);

    private @Inject WebDriverPoller poller;
    private @Inject WebDriver driver;

    private @Inject PageBinder pageBinder;

    private @Inject WebDriver atlassianWebDriver;

    private @Inject PageElementFinder elementFinder;

    private @Inject JavascriptExecutor executor;

    @FindBy(id = "stream-title")
    private WebElement title;

    @FindBy(id = "activity-stream-feed")
    private WebElement feedLink;

    @FindBy(className = "full-view-icon")
    private WebElement fullViewOption;

    @FindBy(className = "list-view-icon")
    private WebElement listViewOption;

    @FindBy(className = "filter-view-icon")
    private WebElement filterViewOption;

    @FindBy(id = "filter-icon")
    private WebElement filter;

    @FindBy(css = "div.aui-message.aui-message-warning")
    private WebElement warningBox;

    @FindBy(className = "submit")
    private WebElement submitButton;

    public Iterable<ActivityItem> getActivityItems() {
        return transform(atlassianWebDriver.findElements(By.className("activity-item")), toActivityItem);
    }

    @WaitUntil
    public void waitUntilInitializedAndFilterOptionsLoaded() {
        poller.waitUntil(not(hasClass("initializing", By.tagName("body"))));
        poller.waitUntil(or(
                or(
                        hasClass("filter-view", STREAM_CONTAINER),
                        hasClass("list-view", STREAM_CONTAINER),
                        hasClass("full-view", STREAM_CONTAINER)),
                hasClass("not-configurable", STREAM_CONTAINER)));
    }

    public Link getFeedLink() {
        return new Link(feedLink, driver);
    }

    public SwitchViewOption getFullViewOption() {
        return new SwitchViewOption(fullViewOption, driver);
    }

    public SwitchViewOption getListViewOption() {
        return new SwitchViewOption(listViewOption, driver);
    }

    public SwitchViewOption getFilterViewOption() {
        return new SwitchViewOption(filterViewOption, driver);
    }

    /**
     * Sets the path to the servlet and reloads the activity stream.
     * @param servletPath the path, such as "/servlet/plugins/streams".
     * Do not inlcude the base url (http://localhost:3990/streams).
     * @return a new reloaded ActivityStreamGadget.
     */
    public ActivityStreamGadget setServletPath(String servletPath) {
        String js = String.format("ActivityStreams.setServletPath('%s');", servletPath);
        executor.executeScript(js);
        atlassianWebDriver.switchTo().defaultContent();

        // Hover over the title
        WebElement gadgetRenderBox =
                elementFinder.find(By.cssSelector("div.gadget")).asWebElement();
        String frameId = gadgetRenderBox.findElement(By.tagName("iframe")).getAttribute("id");
        MouseEvents.hover(gadgetRenderBox, atlassianWebDriver);

        // Click on the drop-down menu
        elementFinder.find(By.cssSelector(".gadget-menu .aui-dd-trigger")).click();

        // Click on Refresh
        gadgetRenderBox.findElement(By.cssSelector("li.dropdown-item.reload")).click();

        // then switch back to gadget frame
        atlassianWebDriver.switchTo().frame(frameId);

        return pageBinder.bind(ActivityStreamGadget.class);
    }

    /**
     * Returns the list of sources which have timed out.
     * @return a list of source names, or null if no warning about timeout is displayed
     */
    public List<String> getTimeoutSources() {
        String text = warningBox.getText();
        int from = text.indexOf("(");
        int to = text.indexOf(")");
        if (from == -1 || to == -1) {
            return null;
        }
        String sourceList = text.substring(from + 1, to);
        return Lists.newArrayList(sourceList.split(", "));
    }

    /** Gets the first activity item matching the specified textual patterns. */
    public Optional<ActivityItem> findActivityItem(java.util.function.Predicate<ActivityItem> p) {
        return toOptional(getActivityItemWhere(p::test));
    }

    /**
     * Gets the first activity item matching the specified textual patterns.
     * @deprecated Use {@link #findActivityItem(java.util.function.Predicate)} instead.
     */
    @Deprecated
    public Option<ActivityItem> getActivityItemWhere(Predicate<ActivityItem> p) {
        Iterable<ActivityItem> activityItems = getActivityItemsWhere(p);
        try {
            return some(get(activityItems, 0));
        } catch (IndexOutOfBoundsException e) {
            return none();
        }
    }

    public Iterable<ActivityItem> findAllActivityItems(java.util.function.Predicate<ActivityItem> p) {
        return getActivityItemsWhere(p::test);
    }

    /**
     * Gets all activity items matching the specified textual patterns.
     * @deprecated Use {@link #findAllActivityItems(java.util.function.Predicate)} instead.
     */
    @Deprecated
    public Iterable<ActivityItem> getActivityItemsWhere(Predicate<ActivityItem> p) {
        return filter(getActivityItems(), p);
    }

    public ActivityStreamConfiguration openConfiguration() {
        if (!elementFinder.find(By.id("filter-options")).isVisible()) {
            filter.click();
        }
        return pageBinder.bind(ActivityStreamConfiguration.class);
    }

    public String getTitle() {
        return title.getText();
    }

    public void close() {
        atlassianWebDriver.switchTo().defaultContent();
    }

    public WebElement getConfigurationSubmitButton() {
        return submitButton;
    }

    private final Function<WebElement, ActivityItem> toActivityItem = new Function<WebElement, ActivityItem>() {
        public ActivityItem apply(WebElement item) {
            return pageBinder.bind(ActivityItem.class, item);
        }
    };

    public static class Link {
        private final WebElement link;

        private WebDriver driver;

        public Link(WebElement link, WebDriver driver) {
            this.link = link;
            this.driver = driver;
        }

        public String getHref() {
            return link.getAttribute("href");
        }

        public boolean hasFocus() {
            return link.equals(driver.switchTo().activeElement());
        }
    }

    public static class SwitchViewOption {
        private final WebElement span;
        private WebDriver driver;

        public SwitchViewOption(WebElement span, WebDriver driver) {
            this.span = span;
            this.driver = driver;
        }

        public String getTabIndex() {
            return span.getAttribute("tabIndex");
        }

        public void click() {
            span.click();
        }

        public void pressEnter() {
            span.sendKeys(Keys.ENTER);
        }

        public void pressTab() {
            span.sendKeys(Keys.TAB);
        }

        public void pressShiftTab() {
            span.sendKeys(Keys.SHIFT, Keys.TAB);
        }

        public boolean isSelected() {
            return span.isSelected();
        }

        public boolean hasFocus() {
            return span.equals(driver.switchTo().activeElement());
        }
    }
}
