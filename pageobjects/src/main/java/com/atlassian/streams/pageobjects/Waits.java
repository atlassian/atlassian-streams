package com.atlassian.streams.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import com.atlassian.streams.api.common.Option;

import static com.atlassian.streams.api.common.Option.none;

public class Waits {
    public static Function<WebDriver, Boolean> and(Function<WebDriver, Boolean> p1, Function<WebDriver, Boolean> p2) {
        return and(ImmutableList.of(p1, p2));
    }

    public static Function<WebDriver, Boolean> and(
            Function<WebDriver, Boolean> p1, Function<WebDriver, Boolean> p2, Function<WebDriver, Boolean> p3) {
        return and(ImmutableList.of(p1, p2, p3));
    }

    public static Function<WebDriver, Boolean> and(final Iterable<Function<WebDriver, Boolean>> ps) {
        return new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                for (Function<WebDriver, Boolean> p : ps) {
                    if (!p.apply(driver)) {
                        return false;
                    }
                }
                return true;
            }
        };
    }

    public static Function<WebDriver, Boolean> or(
            final Function<WebDriver, Boolean> p1, final Function<WebDriver, Boolean> p2) {
        return or(ImmutableList.of(p1, p2));
    }

    public static Function<WebDriver, Boolean> or(
            final Function<WebDriver, Boolean> p1,
            final Function<WebDriver, Boolean> p2,
            final Function<WebDriver, Boolean> p3) {
        return or(ImmutableList.of(p1, p2, p3));
    }

    public static Function<WebDriver, Boolean> or(final Iterable<Function<WebDriver, Boolean>> ps) {
        return new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                for (Function<WebDriver, Boolean> p : ps) {
                    if (p.apply(driver)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

    public static Function<WebDriver, Boolean> not(final Function<WebDriver, Boolean> p) {
        return new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                return !p.apply(driver);
            }
        };
    }

    public static Function<WebDriver, Boolean> hasClass(final String className, final By selector) {
        return hasClass(className, selector, none(SearchContext.class));
    }

    public static Function<WebDriver, Boolean> hasClass(
            final String className, final By selector, final Option<SearchContext> ctx) {
        return new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ctx.getOrElse(driver)
                        .findElement(selector)
                        .getAttribute("class")
                        .contains(className);
            }
        };
    }
}
