package com.atlassian.streams.pageobjects;

import org.openqa.selenium.WebElement;
import com.google.common.base.Function;
import com.google.common.base.Predicate;

import static com.google.common.base.Predicates.and;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;

public class Predicates {
    public static Predicate<String> contains(String s) {
        return new ContainsPredicate(s);
    }

    private static class ContainsPredicate implements Predicate<String> {
        private final String s;

        public ContainsPredicate(String s) {
            this.s = s;
        }

        public boolean apply(String input) {
            return input.contains(s);
        }

        @Override
        public String toString() {
            return "contains(" + s + ")";
        }
    }

    public static Predicate<ActivityItem> summary(Predicate<String> p) {
        return new SummaryPredicate(p);
    }

    private static class SummaryPredicate implements Predicate<ActivityItem> {
        private final Predicate<String> p;

        public SummaryPredicate(Predicate<String> p) {
            this.p = p;
        }

        public boolean apply(ActivityItem item) {
            return p.apply(item.getSummaryText());
        }

        @Override
        public String toString() {
            return "summary(" + p + ")";
        }
    }

    public static Predicate<ActivityItem> summaryContains(String... ss) {
        Predicate<String> containsStrings = and(transform(asList(ss), toContainsPredicate()));
        return summary(containsStrings);
    }

    private static Function<String, Predicate<String>> toContainsPredicate() {
        return ToContainsPredicateFunction.INSTANCE;
    }

    private enum ToContainsPredicateFunction implements Function<String, Predicate<String>> {
        INSTANCE;

        public Predicate<String> apply(String s) {
            return contains(s);
        }
    }

    public static Predicate<WebElement> text(final Predicate<String> p) {
        return new Predicate<WebElement>() {
            public boolean apply(WebElement e) {
                return p.apply(e.getText());
            }
        };
    }
}
