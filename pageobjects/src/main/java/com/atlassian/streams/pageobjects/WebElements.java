package com.atlassian.streams.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import com.google.common.base.Function;

public class WebElements {
    /**
     * Checks if the specified {@code By} exists in the given {@code WebElement}.
     *
     * @param element the {@code WebElement} to look in
     * @param by the {@code By} to search for
     * @return true if it exists, false if not
     */
    public static boolean exists(WebElement element, By by) {
        try {
            element.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Produces a function that returns the visible innerText from a {@code WebElement}.
     */
    public static Function<WebElement, String> getText() {
        return GetTextFunction.INSTANCE;
    }

    private enum GetTextFunction implements Function<WebElement, String> {
        INSTANCE;

        public String apply(WebElement webElement) {
            return webElement.getText();
        }
    }
}
